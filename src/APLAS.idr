module APLAS

import Data.Category
import Data.Category.Functor
import Lib
import Data.Iso

%hide Data.Category.Ops.infixl.(|>)
%default total

typebind infixr 0 |>
record Container where
  constructor (|>)
  request : Type
  response : request -> Type

infixr 5 =%>
infixl 8 <|
record (=%>) (domain, codomain : Container) where
  constructor (<|)
  fwd : domain.request -> codomain.request
  bwd : {x : domain.request} ->
        codomain.response (fwd x) -> domain.response x

(⨾) : a =%> b -> b =%> c -> a =%> c
(⨾) m1 m2 = m2.fwd . m1.fwd <| m1.bwd . m2.bwd

Id : (x : Container) -> x =%> x
Id x = id <| id

Cont : Category Container
Cont = MkCategory
  (=%>)
  Id
  (⨾)
  proofIdL
  proofIdR
  proofComp
  where
    0 proofIdL : {a, b : Container} -> (f : a =%> b) -> Id a ⨾ f ≡ f
    proofIdL (f <| b) = Refl

    proofIdR : {a, b : Container} -> (f : a =%> b) -> f ⨾ Id b ≡ f
    proofIdR (f <| b) = Refl

    proofComp :  {a, b, c, d : Container} ->
                 (f : a =%> b) -> (g : b =%> c) -> (h : c =%> d) ->
                 f ⨾ (g ⨾ h) ≡ (f ⨾ g) ⨾ h
    proofComp {f = f1 <| f2, g = g1 <| g2, h = h1 <| h2}
      = Refl

-- Functor & Laws

Maybe : Container -> Container
Maybe m = (x : Maybe m.request) |> Any m.response x

map_Maybe : a =%> b -> Maybe a =%> Maybe b
map_Maybe m = map m.fwd <| mapAny m.fwd m.bwd

MaybeFunctor : Cont -*> Cont
MaybeFunctor = MkFunctor
    { F = Maybe
    , F' = \x, y => map_Maybe
    , mapId = maybePresId
    , mapHom = maybePresComp
  }
  where
    mapId : {x : Maybe a} -> map (\y => y) x ≡ x
    mapId {x = Nothing} = Refl
    mapId {x = (Just x)} = Refl

    %unbound_implicits off
    mapAnyId :  {a : Type} -> {x : Maybe a} -> {p : a -> Type} -> {y : Any p x} ->
                let val : Any p (map id x)
                    val = replace {p = Any p} (sym mapId) y
                in mapAny {x, q = p} (\x => x) (\y : p _ => y) val ≡ y
    mapAnyId {x = (Just x)} {y = y} = Refl

    0 maybePresId : (x : Container) -> map_Maybe (Id x) ≡ Id (Maybe x)
    maybePresId x = cong2' (<|)
        (funExt $ \vx => mapId)
        (funExt $ \vx =>
         funExt $ \vy => rewrite sym $ mapId {x = vx} in
                                 mapAnyId {a = x.request, p = x.response, y = vy})
    0 maybePresComp : (a : Container) -> (b : Container) -> (c : Container) ->
                      (f : a =%> b) -> (g : b =%> c) ->
                      map_Maybe (f ⨾ g) ≡ map_Maybe f ⨾ map_Maybe g
    maybePresComp a b c f g = cong2' (<|)
        (funExt maybeTypePresComp)
        (funExt $ \case (Just vx) => funExt $ \vy => Refl
                      ; Nothing => funExt $ \vy => void vy)

-- Monad & Laws

maybePure : forall a. a =%> Maybe a
maybePure = Just <| id

maybeJoin' : forall a, p. {x : Maybe (Maybe a)} ->
             Any p (mJoin x) -> Any (Any p) x
maybeJoin' {x = (Just x)} y = y

maybeJoin : forall a. Maybe (Maybe a) =%> Maybe a
maybeJoin = mJoin <| maybeJoin'

%unbound_implicits off
map_join_prf : {0 a : Type} ->
               (x : Maybe (Maybe (Maybe a))) ->
               mJoin (map mJoin x) ≡ mJoin (mJoin {a = Maybe a} x)
map_join_prf Nothing = Refl
map_join_prf (Just x) = Refl

0 maybeMonadSquare : {0 a : Container} -> let
  tm : Maybe (Maybe (Maybe a)) =%> Maybe (Maybe a)
  tm = map_Maybe maybeJoin
  mt : Maybe (Maybe (Maybe a)) =%> Maybe (Maybe a)
  mt = maybeJoin {a = Maybe a}
  in tm ⨾ maybeJoin {a} ≡ mt ⨾ maybeJoin {a}
maybeMonadSquare = cong2' (<|)
    (funExt map_join_prf)
    (funExt $ \case (Just vx) => funExt $ \vy => Refl
                  ; Nothing => funExt $ \x => void x)

0
maybeMonadIdR : {0 a : Container} -> let
    tp : Maybe a =%> Maybe (Maybe a)
    tp = maybePure {a = Maybe a}
    in tp ⨾ maybeJoin {a}  = Id (Maybe a)
maybeMonadIdR = cong2' (<|)
    (funExt $ \case (Just vx) => Refl ; Nothing => Refl)
    (funExt $ \case (Just vx) => funExt $ \vy => Refl
                    Nothing => funExt $ \vy => void vy)
0
maybeMonadIdL : {0 a : Container} -> let
    tp : Maybe a =%> Maybe (Maybe a)
    tp = map_Maybe maybePure
    in tp ⨾ maybeJoin {a}  = Id (Maybe a)
maybeMonadIdL = cong2' (<|)
    (funExt $ \case (Just vx) => Refl ; Nothing => Refl)
    (funExt $ \case (Just vx) => funExt $ \vy => Refl
                    Nothing => funExt $ \vy => void vy)

public export
(+) : Container -> Container -> Container
(+) c1 c2 = (x : c1.request + c2.request) |> elim c1.response c2.response x

IsTrue : Bool -> Type
IsTrue True = Unit
IsTrue False = Void

public export
MaybeC : Container
MaybeC = (b : Bool) |> IsTrue b

(○) : Container -> Container -> Container
c1 ○ c2 = (r : Σ c1.request (\req => c1.response req -> c2.request))
       |> Σ (c1.response r.π1) (\res => c2.response (r.π2 res))

(#>) : Container -> Container -> Container
c1 #> c2 = (r : Σ c1.request (\req => c1.response req -> c2.request))
       |> ((res : c1.response r.π1) -> (c2.response (r.π2 res)))

CUnit : Container
CUnit = (_ : Unit) |> Unit

contToCoproduct : forall c. MaybeC ○ c =%> CUnit + c
contToCoproduct = mapFwd <| mapBwd
  where
    mapFwd : Σ Bool (\req => IsTrue req -> c.request) -> () + c.request
    mapFwd (False && p2) = InL ()
    mapFwd (True && p2) = InR (p2 ())

    mapBwd : {x : Σ Bool (\req => IsTrue req -> c.request)} ->
             elim (\_ => ()) (c .response) (mapFwd x) ->
             Σ (IsTrue (x .π1)) (\res => c.response (x.π2 res))
    mapBwd {x = (False && p2)} y = ?aa
    mapBwd {x = (True && p2)} y = ?bbhuj

coproductToCont : forall c. CUnit + c =%> MaybeC ○ c
coproductToCont = mapFwd <| mapBwd
  where
    mapFwd : () + c.request -> Σ Bool (\req => IsTrue req -> c.request)
    mapFwd (InL x) = False && (\x => void x)
    mapFwd (InR x) = True && (\_ => x)

    mapBwd : {x : () + c.request} ->
             (Σ (IsTrue ((mapFwd x) .π1)) (\res => c.response ((mapFwd x).π2 res)))->
             elim (\_ => ()) (c .response) x
    mapBwd {x = (InL x)} f = ()
    mapBwd {x = (InR x)} f = f.π2

public export
record Ex (cont : Container) (ty : Type) where
  constructor MkEx
  ex1 : cont.request
  ex2 : cont.response ex1 -> ty

public export
MaybeType : Type -> Type
MaybeType = Ex MaybeC

public export
Just : forall a. (x : a) -> MaybeType a
Just x = MkEx True (\_ => x)

public export
Nothing : forall a. MaybeType a
Nothing = MkEx False absurd

export
toIdris : forall a. MaybeType a -> Maybe a
toIdris (MkEx False p) = Nothing
toIdris (MkEx True p) = Just (p ())

public export
fromIdris : forall a. Maybe a -> MaybeType a
fromIdris Nothing = Nothing
fromIdris (Just x) = Just x

%unbound_implicits off
toFromEq : forall a. (x : Maybe a) -> toIdris (fromIdris x) === x
toFromEq Nothing = Refl
toFromEq (Just x) = Refl

0 fromToEq : {0 a : Type} -> (x : MaybeType a) -> fromIdris (toIdris x) === x
fromToEq (MkEx True p) = cong (MkEx True) (funExt $ \() => Refl)
fromToEq (MkEx False p) = cong (MkEx False) (funExt $ \x => void x)

%unbound_implicits on

public export
MaybeTypeIso : MaybeType a `Iso` Maybe a
MaybeTypeIso = MkIso
  toIdris
  fromIdris
  toFromEq
  fromToEq

||| We can always convert from the `MaybeCont ○` monad to the Idris definition
public export
MaybeToAlways : forall x. MaybeC ○ x =%> Maybe x
MaybeToAlways = ?hhh <| ?huiu
    where
      bwd : (v : MaybeType x.request) ->
            Any x.response (toIdris v) -> Σ (IsTrue v.ex1) (\y => x.response (v.ex2 y))
      bwd (MkEx True p) x = () && x
      bwd (MkEx False p) x = void x

||| We can always convert from the Idris definition to the `MaybeCont ○` monad
public export
AnyToMaybe : forall x. Maybe x =%> MaybeC ○ x
AnyToMaybe = MkMorphism
    fromIdris
    bwd
    where
      bwd : (v : Maybe x.request) -> (MaybeAny x).response (fromIdris v) -> Any x.response v
      bwd Nothing x = absurd x.π1
      bwd (Just y) x = Yep x.π2

||| Going from Idris to `MaybeCont ○` and back to Idris is like doing nothing
AnyToMaybeToAny : {0 x : Container} -> (AnyToMaybe {x} |> MaybeToAny {x}) `DepLensEq` (identity {a = MaybeAnyIdris x})
AnyToMaybeToAny = MkDepLensEq
  toFromEq
  (\case Nothing => \x => absurd x
         (Just y) => \(Yep x) => Refl)

||| Going from `MaybeCont ○` to Idris and back is like doing nothing
MaybeToAnyToMaybe : {0 x : Container} -> (MaybeToAny{x} |> AnyToMaybe  {x}) `DepLensEq` (identity {a = MaybeAny x})
MaybeToAnyToMaybe = MkDepLensEq
  fromToEq
  bwdEq
  where
    0 bwdEq : (v : MaybeType x.request) -> (y : (MaybeAny x).response (fromIdris (toIdris v))) ->
            (MaybeToAny{x} |> AnyToMaybe  {x}).bwd v y === replace {p = (MaybeAny x).response} (fromToEq v) y
    bwdEq (MkEx False ex2) y = absurd y.π1
    bwdEq (MkEx True ex2) y = sigEqToEq $ MkSigEq (TUniq _) Refl

||| Putting it all together
AnyMaybeIso : ContIso (MaybeAnyIdris x) (MaybeAny x)
AnyMaybeIso = MkGenIso
  AnyToMaybe
  MaybeToAny
  AnyToMaybeToAny
  MaybeToAnyToMaybe

