module Data.Monoid.Action

import Data.Monoid
import Data.Iso

record MAction  where
  constructor MkAction
  mon : Monoid
  act : mon.T -> Type -> Type
  actUnit : (x : Type) -> (mon.unit `act` x) = x
  actCoh : (x, y : mon.T) -> (z : Type) ->
           ((mon.mult x y) `act` z) = (x `act` (y `act` z))

record Monad where
  constructor MkMonad
  M : Type -> Type
  unit : (x : Type) -> M x
  mult : M (M x) -> M x

MActionMonad : MAction -> Monad
MActionMonad (MkAction mon act actUnit actCoh) = MkMonad
  (\x => act mon.unit x)
  (\ty => ?uuu)
  ?mmm
