module Data.Levitation

import public Data.Sigma
import public Data.Product
import public Data.Tag

%hide Builtin.infixr.(#)

%default total

public export
data Desc : Type where
  One : Desc
  Sig : (s : Type) -> Show s => (d : s -> Desc) -> Desc
  Ind : Desc -> Desc

public export
pure : Desc -> Type -> Type
pure One y = Unit
pure (Sig s d) x = Σ s (\v => pure (d v) x)
pure (Ind d) x = x * pure d x


NatD : Desc
NatD = Sig (#["Z", "S"]) (\case
           "Z" => One
           "S" => Ind One)

ListD : (x : Type) -> Show x => Desc
ListD ty = Sig (#["Nil", "Cons"]) (\case
               "Nil" => One
               "Cons" => Sig ty (const $ Ind One))

TreeD : (x : Type) -> Show x => Desc
TreeD ty = Sig (#["Leaf", "Node"]) (\case
               "Leaf" => One
               "Node" => Ind $ Sig Unit (const $ Ind One))

