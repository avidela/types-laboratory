module Data.Permutations.Inductive

import Data.Vect
import Data.List
import Data.Maybe
import Data.Fin
import Data.Linear
import Data.Linear.LVect

import Syntax.PreorderReasoning

data NatThin : (n : Nat) -> Vect n Bool -> Type where
  Init : NatThin Z []
  Take : NatThin n ls -> NatThin (S n) (True  :: ls)
  Drop : NatThin n ls -> NatThin (S n) (False :: ls)

takeNth : (i : Fin n) -> NatThin n ls -> NatThin n (replaceAt i True ls)
takeNth FZ (Take x) = Take x
takeNth FZ (Drop x) = Take x
takeNth (FS y) (Take x) = Take (takeNth y x)
takeNth (FS y) (Drop x) = Drop (takeNth y x)

data VecThin : (sel : NatThin n ls) -> Vect n (Maybe a) -> Type where
  Empty : VecThin Init []
  ThisOne : {0 a : Type} -> (v : a) -> {0 xs : Vect n (Maybe a)} -> VecThin prev xs -> VecThin (Take prev) (Just v :: xs)
  NotThis : {0 a : Type} -> {0 xs : Vect n (Maybe a)} -> VecThin prev xs -> VecThin (Drop prev) (Nothing :: xs)

Uninhabited (VecThin (Take y) (Nothing :: xs)) where
  uninhabited x = ?aaa

%unbound_implicits off
mapIndex : {0 a, b : Type} -> {0 n : Nat} -> {0 ls : Vect n Bool} -> {0 sel : NatThin n ls} -> {0 xs : Vect n (Maybe a)} ->
           (f : a -> b) -> VecThin sel xs -> VecThin sel (map (map f) xs)
mapIndex f Empty = Empty
mapIndex f (ThisOne v x) = ThisOne (f v) (mapIndex f x)
mapIndex f (NotThis x) = NotThis (mapIndex f x)

range' : (n : Nat) -> Vect n (Fin n)
range' 0 = []
range' (S k) = FZ :: map FS (range' k)


takeAll : (n : Nat) -> NatThin n (Vect.replicate n True)
takeAll 0 = Init
takeAll (S k) = Take (takeAll k)

AllRange : (n : Nat) -> Vect n (Maybe (Fin n))
AllRange n = map Just (range' n)

%unbound_implicits on
mapFusionV : (g : (b -> c)) -> (f : (a -> b)) -> (xs : Vect n a) -> map g (map f xs) = map (g . f) xs
mapFusionV g f [] = Refl
mapFusionV g f (x :: xs) = cong (g (f x) ::) (mapFusionV g f xs)

mapCommutesTablulates : {n : Nat} -> (f : Fin n -> a) -> map f (tabulate (\x => x)) = tabulate f
mapCommutesTablulates {n = 0} f = Refl
mapCommutesTablulates {n = (S k)} f = cong (f FZ ::) (let rr = mapCommutesTablulates {n = k} (f . FS) in ?mapCommutesTablulates_rhs_1 )

fullRange : (n : Nat) -> VecThin (takeAll n) (AllRange n) {a = Fin n}
fullRange 0 = Empty
fullRange (S k) = let rec : ?
                      rec = mapIndex FS (fullRange k)
                      end : VecThin (Take (takeAll k)) (Just Fin.FZ :: map (map FS) (AllRange k))
                      end = ThisOne {a = Fin (S k)} FZ {prev = takeAll k} {xs = map (map FS) (AllRange k)} rec
                      proofTabl : map (map FS) (AllRange k) === map Just (map FS (range' k))
                      proofTabl = Calc $
                               |~ map (map FS) (AllRange k)
                               ~~ map (map FS) (map Just (range' k)) ...(Refl)
                               ~~ map (map FS . Just) (range' k) ...(mapFusionV (map FS) Just (range' k))
                               ~~ map (Just . FS) (range' k) ...(Refl)
                               ~~ map Just (map FS (range' k)) ..<(mapFusionV Just Fin.FS (range' k))
                      goal : VecThin (Take (takeAll k)) (Just FZ :: map Just (map FS (range' k)))
                      goal = rewrite__impl (\xs => VecThin (Take (takeAll k)) (Just FZ :: xs)) (sym proofTabl) end
                  in goal


fromVect : (xs : Vect n (Maybe a)) -> NatThin n (map Maybe.isJust xs)
fromVect [] = Init
fromVect (Just x :: xs) = Take (fromVect xs)
fromVect (Nothing :: xs) = Drop (fromVect xs)

toThin :
   (xs : Vect len (Maybe a)) ->
   VecThin (fromVect xs) xs
toThin [] = Empty
toThin (Nothing :: xs) = NotThis (toThin xs)
toThin ((Just x) :: xs) = ThisOne x (toThin xs)

%unbound_implicits off
insertAt : {0 n : Nat} -> {0 a : Type} ->
           (idx : Fin n) -> (v : a) -> (vector : Vect n (Maybe a)) ->
           VecThin (takeNth idx (fromVect vector)) (replaceAt idx (Just v) vector)
insertAt FZ v (Nothing :: xs) = ThisOne v (toThin xs)
insertAt FZ v ((Just x) :: xs) = ThisOne v (toThin xs)
insertAt (FS y) v (Nothing :: xs) = NotThis (insertAt y v xs)
insertAt (FS y) v ((Just x) :: xs) = ThisOne x (insertAt y v xs)


%unbound_implicits on
mapV : (a -@ b) -> LVect n a -@ LVect n b
mapV f [] = []
mapV f (x :: xs) = f x :: mapV f xs

-- A linear permutation is one implemented by linearity of the input. If all elements of a given list must be used
-- exactly once, and the return type is a list of the same length, then each element must find a new position in
-- the resulting list. Because we are parametric over the type, we cannot perform any operation that would transform
-- the elements of the list
0 LinearPermutation : Nat -> Type
LinearPermutation n = {0 a : Type} -> (1 _ : LVect n a) -> LVect n a

-- We can apply a permutation by giving it a vector
applyLin : LinearPermutation n -@ LVect n a -@ LVect n a
applyLin f xs = f xs

-- An inductive definition for permutation is a list of n numbers from 0 to n (not including n).
-- if we are given the list [0,2,1] it means we map the first element to the first position
-- the second element to the last position, and the last element to the second position
-- so "abc" applied to the [0,2,1] permutation is "abc"
-- This is not strictly correct because some elements might be repeating. For example
-- [0,0,0] is a valueof this type but it not a valid permutation.
0 InductivePermutation : Nat -> Type
InductivePermutation n = Vect n (Fin n)

-- We want to apply an inductive permutation to map a vector of element but we cannot because
-- there is no function with type `index : Fin n -@ LVect n a -@ a
-- The reason is that such a function would only make use of the element at the selected index
-- but drop the rest of the array. There is an equivalent indexing function for linear vectors
-- but is has the type `lookup : Fin (S n) -> LVect (S n) a -> LPair a (LVect n a)
-- This returns the element and the rest of the vector that has not been consumed. But this
-- signature is incompatible with our native implementation of `apply`. Intead we have to
-- come up with something different
failing
  applyInd : InductivePermutation n -@ LVect n a -@ LVect n a
  applyInd xs ys = mapV (\v => index v xs) xs

-- lfoldl : (0 p : (Nat -> Type)) -> a -@ (p n -@ p (S n)) -> p 0 -@ (LVect n a -@ p n)
-- lfoldr : (0 p : (Nat -> Type)) -> a -@ (p n -@ p (S n)) -> p 0 -@ (LVect n a -@ p n)
-- this second version will update a target vector
applyIndTarget : InductivePermutation n -@ LVect n a -@ LVect n (Maybe a) -@ LVect n (Maybe a)
applyIndTarget f xs = ?aduhiuhi

{-
-- Using those two types we can map from linear permutation to inductive permutation by
-- feeding the identity permutation to our linear function.
fromLinear : {n : Nat} -> LinearPermutation n -> InductivePermutation n
fromLinear f = f {a = Fin n} (tabulate id)

-- This does not work because `index` does not have a linear implementation
failing "Trying to use linear name ys in non-linear context"
  toLinear : InductivePermutation n -> LinearPermutation n
  toLinear xs ys = map (\v => index v ys) xs

--



