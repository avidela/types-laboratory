module Data.Permutations.Choose

import Data.Fin
import Data.Linear.Notation
import Data.Linear.LVect
import Data.Vect

data Permutation : Nat -> Type where
  Empty : Permutation Z
  Choose : Fin (S n) -> Permutation n -> Permutation (S n)

remove : Fin (S n) -> Vect (S n) a -> (Vect n a, a)
remove FZ (x :: xs) = (xs, x)
remove (FS FZ) (y :: x :: xs) = (y :: xs, x)
remove (FS (FS n)) (y :: x :: xs) = let
    (nx, v) = remove (FS n) (x :: xs)
    in (y :: nx, v)

extract : Fin (S n) -@ LVect (S n) a -@ LPair (LVect n a) a
extract FZ (x :: xs) = xs # x
extract (FS FZ) (y :: x :: xs) = y :: xs # x
extract (FS (FS n)) (y :: x :: xs) = let
    1 (nx # v) = extract (FS n) (x :: xs)
    in (y :: nx) # v

pvect : Permutation n -@ LVect n a -@ LVect n a
pvect Empty [] = []
pvect (Choose x pn) xs = let
    1 (ys # v) = extract x xs
    1 pv = pvect pn ys
    in v :: pv


toPerm : (n : Nat) -> (forall a. Vect n a -> Vect n a) -> Permutation n
toPerm 0 f = Empty
toPerm (S k) f = let
    (h :: t) = f (Fin.range {len = S k})
  in Choose h (toPerm k (\xn => let
              kk = f ?ahui
              in ?aui))


