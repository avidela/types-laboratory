module Data.Fin.Util

import Data.Fin

export
greater : {a : Nat} -> Fin (a + b) -> Maybe (Fin b)
greater {a = 0} x = Just x
greater {a = (S k)} FZ = Nothing
greater {a = (S k)} (FS x) = greater {a = k} x

export
lower : {a : Nat} -> Fin (a + b) -> Maybe (Fin a)
lower {a = 0} x = Nothing
lower {a = (S k)} FZ = Just FZ
lower {a = (S k)} (FS x) = FS <$> lower {a = k} x

{-
smol : Fin (3 + 3)
smol = 1

beeg : Fin (3 + 3)
beeg = 4

one : Fin 3
one = 1

testGreater : greater {a = 3, b = 3} SparseMatrix.beeg = Just SparseMatrix.one
testGreater = Refl

testGreater' : greater {a = 3, b = 3} SparseMatrix.smol = Nothing
testGreater' = Refl

testSmaller : lower {a = 3, b = 3} SparseMatrix.smol = Just (FS FZ)
testSmaller = Refl

testSmaller' : lower {a = 3, b = 3} SparseMatrix.beeg = Nothing
testSmaller' = Refl
