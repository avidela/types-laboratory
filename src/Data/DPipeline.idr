module Data.DPipeline

import public Data.Telescope.Telescope
import Data.Fin
import Data.Vect

import Data.List.Quantifiers
import Data.Singleton

Enum : List Type -> Type
Enum = All Singleton

export typebind infixr 0 .-
%hide Data.Telescope.Telescope.infixr.(.-)

public export
record DContPair (x : Type) (y : x -> Type)  where
  constructor (>>>)
  fst : x
  cont : (v : x) -> y v

public export
record DContPairM (m : Type -> Type) (x : Type) (y : x -> Type)  where
  constructor (|>>>)
  fst : m x
  cont : (v : x) -> y v

record ContPair (x, y : Type)  where
  constructor (^^)
  fst : x
  cont : x -> y

private autobind infixr 0 ^^
export autobind infixr 0 >>>
export autobind infixr 0 |>>>

ToProd : List Type -> Type
ToProd [] = Unit
ToProd (x :: []) = x
ToProd (x :: (y :: xs)) = ContPair x (ToProd (y :: xs))

ToImpl : List Type -> Type
ToImpl [] = Void
ToImpl (x :: []) = x
ToImpl (x :: (y :: xs)) = x -> ToProd (y :: xs)

public export
ToSigma : Right.Telescope n -> Type
ToSigma [] = Unit
ToSigma (ty .- gamma) = DContPair ty (ToSigma . gamma)

public export
ToImpl' : Right.Telescope n -> Type
ToImpl' [] = Void
ToImpl' (ty .- gamma) = (x : ty) -> ToSigma (gamma x)

parameters (m : Type -> Type)
  public export
  ToSigmaM : Right.Telescope n -> Type
  ToSigmaM [] = Unit
  ToSigmaM (ty .- gamma)
      = DContPairM m ty (ToSigmaM . gamma)

  public export
  ToImplM : Right.Telescope n -> Type
  ToImplM [] = Void
  ToImplM (ty .- gamma) = (x : ty) -> ToSigmaM (gamma x)

