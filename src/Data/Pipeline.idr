module Data.Pipeline

import Data.Vect
import Data.Vect.Quantifiers
import Debug.Trace

import Debug.Trace

pairUp : Vect (S n) a -> Vect n (a, a)
pairUp [a] = []
pairUp (x :: y :: xs) = (x, y) :: pairUp (y :: xs)

public export
Pipeline : Nat -> Type
Pipeline length = Vect length Type

Fn : Type -> Type -> Type
Fn x y = x -> y

Impl : Pipeline (2 + n) -> Type
Impl cs = All (uncurry Fn) (pairUp cs)

Run : (p : Pipeline (2 + n)) -> Impl p -> head p -> last p
Run [x, y] [f] = f
Run (s :: t :: x :: xs) (f :: cont) = Run (t :: x :: xs) cont . f

----------------------------------------------------------------------------------
-- compiler examples
data Tree : Type where
data Token : Type where
data Sema : Type where

CompilerPipeline : Pipeline ?
CompilerPipeline = [ String , List Token
  , Tree
  , Sema
  , String
  ]


lex : String -> List Token
parse : List Token -> Tree
typecheck : Tree -> Sema
codegen : Sema -> String

runCompiler : String -> String
runCompiler = Run CompilerPipeline [lex, parse, typecheck, codegen]

parameters {m : Type -> Type}

  ImplM : Pipeline (2 + n) -> Type
  ImplM (x :: y :: []) = x -> m y
  ImplM (x :: y :: z :: xs) = Pair (x -> m y) (ImplM (y :: z :: xs))

  RunM : (mon : Monad m) => (p : Pipeline (2 + n)) -> ImplM p -> Vect.head p -> m (Vect.last p)
  RunM [x, y]  f = f
  RunM (x :: y :: z :: xs) (f, c) = RunM (y :: z :: xs) c <=< f

lexM : String -> Either String (List Token)
parseM : List Token -> Either String Tree
typecheckM : Tree -> Either String Sema
codegenM : Sema -> Either String String

runCompilerM : String -> Either String String
runCompilerM = RunM CompilerPipeline (lexM, parseM, typecheckM, codegenM)

RunTraceM : Monad m => (p : Pipeline (2 + n)) -> (print : All Show p) => ImplM {m} p -> Vect.head p -> m (Vect.last p)
RunTraceM [x, y] {print = [p1, p2]} f = f >=> pure . traceValBy show
RunTraceM (x :: y :: z :: xs) {print = p :: q :: ps} (f, c) =
  f >=> pure . traceValBy show >=> RunTraceM (y :: z :: xs) c

