module Data.Pi

public export
Π : (a : Type) -> (a -> Type) -> Type
Π a f = (piVal : a) -> f piVal

