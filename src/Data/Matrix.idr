module Data.Matrix

import Data.Vect

public export
Matrix : Nat -> Nat -> Type -> Type
Matrix n m t = Vect n (Vect m t)

export
mult : Num n => Matrix a b n -> Matrix b c n -> Matrix a c n
mult xs ys = ?mult_rhs

