module Data.Func

import Data.Vect

public export
Tn : Nat -> Type
Tn 0 = Type
Tn (S k) = Type -> Tn k

public export
curryVect : {i : Nat} -> (Vect i Type -> Type) -> Tn i
curryVect f {i = 0} = f []
curryVect f {i = (S k)} = \ty => curryVect (\v => f (ty :: v))

public export
unCurryVect : {0 i : Nat} -> Tn i -> Vect i Type -> Type
unCurryVect x [] {i = 0} = x
unCurryVect f (y :: xs) {i = (S k)} = unCurryVect {i=k} (f y) xs

public export
VectType : Vect n Type -> Type
VectType [] = Unit
VectType (x :: xs) = (x, VectType xs)

public export
Functions : Vect n Type -> Vect n Type -> Vect n Type
Functions [] [] = []
Functions (x :: xs) (y :: ys) = (x -> y) :: Functions xs ys

public export
Apply : {xs, ys : Vect n Type}
     -> {z : Fin n}
     -> index z xs
     -> VectType (Functions xs ys)
     -> index z ys
Apply v (f, fs) {z = FZ} {xs = (x :: xs)} {ys = (y :: ys)} =
   f v
Apply v (f, fs) {z = (FS x)} {xs = (y :: xs)} {ys = (z :: ys)} =
  Apply v fs
