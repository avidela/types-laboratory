module Data.Structures

import Data.Container
import Data.Fin
import Data.Vect
import Data.NContainer
import Data.Coproduct
import Data.Sigma
import Data.Fixpoint

ListC : Container
ListC = MkCont Nat Fin

ListF : Type -> Type
ListF = Ex ListC

Empty : {0 x : Type} -> ListF x
Empty = MkEx Z absurd

Cons : x -> ListF x -> ListF x
Cons elem (MkEx number index) = MkEx (S number) (\case FZ => elem; (FS x) => index x)

isTrue : Bool -> Type
isTrue True = Unit
isTrue False = Void

MaybeC : Container
MaybeC = MkCont Bool isTrue

MaybeF : Type -> Type
MaybeF = Ex MaybeC

None : MaybeF x
None = MkEx False absurd

Some : x -> MaybeF x
Some elem = MkEx True (const elem)

BoolC : Container
BoolC = MkCont Bool (const Void)

choice : Ex (MaybeC + ListC) x
choice = MkEx (<+ False) absurd

SomeList : ListF Nat
SomeList = Cons 3 (Cons 2 (Cons 1 (Cons 0 Empty)))

covering
fromList : Fix ListF -> Nat
fromList (In (Delay (MkEx Z unFix))) = 0
fromList (In (Delay (MkEx (S n) unFix))) =
  S (fromList (unFix FZ))

EitherFn : Fin 2 -> Bool -> Type
EitherFn FZ False = Unit
EitherFn FZ True = Void
EitherFn (FS FZ) False = Void
EitherFn (FS FZ) True = Unit

EitherC : IContainer 2
EitherC = MkIContainer Bool EitherFn

Right : y -> @|EitherC|@ [x, y]
Right val = (True ** (absurd, const val))

Left : x -> @|EitherC|@ [x, y]
Left val = (False ** (const val, absurd))

