
import Data.Vect
import Data.Coproduct
import Data.Product
import Data.Container
import Data.Container.Morphism

data U : Vect n Type -> Type where
  Foreign : {0 f : Vect n Type} -> Fin n -> U {n} b
  (+) : U b -> U b -> U b
  (*) : U b -> U b -> U b


Ty : {f : _} -> U f -> Type
Ty (Foreign x) =  index x f
Ty (x + y) = Ty x + Ty y
Ty (x * y) = Ty x * Ty y

Split : {v : _} -> U v -> Container
Split (Foreign x) = Const (index x v)
Split (x + y) = Split x + Split y
Split (x * y) = Split x * Split y

prism : {v : _} -> (0 x : U v) -> Const (Ty x) =%> Split x
prism x =
    ?prism_rhs <!
    ?iosjoijoij

