module Data.Category

import Decidable.Equality
import Relation.Equality
import Control.Order
import Control.Relation
import public Data.Category.Ops
import Syntax.PreorderReasoning

import Proofs.UIP
import Proofs

%default total
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
%hide Syntax.PreorderReasoning.Ops.infixl.(<~)

export infixr 1 ~:>  -- Explicit morphism
private infixl 5 |:>  -- Explicit composition

||| A category is defined by its objects (`o`)and morphisms (`m`)
||| It must provide an identity morphism for each object and a composition
||| operation for composing morphisms. It also must provide proofs that
||| the identity is neutral wrt to composition and that composition is
||| associative.
||| `a`, `b`, `c`, `d` are names for objects
||| `f`, `g`, `h` are names for morphisms
public export
record Category (o : Type) where
  constructor MkCategory

  -- The morphisms
  0 (~:>) : o -> o -> Type

  -- The identity
  id : (v : o) -> v ~:> v

  -- The composition
  (|:>) : {a, b, c : o} -> (a ~:> b) -> (b ~:> c) -> (a ~:> c)

  -- Proofs of identity left and right
  -- Identity is the neutral element on the right of the composition
  0 idRight : (a, b : o) -> (f : a ~:> b) -> f |:> id b = f
  -- Identity is the neutral element on the left of composition
  0 idLeft : (a, b : o) -> (f : a ~:> b) -> id a |:> f = f

  -- Proof of associativity of composition
  0 compAssoc : (a, b, c, d : o) ->
                (f : a ~:> b) ->
                (g : b ~:> c) ->
                (h : c ~:> d) ->
                f |:> (g |:> h) = (f |:> g) |:> h

-- An operator for morphisms without passing the category in argument
public export
0 (~>) : (cat : Category o) => o -> o -> Type
(~>) = Category.(~:>) cat

-- An operator for sequential composition without passing in the category in argument
public export
(|>) : (cat : Category o) => {a, b, c : o} ->
       a ~> b -> b ~> c -> a ~> c
(|>) = Category.(|:>) cat

public export
(⨾) : (cat : Category o) => {a, b, c : o} ->
       a ~> b -> b ~> c -> a ~> c
(⨾) = Category.(|:>) cat

-- An reverse operator for morphisms without passing the category in argument
public export
0 (<~) : (cat : Category o) => o -> o -> Type
(<~) a b = Category.(~:>) cat b a

-- An operator for sequential composition without passing in the category in argument
public export
(•) : (cat : Category o) => {a, b, c : o} ->
       c <~ b -> b <~ a -> c <~ a
(•) f g = Category.(|:>) cat g f

eqSym : (f : a = b) -> (a = b) = (b = a)
eqSym Refl = Refl

-- The opposite category
public export
(.op) : Category o -> Category o
(.op) cat = MkCategory
    (\x, y => y ~> x)
    (cat.id)
    (\f, g => (|:>) cat g f)
    (\a, b, f => cat.idLeft b a f)
    (\a, b, f => cat.idRight b a f)
    (\a, b, c, d, f, g, h => sym (cat.compAssoc _ _ _ _ h g f))

public export
(.op') : Category o -> Category o
(.op') cat = MkCategory
    (\x, y => y ~> x)
    (cat.id)
    (\f, g => (|:>) cat g f )
    (\a, b, f => cat.idLeft b a f)
    (\a, b, f => cat.idRight b a f)
    (\a, b, c, d, f, g, h =>
        replace {p = id} (eqSym (cat.compAssoc _ _ _ _ h g f))
          (cat.compAssoc _ _ _ _ h g f))

opOpSame' : (c : Category o) -> c.op'.op' === c
opOpSame' (MkCategory m i c il ir assoc) = Refl

-- A verbose function for sequential composition
public export 0
Compose : (cat : Category o) => (start, middle, end : o) ->
         (~:>) cat start middle -> (~:>) cat middle end -> (~:>) cat start end
Compose _ _ _ = Category.(|:>) cat

mutual
  public export
  data Comp : (cat : Category o) => (a, b : o) -> Type where
    (-<) : (cat : Category o) => (a : o) -> Next a c -> Comp a c
    End : (cat : Category o) => (a : o) -> Comp a a

  public export
  data Next : (cat : Category o) => (a, c : o) -> Type where
    (>-) : (cat : Category o) => {a, b, c : o} -> (f : a ~> b) -> Comp b c -> Next a c

  public export 0
  Start : (cat : Category o) => {a, b : o} -> Comp a b -> a ~> b
  Start (End a) = cat.id a
  Start ((-<) x y {c=k}) = continue y

  public export 0
  continue : (cat : Category o) => {a, b : o} -> Next a b -> a ~> b
  continue ((>-) f (End l) ) = f
  continue ((>-) f x {b=k} ) = f |> Start x

--         direct
--    a ───────────→ d
--    │ ⟍            ↑
--    │   ⟍          │
--    │     ⟍        │
--  f │      dia     │ h
--    │         ⟍    │
--    │           ⟍  │
--    ↓            ↘︎ │
--    b ───────────→ c
--           g
public export
glueTriangles : (cat : Category o) =>
             {a, b, c, d : o} -> {direct : a ~> d} ->
             {f : a ~> b} -> {g : b ~> c} ->
             {dia : a ~> c} -> {h : c ~> d} ->
             Start (a -< f >-
                    b -< g >-
                End c) = dia ->
             Start (a -< dia >-
                    c -< h >-
                End d) = direct ->
             Start (a -< f >-
                    b -< g >-
                    c -< h >-
                End d) = direct
glueTriangles Refl prf1 = trans (cat.compAssoc _ _ _ _ _ _ _) prf1

-- a ────f────→ b
--       =
-- a ────f'───→ b
--
--
-- a ────f────→ b ────g───→ c
--              =
-- a ────f'───→ b ────g───→ c
public export
congMor1 : (cat : Category o) =>
          {a, b, c : o} ->
          {f, f' : a ~> b} ->
          f = f' ->
          (g : b ~> c) ->
          Start (a -< f  >- b -< g >- End c)
      === Start (a -< f' >- b -< g >- End c)
congMor1 Refl g = Refl

-- a ────g────→ b
--       =
-- a ────g'───→ b
--
--
-- a ────f───→ b ────g───→ c
--              =
-- a ────f───→ b ────g'──→ c
public export
congMor2 : (cat : Category o) =>
          {a, b, c : o} ->
          {g, g' : b ~> c} ->
          (f : a ~> b) ->
          g = g' ->
          Start (a -< f >- b -< g >- End c)
      === Start (a -< f >- b -< g' >- End c)
congMor2 f Refl = Refl

--           f1
--    a ───────────→ b
--    │ ⟍            │
--    │   ⟍          │
--    │     ⟍        │
-- f2 │      dia     │ g1
--    │        ⟍     │
--    │          ⟍   │
--    ↓            ↘︎ ↓
--    c ───────────→ d
--           g2
public export
square : (cat : Category o) =>
         {0 a, b, c, d : o} ->
         {0 f1 : a ~> b} ->
         {0 f2 : a ~> c} ->
         {0 g1 : b ~> d} ->
         {0 g2 : c ~> d} ->
         {0 dia : a ~> d} ->
         Start (a -< f1 >- b -< g1 >- End d) = dia ->
         dia = Start (a -< f2 >- c -< g2 >- End d) ->
         Start (a -< f1 >- b -< g1 >- End d) =
         Start (a -< f2 >- c -< g2 >- End d)
square p1 p2 = trans p1 p2


--           h1                 h2
--    a ───────────→ b    b ───────────→ e
--    │              │    │              │
--    │              │    │              │
--    │              │    │              │
-- v1 │              │ v2 │              │ v3
--    │              │    │              │
--    │              │    │              │
--    ↓              ↓    ↓              ↓
--    c ───────────→ d    d ───────────→ f
--           h1'                h2'
public export
glueSquares : (cat : Category o) =>
              {0 a, b, c, d, e, f : o} ->
              {0 h1 : a ~> b} ->
              {0 h2 : b ~> e} ->
              {0 v1 : a ~> c} ->
              {0 v2 : b ~> d} ->
              {0 v3 : e ~> f} ->
              {0 h1' : c ~> d} ->
              {0 h2' : d ~> f} ->
              (sq1 : (h1 |> v2 ) {cat, a=a, b=b, c=d}
                 === (v1 |> h1') {cat, a=a, b=c, c=d}) ->
              (sq2 : (h2 |> v3 ) {cat, a=b, b=e, c=f}
                 === (v2 |> h2') {cat, a=b, b=d, c=f}) ->
              Start (a -< h1 >- b -< h2 >- e -< v3 >- End f)
              ===
              Start (a -< v1 >- c -< h1' >- d -< h2' >- End f)
glueSquares sq1 sq2 =
  Calc $ |~ h1 |> (h2  |> v3)
         ~~ h1 |> (v2  |> h2')
         ...(cong (h1 |>) sq2)

         ~~ (h1 |> v2) |> h2'
         ...(cat.compAssoc _ _ _ _ h1 v2 h2')

         ~~ (v1 |> h1')|> h2'
         ...(cong (|> h2') sq1)

         ~~ v1 |> (h1' |> h2')
         ...(sym (cat.compAssoc _ _ _ _ v1 h1' h2'))


export
0 forward : (cat : Category o) =>
          {a, b, c, d : o} ->
          {f : a ~> b} ->
          {g : b ~> c} ->
          {h : a ~> c} ->
          {k : c ~> d} ->
          {l : a ~> d} ->
          Compose a b c f g = h ->
          Compose a c d h k = l ->
          Compose a b d f (Compose b c d g k) = l
forward Refl Refl = cat.compAssoc _ _ _ _ _ _ _


export
0 forward' : (cat : Category o) =>
          {a, b, c, d : o} ->
          {f : a ~> b} ->
          {g : b ~> c} ->
          {h : a ~> c} ->
          {k : c ~> d} ->
          {l : a ~> d} ->
          {m : b ~> d} ->
          Compose a b c f g = h ->
          Compose a c d h k = l ->
          Compose b c d g k = m ->
          Compose a b d f m = l
forward' Refl Refl Refl = cat.compAssoc _ _ _ _ _ _ _

public export 0
Triangle :
    (cat : Category o) =>
    (a, b, c : o) ->
    (f : a ~> b) ->
    (g : b ~> c) ->
    (h : a ~> c) ->
    Type
Triangle a b c f g h = (f |> g) === h


public export 0
tetrahedron :
    (cat : Category o) =>
    {a, b, c, d : o} ->
    {f : a ~> b} ->
    {g : b ~> c} ->
    {h : a ~> c} ->
    {k : b ~> d} ->
    {m : c ~> d} ->
    {l : a ~> d} ->
    Triangle a b c f g h ->
    Triangle b c d g m k ->
    Triangle a c d h m l ->
    Triangle a b d f k l
tetrahedron Refl Refl Refl = cat.compAssoc _ _ _ _ _ _ _

public export
UnitCat : Category Unit
UnitCat = MkCategory
  (\_, _ => Unit)
  (\_ => ())
  (\_,_ => ())
  (\_, _, () => Refl)
  (\_, _, () => Refl)
  (\_, _, _, _, _, _, _ => Refl)

public export
DescreteCat : Category o
DescreteCat = MkCategory
  (\_, _ => Unit)
  (\_ => ())
  (\_,_ => ())
  (\_, _, () => Refl)
  (\_, _, () => Refl)
  (\_,_,_,_,_, _, _ => Refl)

public export
NewCat :
    (0 objects : Type) ->
    (0 morphisms : objects -> objects -> Type) ->
    (identity : (x : objects) -> morphisms x x) ->
    (composition : {a, b, c : objects} -> morphisms a b -> morphisms b c ->
        morphisms a c) ->
    (0 identity_right : {a, b : objects} -> (f : morphisms a b) ->
        composition f (identity b) ≡ f) ->
    (0 identity_left : {a, b : objects} -> (f : morphisms a b) ->
        composition (identity a) f ≡ f) ->
    (0 compose_assoc : {a, b, c, d : objects} ->
        (f : morphisms a b) -> (g : morphisms b c) -> (h : morphisms c d) ->
        composition f (composition g h) ≡ composition (composition f g) h) ->
    Category objects
NewCat _ m i c ir il a = MkCategory m i c (\_, _ => ir) (\_, _ => il) (\_, _, _, _ => a)

[CatRefl] {o : Type} -> {cat : Category o} -> Reflexive o ((~:>) cat) where
  reflexive = cat.id _

[CatTrans] {o : Type} -> {cat : Category o} -> Transitive o ((~:>) cat) where
  transitive = (|:>) cat

public export
Category' : Type
Category' = Σ Type Category

