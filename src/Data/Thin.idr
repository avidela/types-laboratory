module Data.Thin

import Data.Fin

public export
data Thin : (m, n : Nat) -> Type where
  Done : Thin n n
  Drop : Thin n m -> Thin (S n)    m
  Keep : Thin n m -> Thin (S n) (S m)

public export
renVar : Thin g d -> (Fin d -> Fin g)
renVar Done     v = v
renVar (Drop x) v = FS $ renVar x v
renVar (Keep x) FZ = FZ
renVar (Keep x) (FS v) = FS $ renVar x v

public export
idThin : {g : _} -> Thin g g
idThin {g = 0} = Done
idThin {g = (S k)} = idThin

public export
wk : {n : _} -> Thin (S n) n
wk = Drop idThin


