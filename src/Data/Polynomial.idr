module Data.Polynomial

import Data.Fin
import Data.Vect
import Data.Coproduct
import Data.Product
import Data.Func
import Data.Fixpoint

-- A polynomial functor with n variables
public export
data Poly : Nat -> Type where
  Var : Fin n -> Poly n
  (+) : Poly n -> Poly n -> Poly n
  (*) : Poly n -> Poly n -> Poly n
  One : Poly n
  Zero : Poly n
  Mu : Poly (S n) -> Poly n

record Component (n : Nat) where
  constructor MkComp
  factor : Nat
  variable : Fin n
  exp : Nat

record Products (n : Nat) where
  constructor MkProd
  prods : List (Component n)

record PolyFlat (n : Nat) where
  constructor MkFlat
  sums : List (Products n)

deriveComponent : (along : Fin n) -> Component n -> Component n
deriveComponent along n@(MkComp factor variable (S exp))
    = if variable == along
         then MkComp (factor * exp) variable exp
         else n
deriveComponent along n@(MkComp factor variable Z) = n

deriveProd : (along : Fin n) -> Products n -> Products n
deriveProd along (MkProd prods) = MkProd $ map (deriveComponent along) prods

deriveFlat : PolyFlat n -> (along : Fin n) -> PolyFlat n
deriveFlat (MkFlat sums) along = MkFlat $ map (deriveProd {along}) sums

record CanonicalPoly (n : Nat) where
  constructor MkCanon
  polys : Vect n (Nat, Nat)

public export
(^) : Poly n -> Nat -> Poly n
(^) x 0 = One
(^) x 1 = x
(^) x (S k) = x * x ^ k

fromNat : Nat -> Poly n
fromNat 0 = Zero
fromNat 1 = One
fromNat (S k) = One + fromNat k

public export
F : Poly n -> Vect n Type -> Type
F (Var x) xs {n = n} = index x xs
F (x + y) xs {n = n} = F x xs + F y xs
F (x * y) xs {n = n} = F x xs * F y xs
F (Mu x) xs {n = n} = Fix (F x . (:: xs))
F One xs {n = n} = Unit
F Zero xs {n = n} = Void

public export
F' : {n : Nat} -> Poly n -> Tn n
F' p = curryVect (F p)

export
fromInteger : Integer -> Poly n
fromInteger = fromNat . fromInteger

mutual
  fromFix : {n : Nat} -> {z : Poly (S n)} -> {xs, ys : Vect n Type}
         -> (VectType $ Functions xs ys)
         -> Fix (\x => F z (x :: xs))
         -> Fix (\t => F z (t :: ys))
  fromFix fns (In unFix) = In (Func (fromFix fns {z} {xs} {ys}, fns) unFix)

  -- F is a functor
  Func : {n : Nat} -> {p : Poly n} -> {xs, ys : Vect n Type} ->
         (VectType $ Functions xs ys) ->
         F p xs -> F p ys
  Func {p = (Var z)} x y {n = 0} = absurd z
  Func {p = (Var z)} x y {n = (S k)} = Apply y x
  Func {p = (z + w)} x y = bimap (Func x) (Func x) y
  Func {p = (z * w)} x y = bimap (Func x) (Func x) y
  Func {p = One} x y = y
  Func {p = Zero} x y = y
  Func {p = (Mu z)} x (In y) =
    let v = Func {p = z}
                 {n = S n}
                 {ys = Fix (\t => F z (t :: ys)) :: ys}
                 {xs = (Fix (\t => F z (t :: xs)) :: xs)}
                 (fromFix x , x) y
    in In v

  -- Examples
  -- PList : Poly 1
  -- PList = Mu (1 + (Var 0 * Var 1))
  --
  -- TyList : Type -> Type
  -- TyList = F' PList
