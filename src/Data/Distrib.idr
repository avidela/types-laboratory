module Data.Distrib

import Data.Category.Ops
import Proofs.Extensionality
import Proofs.Congruence
import Data.Vect

import Data.List.Quantifiers

(|>) : (a -> b) -> (b -> c) -> a -> c
(|>) f g = g . f

maybeListDistrib1 : Maybe (List a) -> List (Maybe a)
maybeListDistrib1 Nothing = []
maybeListDistrib1 (Just xs) = Just <$> xs

maybeVectDistrib1 : {n : Nat} -> Maybe (Vect n a) -> Vect n (Maybe a)
maybeVectDistrib1 Nothing = replicate n Nothing
maybeVectDistrib1 (Just xs) = Just <$> xs

listMaybeDistrib : List (Maybe a) -> Maybe (List a)
listMaybeDistrib [] = Just []
listMaybeDistrib (Nothing :: xs) = Nothing
listMaybeDistrib ((Just x) :: xs) = (x ::) <$> listMaybeDistrib xs

listMaybeDistrib2 : List (Maybe a) -> Maybe (List a)
listMaybeDistrib2 [] = Just []
listMaybeDistrib2 (Nothing :: xs) = Just []
listMaybeDistrib2 ((Just x) :: xs) = (x ::) <$> listMaybeDistrib2 xs

0 diagram1 : let
  0 ls : List (Maybe (Maybe x)) -> Maybe (List (Maybe x))
  ls arg = Distrib.listMaybeDistrib arg
  0 sl : Maybe (List (Maybe x)) -> Maybe (Maybe (List x))
  sl = map Distrib.listMaybeDistrib
  μst : Maybe (Maybe (List x)) -> Maybe (List x)
  μst = join
  tμs : List (Maybe (Maybe x)) -> List (Maybe x)
  tμs = map join
  in  (ls |> sl |> μst) === (tμs |> Distrib.listMaybeDistrib)
diagram1 = funExt $ \case [] => Refl
                          (x :: xs) => let rec = app _ _ diagram1 xs in  ?res

0 diagram2 : let
  0 tl : List (List (Maybe x)) -> List (Maybe (List x))
  tl = map Distrib.listMaybeDistrib
  0 lt : List (Maybe (List x)) -> Maybe (List (List x))
  lt = Distrib.listMaybeDistrib
  sμt : Maybe (List (List x)) -> Maybe (List x)
  sμt = map join
  μts : List (List (Maybe x)) -> List (Maybe x)
  μts = join
  in (tl |> lt |> sμt) === (μts |> Distrib.listMaybeDistrib)
diagram2 = funExt $ \case [] => Refl
                          (x :: xn) =>
                               let rec = app _ _ diagram2 xn
                               in ?diagram2_rhs

0 unit1 : let
  ηst : List x -> Maybe (List x)
  ηst = Just
  tηs : List x -> List (Maybe x)
  tηs = map Just
  in (tηs |> Distrib.listMaybeDistrib) === ηst
unit1 = funExt $ \case [] => Refl
                       (x :: xs) => let rec = app _ _ unit1 xs in rewrite rec in Refl

0 unit2 : let
  sηt : Maybe x -> Maybe (List x)
  sηt = map pure
  ηts : Maybe x -> List (Maybe x)
  ηts = pure
  in (ηts |> Distrib.listMaybeDistrib) === sηt
unit2 = funExt $ \case Nothing  => Refl
                       (Just xn) => Refl

{-
%unbound_implicits off

dist1 : DistributiveLaw Distrib.listMaybeDistrib
dist1 = MkDistLawListMaybe
    (funExt dia1)
    ?bb
    ?Cc
    ?dist1_rhs
    where
      0 dia1 : (v : List (Maybe (Maybe ?))) -> (listMaybeDistrib |> map listMaybeDistrib |> join) v === (map join |> listMaybeDistrib) v
      dia1 [] = Refl
      dia1 (x :: xs) = let r = dia1 xs in ?dia1_rhs_1

