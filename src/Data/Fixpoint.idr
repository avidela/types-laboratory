module Data.Fixpoint

import Data.Container
import Data.Container.Morphism
import Data.Product
import Data.Sigma
import Data.Coproduct

public export
record Fix (f : Type -> Type)  where
  constructor In
  unFix : Inf (f (Fix f))

public export
record Fix2 (f : Type -> Type -> Type) (a : Type) where
  constructor In2
  unFix : Inf (f a (Fix2 f a))

export
cata : Functor f => (f a -> a) -> Fix f -> a
cata f (In x) = f (assert_total $ map (cata f) x)

export
cata2 : Bifunctor f => (f a b -> b) -> Fix2 f a -> b
cata2 f (In2 x) = f (assert_total $ bimap id (cata2 f) x)
--
-- Ls : Type -> Type
-- Ls a = Fix (\x => Unit + a * x)

Slice : Type -> Type
Slice o = (a : Type ** (a -> o))

mutual
  data Mu1 : (Slice o -> Slice o) -> Type where
    Val : (f (Mu f)).fst -> Mu1 f

  Mu : (Slice o -> Slice o) -> Slice o
  Mu = \f => (Mu1 f ** Mu2 f)

  Mu2 : (f : Slice o -> Slice o) -> Mu1 f -> o
  Mu2 f (Val xs) = (f (Mu f)).snd xs

-- Star x = 1 + (x ○ Star x)

data StarShpF : Container -> Type  where
  Nil : StarShpF cont
  Cons : Σ cont.shp (\vc => cont.pos vc -> cont.shp) -> StarShpF cont

StarPosF : (r : Container) -> StarShpF r -> Type
StarPosF r Nil = ()
StarPosF (r !> r') (Cons (a ## b)) = Σ r (\vx => Σ (r' vx) (\rv => ?think))

StarShp : Container -> Type
StarShp c = Mu1 (\x => ?lol)
