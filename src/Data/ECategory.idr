module Data.ECategory

import Data.Category.Ops
import Data.Ops
import Control.Relation

private infixr 1 ~:>  -- Explicit morphism
private infixl 5 |:>  -- Explicit composition
private infix 0 ≡:  -- Explicit composition
private infixl 1 ⨾:  -- Explicit composition

export infix 0 ≡  -- Explicit composition

%unbound_implicits off
public export
record Category (o : Type) where
  constructor Cat
  (~:>) : Rel o
  id : {x : o} -> x ~:> x

  (⨾:) : {a, b, c : o} -> a ~:> b -> b ~:> c -> a ~:> c

  (≡:) : {a, b : o} -> Rel (a ~:> b)

  0 assoc : {a, b, c, d : o} ->
            {f : a ~:> b} ->
            {g : b ~:> c} ->
            {h : c ~:> d} ->
            (f ⨾: (g ⨾: h)) ≡: ((f ⨾: g) ⨾: h)

  0 idL : {a, b : o} -> {f : a ~:> b} -> (f ⨾: id) ≡: f

  0 idR : {a, b : o} -> {f : a ~:> b} -> (id ⨾: f) ≡: f

public export
(~>) : forall o. (cat : Category o) => Rel o
(~>) = (~:>) cat

public export
(⨾) : forall o. (cat : Category o) => {a, b, c : o} -> a ~> b -> b ~> c -> a ~> c
(⨾) = (⨾:) cat

public export
(≡) : forall o. (cat : Category o) => {a, b : o} -> Rel (a ~> b)
(≡) = (≡:) cat


private infixr 0 ~/>
private infixr 0 ~\>

%unbound_implicits off
record EFunctor {0 o1, o2 : Type} (c1 : Category o1) (c2 : Category o2) where
  constructor MkEFunc
  mapObj : o1 -> o2
  mapHom : (0 x, y : o1) -> (~:>) c1 x y -> (~:>) c2 (mapObj x) (mapObj y)
  presId : (0 h : o1) ->
           let
           0 (~/>) : o2 -> o2 -> Type
           (~/>) = (~:>) c2
           0 h1 : mapObj h ~/> mapObj h
           h1 = mapHom h h (c1.id {x=h})
           0 h2 : mapObj h ~/> mapObj h
           h2 = c2.id {x = mapObj h}
           0 res : Type
           res = (≡:) c2 {a = mapObj h, b = mapObj h} h1 h2
           in res

  presComp : {0 a, b, c : o1} -> let
           0 (~/>) : o1 -> o1 -> Type
           (~/>) = (~:>) c1
           in (f : a ~/> b) -> (g : b ~/> c) ->
              (≡:) c2 {a = mapObj a, b = mapObj c} (mapHom a c ((⨾:) c1 {a, b, c} f g))
                ((⨾:) c2 {a = mapObj a, b = mapObj b, c = mapObj c} (mapHom a b f) (mapHom b c g))
