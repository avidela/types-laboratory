module Data.Ornaments

import public Data.Tag
import public Data.Sigma
import public Data.Product
import public Data.Fixpoint

import Data.Vect
import Data.List1

%hide Builtin.infixr.(#)
%hide Prelude.Nat
%hide Prelude.Z
%hide Prelude.S
%hide Prelude.(<*>)
%hide Data.Singleton.(<*>)

%default partial

public export
data Desc : (i : Type) -> Type where
  Say : i -> Desc i
  Sig : (s : Type) -> (d : s -> Desc i) -> Desc i
  Ask : i -> Desc i -> Desc i

public export
pure : Desc i -> (i -> Type) -> i -> Type
pure (Say x) f y = x === y
pure (Sig s d) f y = Σ s $ \s' => pure (d s') f y
pure (Ask x z) f y = Σ (f x) (\_ => pure z f y)

namespace Constraints
  public export
  data Desc : (c : Type -> Type) -> (i : Type) -> Type where
    Say : i -> Desc c i
    Sig : (s : Type) -> (constraint : c s) => (d : s -> Desc c i) -> Desc c i
    Ask : i -> Desc c i -> Desc c i

  public export
  pure : Desc c i -> (i -> Type) -> i -> Type
  pure (Say x) f y = x === y
  pure (Sig s d) f y = Σ s $ \s => pure (d s) f y
  pure (Ask x z) f y = Σ (f x) (\_ => pure z f y)

  equals : {d : Desc DecEq i} -> {0 f : i -> Type} -> (forall x . Eq (f x)) => (a, b : Ornaments.Constraints.pure d f x) -> Bool
  equals {d = (Say y)} a b = True
  equals {d = (Sig s d)} (x1 ## x2) (y1 ## y2) with (decEq x1 y1)
    equals {d = (Sig s d)} (x1 ## x2) (x1 ## y2) | (Yes Refl) = equals x2 y2
    equals {d = (Sig s d)} (x1 ## x2) (y1 ## y2) | (No contra) = False
  equals {d = (Ask y z)} (x1 ## x2) (y1 ## y2) = x1 == y1 && (x2 `equals` y2)

  decidable : {d : Desc DecEq i} -> {0 f : i -> Type} -> (forall x . DecEq (f x)) => (a, b : Ornaments.Constraints.pure d f x) -> Dec (a = b)
  decidable {d = (Say y)} Refl Refl = Yes Refl
  decidable {d = (Sig s d)} (x1 ## x2) (y1 ## y2) with (decEq x1 y1)
    decidable {d = (Sig s d)} (x1 ## x2) (x1 ## y2) | (Yes Refl) with (decidable x2 y2)
      decidable {d = (Sig s d)} (x1 ## x2) (x1 ## x2) | (Yes Refl) | (Yes Refl) = Yes Refl
      decidable {d = (Sig s d)} (x1 ## x2) (x1 ## y2) | (Yes Refl) | (No contra) = No $ \Refl => contra Refl
    decidable {d = (Sig s d)} (x1 ## x2) (y1 ## y2) | (No contra) = No $ \Refl => contra Refl
  decidable {d = (Ask y z)} (x1 ## x2) (y1 ## y2) with (decEq x1 y1)
    decidable {d = (Ask y z)} (x1 ## x2) (x1 ## y2) | (Yes Refl) with (decidable x2 y2)
      decidable {d = (Ask y z)} (x1 ## x2) (x1 ## x2) | (Yes Refl) | (Yes Refl) = Yes Refl
      decidable {d = (Ask y z)} (x1 ## x2) (x1 ## y2) | (Yes Refl) | (No contra) = No $ \Refl => contra Refl
    decidable {d = (Ask y z)} (x1 ## x2) (y1 ## y2) | (No contra) = No $ \Refl => contra Refl

  printable : {d : Desc Show i} -> {0 f : i -> Type} -> (forall x . Show (f x)) => (a : Ornaments.Constraints.pure d f x) -> String
  printable {d = (Say y)} a = ""
  printable {d = (Sig s d)} (x1 ## x2) = "(\{show x1},  \{printable x2})"
  printable {d = (Ask y z)} (x1 ## x2) = "(\{show x1},  \{printable x2})"

  {d : Desc Show i} -> {0 f : i -> Type} -> (forall x . Show (f x)) => Show (Ornaments.Constraints.pure d f x) where
    show x = printable x

  {d : Desc DecEq i} -> {0 f : i -> Type} -> (forall x . Eq (f x)) => Eq (Ornaments.Constraints.pure d f x) where
    (==) = equals

  {d : Desc DecEq i} -> {0 f : i -> Type} -> (forall x . DecEq (f x)) => DecEq (Ornaments.Constraints.pure d f x) where
    decEq = decidable

export
infix 0 ::=

public export
record Con (i : Type) where
  constructor (::=)
  label : String
  type : Desc i

public export
(+) : Con i -> Con i -> Desc i
(+) (label1 ::= type1) (label2 ::= type2)
    = Sig (#[label1, label2]) $ \case
    (Here x) => type1
    (There (Here x)) => type2
    (There (There x)) => absurd x

total public export
flatPlus : Desc i -> Desc i -> Desc i
flatPlus (Sig (Label str) x) (Sig (Label str') x')
  = (str ::= x (MkLabel str)) + (str' ::= x' (MkLabel str'))
flatPlus (Sig (Label str) t1) (Sig (OneOf xs) t2)
  = Sig (OneOf (str :: xs)) $ \case (Here l) => t1 l
                                    (There l) => t2 l
flatPlus (Sig (OneOf xs) t2) (Sig (Label str) t1)
  = Sig (OneOf (str :: xs)) $ \case (Here l) => t1 l
                                    (There l) => t2 l
flatPlus (Sig (OneOf xs) t1) (Sig (OneOf ys) t2)
  = Sig (OneOf (xs ++ ys)) $ elimConcat _ _ t1 t2
flatPlus x y = Sig Bool $ \case True => x ; False => y

public export
enumeration : List String -> Desc ()
enumeration constructors
  = Sig (#constructors) $ const (Say ())

public export
labeled : String -> Desc i -> Desc i
labeled str x = Sig (Label str) (const x)

public export
liftType : Type -> Desc ()
liftType ty = Sig ty $ const $ Say ()

public export
labelType : String -> (ty : Type) -> Desc ()
labelType str ty = labeled str (liftType ty)

public export
data Data : Desc i -> i -> Type where
  Val : pure d (Data d) iv -> Data d iv

public export
(.getVal) : Data d i -> pure d (Data d) i
(.getVal) (Val x) = x

public export
NatD : Desc ()
NatD = Sig (#["Z", "S"]) $ \case
    "Z" => Say ()
    "S" => Ask () (Say ())

public export
NatT : Type
NatT = Data NatD ()

public export
Z : NatT
Z = Val ("Z" ## Refl)

public export
S : NatT -> NatT
S n = Val ("S" ## n ## Refl)

NatRet : (str : String) ->
         (e : IsYes (str `isElem` ["Z", "S"])) =>
         Type
NatRet "Z" = NatT
NatRet "S" = NatT -> NatT

namespace NatString
  fromString : (str : String) ->
          (e : IsYes (str `isElem` ["Z", "S"])) =>
          NatRet str
  fromString str with (str `isElem` ["Z", "S"])
    fromString "Z" | (Yes Here) = Z
    fromString "S" | (Yes (There Here)) = S
    fromString str | (Yes (There (There x))) = absurd x
    fromString str | (No contra) = absurd e

  %hide Builtin.fromString

  testZero : NatT
  testZero = "Z"

  testOne : NatT
  testOne = "S" "Z"

  testTwo : NatT
  testTwo = "S" ("S" "Z")

  %unhide Builtin.fromString

-- VecD : Type -> Desc NatT
-- VecD x = Sig (#["Nil", "Cons"]) $
--     \case "Nil" => Say Z
--           "Cons" => Sig x (\vx => Sig NatT $ \nx => Ask nx (Say (S nx)))
--
-- Vec : Type -> NatT -> Type
-- Vec x n = Data (VecD x) n
--
-- Nil : Vec x Z
-- Nil = Val ("Nil" ## Refl)
--
-- Cons : {n : NatT} -> x -> Vec x n -> Vec x (S n)
-- Cons y z = Val ("Cons" ## y ## n ## z ## Refl)

export
infixr 0 -:>

public export
0 (-:>) : (i -> Type) -> (i -> Type) -> Type
(-:>) x y  = {0 v : i} -> x v -> y v

public export
map : (d : Desc i) -> (x -:> y) -> [| d |] x -:> [| d |] y
map (Say w) f z = z
map (Sig s d) f z = z.π1 ## map (d z.π1) f z.π2
map (Ask w s) f (z1 ## z2) = f z1 ## Ornaments.map s f z2


covering public export
fold : {d : Desc i} -> {0 x : i -> Type} -> [| d |] x -:> x -> Data d -:> x
fold f (Val y) = f (map d (fold f) y)

public export
CBool : forall i. i -> Type
CBool _ = Bool

export
foldEq : {d : Desc i} -> [| d |] CBool -:> CBool -> Data d -:> CBool
foldEq f x = fold f x

public export
data Inverse : {i, j : Type} -> (j -> i) -> i -> Type where
  Ok : (jval : j) -> Inverse m (m jval)

public export
data Erased : Type -> Type where
  E : (0 x : t) -> Erased t

namespace Ornaments
  public export
  data Orn : (j : Type) -> (j -> i) -> Desc i -> Type where
    Say : Inverse e i -> Orn j e (Say i)
    Sig : (s : Type) -> {d : s -> Desc i} -> ((sv : s) -> Orn j e (d sv)) -> Orn j e (Sig s d)
    Ask : Inverse e i -> Orn j e d -> Orn j e (Ask i d)
    Del : (s : Type) -> (s -> Orn j e d) -> Orn j e d

  public export
  △ : (s : Type) -> (s -> Orn j e d) -> Orn j e d
  △ = Del

  public export
  Σ : (s : Type) -> {d : s -> Desc i} -> ((sv : s) -> Orn j e (d sv)) -> Orn j e (Sig s d)
  Σ = Sig

  -- identity ornament
  public export
  IdO : (d : Desc i) -> Orn i (\x => x) d
  IdO (Say x) = Say (Ok x)
  IdO (Sig s d) = Sig s (\sv => IdO (d sv))
  IdO (Ask x y) = Ask (Ok x) (IdO y)

  -- lists as an ornament on nats

  public export
  ListO : Type -> Orn () (\_ => ()) NatD
  ListO x = Sig (#["Z", "S"]) $ \case
      "Z" => Say (Ok ())
      "S" => Del x (\xval => Ask (Ok ()) (Say (Ok ())))

  public export
  extract : {0 d : Desc i} -> Orn j f d -> Desc j
  extract {d = .(Say _)} (Say (Ok x)) = Say x
  extract {d = .(Sig s d)} (Sig s g) = Sig s (\xs => extract (g xs))
  extract {d = .(Ask _ d)} (Ask (Ok x) y) = Ask x (extract y)
  extract {d = d} (Del s g) = Sig s (\xs => extract (g xs))

  public export
  ListD : Type -> Desc ()
  ListD x = extract (ListO x)

  public export
  0 List : Type -> Type
  List x = Data (ListD x) ()

  public export
  NilL : Ornaments.List x
  NilL = Val ("Z" ## Refl)

  public export
  ConsL : x -> Ornaments.List x -> Ornaments.List x
  ConsL y z = Val $ "S" ## y ## z ## Refl

  %unbound_implicits off

  public export
  erase : {0 i, j : Type} -> {e : j -> i} -> {d : Desc i} -> {x : i -> Type} ->
          (o : Orn j e d) -> pure (extract o) (x . e) -:> pure d x . e
  erase (Say (Ok jval)) y = cong e y
  erase (Sig s f) (y ## z) = y ## erase (f y) z
  erase (Ask (Ok jval) w) (y ## z) = y ## erase w z
  erase (Del s f) (y ## y2) = erase (f y) y2

  public export
  ornAlg : {0 i, j : Type} -> {e : j -> i} -> {d : Desc i} ->
           (o : Orn j e d) -> pure (extract o) (Data d . e) -:> Data d . e
  ornAlg o x = Val $ erase o x

  public export
  forget : {0 i, j : Type} -> {e : j -> i} -> {d : Desc i} ->
           (o : Orn {i} j e d) -> {jv : j} ->
           (((Data {i=j})) (extract o) jv) -> Data d (e jv)
  forget o = fold {x = Data d . e} (ornAlg o)

  public export
  length : {a : Type} -> Ornaments.List a -> NatT
  length x = forget (ListO a) x

  public export
  FinO : Orn NatT (const ()) NatD
  FinO = Sig (#["Z", "S"]) $ \case
      "Z" => Del NatT (\xn => Say (Ok (S xn)))
      "S" => Del NatT (\xn => Ask (Ok xn) (Say (Ok (S xn))))

  public export
  FinD : Desc NatT
  FinD = extract FinO

  public export
  FinT : NatT -> Type
  FinT = Data FinD

  public export
  FZ : (n : NatT) -> FinT (S n)
  FZ n = Val $ "Z" ## n ## Refl

  public export
  FS : (n : NatT) -> FinT n -> FinT (S n)
  FS n fn = Val $ "S" ## n ## fn ## Refl

  public export
  algOrn : {i : Type} -> {j : i -> Type} ->
           (d : Desc i) -> ([| d |] j -:> j) -> Orn (Σ i j) π1 d
  algOrn (Say x) f = Say (Ok {m = π1} (x ## f Refl))
  algOrn (Sig s d) f = Sig s (\s' => algOrn (d s') (f . (s' ##)))
  algOrn (Ask x y) f = Del (j x) (\arg => Ask (Ok (x ## arg)) (algOrn y (f . (arg ##))))

  public export
  aOoA : {i, j : Type} -> {e : j -> i} -> {d : Desc i} ->
         (o : Orn j e d) -> Orn (Σ j (Data d . e)) π1 (extract o)
  aOoA o = algOrn (extract o) (ornAlg o)

  public export
  VectO : (x : Type) -> Orn (Σ Unit (const NatT)) π1 (ListD x)
  VectO x = aOoA (ListO x)

  public export
  VectD : (x : Type) -> Desc (Σ Unit (const NatT))
  VectD x = extract (VectO x)

  public export
  Vect : Type -> NatT -> Type
  Vect x n = Data (VectD x) (MkUnit ## n)

  public export
  NilV : {0 t : Type} -> Vect t Z
  NilV = Val $ "Z" ## Refl

  public export
  ConsV : {t : Type} -> {n : NatT} -> t -> Vect t n -> Vect t (S n)
  ConsV x xs = Val $ "S" ## x ## n ## xs ## Refl

  public export
  toList : {a : Type} -> {n : NatT} -> Vect a n -> Ornaments.List a
  toList x = forget (VectO a) x

  public export
  vLength : {a, n : _} -> Ornaments.Vect a n -> NatT
  vLength = length . toList

  -- elem as an ornament on Fin using Σ Nat (Vect a) instead of Lists
  ElemO : (a : Type) -> Orn (Σ NatT (Vect a)) π1 FinD
  ElemO a = Sig (#["Z", "S"]) $ \case
      "Z" => Sig NatT $ \xn =>
             Del a $ \va =>
             Del (Vect a xn) $ \xs =>
             Say (Ok {j = Σ NatT (Vect a ) , m = π1} (S xn ## ConsV va xs))
      "S" => Sig NatT $ \size =>
             Del (Vect a size) $ \vec =>
             Del a $ \extra =>
             Ask (Ok (size ## vec)) $
             Say (Ok (S size ## ConsV extra vec))

  All : {i : Type} -> (e : Desc i) -> {d : i -> Type} ->
        (p : {iv : i} -> d iv -> Type) ->
        {iv : i} -> [| e |] d iv -> Type
  All (Say y) p x = Unit
  All (Sig s f) p x = All (f x.π1) p x.π2
  All (Ask y z) p x = p x.π1 * All z p x.π2
  everyInd : {i : Type} ->
             (e, d : Desc i) ->
             (p : {iv : i} -> Data d iv -> Type) ->
             ({iv' : i} -> (ds : pure {i} d (Data d) iv') ->
             All {iv=iv', d = Data d} d p ds -> p {iv=iv'} (Val {iv = iv'} ds)) ->
             {iv2 : i} -> (ds : pure e (Data d) iv2) ->
             Ornaments.All {iv=iv2} e {d = Data d} p ds

  induction : {i : Type} -> (desc : Desc i) ->
              (p : {iv : i} -> Data desc iv -> Type) ->
              ({iv' : i} -> (ds : pure {i} desc (Data desc) iv') ->
                Ornaments.All desc {iv=iv', d = Data desc} p ds ->
                p {iv=iv'} (Val {iv=iv'} {d = desc} ds)) ->
              {iv : i} -> (x : Data desc iv) -> p x
  induction desc p f (Val x) = f x (everyInd desc desc p f x)

  everyInd (Say x) d p f ds = ()
  everyInd (Sig s g) d p f ds = everyInd (g ds.π1) d p f ds.π2
  everyInd (Ask x y) d p f ds = induction d p f ds.π1 && everyInd y d p f ds.π2

