module Data.Focus

-- (<>>) : SnocList x -> List x -> List x
import Data.List.Quantifiers
import Data.List.Elem

infixl 1 ∈

data (∈) : x -> List x -> Type where
  In : (sx : SnocList x) -> (e : x) -> (xs : List x) -> e ∈ sx <>> e :: xs

here : {x : _} -> {xs : _} -> x ∈ (x :: xs)
here = In [<] x (xs)

total
to : {y, sx : _} -> e ∈ (([<y] ++ sx) <>> xs) -> e ∈ y :: (sx <>> xs)
to x with (sx)
  to x | [<] = x
  to x | (sy :< z) = let rec = to x in rec

total
there : {x, y, xs: _} -> x ∈ xs -> x ∈ (y :: xs)
there (In sx e xs) = to $ In ([<y] ++ sx) e (xs)

-- focus : (xs : List a) -> All (\x => x ∈ xs) xs
-- focus [] = []
-- focus (x :: xs) = here :: ( let rec = focus xs ; gn = map ?focux_rhs_1 rec in ?ebnd)

head : All p (x :: xs) -> p x
head (x :: xs) = x

tail : All p (x :: xs) -> All p xs
tail (x :: xs) = xs

drop : {sx : _} -> All p (sx <>> xs) -> All p xs
drop {sx = [<]} x = x
drop {sx = (sx :< y)} x with (drop {sx} x)
  drop {sx = (sx :< y)} x | rec = tail rec

lookup : x ∈ xs -> All p xs -> p x
lookup (In sx x ys) z = Focus.head (drop z)

