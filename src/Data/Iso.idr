module Data.Iso

import Proofs

import Data.Coproduct
import Data.Category
import Data.Product

import Control.Order
import Control.Relation

import Syntax.PreorderReasoning

public export
record GenIso (carrier : Type)
              (mor : Rel carrier)
              (rel : {0 x, y : carrier} -> Rel (mor x y))
              {auto pre : Preorder carrier mor}
              (left, right : carrier) where
  constructor MkGenIso
  to : mor left right
  from : mor right left
  0 toFrom : rel (transitive to from) Relation.reflexive
  0 fromTo : rel (transitive from to) Relation.reflexive

-- %unbound_implicits off
public export
record Iso (left, right : Type) where
  constructor MkIso
  to : left -> right
  from : right -> left
  0 toFrom : (x : right) -> to (from x) = x
  0 fromTo : (x : left) -> from (to x) = x

public export
record IsoEq (i1, i2 : Iso left right) where
  constructor MkIsoEq
  0 eqTo : (x : left) -> i1.to x === i2.to x
  0 eqFrom : (x : right) -> i1.from x === i2.from x


public export
identityIso : (0 x : Type) -> Iso x x
identityIso x = MkIso
  id
  id
  (\_ => Refl)
  (\_ => Refl)

public export
transIso : {0 x, y, z : Type} -> Iso x y -> Iso y z -> Iso x z
transIso iso1 iso2 =
  MkIso
    (iso2.to . iso1.to)
    (iso1.from . iso2.from)
    (\v => cong iso2.to (iso1.toFrom (iso2.from v)) `trans` iso2.toFrom v)
    (\v => trans (cong iso1.from (iso2.fromTo (iso1.to v))) (iso1.fromTo v))

public export
congIso : {0 t1, s1 : Type} ->
          {0 f1, f2 : t1 -> s1} ->
          {0 b1, b2 : s1 -> t1} ->
          {0 fb1 : (x : s1) -> f1 (b1 x) === x} ->
          {0 fb2 : (x : s1) -> f2 (b2 x) === x} ->
          {0 bf1 : (x : t1) -> b1 (f1 x) === x} ->
          {0 bf2 : (x : t1) -> b2 (f2 x) === x} ->
          (p1 : f1 === f2) ->
          (p2 : b1 === b2) ->
          (p3 : fb1 === (rewrite p1 in rewrite p2 in fb2)) ->
          (p4 : bf1 === (rewrite p2 in rewrite p1 in bf2)) ->
          MkIso f1 b1 fb1 bf1 === MkIso f2 b2 fb2 bf2
congIso Refl Refl Refl Refl = Refl

export
0 fromIsoEq : (a, b : Iso left right) -> IsoEq a b -> a === b
fromIsoEq (MkIso to1 from1 toFrom1 fromTo1) (MkIso to2 from2 toFrom2 fromTo2)
    (MkIsoEq eqTo eqFrom) = congIso (funExt eqTo) (funExt eqFrom) (funExtDep $ \_ => UIP _ _) (funExtDep $ \_ => UIP _ _)

transAssoc : {0 d : Type} -> {0 x, y, z, w : d} -> (a : x = y) -> (b : y = z) -> (c : z = w) -> trans a (trans b c) === trans (trans a b) c
transAssoc Refl Refl Refl = Refl

congDistrib : {0 ty, ty' : Type} -> {0 x, y, z : ty} -> {0 f : ty -> ty'} ->
              (a : x = y) -> (b : y = z) ->
              trans (cong f a) (cong f b) === cong f (trans a b)
congDistrib Refl Refl = Refl

congCompose : {0 ty, ty', ty'' : Type} -> {0 x, y : ty} ->
              {0 g : ty -> ty'} -> {0 f : ty' -> ty''} ->
              (a : x = y) -> cong (f . g) a = cong f (cong g a)
congCompose Refl = Refl

transIsoAssoc : {0 a, b, c, d : Type} ->
                 (f : Iso a b) -> (g : Iso b c) -> (h : Iso c d) ->
                 (f `transIso` (g `transIso` h)) `IsoEq`
                 ((f `transIso` g) `transIso` h)
transIsoAssoc f g h = MkIsoEq (\x => Refl) (\_ => Refl)

transIdLeft : trans Refl p = p
transIdLeft {p = Refl} = Refl

transIdRight : trans p Refl = p
transIdRight {p = Refl} = Refl

congId : cong (Prelude.id) p = p
congId {p = Refl} = Refl

public export
symIso : Iso x y -> Iso y x
symIso (MkIso to from toFrom fromTo) = MkIso from to fromTo toFrom

public export
isoIdRight : (f : Iso a b) -> transIso f (identityIso b) `IsoEq` f
isoIdRight (MkIso to from toFrom fromTo) =
  MkIsoEq (\_ => Refl) (\_ => Refl)

public export
isoIdLeft : (f : Iso a b) -> transIso (identityIso a) f `IsoEq` f
isoIdLeft (MkIso to from toFrom fromTo) =
  MkIsoEq (\_ => Refl) (\_ => Refl)


export
Reflexive Type Iso where
  reflexive = identityIso _

export
Transitive Type Iso where
  transitive = transIso

export
Symmetric Type Iso where
  symmetric = symIso

export
Equivalence Type Iso where

public export
IsoCoprod : Iso a b -> Iso x y -> Iso (a + x) (b + y)
IsoCoprod iso1 iso2 = MkIso
  (bimap iso1.to iso2.to)
  (bimap iso1.from iso2.from)
  (\case (+> r) => cong (+>) (iso2.toFrom r)
         (<+ l) => cong (<+) (iso1.toFrom l))
  (\case (+> r) => cong (+>) (iso2.fromTo r)
         (<+ l) => cong (<+) (iso1.fromTo l))

public export
IsoProd : Iso a b -> Iso x y -> Iso (a * x) (b * y)
IsoProd iso1 iso2 = MkIso
  (bimap iso1.to iso2.to)
  (bimap iso1.from iso2.from)
  (\x => cong2 (&&) (iso1.toFrom x.π1) (iso2.toFrom x.π2)`trans` prodUniq x)
  (\x => cong2 (&&) (iso1.fromTo x.π1) (iso2.fromTo x.π2) `trans` prodUniq x)

||| Types and isomorphisms form a category
public export
IsoCat : Category Type
IsoCat = MkCategory
  Iso
  (\x => identityIso _)
  transIso
  (\_, _, x => fromIsoEq _ _ (isoIdRight x))
  (\_, _, x => fromIsoEq _ _ (isoIdLeft x))
  (\_, _, _, _, x, y, z => fromIsoEq _ _ (transIsoAssoc x y z))

liftProof : {0 x, y : a} -> (iso : Iso a b) -> x === y -> iso.to x === iso.to y
liftProof iso Refl = Refl

