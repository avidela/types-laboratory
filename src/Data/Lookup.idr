||| Modular lookup of data
module Data.Lookup

||| Gien a container T obtain the lookup key
public export
interface HasKey t k | t where
  getKey : t -> k

||| Gien a container T obtain the return value
public export
interface HasVal t v | t where
  getVal : t -> v

||| For any foldable with equality, if the content has a key and a value then we can perform lookup
export
lookup : Eq key => HasKey a key => HasVal a v => Foldable f => key -> f a -> Maybe v
lookup key = foldr (\val, acc => if getKey val == key then Just (getVal val) else acc) Nothing

||| For any foldable with equality, if the content has a key and the value is a pair, return the
||| first projection
export
lookupFst : Eq key => HasKey a key => HasVal a (v1, v2) => Foldable f => key -> f a -> Maybe v1
lookupFst key = map fst . lookup key

||| For any foldable with equality, if the content has a key and the value is a pair, return the
||| second projection
export
lookupSnd : Eq key => HasKey a key => HasVal a (v1, v2) => Foldable f => key -> f a -> Maybe v2
lookupSnd key = map snd . lookup key

export
[KeyFst] HasKey (a, b) a where
  getKey = fst

export
[ValSnd] HasVal (a, b) b where
  getVal = snd
