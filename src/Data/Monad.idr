module Data.Monad

import Data.List.Quantifiers

%unbound_implicits off
record DMonad {0 m : Type -> Type} {auto 0 mon : Monad m} (0 α : m Type -> Type) where
  constructor MkDMonad
  bind : {0 a : Type} -> {0 b : a -> Type} ->
         (x : m a) ->
         (f : (y : a) -> m (b y)) ->
         m (α (map b x))

MJust : Maybe Type -> Type
MJust Nothing = Void
MJust (Just x) = x

MList : List Type -> Type
MList = All id

ListDMonad : DMonad MList
ListDMonad = MkDMonad $
    \case [] => const []
          (x :: xs) => \f => let x1 = f x in (?aa :: ?bb) :: ?ListDMonad_rhs_1

JustDMonad : DMonad MJust
JustDMonad = MkDMonad $
    \case Nothing => const Nothing
          (Just x) => \f => f x


