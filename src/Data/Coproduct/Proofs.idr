module Data.Coproduct.Proofs

import Data.Coproduct
import Data.Iso

public export
maybeToSum : Maybe a -> Unit + a
maybeToSum Nothing = <+ ()
maybeToSum (Just x) = +> x

public export
sumToMaybe : Unit + a -> Maybe a
sumToMaybe (<+ x) = Nothing
sumToMaybe (+> x) = Just x

%unbound_implicits off
public export
sumMaybeSumId : forall a. (x : Unit + a) -> maybeToSum (sumToMaybe x) = x
sumMaybeSumId (<+ ()) = Refl
sumMaybeSumId (+> x) = Refl

public export
maybeSumMaybeId : forall a. (x : Maybe a) -> sumToMaybe (maybeToSum x) = x
maybeSumMaybeId Nothing = Refl
maybeSumMaybeId (Just x) = Refl

