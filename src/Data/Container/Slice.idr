module Data.Container.Slice

import Data.Container.Category
import Data.Category.Comma
import Data.Category.Functor

m1 : (x : Container) -> CommaMorphism
    { left = UnitCat
    , right = Cont
    , middle = Cont
    , f1 = FromUnit Cont x
    , f2 = idF Cont
    }
    ?aaa
    ?bbb
