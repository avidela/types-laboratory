module Data.Container.W

import Data.Container
import Data.Container.Descriptions as Desc
import Data.Container.Morphism
import Data.Product
import Data.Sigma

%default total

%hide Prelude.List

public export
record W (c : Container) where
  constructor Sup
  rec : Ex c (W c)

WNat : Type
WNat = W MaybeCont

zero : WNat
zero = Sup (MkEx False absurd)

succ : WNat -> WNat
succ n = Sup (MkEx True (const n))

split : Bool -> Type
split True = Bool
split False = Void

WTree : Type
WTree = W (MkCont Bool split)

leaf : WTree
leaf = Sup (MkEx False absurd)

node : WTree -> WTree -> WTree
node l r = Sup (MkEx True (\x => if x then l else r))

-- What kind of container is that???
-- record LW (s : Type) (lp : s -> Type * Type) where
--   constructor Dup
--   -- cursed definition?
--   drec : W (MkCont (DPair s (π1 . lp)) (π2 . lp . DPair.fst))

public export
LW : (s : Type) -> (lp : s -> Type * Type) -> Type
LW s lp = W (MkCont (Σ s (π1 . lp)) (π2 . lp . π1))

public export
List : Type -> Type
List ty = LW Bool (\x => if x then ty && Unit else Unit && Void)

export
Nil : W.List a
Nil = Sup (MkEx (False ## ()) absurd)

export
Cons : a -> W.List a -> W.List a
Cons x y = Sup (MkEx (True ## x) (const y))


-- Induction on W
indW : (cont : Container) ->
       (C : W cont -> Type) ->        -- some conclusion we hope holds
       (c : (s : cont.shp) ->             -- given a shape…
            (f : cont.pos s -> W cont) ->  -- …and a bunch of kids…
            (h : (p : cont.pos s) -> C (f p)) ->     -- …and C for each kid in the bunch…
            C (Sup (MkEx s f))         -- …does C hold for the node?
       ) ->    -- If so,…
       (x : W cont) -> C x
indW cont conclusion sub (Sup (MkEx s f)) = sub s f (\p => indW cont conclusion sub (f p))

-- map across W types
map : (a =%> b) -> W a -> W b
map m (Sup (MkEx x f)) = Sup (MkEx (m.fwd x) (\y => map m (f (m.bwd x y))))

