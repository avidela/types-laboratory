module Data.Container.SeqAll

import Data.Container.Definition
import Data.Container.Morphism.Definition
import Data.Container.Extension

-- The new one
public export
(#>) : Container -> Container -> Container
(#>) c1 c2 = (x : Ex c1 c2.shp) !>
             ((val : c1.pos x.ex1) -> c2.pos (x.ex2 val))
