module Data.Container.Coproduct

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Morphism.Definition
import Data.Container.Morphism.Eq

import public Data.Coproduct

import Data.Category.Bifunctor

import Proofs.Sum

||| The coproduct on containers
public export
(+) : (c1, c2 : Container) -> Container
(+) c1 c2 = (!>) (c1.shp + c2.shp) (choice c1.pos c2.pos)

||| Eliminator for coproducts
public export
elim : a =%> c -> b =%> c -> (a + b) =%> c
elim x y = choice x.fwd y.fwd
    <! \case (<+ z) => x.bwd z
             (+> z) => y.bwd z

||| Choice of container morphisms
public export
bimap : c1 =%> c2 -> c3 =%> c4 -> (c1 + c3) =%> (c2 + c4)
bimap x y =
  (bimap x.fwd y.fwd) <!
  (\case (<+ w) => x.bwd w
         (+> w) => y.bwd w)

||| An operator version of choice
public export
(~+~) : c1 =%> c2 -> c3 =%> c4 -> (c1 + c3) =%> (c2 + c4)
(~+~) = bimap

export prefix 7 +~
||| An operator for mapping on the right side
public export
(+~) : c3 =%> c4 -> (c1 + c3) =%> (c1 + c4)
(+~) = (identity c1 ~+~)

||| An operator for mapping on the left side
public export
(~+) : c1 =%> c2 -> (c1 + c3) =%> (c2 + c3)
(~+) = (~+~ identity c3)

public export
dia : a + a =%> a
dia =
  dia <!
  (\case (+> r) => id
         (<+ l) => id)

public export
dia3 : a + a + a =%> a
dia3 = dia ~+~ identity a ⨾ dia

public export
CoprodContBifunctor : Functor (Cont * Cont) Cont
CoprodContBifunctor = MkFunctor
  (Product.uncurry (+))
  (\_, _, m => m.π1 ~+~ m.π2)
  (\(x1 && x2) => depLensEqToEq $ MkDepLensEq bifunctorId'
      (\case (+> v) => \_ => Refl
             (<+ v) => \_ => Refl
      )
  )
  (\a, b, c, (f1 && f2), (g1 && g2) => depLensEqToEq $ MkDepLensEq
      (bimapCompose)
      (\case (<+ x) => \_ => Refl
             (+> x) => \_ => Refl))
