module Data.Container.Hom

import Data.Container
import Data.Container.Cartesian
import Data.Container.Category
import Data.Category.Bifunctor
import Data.Sigma
import Data.Coproduct
import Proofs.Sigma
import Data.Iso

%unbound_implicits off

-- This does not exist!!!
-- eval : (a, b : Container) -> a ⊗ (a `hom2` b) =%> b
-- eval a b = (\x => x.π2 x.π1) <!
--            (\x, y => ?bbb && ?addad)

{-
homF : (0 a, b : Container * Container) ->
       (b.π1 =|> a.π1) * (a.π2 =|> b.π2) ->
       (uncurry hom) a =%> (uncurry hom) b
homF a b x = MkMorphism
    (\x1, y1 => x.π2.top (x1 (x.π1.top y1)))
    (\xn, yn => x.π1.top yn.π1 ## (\xv => x.π1.bot yn.π1 (yn.π2 (x.π2.bot (xn $ x.π1.top yn.π1) xv))))

homBifunctor : Bifunctor (FwdContCat).op FwdContCat Cont
homBifunctor = MkFunctor
   (uncurry hom)
   homF
   (\v => depLensEqToEq (MkDepLensEq (\vn => Refl) (\vn, gn => dpairUniq _)))
   (\v, w, z, f, g => depLensEqToEq $ MkDepLensEq (\vn => Refl) (\zn, wn => Refl))

mohF : (0 a, b : Container * Container) ->
       (b.π1 =|> a.π1) * (a.π2 =|> b.π2) ->
       (uncurry moh) a =%> (uncurry moh) b
mohF a b x = MkMorphism
    (\v, w => x.π2.top (v $ x.π1.top w))
    (\v, w, z, dr => let rrr = w ?aa ?bb in ?mohF_rhs)

mohBifunctor : Bifunctor (FwdContCat).op FwdContCat Cont
mohBifunctor = MkFunctor
  (uncurry moh)
  mohF
  ?asdad
  ?mohBifunctor_rhs

