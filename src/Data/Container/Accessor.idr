||| All interpretations of dependent lenses as data accessors are here
module Data.Container.Accessor

import Optics.Lens

import Data.Product
import Data.Boundary
import Data.Coproduct
import Data.Sigma
import Data.Container.Morphism
import Data.Container.Morphism.Eq
import Data.Container.Morphism.Closed
import Data.Container.Descriptions
import Data.Iso
import Data.Vect

import Proofs.Extensionality
import Proofs.Congruence
import Proofs.Sigma
import Proofs.Unit
import Proofs.Void

%default total

%unbound_implicits off

vectToFn : {0 a : Type} -> {0 m : Nat} -> Vect m a -> Fin m -> a
vectToFn v i = index i v

fnToVect : {0 a : Type} -> {m : Nat} -> (Fin m -> a) -> Vect m a
fnToVect f {m = 0} = []
fnToVect f {m = (S k)} = f FZ :: fnToVect (f . FS)

vecIso : {0 a : Type} -> {n : Nat} -> Vect n a `Iso` (Fin n -> a)
vecIso = MkIso
  vectToFn
  fnToVect
  toFrom
  fromTo
  where
    fromTo : {0 n : Nat} -> (x : Vect n a) -> fnToVect (vectToFn x) === x
    fromTo [] = Refl
    fromTo (x :: xs) = cong (x ::) (fromTo xs)

    0 toFrom : {m : Nat} -> (fn : Fin m -> a) -> vectToFn {m} (fnToVect {m} fn) === fn
    toFrom {m = 0} fn = funExt $ \w => absurd w
    toFrom {m = (S k)} fn = funExt go
      where
        go : (w : Fin (S k)) -> index w (fn FZ :: fnToVect (\y => fn (FS y))) = fn w
        go FZ = Refl
        go (FS x) = app (vectToFn (fnToVect (fn . FS))) (fn . FS) (toFrom (fn . FS)) x

%unbound_implicits on
listIsTraversal : Const2 a a' =%> ListCont #> Const2 b b' -> Traversal (MkB a a') (MkB b b')
listIsTraversal (fwd <! bwd) = MkTraversal $ \x =>
                                       (fwd x).ex1 ## (vecIso.from (fwd x).ex2 && \vx => bwd x (vecIso.to vx))

traversalIsList : Traversal (MkB a a') (MkB b b') -> Const2 a a' =%> ListCont #> Const2 b b'
traversalIsList (MkTraversal e) =
  (\x => MkEx (e x).π1  (vecIso.to (e x).π2.π1)) <!
  (\x, f => (e x).π2.π2 (vecIso.from f))

lemma : {0 a : Type} ->
        (f : a -> Σ Nat (\x => Fin x -> b)) ->
        (x : a) -> ((f x) .π1 ## vectToFn {a=b, m = (f x).π1} (fnToVect {a=b, m = (f x).π1} ((f x) .π2))) === (f x)
lemma f x with (f x)
  lemma f x | (n ## m) = cong (n ##) (vecIso.toFrom m)

traversalIso : (Const2 a a' =%> ListCont #> Const2 b b') `Iso` (Traversal (MkB a a') (MkB b b'))
traversalIso = MkIso
  listIsTraversal
  traversalIsList
  pp
  qq
  where
    pp : (zv : Traversal (MkB a a') (MkB b b')) -> listIsTraversal (traversalIsList zv) === zv
    pp (MkTraversal ex) = cong MkTraversal $ funExt $ fg
    where
      fg : (z : a) -> ? === ex z
      fg z with (ex z) proof prf
        fg z | (0 ## ([] && p)) = rewrite sym $ proj1 prf in
              cong2Dep (##) Refl $
              cong2 Product.(&&)
                       { a = fnToVect (vectToFn (((ex z) .π2) .π1))
                       , b = replace {p = \nm => Vect nm b} (sym $ proj1 prf) []
                       , c = (\vx => ((ex z) .π2) .π2 (fnToVect (vectToFn vx)))
                       , d = replace {p = \nm => Vect nm b' -> a'} (sym $ proj1 prf) p}
                       (rewrite prf in Refl)
                       (funExt $ \vx => rewrite vecIso.fromTo vx in rewrite prf in Refl)
        fg z | ((S k) ## (xs && p)) = rewrite sym $ proj1 prf in
              cong2Dep' (##) Refl $
              cong2 Product.(&&)
                  { a = fnToVect (vectToFn (((ex z) .π2) .π1))
                  , b = replace {p = \nm => Vect nm b} (sym $ proj1 prf) xs
                  , c = (\vx => ((ex z) .π2) .π2 (fnToVect (vectToFn vx)))
                  , d = replace {p = \nm => Vect nm b' -> a'} (sym $ proj1 prf) p}
                  (rewrite vecIso.fromTo (((ex z) .π2) .π1) in rewrite prf in Refl)
                  (funExt $ \vx => rewrite vecIso.fromTo vx in rewrite prf in Refl)

    0 qq : (x : Const2 a a' =%> (ListCont #> Const2 b b')) -> traversalIsList (listIsTraversal x) === x
    qq (f <! b) = depLensEqToEq $ MkDepLensEq
        (\vx => exEqToEq $ MkExEq Refl (app _ _ (vecIso.toFrom (f vx).ex2)))
        (\v, w => cong (b v) (vecIso.toFrom w))

---------------------------------------------------------------------------------
-- Affine traversal is isomorphic to the Maybe Monad on Container from #>
---------------------------------------------------------------------------------

affineX : {x : _} -> MkAffine (x.read) === x
affineX {x = MkAffine r} = Refl

%unbound_implicits off
parameters {0 a, a', b, b' : Type}
  readImpl :
             {fwd : a -> Ex MaybeCont b} ->
             {bwd : (x : a) -> (isTrue ((fwd x).ex1) -> b') -> a'} ->
             a -> a' + (b * (b' -> a'))
  readImpl {fwd} {bwd} x with (fwd x) proof p
    readImpl x | (MkEx False p2) = <+ (bwd x (replace
                {p = \x : MaybeType b => isTrue x.ex1 -> b'}
                (sym p)
                absurd))
    readImpl x | (MkEx True p2) = +> (p2 () && \bv => bwd x (rewrite p in const bv))

  maybeIsAffine : Const2 a a' =%> MaybeCont #> Const2 b b' -> Affine (MkB a a') (MkB b b')
  maybeIsAffine mor =
    MkAffine (readImpl {fwd = mor.fwd, bwd=mor.bwd})


  just : (aff : a -> a' + (b * (b' -> a'))) ->
         a -> Ex MaybeCont b
  just aff x = case aff x of
               (<+ y) => Nothing
               (+> y) => Just y.π1

  recover : (aff : a -> a' + (b * (b' -> a'))) ->
            (v : a) -> (isTrue (just aff v).ex1 -> b') -> a'
  recover aff v f with (aff v)
    recover aff v f | (<+ x) = x
    recover aff v f | (+> x) = x.π2 (f ())

  affineIsMaybe : Affine (MkB a a') (MkB b b') -> Const2 a a' =%> MaybeCont #> Const2 b b'
  affineIsMaybe aff =
      (just aff.read) <!
      (recover aff.read)

  lemma1 : (x : Affine (MkB a a') (MkB b b')) -> maybeIsAffine (affineIsMaybe x) = x
  lemma1 x = (cong MkAffine $ funExt $ lemma11) `trans` affineX {x}
    where
      0 lemma11 : (v : a) -> readImpl {fwd = fwd (affineIsMaybe x), bwd = bwd (affineIsMaybe x)} v === x.read v
      lemma11 v with (fwd (affineIsMaybe x) v) proof p'
        lemma11 v | (MkEx False p2) with (x.read v)
          lemma11 v | (MkEx False p2) | (<+ y) = Refl
          lemma11 v | (MkEx False p2) | (+> y) = absurd $ inj1 p'
        lemma11 v | (MkEx True p2) with (x.read v) proof q'
          lemma11 v | (MkEx True p2) | (<+ y) = absurd $ inj1 p'
          lemma11 v | (MkEx True p2) | (+> y) =
            let fnWrangling : p2 () === y.π1
                fnWrangling = let v = app _ _ (inj2 p') MkUnit in sym v
            in cong (+>) (cong2 (&&) fnWrangling Refl `trans` projIdentity y)


  0 lemma21 : {fwd : a -> Ex MaybeCont b} ->
              {bwd : (x : a) -> (isTrue ((fwd x).ex1) -> b') -> a'} ->
              (x : a) -> (Accessor.just (Accessor.readImpl {fwd, bwd})) x = fwd x
  lemma21 {fwd} {bwd } x with (fwd x) proof pq
    lemma21 x | (MkEx True pp) = cong (MkEx True) (funExt $ \w => cong pp (unitUniq w))
    lemma21 x | (MkEx False pp) = cong (MkEx False) (allVoid absurd pp)

  0 lemma22 : {fwd : a -> Ex MaybeCont b} ->
              {bwd : (x : a) -> (isTrue ((fwd x).ex1) -> b') -> a'} ->
              (x : a) -> (vx : isTrue ((just (Accessor.readImpl {fwd,bwd}) x).ex1) -> b') ->
              let leftside : a'
                  leftside = Accessor.recover (Accessor.readImpl {fwd,bwd}) x vx
                  rightSide : a'
                  rightSide = bwd x (replace
                                 {p = \xa : MaybeType b => (isTrue (xa.ex1) -> b')}
                                 (lemma21 {fwd, bwd, x})
                                 vx)
              in leftside === rightSide
  lemma22 {fwd} {bwd} x vx with (fwd x) proof pr
    lemma22 x vx | (MkEx True p) = cong (bwd x) (funExt $ rewrite pr in \() => Refl)
    lemma22 x vx | (MkEx False q) = cong (bwd x) (rewrite pr in allVoid absurd vx)

  0 lemma2 : (x : Const2 a a' =%> MaybeCont #> Const2 b b') -> affineIsMaybe (maybeIsAffine x) = x
  lemma2 (fwd <! bwd) = cong2Dep' (<!)
    (funExt (lemma21 {fwd}))
    (funExtDep $ \x => funExt $ \vx => lemma22 {fwd, bwd} x vx)

  affineIso : (Const2 a a' =%> MaybeCont #> Const2 b b') `Iso` (Affine (MkB a a') (MkB b b'))
  affineIso = MkIso
      maybeIsAffine
      affineIsMaybe
      lemma1
      lemma2



  affineIsLinear : Affine (MkB a a') (MkB b b') -> Closed (Const2 a a') (MaybeCont #> Const2 b b')
  affineIsLinear (MkAffine eff) = MkClosed $
    \v => case eff v of
               <+ bval => Nothing ## const bval
               +> bval => Just bval.π1 ## (\p => bval.π2 (p ()))

  linearIsAffine : Closed (Const2 a a') (MaybeCont #> Const2 b b') -> Affine (MkB a a') (MkB b b')
  linearIsAffine (MkClosed eff) = MkAffine $ \v =>
      case eff v of
           ((MkEx False p3) ## p2) => <+ (p2 absurd)
           ((MkEx True p3) ## p2) => +> (p3 () && \bv => p2 (const bv))

record DepAffine (a, b : Container) where
  constructor MkAffine
  read : (x : a.shp) -> a.pos x + (Σ b.shp (\y => b.pos y -> a.pos x))
