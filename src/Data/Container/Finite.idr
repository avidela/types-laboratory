module Data.Container.Finite

import Data.Container
import Data.Fin

-- TODO: check distrib law with this
record FContainer where
  constructor MkFCont
  shape : Type
  positions : shape -> Nat

asCont : FContainer -> Container
asCont x = MkCont x.shape (Fin . x.positions)


