module Data.Container.Directed

import Data.Container
import Data.Product
import Data.Coproduct

export
interface Directed (0 c : Container) where
  act : (s : c.shp) -> c.pos s -> c.shp
  root : {s : c.shp} -> c.pos s
  ctx : {s: c.shp} -> (p : c.pos s) -> c.pos (act s p) -> c.pos s

||| Unit container is a directed container
export
Directed CUnit where
  act a b = ()
  root {s} = ()
  ctx {s} p v = ()

||| from any constant container with a monoidal structure emerges a directed container
export
{ty : Type} -> Monoid ty => Directed (Const ty) where
  act a = id
  root = neutral
  ctx p v = p <+> v

export
{c1 : Container} -> {c2 : Container} -> Directed c1 => Directed c2 =>
      Directed (c1 ⊗ c2) where
  act a = bimap (act {c=c1} a.π1) (act {c=c2} a.π2)
  root {s= s1 && s2} = root && root
  ctx (p && p') = bimap (ctx p) (ctx p')

export
{c1 : Container} -> Directed c1 => Directed (c1 * CUnit) where
  act a (<+ x) = act a.π1 x && a.π2
  act a (+> x) = a.π1 && ()
  root {s= s1 && s2} = +> ()
  ctx (<+ x) (<+ q) = <+ x
  ctx (<+ x) (+> q) = <+ x
  ctx (+> x) (<+ q) = <+ q
  ctx (+> x) (+> q) = +> ()
