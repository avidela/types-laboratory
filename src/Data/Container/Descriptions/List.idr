module Data.Container.Descriptions.List

import Data.Container
import Data.Container.Morphism
import Data.Container.Morphism.Cartesian.Bifunctor.Cont
import Data.Container.Cartesian
import Data.Fin
import Data.Sigma
import Data.List.Quantifiers
import Data.Iso

import Proofs.Sigma
import Proofs.Congruence
import Proofs.Void
import Proofs.Extensionality

%default total
%hide Prelude.Ops.infixl.(|>)

public export
ListCont : Container
ListCont = MkCont Nat Fin

public export
TyList : Type -> Type
TyList = Ex ListCont

public export
ListM : Container -> Container
ListM = (#>) ListCont

public export
ListAny : Container -> Container
ListAny = (○) ListCont

public export
Nil : TyList a
Nil = MkEx Z absurd

public export
Cons : a -> TyList a -> TyList a
Cons x ls = MkEx (S ls.ex1) (\case (FS n) => ls.ex2 n
                                 ; FZ => x)

tyListToList : TyList x -> List x
tyListToList (MkEx Z y) = []
tyListToList (MkEx (S n) y) = y FZ :: assert_total (tyListToList (MkEx n (y . FS)))

listToTyList : List x -> TyList x
listToTyList [] = Nil
listToTyList (y :: xs) = Cons y (listToTyList xs)

tyListIso : TyList x `Iso` List x
tyListIso = MkIso
  tyListToList
  listToTyList
  prf1
  prf2
  where
    prf1 : (x : List x) -> tyListToList (listToTyList x) = x
    prf1 [] = Refl
    prf1 (x :: xs) = cong (x ::) (let ppp = prf1 xs in cong tyListToList (exUniq _) `trans` ppp)
    0 prf2 : (v : TyList x) -> listToTyList (tyListToList v) = v
    prf2 (MkEx Z f) = exEqToEq $ MkExEq Refl (\x => absurd x)
    prf2 (MkEx (S n) f) with (assert_total $ prf2 (MkEx n (f . FS)))
      prf2 (MkEx (S n) f) | pat = exEqToEq $ MkExEq (cong S ?add) ?bbu

public export
concat : TyList a -> TyList a -> TyList a
concat (MkEx Z dd) y = y
concat (MkEx (S n) dd) y = assert_total $ Cons (dd FZ) (concat (MkEx n (dd . FS)) y)

public export
singleton : a -> TyList a
singleton x = Cons x Nil

public export
pure : a =%> ListM a
pure = singleton <! bwd
    where
      bwd : (x : a.shp) -> (ListM a).pos (singleton x) -> a.pos x
      bwd x f = f FZ

public export
flatList : List (List x) -> List x
flatList [] = []
flatList (y :: xs) = y ++ flatList xs

public export
ListAll : Container -> Container
ListAll c2 = MkCont
    (List c2.shp)
    (All c2.pos)

fromListM : ListM x =%> ListAll x
fromListM =
    tyListIso.to <!
    bwd
    where
      bwd : (v : TyList x.shp) -> All x.pos (tyListToList v) -> (val : Fin (v.ex1)) -> x.pos (v.ex2 val)
      bwd (MkEx 0 p2) _ val impossible
      bwd (MkEx (S k) p2) (x :: xs) FZ = x
      bwd (MkEx (S k) p2) (x :: xs) (FS v) = bwd (MkEx k (p2 . FS)) xs v

toListM : ListAll x =%> ListM x
toListM = tyListIso.from <! bwd
    where
      bwd : (v : List x.shp) -> ((val : Fin ((listToTyList v).ex1)) -> x .pos ((listToTyList v).ex2 val)) -> All x.pos v
      bwd [] f = []
      bwd (y :: (xs)) f = f FZ :: bwd xs (\x => f (FS x))

public export
AllListMap : a =%> b -> ListAll a =%> ListAll b
AllListMap mor = (map mor.fwd) <! mapBwd
  where
    mapBwd : (x : List (a .shp)) -> All (b .pos) (mapImpl (mor.fwd) x) -> All (a .pos) x
    mapBwd [] [] = []
    mapBwd (x :: xs) (y :: ys) = mor.bwd x y :: mapBwd xs ys

join' : ListAll (ListAll x) =%> ListAll x
join' =  flatList <! bwd
    where
      bwd : (val : List (List (x .shp))) -> All (x .pos) (flatList val) -> All (All (x .pos)) val
      bwd [] x = []
      bwd (y :: xs) arg = let k : (All x.pos y, All x.pos (flatList xs))
                              k = splitAt y arg in fst k :: bwd xs (snd k)
public export
fromList : ((x : a) -> p x) -> (xs : List a) -> All p xs
fromList _ [] = []
fromList e (x :: xs) = e x :: fromList e xs

public export
join : {0 x : Container} -> ListM (ListM x) =%> ListM x
join = let
      pre2 : ListM (ListAll x) =%> ListAll (ListAll x)
      pre2 = fromListM {x = ListAll x}

      pre1 : ListM  (ListM x) =%> ListM (ListAll x)
      pre1 = contFunctor {a = ListCont} fromListM

    in pre1 ⨾ pre2 ⨾ join' ⨾ toListM

public export
close : ListAll CUnit =%> CUnit
close = (const ()) <! (\ls, vx => fromList (const ()) ls)

----------------------------------------------------------------------------------
-- Any Lists
----------------------------------------------------------------------------------

public export
AnyList : Container -> Container
AnyList c2 = MkCont
    (List c2.shp)
    (Any c2.pos)

AnyToList : AnyList x =%> ListAny x
AnyToList =  tyListIso.from <! bwd
    where
      bwd : (v : List (x.shp)) -> Σ (Fin ((listToTyList v).ex1)) (\y => x.pos ((listToTyList v).ex2 y)) -> Any (x.pos) v
      bwd [] x = absurd x.π1
      bwd (x :: xs) (FZ ## p2) = Here p2
      bwd (x :: xs) (FS p1 ## p2) = There (bwd xs (p1 ## p2))

ListToAny : ListAny x =%> AnyList x
ListToAny = tyListIso.to <! bwd
    where
      bwd : (v :TyList x.shp) -> Any x.pos (tyListToList v) -> Σ (Fin (v.ex1)) (\y => x .pos (v.ex2 y))
      bwd (MkEx 0 v2) x = absurd x
      bwd (MkEx (S k) v2) (Here x) = FZ ## x
      bwd (MkEx (S k) v2) (There x) = let rec = bwd (MkEx k (v2 . FS)) x in FS rec.π1 ## rec.π2

