
module Data.Container.Descriptions.Maybe

import Data.Container.Definition
import Data.Container.Morphism
import Data.Container.Extension
import Data.Container.Sequence
import Data.Container.SeqAll
import Data.Container.Coproduct
import Data.Container.Tensor
import Data.Container.Lift
import Data.Container.Morphism.Eq
import Data.Product
import Data.Sigma
import Proofs.Sigma
import Data.Iso
import Data.Coproduct.Proofs
import Data.Coproduct

import Proofs

%default total
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

public export
isTrue : Bool -> Type
isTrue True = Unit
isTrue False = Void

public export
MaybeCont : Container
MaybeCont = MkCont Bool isTrue

public export
MaybeType : Type -> Type
MaybeType = Ex MaybeCont

public export
MaybeM : Container -> Container
MaybeM = (MaybeCont ○)

public export
MaybeHandle : Container -> Container
MaybeHandle = (MaybeCont #>)

public export
just : x =%> MaybeM x
just = (\x => MkEx True (const x)) <! (\a, b => b.π2)

public export
join : {0 x : Container} -> (MaybeM (MaybeM x)) =%> MaybeM x
join =  joinFwd <! joinBwd
    where
      joinFwd : (MaybeM (MaybeM x)).shp ->
                (MaybeM x).shp
      joinFwd (MkEx True fn) with (fn ())
        joinFwd (MkEx True fn) | (MkEx False gn) = MkEx False absurd
        joinFwd (MkEx True fn) | (MkEx True gn) = MkEx True (\_ => gn ())
      joinFwd (MkEx False fn) = MkEx False absurd

      joinBwd : (s : (MaybeM (MaybeM x)).shp) ->
                ((MaybeM x).pos (joinFwd s)) ->
                (MaybeM (MaybeM x)).pos s
      joinBwd (MkEx True fn) (x1 ## x2) with (fn ()) proof p
        joinBwd (MkEx True fn) (x1 ## x2) | (MkEx False p2)
            = absurd x1
        joinBwd (MkEx True fn) (x1 ## x2) | (MkEx True p2)
            = () ## (rewrite p in () ## x2)
      joinBwd (MkEx False fn) (x1 ## x2) = absurd x1

public export
strength : ((MaybeM x) ⊗ y) =%> (MaybeM (x ⊗ y))
strength = fwd <! bwd
  where
    fwd : (MaybeM x ⊗ y).shp -> (MaybeM (x ⊗ y)).shp
    fwd arg = MkEx arg.π1.ex1 (\k => arg.π1.ex2 k && arg.π2)

    bwd : (s : (MaybeM x).shp * y.shp) ->
          (p : (MaybeM (x ⊗ y)).pos (fwd s)) ->
          ((MaybeM x) ⊗ y).pos s
    bwd s p = (p.π1 ## p.π2.π1) && p.π2.π2

public export
Just : (x : a) -> MaybeType a
Just x = MkEx True (\_ => x)

public export
Nothing : MaybeType a
Nothing = MkEx False absurd

export
toIdris : MaybeType a -> Maybe a
toIdris (MkEx False p) = Nothing
toIdris (MkEx True p) = Just (p ())

public export
fromIdris : Maybe a -> MaybeType a
fromIdris Nothing = Nothing
fromIdris (Just x) = Just x

%unbound_implicits off
toFromEq : {0 a : Type} -> (x : Maybe a) -> toIdris (fromIdris x) === x
toFromEq Nothing = Refl
toFromEq (Just x) = Refl

0 fromToEq : {0 a : Type} -> (x : MaybeType a) -> fromIdris (toIdris x) === x
fromToEq (MkEx True p) = cong (MkEx True) (funExt $ \() => Refl)
fromToEq (MkEx False p) = cong (MkEx False) (allUninhabited _ _)

%unbound_implicits on

MaybeTypeIso : MaybeType a `Iso` Maybe a
MaybeTypeIso = MkIso
  toIdris
  fromIdris
  toFromEq
  fromToEq

----------------------------------------------------------------------------------
-- Ignore errors
----------------------------------------------------------------------------------

public export
data Any : (x -> Type) -> Maybe x -> Type where
  Yep : {v : x} -> pred v -> Any pred (Just v)

public export
Uninhabited (Any p Nothing) where
  uninhabited _ impossible

public export
extract : Any p (Just x) -> p x
extract (Yep y) = y

public export
extract' : {0 x : Maybe a} -> Any p x -> (v : a ** p v)
extract' (Yep {v} y) = (v ** y)

public export
extractAny : {x : Maybe a} -> Any p x -> (v : a ** ((x ≡ Just v) * p v))
extractAny (Yep {v} y) = (v ** Refl && y)

public export
MaybeAny : Container -> Container
MaybeAny c = MkCont (Maybe c.shp) (Any c.pos)

public export
map_MaybeAny : a =%> b -> MaybeAny a =%> MaybeAny b
map_MaybeAny m = (map m.fwd) <!
    (\case Nothing => \x => absurd x
           (Just x) => \(Yep y) => Yep $ m.bwd x y)

-- Isomorphism with MaybeC ○ x

MaybeToAny : MaybeM x =%> MaybeAny x
MaybeToAny =  toIdris <! bwd
    where
      bwd : (v : MaybeType x.shp) -> Any x.pos (toIdris v) -> Σ (isTrue v.ex1) (\y => x.pos (v.ex2 y))
      bwd (MkEx True p) (Yep x) = () ## x
      bwd (MkEx False p) _ impossible

AnyToMaybe : MaybeAny x =%> MaybeM x
AnyToMaybe =  fromIdris <! bwd
    where
      bwd : (v : Maybe x.shp) -> Σ (isTrue ((fromIdris v).ex1)) (\y => x.pos ((fromIdris v).ex2 y)) -> Any x.pos v
      bwd Nothing x = absurd x.π1
      bwd (Just y) x = Yep x.π2

AnyMaybeAnyId : {x : Container} ->
                (AnyToMaybe {x} ⨾ MaybeToAny {x}) `DepLensEq` identity {a = MaybeAny x}
AnyMaybeAnyId = MkDepLensEq toFromEq
   (\case Nothing => \x => absurd x
          (Just x) => \(Yep y) => Refl)

0
MaybeAnyMaybeEqBwd : forall x.
    (v : (MaybeM x).shp) -> (y : (MaybeM x).pos ((MaybeToAny {x} ⨾ AnyToMaybe {x}).fwd v)) ->
          let 0 p1, p2 : (MaybeM x).pos v
              p1 = (MaybeToAny {x} ⨾ AnyToMaybe {x}).bwd v y
              p2 = (replace {p = (MaybeM x).pos} (fromToEq v) y)
          in p1 === p2
MaybeAnyMaybeEqBwd (MkEx False p2) y = void y.π1
MaybeAnyMaybeEqBwd (MkEx True p2) (() ## q2) = sigEqToEq $
    MkSigEq Refl Refl

MaybeAnyMaybeId : {x : Container} ->
                  MaybeToAny {x} ⨾ AnyToMaybe {x} `DepLensEq` identity {a = MaybeM x}
MaybeAnyMaybeId = MkDepLensEq fromToEq MaybeAnyMaybeEqBwd

MaybeAnyContIso : forall x. ContIso (MaybeM x) (MaybeAny x)
MaybeAnyContIso = MkGenIso
    MaybeToAny
    AnyToMaybe
    MaybeAnyMaybeId
    AnyMaybeAnyId

-- Isomorphism with (1 :- 0) + x
maybeToOneBwd : (x : Maybe c.shp) ->
                choice (\x => Void) c.pos (maybeToSum x) ->
                Any c.pos x
maybeToOneBwd Nothing y = void y
maybeToOneBwd (Just x) y = Yep y

MaybeAnyToOne : MaybeAny x =%> CNeutral + x
MaybeAnyToOne = maybeToSum <! maybeToOneBwd

oneToMaybeAnyBwd : (x : () + c.shp) ->
                   Any c.pos (sumToMaybe x) -> choice (\x => Void) c.pos x
oneToMaybeAnyBwd (<+ x) y = absurd y
oneToMaybeAnyBwd (+> x) (Yep y) = y

OneToMaybeAny : CNeutral + x =%> MaybeAny x
OneToMaybeAny = sumToMaybe <! oneToMaybeAnyBwd

OneToMaybeAnyToOneId : {x : Container} ->
                       OneToMaybeAny {x} ⨾ MaybeAnyToOne {x} `DepLensEq` identity {a = CNeutral + x}
OneToMaybeAnyToOneId = MkDepLensEq sumMaybeSumId
    (\case (<+ x) => \x => void x
           (+> x) => \_ => Refl)

MaybeAnyToOneToMaybeAnyId : {x : Container} ->
                            MaybeAnyToOne {x} ⨾ OneToMaybeAny {x}  `DepLensEq` identity {a = MaybeAny x}
MaybeAnyToOneToMaybeAnyId = MkDepLensEq maybeSumMaybeId
    (\case Nothing => \x => absurd x
           (Just x) => \(Yep y) => Refl)

MaybeAnyOneIso : {x : Container} -> ContIso (MaybeAny x) (CNeutral + x)
MaybeAnyOneIso = MkGenIso
    MaybeAnyToOne
    OneToMaybeAny
    MaybeAnyToOneToMaybeAnyId
    OneToMaybeAnyToOneId

----------------------------------------------------------------------------------
-- Handle errors
----------------------------------------------------------------------------------

public export
data All : (x -> Type) -> Maybe x -> Type where
  Nah : All pred Nothing
  Aye : {v : x} -> pred v -> All pred (Just v)

public export
MaybeAll : Container -> Container
MaybeAll c = MkCont (Maybe c.shp)
                    (All c.pos)

public export
maybeAUnit : MaybeAll CUnit =%> CUnit
maybeAUnit = (const ()) <!
    (\case Nothing => \x => Nah
           (Just x) => const (Aye x))

public export
distribMaybeAF : Monad m => m • (MaybeAll a) =%> MaybeAll (m • a)
distribMaybeAF = id <!
    (\case Nothing => \x => pure Nah
           (Just x) => \(Aye y) => map Aye y)

public export
distriMaybeAnyF : Monad m => m • (MaybeAny a) =%> MaybeAny (m • a)
distriMaybeAnyF = id <!
    (\case Nothing => \x => absurd x
           (Just x) => map Yep . extract)



public export
map_MaybeAll : a =%> b -> MaybeAll a =%> MaybeAll b
map_MaybeAll m =
    (map m.fwd) <!
    (\case Nothing => \x => Nah
           (Just x) => \(Aye y) => Aye $ m.bwd x y)

toIdrisBwd : (v : MaybeType x.shp) -> All x.pos (toIdris v) -> (val : isTrue v.ex1) -> x.pos (v.ex2 val)
toIdrisBwd (MkEx False v2) x val = absurd val
toIdrisBwd (MkEx True v2) (Aye x) () = x

-- Isomorphism MaybeC #> x ~ MaybeAll x
MaybeToAll : MaybeHandle x =%> MaybeAll x
MaybeToAll = toIdris <! toIdrisBwd

fromIdrisBwd : (v : Maybe x.shp) -> ((val : isTrue ((fromIdris v).ex1)) -> x.pos ((fromIdris v).ex2 val)) -> All x.pos v
fromIdrisBwd Nothing f = Nah
fromIdrisBwd (Just y) f = Aye (f ())

AllToMaybe : MaybeAll x =%> MaybeHandle x
AllToMaybe =
    fromIdris <!
    fromIdrisBwd

MaybeAllMaybeId : {x : Container} ->
                    DepLensEq (MaybeToAll {x} ⨾ AllToMaybe {x})
                              (identity {a = MaybeHandle x})
MaybeAllMaybeId = MkDepLensEq
    fwdEq
    bwdEq
    where
      fwdEq : (v : MaybeType x.shp) -> fromIdris (toIdris v) = v
      fwdEq (MkEx True p) = cong (MkEx True) (funExt $ \() => Refl)
      fwdEq (MkEx False p) = cong (MkEx False) (allUninhabited _ _)

      0 bwdEq : (v : MaybeType x.shp) ->
              (y : ((val : isTrue ((fromIdris (toIdris v)).ex1)) ->
                   x.pos ((fromIdris (toIdris v)).ex2 val))) ->
          let 0 p1 : (MaybeHandle x).pos v
              p1 = (MaybeToAll {x} ⨾ AllToMaybe {x}).bwd v y
              0 p2 : (MaybeHandle x).pos v
              p2 = (identity {a = MaybeHandle x}).bwd v (replace {p = (MaybeHandle x).pos} (fwdEq v) y)
          in p1 === p2
      bwdEq (MkEx True v) y = funExtDep $ \() => Refl
      bwdEq (MkEx False v) y = funExtDep $ \x => void x

AllMaybeAllId : {x : Container} ->
                    DepLensEq (AllToMaybe {x} ⨾ MaybeToAll {x})
                              (identity {a = MaybeAll x})
AllMaybeAllId = MkDepLensEq
    fwdEq
    bwdEq
    where
      fwdEq : (v : Maybe (x .shp)) -> toIdris (fromIdris v) = v
      fwdEq Nothing = Refl
      fwdEq (Just y) = Refl
      bwdEq : (v : Maybe (x .shp)) ->
              (y : All (x .pos) (toIdris (fromIdris v))) ->
              let 0 p1 : (MaybeAll x).pos v
                  p1 = (AllToMaybe {x} ⨾ MaybeToAll {x}).bwd v y
                  0 p2 : (MaybeAll x).pos v
                  p2 = (identity {a = MaybeAll x}).bwd v (replace {p = (MaybeAll x).pos} (fwdEq v) y)
              in p1 === p2
      bwdEq Nothing Nah = Refl
      bwdEq (Just z) (Aye y) = Refl

AllMaybeIso : {x : Container} -> ContIso (MaybeAll x) (MaybeHandle x)
AllMaybeIso = MkGenIso
    AllToMaybe
    MaybeToAll
    AllMaybeAllId
    MaybeAllMaybeId

-- Isomorphism MaybeAll x ~ CUnit + x
MaybeAllToCUnitBwd : (x : Maybe (c .shp)) ->
                      choice (\x => ()) (c .pos) (maybeToSum x) -> All (c .pos) x
MaybeAllToCUnitBwd Nothing y = Nah
MaybeAllToCUnitBwd (Just x) y = Aye y

MaybeAllToCUnit : MaybeAll c =%> CUnit + c
MaybeAllToCUnit =
    maybeToSum <!
    MaybeAllToCUnitBwd

CUnitToMaybeAllBwd : (x : () + c .shp) ->
                     All c.pos (sumToMaybe x) ->
                     choice (\x => ()) c.pos x
CUnitToMaybeAllBwd (<+ x) y = ()
CUnitToMaybeAllBwd (+> x) (Aye y) = y

public export
CUnitToMaybeAll : CUnit + c =%> MaybeAll c
CUnitToMaybeAll =
    sumToMaybe <!
    CUnitToMaybeAllBwd

%unbound_implicits off
CUnitMaybeAllCUnitBwd : forall c.
    (v : () + c.shp) ->
    (y : choice (\x => ()) c.pos (maybeToSum (sumToMaybe v))) ->
    CUnitToMaybeAllBwd {c=c} v (MaybeAllToCUnitBwd {c} (sumToMaybe {a = c.shp} v) y) ===
      (replace {p = choice (\_ => ()) c.pos} (sumMaybeSumId v) y)

CUnitMaybeAllCUnit : forall c. CUnitToMaybeAll {c}  ⨾ MaybeAllToCUnit {c} `DepLensEq`
                               identity {a = CUnit + c}
CUnitMaybeAllCUnit = MkDepLensEq
    sumMaybeSumId
    CUnitMaybeAllCUnitBwd

MaybeAllCUnitMaybeAllBwd : forall c.
    (v : Maybe c.shp) ->
    (y : All c.pos (sumToMaybe (maybeToSum v))) ->
    MaybeAllToCUnitBwd {c} v (CUnitToMaybeAllBwd {c} (maybeToSum v) y) =
      (replace {p = All c.pos} (maybeSumMaybeId v) y)
MaybeAllCUnitMaybeAllBwd Nothing Nah = Refl
MaybeAllCUnitMaybeAllBwd (Just x) (Aye y) = Refl

MaybeAllCUnitMaybeAll : forall c. MaybeAllToCUnit {c} ⨾ CUnitToMaybeAll {c} `DepLensEq`
                                  identity {a = MaybeAll c}
MaybeAllCUnitMaybeAll = MkDepLensEq
    maybeSumMaybeId
    MaybeAllCUnitMaybeAllBwd

MaybeAllCUnitIso : forall c. ContIso (MaybeAll c) (CUnit + c)
MaybeAllCUnitIso = MkGenIso
    MaybeAllToCUnit
    CUnitToMaybeAll
    MaybeAllCUnitMaybeAll
    CUnitMaybeAllCUnit

{-
public export
handleErr : forall a. MaybeAny a =%> MaybeAll a
handleErr = id <!
    (\case Nothing => \case Nah => ?as_0
           (Just x) => \case _ => ?handleErr_rhs_1)

AnyToAllTypes : forall a. (x : Maybe a.shp) -> All a.pos x -> Any a.pos x
AnyToAllTypes Nothing Nah = ?AnyToAllTypes_rhs_2
AnyToAllTypes (Just x) y = ?AnyToAllTypes_rhs_1

AllToAnyTypes : forall a. (x : Maybe (a .shp)) -> Any (a .pos) x -> All (a .pos) x
AllToAnyTypes Nothing (Yep x) impossible
AllToAnyTypes (Just x) (Yep y) = Aye y

AnyToAll : forall a. MaybeAll a =%> MaybeAny a
AnyToAll = id <! AllToAnyTypes

