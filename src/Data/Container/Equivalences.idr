module Data.Container.Equivalences

import Data.Container
import Data.Container.W
import Data.Container.Descriptions as Desc
import Data.Fin
import Data.Product
import Relation.Isomorphism
import Proofs.Extensionality
import Proofs.Void
import Proofs.Sigma
import Control.Relation
import Data.Sigma


||| List as descriptions and lists from Prelude as isomorphic
ListContIsoList : TyList a ~= Prelude.List a
ListContIsoList = MkIso
  { from = fromDesc
  , to = toDesc
  , fromTo = fromToPrf
  , toFrom = toFromPrf
  }
  where
    fromDesc : TyList a -> Prelude.List a
    fromDesc (MkEx 0 f) = Nil
    fromDesc (MkEx (S k) f) = f FZ :: fromDesc (MkEx k (f . FS))

    toDesc : Prelude.List a -> TyList a
    toDesc [] = MkEx 0 absurd
    toDesc (x :: xs) = let v = toDesc xs in MkEx (S v.ex1) (\case FZ => x ; (FS y) => v.ex2 y)

    fromToPrf : {x : Prelude.List a} -> fromDesc (toDesc x) = x
    fromToPrf {x = []} = Refl
    fromToPrf {x = (x :: xs)} =
      let ppp = fromToPrf {x = xs} in
          cong (x :: ) (trans (cong (fromDesc) $ exUniq (toDesc xs)) ppp)

    0 toFromPrf : {x : TyList a} -> toDesc (fromDesc x) = x
    toFromPrf {x = (MkEx 0 f)} = cong (MkEx Z) (allUninhabited absurd f)
    toFromPrf {x = (MkEx (S k) f)} = let
      rec = toFromPrf {x = MkEx k (f . FS)}
      in rewrite rec in cong (MkEx (S k)) (funExt (\case FZ => Refl ; (FS x) => Refl))
fromDesc : (x : Type) -> TyList x -> W.List x
fromDesc a (MkEx Z idx) = Sup (MkEx (False ## ()) absurd)
fromDesc a (MkEx (S len) idx) = Sup (MkEx (True ## idx FZ) (\_ => fromDesc a (MkEx len (idx . FS))))

toDesc : (a : Type) -> W.List a -> TyList a
toDesc a (Sup (MkEx (False ## b) rec)) = Nil
toDesc a (Sup (MkEx (True ## b) xs)) =
  let rec = toDesc a (xs ()) in (MkEx (S rec.ex1) (\case FZ => b ; (FS n) => rec.ex2 n))

WListIsoListCont : {a : Type} -> TyList a ~= W.List a
WListIsoListCont {a} = MkIso
  { from = fromDesc a
  , to = toDesc a
  , fromTo = fromToPrf a
  , toFrom = toFromPrf
  }
  where


    0 fromToPrf : (a : Type) -> {v : W.List a} -> fromDesc a (toDesc a v) = v

    -- 0 help2 : (a : Type) ->
    --           (cb : Bool) ->
    --           (cc : (if cb then a && () else () && Void).fst) ->
    --           (c2 : (if c1 .π1 then a && () else () && Void).snd ->
    --                 W (x : Σ Bool (\x => (if x then a && () else () && Void) .fst) !>
    --                        ((if x.π1 then a && () else () && Void) .snd))) ->
    --           fromDesc a (toDesc a (Sup (MkEx c1 c2))) === Sup (MkEx c1 c2)

    0 help : (a : Type) -> (c : _) -> fromDesc a (toDesc a (Sup c)) = Sup c
    help a (MkEx c1 c2) = ?aaaa

    fromToPrf a {v = (Sup c)} = help a c

    0 lemma : {a : Type} -> {r : Nat} ->  (rec : Fin r -> a) -> (s : Fin (S r) -> a) -> rec = (\arg => s (FS arg)) ->
              (\lcase : Fin (S r) => case lcase of { FZ => s FZ ; FS n =>  DPair.(.snd) (MkDPair r {p= \arg => Fin arg -> a} rec) n }) = s
    lemma rec s prf = funExt (\k => case k of
                                         FZ => Refl
                                         FS n => applySame n prf)

    0 toFromPrf : {a : Type} -> {x : TyList a} -> toDesc a (fromDesc a x) = x
    toFromPrf {x = (MkEx 0 z)} = cong (MkEx Z) (allUninhabited absurd z)
    toFromPrf {a} {x = MkEx (S k) s} with
      (toDesc a (fromDesc a (MkEx k (\x => s (FS x))))) proof p
      _ | (MkEx r rec) =
        let y = toFromPrf {x = MkEx k (s . FS)} in
        let y' = toFromPrf {x = MkEx r rec} in
        let py  = sym p `trans` y in
        -- let 0 (n, p2)  = dpair_inj {x=r} {y=k} py in
        -- let l = lemma {a} {r} rec (rewrite n in s) ?arg in
        -- let l' = cong2 MkDPair (cong S n)  l in
            ?sl'

{-

Equal {a = (arg : Nat ** Fin (S r) -> a)} {b = (arg : Nat ** Fin (S r) -> a)}
Equal {a = (x   : Nat ** Fin x     -> a)} {b = (x   : Nat ** Fin x     -> a)}
  (MkDPair {a = Nat} {p = \arg     => Fin (S r) -> a} (S r) (\lcase : Fin (S r) =>
  (MkDPair {a = Nat} {p = \x : Nat => Fin x     -> a} (S r) (\lcase : Fin (S r) =>
                case lcase of { FZ {k = r} => s (FZ {k = r})
                case lcase of { FZ {k = .fst {a = Nat} {p = \x : Nat => Fin x -> a} rec} => s (FZ {k})
                              ; FS {k = r} n =>
                              ; FS {k = .fst {a = Nat} {p = \x : Nat => Fin x -> a} rec} n =>
                                        .snd {a = Nat} {p = ?p} (MkDPair {a = Nat} {p = ?p} r rec) n }))
                                        .snd {a = Nat} {p = ?p} (MkDPair {a = Nat} {p = \x : Nat => Fin x -> a} r rec) n }))
  (MkDPair {a = Nat} {p = \arg     => Fin (S r) -> a} (S k) s)
  (MkDPair {a = Nat} {p = \x : Nat => Fin x     -> a} (S k) s)
 ------------------------------
Equal {a = (x   : Nat ** Fin x     -> a)} {b = (x   : Nat ** Fin x     -> a)}
  (MkDPair {a = Nat} {p = \x : Nat => Fin x    -> a} (S r) (\lcase : Fin (S r) =>
                case lcase of { FZ {k = .fst {a = Nat} {p = \x : Nat => Fin x -> a} rec} => s (FZ {k})
                              ; FS {k = .fst {a = Nat} {p = \x : Nat => Fin x -> a} rec} n =>
                                        .snd {a = Nat} {p = ?p} (MkDPair {a = Nat} {p = \x : Nat => Fin x -> a} r rec) n }))
 (MkDPair {a = Nat} {p = \x : Nat => Fin x -> a} (S k) s)



WListIsoList : {a : _} -> Prelude.List a ~= W.List a
-- WListIsoList = transitive (symmetric ListContIsoList) WListIsoListCont
