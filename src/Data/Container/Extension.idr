module Data.Container.Extension

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Morphism.Definition

import Data.Category.Bifunctor
import Data.Category.Set
import Data.Category.FunctorCat

import Proofs

||| Extension of a container as a functor, Also interpreted as "existential" monads
public export
record Ex (cont : Container) (ty : Type) where
  constructor MkEx
  ex1 : cont.shp
  ex2 : cont.pos ex1 -> ty

%pair Ex ex1 ex2

public export
record ExEq {a : Container} {b : Type} (c1, c2 : Ex a b) where
  constructor MkExEq
  pex1 : c1.ex1 === c2.ex1
  pex2 : (v : a.pos c1.ex1) ->
         let 0 b1, b2 : b
             b1 = c1.ex2 v
             b2 = c2.ex2 (rewrite__impl a.pos (sym pex1) v)
          in b1 === b2

public export
0 exEqToEq : ExEq c1 c2 -> c1 === c2
exEqToEq {c1 = MkEx x1 x2} {c2 = MkEx y1 y2} (MkExEq x y) =
  cong2Dep' MkEx x (funExt $ y)

public export
ExProj1 :
   (x : Container) ->
   (y : Type) ->
   (v2 : Ex x y) ->
   (v2.ex1 === v2.ex1)
ExProj1 x y v2 = Refl

ExProj2 :
   (x : Container) ->
   (y : Type) ->
   (v2 : Ex x y) ->
   (a : x.pos v2.ex1) ->
   (v2.ex2 a === v2.ex2 a)
ExProj2 x y v2 a = Refl

export
inj1 : MkEx v1 v2 === MkEx v1' v2' -> v1 === v1'
inj1 Refl = Refl

export
inj2 : {0 c : Container} -> {0 t : Type} ->
       {0 v1 : c.shp} -> {0 v2 : c.pos v1 -> t} ->
       {0 v2' : c.pos v1 -> t} ->
       (v : MkEx {cont = c} v1 v2 === MkEx {cont = c} v1 v2') -> v2 === v2'
inj2 Refl = Refl

export
exUniq : (x : Ex a b) -> MkEx x.ex1 x.ex2 = x
exUniq (MkEx ex1 ex2) = Refl


public export
exFunc :
    {0 a, b : Type} -> {0 c : Container} ->
    (a -> b) -> Ex c a -> Ex c b
exFunc f x = MkEx x.ex1 (f . x.ex2)

||| F is a functor indeed
export
{0 c : Container} -> Functor (Ex c) where
  map f x = exFunc f x

export
exMap : {0 a, a' : Container} -> {0 b : Type} ->
        (f : a.shp -> a'.shp) ->
        ((x : a.shp) -> a'.pos (f x) -> a.pos x) ->
        Ex a b -> Ex a' b
exMap fwd bwd y = MkEx (fwd y.ex1) (\z => y.ex2 (bwd y.ex1 z))

exMap2 : {0 a, a' : Container} -> {0 b, c : Type} ->
         (f : a.shp -> a'.shp) ->
         ((x : a.shp) -> a'.pos (f x) -> a.pos x) ->
         (b -> c) ->
         Ex a b -> Ex a' c
exMap2 fwd bwd f x = map f (exMap fwd bwd x)


public export
exMap' : a =%> a' -> Ex a b -> Ex a' b
exMap' x y = MkEx (x.fwd y.ex1) (\z => y.ex2 (x.bwd y.ex1 z))

public export
exBimap : a =%> a' -> (b -> b') -> Ex a b -> Ex a' b'
exBimap f g x = exFunc g (exMap' f x)

ExBifunctor : Bifunctor Cont Set Set
ExBifunctor = MkFunctor
    (uncurry Ex)
    (\x, y, m => exBimap m.π1 m.π2)
    (\v => funExt $ \z => exEqToEq $ MkExEq Refl (\_ => Refl))
    (\x, y, z, f, g => funExt $ \z =>
        exEqToEq $ MkExEq Refl (\_ => Refl))

ExFunctorSet : Functor Cont (FunctorCat Set Set)
ExFunctorSet = uncurryFunctor ExBifunctor

