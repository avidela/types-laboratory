module Data.Container.Morphism

import Data.Container.Definition
import Data.Container.Extension
import Data.Container.Sequence
import Data.Container.Lift
import Data.Container.Coproduct
import Data.Container.SeqAll
import public Data.Container.Morphism.Definition
import public Data.Container.Morphism.Eq

import Data.Coproduct

import Proofs

import Interface.Comonad

import Data.List1

public export
getbwd : (lens : a =%> b) -> (x : a.shp) -> b.pos (lens.fwd x) -> a.pos x
getbwd lens = lens.bwd

public export
embed : ((x : a) -> b x) -> MkCont a b =%> CUnit
embed f = (\_ => ()) <! (\x, _ => f x)


public export
extract : a =%> CUnit -> (x : a.shp) -> a.pos x
extract y x = y.bwd x ()

0 toCont : a =%> b -> Container
toCont _ = (f : a.shp -> b.shp) !> ((x : a.shp) -> (y : b.pos (f x)) -> a.pos x)


comJoin : Comonad f => f (f a) -> f a
comJoin x = map extract x


distribCirc : Comonad f => f • (a ○ b) =%> (f • a) ○ (f • b)
distribCirc = (\x => MkEx x.ex1 (x.ex2 . extract )) <!
              (\(MkEx x1 x2), (y1 ## y2) => map (\vx => extract y1 ## vx) y2)

failing "Can't solve constraint between: pure v1 and map .π1 y"

  distribCirc2 : Monad f => (f • a) ○ (f • b) =%> f • (a ○ b) -- here
  distribCirc2 = (\(MkEx x1 x2) => MkEx x1 (\vx => x2 (pure vx))) <!
                 (\(MkEx x1 x2), y => map π1 y ## map (\(v1 ## v2) => v2) y)

-- look at orchard papers on coeffect systems
costronk : Monad f => f • (a ○ b) =%> a ○ (f • b)
costronk = id <! (\(MkEx x1 x2), (y1 ## y2) => map (\vx => y1 ## vx) y2)

public export
State : Container -> Type
State = (CUnit =%>)

public export
state : a.shp -> State a
state x = (const x) <! (\_, _ => ())

public export
Costate : Container -> Type
Costate = (=%> CUnit)

public export
costate : ((x : a.shp) -> a.pos x) -> Costate a
costate f = (const ()) <! (\x, _ => f x)

public export
runCostate : Costate a -> (x : a.shp) -> a.pos x
runCostate c x = c.bwd x ()

public export
(.getVal) : State a -> a.shp
x.getVal = x.fwd ()

public export
run : (x : State a) -> Costate a -> a.pos x.getVal
run x y = extract y x.getVal

public export
runM : (x : State a) -> Costate (m • a) -> m (a.pos x.getVal)
runM x y = extract y x.getVal

export
compUnit : CUnit ○ CUnit =%> CUnit
compUnit = (const ()) <! (\_, _ => () ## ())

export
compLUnit : CUnit ○ c =%> c
compLUnit = (\x => x.ex2 ()) <! (\x, y => () ## y)

export
compRUnit : c ○ CUnit =%> c
compRUnit = (\x => x.ex1) <! (\x, y => y ## ())

export
comp2Unit : a =%> CUnit -> b =%> CUnit -> a ○ b =%> CUnit
comp2Unit m1 m2 = m1 ~○~ m2 ⨾ compUnit

export
counit : Monad m => m • a =%> a
counit = id <! (\x, y => pure y)

export
comult : Monad m => m • a =%> m • (m • a)
comult = id <! (\x, y => join y)

public export
distrib_plus : f • (a + b) =%> (f • a) + (f • b)
distrib_plus = id <! (\case (<+ x) => id ; (+> x) => id)

public export
seq2M : Monad m => Costate (m • a) -> Costate (m • b) -> Costate (m • (a ○ b))
seq2M x y = costate
    (\xn => do v1 <- extract x xn.ex1
               v2 <- extract y (xn.ex2 v1)
               pure (v1 ## v2))

public export
distrib_plus_3 : f • (a + b + c) =%> (f • a) + (f • b) + (f • c)
distrib_plus_3 = id <! (\case (<+ (<+ x)) => id
                                    ; (<+ (+> x)) => id
                                    ; (+> x) => id)

public export
runSequenceM3 : Monad m =>
                (inp : (a ○ b ○ c).req) ->
                m • a =%> CUnit -> m • b =%> CUnit -> m • c =%> CUnit ->
                m ((a ○ b ○ c).res inp)
runSequenceM3 inp x y z =
  let xn : Costate (m • (a ○ b))
      xn = seq2M {m} x y
      yn : Costate (m • ((a ○ b) ○ c))
      yn = seq2M {m, a = a ○ b, b = c} xn z
      rr := run {a = m • (a ○ b ○ c)} (state inp) yn
  in rr

public export
univLUnit : CUnit #> c =%> c
univLUnit =
    (\x => x.ex2 ()) <!
    (\x, y, () => y)


-- runSequenceM3 ((in1 ## cont) ## cont2) c1 c2 c3
--     = let f = Morphism.extract c1
--         ; g = extract c2
--         ; h = extract c3
--     in do res1 <- f in1
--           res2 <- g (cont res1)
--           res3 <- h (cont2 (res1 ## res2))
--           pure ((res1 ## res2) ## res3)

{-
-- The hole has context:
--    x : a .shp
--    y1 : b .shp
--    y2 : f (b .pos y1) -> f (a .pos x)
-- ------------------------------
-- bb : f (Σ (b .shp) (\y => b .pos y -> a .pos x))
--
-- But we cannot fill it because we cannot obtain a value `b.pos y1` to call on y2
appMNope : Comonad f => Lift f (hom a b) =%> hom (Lift f a) (Lift f b)
appMNope = id <! (\x, (y1 ## y2) => (?bb))

-- The hole has context:
--    x : a .shp
--    y : f (Σ (b .shp) (\y => b .pos y -> a .pos x))
-- ------------------------------
-- bimpossible : Σ (b .shp) (\y => f (b .pos y) -> f (a .pos x))
--
-- But we cannot fill it because we cannot extract a value `b.shp` from `y`
appMNopeAgain : Monad f => hom (Lift f a) (Lift f b) =%> Lift f (hom a b)
appMNopeAgain = id <! (\x, y => ?bimpossible)
-}


