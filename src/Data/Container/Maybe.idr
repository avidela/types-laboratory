module Data.Container.Maybe

import Data.Container.Category
import Data.Container
import Data.Container.Descriptions
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation
import Data.Sigma
import Data.Coproduct
import Data.Iso

import Proofs

%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

MaybeFunctor : Functor Cont Cont
MaybeFunctor = applyBifunctor {a = Cont, b = Cont} MaybeCont ComposeContBifunctor

OnePlusFunctor : Functor Cont Cont
OnePlusFunctor = applyBifunctor {a = Cont, b = Cont} CNeutral CoprodContBifunctor

maybeCoprod : MaybeType x -> () + x
maybeCoprod (MkEx False p2) = <+ ()
maybeCoprod (MkEx True p2) = +> p2 ()

coprodMaybe : () + x -> MaybeType x
coprodMaybe (<+ y) = Nothing
coprodMaybe (+> y) = Just y

coMayInverse : (vx : _) -> (coprodMaybe (maybeCoprod vx)) === vx
coMayInverse (MkEx False p) = cong (MkEx False) (allVoid _ _)
coMayInverse (MkEx True p) = cong (MkEx True) (funExt $ \_ => cong p (unitUniq _))

public export
conv1 : (x : Container) -> MaybeCont ○ x =%> CNeutral + x
conv1 x =
    (maybeCoprod {x = x.shp}) <!
    (\case (MkEx False y) => absurd
           (MkEx True y) => \z => () ## z)

conv2 : (x : Container) -> CNeutral + x =%> MaybeCont ○ x
conv2 x =
  (coprodMaybe {x=x.shp}) <!
  (\case (<+ x) => absurd . π1
         (+> x) => π2)

theThing : {0 a, b : Container} -> (m : a =%> b) -> (v : Ex MaybeCont a.shp) ->
           (conv1 a ⨾ mapHom OnePlusFunctor _ _ m).fwd v
           ===
           (mapHom MaybeFunctor a b m ⨾ conv1 b).fwd v
theThing m (MkEx False ex3) = Refl
theThing m (MkEx True ex2) = Refl

theThingBwd : {0 a, b : Container} -> (m : a =%> b) ->
              (v : Ex MaybeCont a.shp) ->
              (y : (CNeutral + b).pos ((conv1 a ⨾ mapHom OnePlusFunctor _ _ m).fwd v)) ->
              let 0 p1, p2 : (MaybeCont ○ a).pos v
                  p1 = (conv1 a ⨾ mapHom OnePlusFunctor _ _ m).bwd v y
                  p2 = (mapHom MaybeFunctor a b m ⨾ conv1 b).bwd v
                           (replace {p = (CNeutral + b).pos} (theThing m v) y)
              in p1 === p2
theThingBwd m (MkEx False ex2) y = absurd y
theThingBwd m (MkEx True ex2) y = Refl

onePlusSquare : (m : a =%> b) ->
    conv1 a ⨾ mapHom OnePlusFunctor _ _ m
    `DepLensEq`
    mapHom MaybeFunctor a b m ⨾ conv1 b
onePlusSquare m = MkDepLensEq (theThing m) (theThingBwd m)

onePlusMaybe : MaybeFunctor =>> OnePlusFunctor
onePlusMaybe = MkNT
   conv1
   (\a, b, m => depLensEqToEq (onePlusSquare m))

%unbound_implicits off

0 unitEq : {0 a : Type} -> {0 f : Unit -> a} -> (\x : Unit => f x) === (\_ : Unit=> f ())
unitEq = funExt $ \() => Refl

-- The natural isomorphism between 1 + x and Maybe ○ x in the category of containers
OnePlusMaybeIso : MaybeFunctor =~= OnePlusFunctor
OnePlusMaybeIso = MkNaturalIsomorphism
  onePlusMaybe
  conv2
  conv21
  conv12
  where
    0 conv12 : (x : Container) -> ((conv2 x) ⨾ (conv1 x)) === identity (CNeutral + x)
    conv12 x = cong2Dep' (<!)
        (funExtDep $ \case (<+ _) => cong (<+) (unitUniq _)
                           (+> _) => Refl )
        (funExtDep $ \case (<+ _) => funExt $ \x => void x
                           (+> _) => Refl)

    0 conv21Bwd : {c : Container} -> (vx : MaybeType c.shp) ->
                  (wx : (MaybeCont ○ c).pos (coprodMaybe (maybeCoprod vx))) ->
                  let b1 : pos (CNeutral + c) (maybeCoprod vx) -> (MaybeCont ○ c).pos vx
                      b1 = bwd (conv1 c) vx
                      b2 : pos (CNeutral + c) (maybeCoprod vx)
                      b2 = bwd (conv2 c) ((conv1 c).fwd vx) wx
                      b3 : (MaybeCont ○ c).pos vx
                      b3 = replace {p = \x => (MaybeCont ○ c).pos x} (coMayInverse vx) wx
                  in b1 b2 === b3
    conv21Bwd (MkEx False p) wx = absurd wx.π1
    conv21Bwd (MkEx True p) (() ## p2) =
      -- can't deal with this other than with a rewrite
        rewrite unitEq {f = p}
        in cong (() ##) Refl

    0 conv21 : (c : Container) -> conv1 c ⨾ conv2 c ≡ identity (MaybeCont ○ c)
    conv21 c = depLensEqToEq $ MkDepLensEq
        ?ad
        ?conv21_rhs

