module Data.Container.Action

import Data.Container.SeqAll
import Data.Container.Tensor
import Data.Container.Definition
import Data.Container.Morphism.Definition
import Data.Container.Cartesian
import Data.Container.Sequence

---------------------------------------------------------------------------------------------
||| #> is an action on Cont with ○ as a monoidal category on Cont#
---------------------------------------------------------------------------------------------


-- Morphism from ((c1 ⊗ c2) ||> c2) to (c1 ||> (c2 ||> c3))
-- The other way around does not hold.
checkToMorphism :
    (c1, c2, c3 : Container) ->
    ((c1 ⊗ c2) #> c3) =%> (c1 #> (c2 #> c3))
checkToMorphism (a !> ap) (b !> bp) (c !> cp) = MkMorphism
   (\v => MkEx v.ex1.π1 (\y => (MkEx v.ex1.π2 (curry v.ex2 y))))
   (\(MkEx (v1 && v2) v'), w, (z1 && z2) => w z1 z2)

contNatPrf : (a, b, c : Container) -> x #> (y #> z) =%> (x ○ y) #> z
contNatPrf a b c = MkMorphism
  (\v => MkEx (MkEx v.ex1 (ex1 . v.ex2)) (\k => (v.ex2 k.π1).ex2 k.π2))
  (\x, x', y, z => x' (y ## z))

%ambiguity_depth 10

combineMor : (x, x' : Container) -> (m1 : x =#> x') ->
             (y, y' : Container) -> (m2 : y =#> y') ->
             (z, z' : Container) -> (m3 : z =%> z') ->
             x #> (y #> z) =%> x' #> (y' #> z')
combineMor x x' m1 y y' m2 z z' m3 = MkMorphism
    (\a => MkEx (m1.fwd a.ex1) (\v => let
        iso1 : Iso (x'.pos (m1.fwd a.ex1)) (x.pos a.ex1)
        iso1 = m1.bwd a.ex1
        yshp : Ex y z.shp
        yshp = (a.ex2 (iso1.to v))
     in MkEx (m2.fwd yshp.ex1) (\xx => m3.fwd ((a.ex2 (iso1.to v)).ex2 ((m2.bwd yshp.ex1).to xx)))))
    (\a, b, c, d => m3.bwd ((a.ex2 c).ex2 d)
      (let
        0 prf : ((m1 .bwd (a.ex1)).to ((m1.bwd a.ex1).from c)) === c
        prf = (m1.bwd a.ex1).toFrom c
        bee := b
            ((m1.bwd a.ex1).from c)
            (rewrite prf in (m2.bwd (a.ex2 c).ex1).from d)
        0 prf2 : (rewrite sym prf in (m2.bwd ((a.ex2 ((m1.bwd (a.ex1)).to ((m1.bwd a.ex1).from c))).ex1)).to
            (rewrite prf in (m2 .bwd ((a.ex2 c).ex1)).from d)) === d
        prf2 = rewrite prf in (m2.bwd (a.ex2 c).ex1).toFrom d
        in rewrite sym prf
        in rewrite sym prf2
        in bee))

%ambiguity_depth 10
combineMor' : (x, x' : Container) -> (m1 : x =#> x') ->
              (y, y' : Container) -> (m2 : y =#> y') ->
              (z, z' : Container) -> (m3 : z =%> z') ->
              (x ○ y) #> z =%> (x' ○ y') #> z'
combineMor' x x' m1 y y' m2 z z' m3 =
  MkMorphism fwd bwd
  where
    fwd : ((x ○ y) #> z).shp -> ((x' ○ y') #> z').shp
    fwd x = MkEx (MkEx (m1.fwd x.ex1.ex1)
         (\vx => m2.fwd (x.ex1.ex2 $ (m1.bwd x.ex1.ex1).to vx)))
         (\vx => m3.fwd $ x.ex2 $ (m1.bwd x.ex1.ex1).to vx.π1
                ## (m2.bwd $ x.ex1.ex2 $ (m1.bwd x.ex1.ex1).to vx.π1).to vx.π2)

    bwd : (v : ((x ○ y) #> z).shp) ->
          ((x' ○ y') #> z').pos (fwd v) ->
          ((x ○ y) #> z).pos v
    bwd (MkEx (MkEx p1 p2) a2) f (v1 ## v2) =
      let
          0 tb = (m1.bwd p1).toFrom v1
          0 tq = (m2.bwd (p2 v1)).toFrom v2
          arr : y' .pos (m2 .fwd (p2 ((m1 .bwd p1) .to ((m1 .bwd p1) .from v1))))
          arr = replace {p = \arg => y' .pos (m2 .fwd (p2 arg))}
                   (sym tb) ((m2.bwd (p2 v1)).from v2)
          fn = f ((m1.bwd p1).from v1 ## arr)
          leftSide : y .pos (p2 ((m1 .bwd p1) .to ((m1 .bwd p1) .from v1)))
          leftSide = to (m2 .bwd (p2 ((m1 .bwd p1) .to ((m1 .bwd p1) .from v1)))) arr
          rightSide : y .pos (p2 ((m1 .bwd p1) .to ((m1 .bwd p1) .from v1)))
          rightSide = replace {p = \arg => y.pos (p2 arg)} (sym tb) v2
          0 prf2 : leftSide === rightSide
          prf2 = rewrite tb in tq

       in m3.bwd (a2 (v1 ## v2))
           (replace {p = \arg => z' .pos (m3 .fwd (a2 arg))} (cong2Dep' (##) tb prf2) fn)

contNatPrfOp : (a, b, c : Container) -> (x ○ y) #> z =%> x #> (y #> z)
contNatPrfOp a b c = MkMorphism
  (\v => MkEx v.ex1.ex1 (\l => MkEx (v.ex1.ex2 l) (\j => v.ex2 (l ## j))))
  (\aa, bb, (c1 ## c2) => bb c1 c2 )


