module Data.Container.Definition

import Data.Boundary
import Data.Category.Ops
import Data.Sigma

||| A container is a product of a shape and a set of positions indexed by that shape
||| They can be used to describe data types
public export
record Container where
  constructor (!>)
  ||| Shapes
  shp : Type
  ||| Positions
  pos : shp -> Type

public export
(.req) : Container -> Type
(.req) = shp

public export
(.res) : (c : Container) -> c.req -> Type
(.res) = pos

public export
(.question) : Container -> Type
(.question) = shp

public export
(.answer) : (c : Container) -> c.req -> Type
(.answer) = pos

%pair Container shp pos

public export
MkCont : (s : Type) -> (s -> Type) -> Container
MkCont = (!>)

public export
RC' : Container -> Boundary
RC' cont = MkB cont.shp ((x : cont.shp) -> cont.pos x)

public export
RC : Container -> Container
RC cont = (x : cont.shp) !> ((x' : cont.shp) -> cont.pos x')

public export
Const2 : Type -> Type -> Container
Const2 ty sy = (x : ty) !> sy

public export
(:-) : Type -> Type -> Container
(:-) ty sy = (x : ty) !> sy

public export
fromB : Boundary -> Container
fromB m = Const2 m.proj1 m.proj2

||| The constant container
public export
Const : Type -> Container
Const ty = Const2 ty ty

||| The unit for ⊗
public export
CUnit : Container
CUnit = Const Unit

||| The empty container
public export
CVoid : Container
CVoid = Const Void

||| The neutral for composition
public export
CNeutral : Container
CNeutral = Const2 Unit Void


||| Forall-monads
public export
Fa : (cont : Container) -> Type -> Type
Fa cont ty = (x : cont.shp) -> cont.pos x -> ty

-- internal hom operator
public export
hom : Container -> Container -> Container
hom c1 c2 =
  (x : c1.shp) !>
  (Σ c2.shp (\y => c2.pos y -> c1.pos x))

public export
hom2 : Container -> Container -> Container
hom2 c1 c2 =
  (fwd : c1.shp -> c2.shp) !>
  ((x : c1.shp) -> c2.pos (fwd x) -> c1.pos x)

-- what is this?
moh : Container -> Container -> Container
moh x y =
  (f : x.shp -> y.shp) !>
  ((z : x.shp) -> y.pos (f z) -> x.pos z)

