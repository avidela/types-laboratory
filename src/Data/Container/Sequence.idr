module Data.Container.Sequence

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Cartesian
import Data.Container.Cartesian.Category
import Data.Container.Extension
import Data.Container.Morphism.Definition
import Data.Container.Morphism.Eq

import Data.Sigma
import Data.Iso

import Proofs

import Data.Category.Bifunctor

-- composition of containers
public export
(○) : Container -> Container -> Container
(○) c1 c2 =
  (x : Ex c1 c2.shp) !>
  (Σ (c1.pos x.ex1) (c2.pos . x.ex2))

-- General things about units
public export
composeUnits : CUnit ○ CUnit =%> CUnit
composeUnits = (const MkUnit) <! (const (const (MkUnit ## MkUnit)))

public export
assoc1 : a ○ (b ○ c) =%> (a ○ b) ○ c

public export
assoc2 : (a ○ b) ○ c =%> a ○ (b ○ c)
assoc2 = (\(MkEx x y) => MkEx x.ex1 (\w => MkEx (x.ex2 w) (\z => y (w ## z))))
    <! \(MkEx x y), (z ## w ## q) => (z ## w) ## q

--------------------------------------------------------------------------------
||| ○ is a bifunctor Cont × Cont → Cont                                      |||
--------------------------------------------------------------------------------

public export
(~○~) :
  {0 a, a', b, b' : Container} ->
  (a =%> a') -> (b =%> b') -> a ○ b =%> a' ○ b'
(~○~) m1 m2 =
  (\x => MkEx (m1.fwd x.ex1) (\y => m2.fwd (x.ex2 (m1.bwd x.ex1 y)))) <!
  (\x, y => (m1.bwd x.ex1 y.π1) ## m2.bwd (x.ex2 (m1.bwd x.ex1 y.π1)) y.π2)


public export
ComposeContBifunctor : Bifunctor Cont Cont Cont
ComposeContBifunctor = MkFunctor
  (Product.uncurry (○))
  (\_, _, m => m.π1 ~○~ m.π2)
  (\x => depLensEqToEq $ MkDepLensEq
      (\v2 => exEqToEq $ MkExEq (ExProj1 x.π1 x.π2.shp v2) (\_ => Refl))
      (\v, v2 => sigEqToEq $ MkSigEq Refl Refl))
  (\_, _, _, f, g => Refl)

||| Mapping of ○ as a functor, useful when used as a monad
public export %hint
composeFunctor :
  {0 a, x, y : Container} ->
  (x =%> y) ->
  a ○ x =%> a ○ y
composeFunctor = (identity a ~○~)

--------------------------------------------------------------------------------
-- ○ is a bifunctor Cont# ⊗ Cont# → Cont#
--------------------------------------------------------------------------------

public export
mapForward : (0 a, b : Container * Container)
    -> (a.π1 =#> b.π1) * (a.π2 =#> b.π2)
    -> Ex a.π1 a.π2.shp
    -> Ex b.π1 b.π2.shp
mapForward a b m x = MkEx (m.π1.fwd x.ex1) (m.π2.fwd . x.ex2 . (m.π1.bwd x.ex1).to)

public export
mapBackward : (0 a, a', b, b' : Container) ->
    (m1 : a =#> a') -> (m2 : b =#> b') ->
    (x : (a ○ b).shp) -> Iso
    ((a' ○ b').pos (mapForward (a && b) (a' && b') (m1 && m2) x))
    ((a ○ b).pos x)
mapBackward a a' b b' m1 m2 x = MkIso
    (\y => (m1.bwd x.ex1).to y.π1
        ## (m2.bwd $ x.ex2 ((m1.bwd x.ex1).to y.π1)).to y.π2)
    (\y => (m1.bwd x.ex1).from y.π1 ##
        rewrite__impl (b'.pos . m2.fwd . x.ex2)
          ((m1 .bwd (x .ex1)).toFrom y.π1)
          ((m2.bwd (x.ex2 y.π1)).from y.π2))
    (\(a1 ## a2) => let
        pp : (m1.bwd x.ex1).to ((m1.bwd x.ex1).from a1) === a1
        pp = ((m1.bwd x.ex1).toFrom a1)
        qq : (m2.bwd (x.ex2 a1)).to ((m2.bwd (x.ex2 a1)).from a2) === a2
        qq = (m2.bwd (x.ex2 a1)).toFrom a2
        in
        cong2Dep (##) pp (rewrite pp in qq))
    (\(a1 ## a2) => rewrite (m1.bwd x.ex1).fromTo a1
                 in rewrite (m2.bwd (x.ex2 ((m1.bwd x.ex1).to a1))).fromTo a2
                 in Refl)

-- Sequence as a bifunctor on Cont#
public export
(~○#~) :
    {0 a, a', b, b': Container} ->
    (a =#> a') -> (b =#> b') ->
    a ○ b =#> a' ○ b'
(~○#~) m1 m2 = MkCartDepLens
    (mapForward (a && b) (a' && b') (m1 && m2))
    (mapBackward a a' b b' m1 m2)

-- proof that the bifunctor preserves identity
public export
0 circBifunctorPreservesIdentity : (v : Container * Container) ->
  (identity v.π1) ~○#~ (identity v.π2)
  `CartDepLensEq` identity (uncurry (○) v)
circBifunctorPreservesIdentity v = MkCartDepLensEq exUniq
   (\_ => MkIsoEq dpairUniq dpairUniq)

-- Proof that the bifunctor preserves composition
public export
0 circBifunctorPreservesComposition : (c1, c2, c3 : Container * Container)
    -> (f : (c1.π1 =#> c2.π1) * (c1.π2 =#> c2.π2))
    -> (g : (c2.π1 =#> c3.π1) * (c2.π2 =#> c3.π2))
    -> (f.π1 |#> g.π1) ~○#~ (f.π2 |#> g.π2) ≡
       (f.π1 ~○#~ f.π2 |#> g.π1 ~○#~ g.π2)
circBifunctorPreservesComposition (c1 && c1') (c2 && c2') (c3 && c3')
    (fp1 && fp2) (gp1 && gp2) = cartEqToEq $ MkCartDepLensEq
    (\x => Refl)
    (\(MkEx x1 x2) => MkIsoEq
        (\y => Refl)
        (\(y1 ## y2) => sigEqToEq $ MkSigEq
            Refl
            (rewrite (fp1.bwd x1).toFrom y1 in Refl)
        )
    )

||| Monoidal bifunctor on cartesian lense, defined by (○) on containers
public export
ComposeContCartBifunctor : Bifunctor ContCartCat ContCartCat ContCartCat
ComposeContCartBifunctor = MkFunctor
  (Product.uncurry (○))
  (\x, y, m => m.π1 ~○#~ m.π2)
  (\x => cartEqToEq (circBifunctorPreservesIdentity x))
  circBifunctorPreservesComposition
