||| A description of common data structures as containers
module Data.Container.Descriptions

import public Data.Container
import public Data.Fin
import Data.Sigma
import Data.Coproduct
import Data.Product
import public Data.Container.Morphism
import Proofs.Sum
import Proofs.Sigma
import Proofs.Congruence
import Proofs.Extensionality
import Data.Iso
import Data.Container.Category
import public Data.Container.Descriptions.List
import public Data.Container.Descriptions.Maybe
import Data.Category.Functor
import Data.Category.NaturalTransformation

%hide Prelude.Ops.infixl.(|>)

Annotated : Type -> Type
Annotated = Ex (ListCont #> (() :- Nat))


namespace NoFin

  Finite : Nat -> Type
  Finite Z = Void
  Finite (S n) = Either Unit (Finite n)

  ListCont : Container
  ListCont = MkCont Nat Finite

  ListType : Type -> Type
  ListType = Ex NoFin.ListCont

  Nil : NoFin.ListType a
  Nil = MkEx Z absurd

  Cons : a -> NoFin.ListType a -> NoFin.ListType a
  Cons x ls = MkEx (S ls.ex1) (\case (Left l) => x
                                   ; (Right r) => ls.ex2 r)


-- Tree with values on the leaves
namespace Leaves
  data EmptyTree : Type where
    Leaf : EmptyTree
    Node : EmptyTree -> EmptyTree -> EmptyTree

  data InTree : EmptyTree -> Type where
    InLeaf : InTree Leaf
    InLeft : InTree t -> InTree (Node t r)
    InRight : InTree t -> InTree (Node l t)

  TreeCont : Container
  TreeCont = MkCont EmptyTree InTree

  Tree : Type -> Type
  Tree = Ex TreeCont

  LeafTree : a -> Tree a
  LeafTree x = MkEx Leaf (\InLeaf => x)

  BranchTree : Tree a -> Tree a -> Tree a
  BranchTree (MkEx t1 p1) (MkEx t2 p2) = MkEx (Node t1 t2) (
    \case (InLeft x) => p1 x
          (InRight x) => p2 x)

namespace Nodes
  data EmptyTree : Type where
    Leaf : EmptyTree
    Node : EmptyTree -> EmptyTree -> EmptyTree

  data InTree : EmptyTree -> Type where
    InHere : InTree (Node l r)
    InLeft : InTree t -> InTree (Node t r)
    InRight : InTree t -> InTree (Node l t)

  Uninhabited (InTree Leaf) where
    uninhabited InHere impossible
    uninhabited (InLeft x) impossible
    uninhabited (InRight x) impossible

  TreeCont : Container
  TreeCont = MkCont EmptyTree InTree

  Tree : Type -> Type
  Tree = Ex TreeCont

  LeafTree : Tree a
  LeafTree = MkEx Leaf absurd

  BranchTree : Tree a -> a -> Tree a -> Tree a
  BranchTree (MkEx t1 p1) a (MkEx t2 p2) = MkEx (Node t1 t2) (
    \case (InHere) => a
          (InLeft x) => p1 x
          (InRight x) => p2 x)
  LeafVal : a -> Tree a
  LeafVal x = BranchTree LeafTree x LeafTree

--
--     1
--    / \
--   2   3
--  /\
-- 4  5
  testTree : Tree Nat
  testTree = BranchTree (BranchTree (LeafVal 4) 2 (LeafVal 5)) 1 (LeafVal 3)

-- handle the nothing case with a default value
public export
orElse : {0 y : Container} -> y.shp -> MaybeCont #> x =%> y + x
orElse val = fwd <! bwd
  where
    fwd : Ex MaybeCont x.shp -> y.shp + x.shp
    fwd (MkEx True x) = +> (x ())
    fwd (MkEx False x) = <+ val

    bwd : (v : Ex MaybeCont x.shp) ->
          (y + x).pos (fwd v) ->
          (MaybeCont #> x).pos v
    bwd (MkEx True p2) x = \() => x
    bwd (MkEx False p2) x = \v => absurd v

public export
convertList : Const (Prelude.List a) =%> ListM (Const a)
convertList =  toContList <! fromContList
  where
    toContList : Prelude.List a -> Ex ListCont a
    toContList [] = Nil
    toContList (x :: xs) = Cons x (toContList xs)

    fromContList : (x : Prelude.List a) ->
                   pos (ListM (Const a)) (toContList x) ->
                   Prelude.List a
    fromContList [] f = []
    fromContList (x :: xs) f = f FZ :: (assert_total $ fromContList xs (f . FS))

IsJust : (x -> Type) -> Maybe x -> Type
IsJust f Nothing = Void
IsJust f (Just v) = f v

CanonMaybe : Container -> Container
CanonMaybe x = MkCont (Maybe x.shp) (IsJust x.pos)

0 bimapIdTwice : Prelude.Interfaces.bimap {f = (+)} Prelude.id Prelude.id = Prelude.id
bimapIdTwice = funExt $ \case (<+ x) => Refl
                              (+> x) => Refl

CanonMaybeF : Functor Cont Cont


{-
%unbound_implicits off
OnePlusF : Functor Cont Cont
OnePlusF = MkFunctor
  (CNeutral +)
  (\_, _ => FunctorPlus {f = CNeutral})
  (\v => cong2Dep (<!) bifunctorId
                             chooseId)
  ?OnePlusF_rhs5

onePlusCanonNT : OnePlusF ==>> CanonMaybeF
