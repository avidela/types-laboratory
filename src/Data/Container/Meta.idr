
module Data.Container.Meta

import Data.Category
import Data.Container
import Data.Container.Morphism
import Data.Sigma
import Data.Coproduct

import Data.Fixpoint

import Decidable.Equality

Meta : Container
Meta = (c : Container) !> ?meta_container

record MetaContainer where
  constructor MkMetaCont
  obj : Container
  fam : obj =%> Meta

ContEx : Type -> Container
ContEx ty = (c : Container) !> Ex c ty


public export
deriv : Container -> Container
deriv c =
  (s : Σ c.shp c.pos) !>
  Σ (c.pos s.π1) (\p' => Not (s.π2 === p'))

deriv' : Container -> Type
deriv' c =
  Σ (Σ c.shp c.pos) (\s =>
    Σ (c.pos s.π1) (\p' => Not (s.π2 === p')))

hole : (v : Ex (deriv c) x) -> c.pos v.π1.π1
hole p = p.π1.π2

unplug : (v : Ex c x) -> c.pos v.π1 -> (Ex (deriv c) x, x)
unplug (p1 ## p2) y = ((p1 ## y) ## p2 . π1 , p2 y)

ContLens : Type
ContLens = Σ Container deriv'

data Deriv e a = Sum a a | Prod a a | Comp a a | Val e | Zero

DerivF : Type -> Type
DerivF e = Fix (Deriv e)

derivMap : (f : a -> b) -> Deriv e a -> Deriv e b
derivMap f (Sum x y) = Sum (f x) (f y)
derivMap f (Prod x y) = Prod (f x) (f y)
derivMap f (Comp x y) = Comp (f x) (f y)
derivMap f (Val x) = Val x
derivMap f Zero = Zero

Functor (Deriv e) where
  map = derivMap

toPair : Ex ((deriv c * d) + (c * deriv d)) x ->
         Ex (deriv (c * d)) x

composeType : (Ex d . Ex c) x -> Ex (d ○ c) x
composeType p1 = (p1.π1 ## π1 . p1.π2) ## (\sig => (p1.π2 sig.π1).π2 sig.π2)

exp : Type -> Container -> Container
exp t c = (f : t -> c.shp) !> Σ t (c.pos . f)

toExp : (k -> Ex c x) -> Ex (exp k c) x
toExp f = π1 . f ## \v => (f v.π1).π2 v.π2

fromExp : Ex (exp k c) x -> (k -> Ex c x)
fromExp p1 y = p1.π1 y ## \vx => p1.π2 (y ## vx)

toSum : (Ex c x) + (Ex d x) -> Ex (c + d) x
toSum (<+ (shp ## chld)) = (<+ shp) ## chld
toSum (+> (shp ## chld)) = (+> shp) ## chld

fromSum : Ex (c + d) x -> (Ex c x) + (Ex d x)
fromSum ((<+ shp) ## chld) = <+ (shp ## chld)
fromSum ((+> shp) ## chld) = +> (shp ## chld)

toSumEx : Ex (deriv c + deriv d) x ->
        Ex (deriv (c + d)) x
toSumEx p1 = case fromSum {c = deriv c, d = deriv d} p1 of
                  (<+ y) => (<+ y.π1.π1 ## ?bb) ## ?toSum_rhs_0
                  (+> y) => ?toSum_rhs_1

-- fromCompose : Ex (deriv (c ○ d)) x ->

--               Ex (deriv d * (deriv c ○ d)) x
-- fromCompose p1 = toPair {c = deriv d} {d = deriv c ○ d} ?fromCompose_rhs

chainRule : (c1, c2 : ContLens) -> ContLens
chainRule (q1 ## q2) (p1 ## p2) = ?uqu_1

composeContLens : ContLens -> ContLens -> ContLens
composeContLens p1 p2 = (p1.π1 ○ p2.π1) ## ?composeContLens_rhs

