module Data.Container.Distrib

import Data.Container
import Data.Container.Morphism
import Data.Container.Descriptions
import Data.Sigma
import Proofs.Sigma

%default total

-- Concatenate two lists from their container descriptions
cons : (x : Container) -> x.shp -> (ListCont #> x).shp -> (ListCont #> x).shp
cons c v l = MkEx (S l.ex1) (\case FZ => v ; (FS n) => l.ex2 n)

-- Using `fwd'` instead of `fwd` to help with totality checking
fwd' : {a : Container} ->
       (x : Nat) ->
       (Fin x -> MaybeType a .shp) ->
       (MaybeCont ○ (ListCont #> a)).shp
fwd' 0 nx = MkEx True (\a => MkEx 0 absurd)
fwd' (S k) nx with (nx FZ)
  fwd' (S k) nx | (MkEx False _) = MkEx False (\x => void x)
  fwd' (S k) nx | (MkEx True p2) with (fwd' k (nx . FS))
    fwd' (S k) nx | (MkEx True p2) | (MkEx False ny) = MkEx False absurd -- if the recursive call is `Nothing` return `Nothing`
    fwd' (S k) nx | (MkEx True p2) | (MkEx True ny) = MkEx True (\_ => cons a (p2 ()) (ny ())) -- otherwise concatenate

fwd : {a : Container} -> (ListCont #> (MaybeCont ○ a)).shp -> (MaybeCont ○ (ListCont #> a)).shp
fwd x = fwd' x.ex1 x.ex2

bwd : {a : Container} -> (y : (ListCont #> (MaybeCont ○ a)).shp) ->
      (MaybeCont ○ (ListCont #> a)).pos (Distrib.fwd' {a} y.ex1 y.ex2) ->
      (ListCont #> (MaybeCont ○ a)).pos y
bwd (MkEx 0 v2) x val = absurd val
bwd (MkEx (S k) v2) (x1 ## x2) val with (v2 FZ) proof pfz
  bwd (MkEx (S k) v2) (x1 ## x2) val | (MkEx False xs) = void x1
  bwd (MkEx (S k) v2) (x1 ## x2) FZ | (MkEx True xs) with (fwd' k (\x => v2 (FS x))) proof pfy
    bwd (MkEx (S k) v2) (x1 ## x2) FZ | (MkEx True xs) | (MkEx False f2) = void x1
    bwd (MkEx (S k) v2) (x1 ## x2) FZ | (MkEx True xs) | (MkEx True f2) = rewrite pfz in () ## x2 FZ
  bwd (MkEx (S k) v2) (x1 ## x2) (FS x) | (MkEx True xs) with (fwd' k (v2 . FS)) proof pfwd
    bwd (MkEx (S k) v2) (x1 ## x2) (FS x) | (MkEx True xs) | (MkEx False _) = void x1
    bwd (MkEx (S k) v2) (x1 ## x2) (FS xt) | (MkEx True xs) | (MkEx True fn) with (v2 (FS xt)) proof pfs
      _ | (MkEx True rec) = () ## (let vn = ?endarg in ?ending)
          -- let
          --     term : Σ () (\x => (val : Fin ((fn x).ex1)) -> a.pos ((fn x).ex2 val))
          --     term = () ## \k => x2 (FS k)
          --     ppp := ?howel
          --     -- ppp := sigmalemma Bool (Ex ListCont a.shp) isTrue
          --     --     (\v => (val : Fin v.π1) -> a .pos (v.π2 val))
          --     --     (fwd' k (\x => v2 (FS x))) True fn pfwd
          --     recBwd := Distrib.bwd
          --            (k ## (v2 . FS))
          --            (replace {p=id} (sym ppp) term)
          --            xt
          --     fsProof := sigmalemma Bool a.shp isTrue a.pos (v2 (FS xt)) True rec pfs
          -- in replace {p=id} fsProof recBwd

      _ | (MkEx False pn) = ?hello
          -- let
          --     term : Σ () (\x => (val : Fin ((fn x).ex1)) -> a .pos ((fn x).ex2 val))
          --     term = () ## \k => x2 (FS k)
          --     ppp := sigmalemma Bool (Ex ListCont a.shp) isTrue
          --                (\v => (val : Fin (v.π1)) -> a .pos (v.π2 val))
          --                (fwd' k (\x => v2 (FS x))) True fn pfwd
          --     recBwd := bwd (k ## (v2 . FS))
          --                   (replace {p=id} (sym ppp) term)
          --                   xt
          --     fsProof := sigmalemma Bool a.shp isTrue a.pos (v2 (FS xt)) False pn pfs
          -- in replace {p=id} fsProof recBwd

-- Maybe and List distribute around |> and ○
MaybeListDistrib : (x : Container) ->
    (ListCont #> (MaybeCont ○ x)) =%> (MaybeCont ○ (ListCont #> x))
MaybeListDistrib x = fwd <! bwd
----------------------------------------------------------------------------------
-- Distributive diagrams
----------------------------------------------------------------------------------

-- List (List (Maybe x)) ----> List (Maybe (List x))
--          |                       |
--          |                       V
--          |                 Maybe (List (List x))
--          |                       |
--          V                       V
--     List (Maybe x) ------> Maybe (List x)

0 square1 : (x : Container) -> let
    0 top : ListM (ListM (MaybeM x)) =%> ListM (MaybeM (ListM x))
    0 topRight : ListM (MaybeM (ListM x)) =%> MaybeM (ListM (ListM x))
    0 botRight : MaybeM (ListM (ListM x)) =%> MaybeM (ListM x)
    0 left : ListM (ListM (MaybeM x)) =%> ListM (MaybeM x)
    0 bot : ListM (MaybeM x) =%> MaybeM (ListM x)
    bot = MaybeListDistrib x
  in (left ⨾ bot) `DepLensEq` (top ⨾ topRight ⨾ botRight)
square1 x = MkDepLensEq
    (\case (MkEx vx vx') => ?aaa)
    (\vx, vy => ?square1_rhs)

----------------------------------------------------------------------------------
-- General distributive law?
----------------------------------------------------------------------------------


-- Nope, cannot implement this.
genFwd : {a, b, x : Container} ->
    (a #> (b ○ x)).shp -> (b ○ (a #> x)).shp
genFwd (MkEx p1 p2) = ?huh

genBwd : {a, b, x : Container} ->
    (v : (a #> (b ○ x)).shp) ->
    (b ○ (a #> x)).pos (genFwd {a, b, x} v) ->
    (a #> (b ○ x)).pos v

generalDistrib : (a, b, x : Container) ->
    (a #> (b ○ x)) =%> (b ○ (a #> x))
generalDistrib a b x = genFwd <! genBwd


