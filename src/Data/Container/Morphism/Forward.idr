module Data.Container.Morphism.Forward

import Data.Container
import Data.Category
import Proofs.Congruence
import Proofs.Extensionality

infixr 0 =|>

public export
record (=|>) (a, b : Container) where
  constructor MkAdapter
  top : a.shp -> b.shp
  bot : (x : a.shp) -> a.pos x -> b.pos (top x)

record AdapterEq (f, g : a =|> b) where
  constructor MkAdapterEq
  topEq : (x : a.shp) -> f.top x === g.top x
  botEq : (x : a.shp) -> (y : a.pos x) -> let
           0 p1 : b.pos (top f x)
           p1 = f.bot x y
           0 p2 : b.pos (top f x)
           p2 = replace {p = b.pos} (sym $ topEq x) (g.bot x y)
          in p1 === p2

0 adapterEqToEq : AdapterEq a b -> a === b
adapterEqToEq {a = (MkAdapter top bot)} {b = (MkAdapter top' g)}
    (MkAdapterEq topEq botEq) = cong2Dep' MkAdapter (funExt topEq) (funExtDep (\xv => funExt $ botEq xv))

public export
compose : a =|> b -> b =|> c -> a =|> c
compose x y = MkAdapter (y.top . x.top) (\xv, yv => y.bot (x.top xv) (x.bot xv yv))

public export
adapterId : a =|> a
adapterId = MkAdapter id (\_ => id)

%unbound_implicits off
public export
0 adaptedIdLeft : {0 x, y : Container} -> (0 f : x =|> y) -> (adapterId `compose` f) === f
adaptedIdLeft (MkAdapter top bot) = Refl

public export
0 adaptedIdRight : {0 x, y : Container} -> (0 f : x =|> y) -> (f `compose` adapterId) === f
adaptedIdRight (MkAdapter top bot) = Refl

public export
0 adapterComposeAssoc : {0 a, b, c, d : Container} ->
                      (f : a =|> b) -> (g : b =|> c) -> (h : c =|> d) ->
                      (f `compose` (g `compose` h)) === ((f `compose` g) `compose` h)
adapterComposeAssoc f g h = adapterEqToEq $ MkAdapterEq (\_ => Refl) (\_, _ => Refl)

public export
FwdContCat : Category Container
FwdContCat = MkCategory
  (=|>)
  (\_ => adapterId)
  compose
  (\xn => adaptedIdRight xn)
  (\xn => adaptedIdLeft xn)
  adapterComposeAssoc

