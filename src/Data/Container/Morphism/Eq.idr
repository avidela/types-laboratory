module Data.Container.Morphism.Eq

import Data.Container.Definition
import Data.Container.Morphism.Definition

import Data.Iso

import Proofs

import public Control.Relation
import public Control.Order

public export
record DepLensEq (a, b : dom =%> cod) where
  constructor MkDepLensEq
  0 eqFwd : (v : dom.shp) -> a.fwd v === b.fwd v
  0 eqBwd : (v : dom.shp) -> (y : cod.pos (a.fwd v)) ->
          let 0 p1 : dom.pos v
              p1 = a.bwd v y
              0 p2 : dom.pos v
              p2 = b.bwd v (replace {p = cod.pos} (eqFwd v) y)
          in p1 === p2

export
0 depLensEqToEq : DepLensEq a b -> a === b
depLensEqToEq {a = (fwd1 <! bwd1)} {b = (fwd2 <! bwd2)} (MkDepLensEq eqFwd eqBwd) =
  cong2Dep' (<!) (funExt eqFwd) (funExtDep $ \x => funExt $ \y => eqBwd x y)

public export
Transitive (dom =%> cod) DepLensEq where
  transitive a b = MkDepLensEq (\v => transitive (a.eqFwd v) (b.eqFwd v))
      (\v, w => transitive
           (a.eqBwd v w)
           (b.eqBwd v (replace {p = cod.pos} (a.eqFwd v) w)))

public export
Reflexive (dom =%> cod) DepLensEq where
  reflexive = MkDepLensEq (\_ => Refl) (\_, _ => Refl)

public export
Preorder (dom =%> cod) DepLensEq where

||| An isomorphism of container morphisms
public export
ContIso : (x, y : Container) -> Type
ContIso = GenIso Container (=%>) DepLensEq
