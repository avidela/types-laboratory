module Data.Container.Morphism.Effect

import Data.Container.Morphism
import Data.Container.Category
import Data.Container.Lift
import Data.Category.Monad
import Data.Category.Functor
import Data.Category.NaturalTransformation
import Data.Category.Set

import Proofs.Congruence
import Proofs.Extensionality
import Interface.Comonad

import Syntax.PreorderReasoning
import Syntax.PreorderReasoning.Generic

%hide Prelude.Ops.infixl.(|>)

public export
liftBwd : Functor f => a =%> b -> f • a =%> f • b
liftBwd mor = mor.fwd <! (\x, y => map (mor.bwd x) y)

-- Lift f is a monad when f is a comonad
export
pureM : Comonad f => a =%> f • a
pureM = id <! (\_ => extract)

export
joinM : Comonad f => f • a =%> f • (f • a)
joinM = id <! (\_, x => map extract x)

-- Lift f is a comonad on containers when `f` is a monad
export
extractM : Monad f => (f • a) =%> a
extractM = id <! (\_ => pure)

export
counit : Monad f => (f • a) =%> a
counit = id <! (\_ => pure)

export
comultM : Monad f => f • a =%> f • (f • a)
comultM = id <! (\x => join)

export
mapLift : {a, b : _} -> (f : Functor Set Set) -> a =%> b -> f.mapObj • a =%> f.mapObj • b
mapLift func mor = mor.fwd <! (\x, y => func.mapHom _ _ (mor.bwd x) y)

export
mapLift' : Prelude.Functor f => a =%> b -> f • a =%> f • b
mapLift' mor = mor.fwd <! (\x, y => map (mor.bwd x) y)

----------------------------------------------------------------------------
-- Given a functor on set, `F` is a functor on Cont
----------------------------------------------------------------------------

FFunctor : Functor Set Set -> Functor Cont Cont
FFunctor fn = MkFunctor (fn.mapObj •)
    (\a, b => mapLift fn)
    functorID
    functorCompose
  where
    0 functorID : (v : Container) ->
                  mapLift fn (identity v) === identity (fn.mapObj • v)
    functorID v = cong2Dep (<!) Refl (funExtDep $ \vx => fn.presId (v.pos vx))

    0 functorCompose : (a, b, c : Container) ->
                       (f : a =%> b) ->
                       (g : b =%> c) ->
                       let 0 h1, h2 : fn.mapObj • a =%> fn.mapObj • c
                           h1 = mapLift fn (f ⨾ g)
                           h2 = mapLift fn f ⨾ mapLift fn g
                        in h1 === h2
    functorCompose a b c f g = cong2Dep (<!)
        Refl
        (funExtDep $ \vx =>
         fn.presComp (c.pos (g.fwd (f.fwd vx))) (b.pos (f.fwd vx)) (a.pos vx)
                            (g.bwd (f .fwd vx)) (f.bwd vx))

FNatTrans : (f, g : Functor Set Set) -> (m : f =>> g) -> FFunctor g =>> FFunctor f
FNatTrans f g (MkNT m1 m2) = MkNT
    (\v => id <! (\vx => m1 (v.pos vx)))
    (\vx, vy, m => cong2Dep (<!) Refl (funExtDep $ \vz => sym (m2 _ _ (m.bwd vz))))

----------------------------------------------------------------------------
-- Given a functor on set, `F` is a functor on Cont^op
----------------------------------------------------------------------------

FFunctorOp : Functor Set Set -> Functor (Cont).op (Cont).op
FFunctorOp fn = MkFunctor (fn.mapObj •)
    (\a, b => mapLift fn)
    functorID
    functorCompose
  where
    0 functorID : (v : Container) ->
                  mapLift fn (identity v) === identity (fn.mapObj • v)
    functorID v = cong2Dep (<!) Refl (funExtDep $ \vx => fn.presId (v.pos vx))

    0 functorCompose : (a, b, c : Container) ->
                       (f : b =%> a) ->
                       (g : c =%> b) ->
                       let 0 h1, h2 : fn.mapObj • c =%> fn.mapObj • a
                           h1 = mapLift fn (g ⨾ f)
                           h2 = mapLift fn g ⨾ mapLift fn f
                        in h1 === h2
    functorCompose a b c f g = cong2Dep (<!)
        Refl
        (funExtDep $ \vx => fn.presComp _ _ _ (f.bwd (g.fwd vx)) (g.bwd vx))


FNatTransOp : (f, g : Functor Set Set) -> (m : f =>> g) -> FFunctorOp f =>> FFunctorOp g
FNatTransOp f g nt = MkNT
    (\v => id <! (\vx => nt.component (v.pos vx)))
    (\vx, vy, m => cong2Dep (<!) Refl (funExtDep $ \vz => nt.commutes _ (vy.pos vz) (m.bwd vz)))

----------------------------------------------------------------------------
-- From a comonad we get a monad
----------------------------------------------------------------------------

pureF : (com : Comonad Set) -> (a : Container) -> a =%> com.endo.mapObj • a
pureF com c = id <! (\x, y => com.counit.component (c.pos x) y)

pureNT : (com : Comonad Set) -> Functor.idF Cont =>> FFunctor com.endo
pureNT com = MkNT (pureF com) (\a, b, x => cong2Dep (<!) Refl
                  (funExtDep $ \vx => sym (com.counit.commutes _ _ (x.bwd vx))))

joinF : (com : Comonad Set) -> (a : Container) ->
        com.endo.mapObj • (com.endo.mapObj • a) =%> com.endo.mapObj • a
joinF com c = id <! (\x, y => com.comult.component (c.pos x) y)

joinNT : (com : Comonad Set) -> (FFunctor com.endo !*> FFunctor com.endo) =>> FFunctor com.endo
joinNT com = MkNT (joinF com) (\a, b, mor => cong2Dep (<!) Refl
                  (funExtDep $ \vx => sym $ com.comult.commutes _ _ (mor.bwd vx)))

0
monadSquare : (com : Comonad Set) -> (x : Container) -> let

              func : Functor Cont Cont
              func = FFunctor com.endo

              top : func.mapObj (func.mapObj (func.mapObj x)) =%>
                    func.mapObj (func.mapObj x)
              top = let qn = com.comult.component in func.mapHom (func.mapObj (func.mapObj x)) (func.mapObj x) (joinF com x)

              bot, right : func.mapObj (func.mapObj x) =%> func.mapObj x
              bot = joinF com x
              right = joinF com x

              left : com.endo.mapObj • (com.endo.mapObj • (com.endo.mapObj • x)) =%>
                     com.endo.mapObj • (com.endo.mapObj • x)
              left = joinF com (func.mapObj x)

              0 arm2, arm1 : com.endo.mapObj • (com.endo.mapObj • (com.endo.mapObj • x)) =%> com.endo.mapObj • x
              arm1 = top ⨾ right
              arm2 = left ⨾ bot
              in arm1 === arm2
monadSquare com x = cong2Dep (<!) Refl (funExtDep $ \vx => com.square (x.pos vx))

0
monadIdLeft : (com : Comonad Set) -> (x : Container) -> let
              func : Functor Cont Cont
              func = FFunctor com.endo
              0 compose : func.mapObj x =%> func.mapObj x
              compose = ((pureNT com).component (func.mapObj x) ⨾ (joinNT com).component x)
              0 straight : func.mapObj x =%> func.mapObj x
              straight = identity _
              in compose === straight
monadIdLeft com x = cong2Dep (<!) Refl (funExtDep $ \vx => com.identityLeft (x.pos vx))

0 monadIdRight : (com : Comonad Set) -> (x : Container) -> let
      func : Functor Cont Cont
      func = FFunctor com.endo
      0 compose : func.mapObj x =%> func.mapObj x
      compose = (func.mapHom _ _ ((pureNT com).component x) ⨾ (joinNT com).component x)
      0 straight : func.mapObj x =%> func.mapObj x
      straight = identity _
      in compose === straight
monadIdRight com x = cong2Dep (<!) Refl (funExtDep $ \vx => com.identityRight (x .pos vx))

FMonad : (com : Comonad Set) -> Monad.Monad Cont
FMonad com = Monad.MkMonad
    (FFunctor com.endo)
    (pureNT com)
    (joinNT com)
    (monadSquare com)
    (monadIdLeft com)
    (monadIdRight com)

----------------------------------------------------------------------------
-- From a monad we get a comonad
----------------------------------------------------------------------------

extractF : (mon : Monad Set) -> (a : Container) -> mon.endo.mapObj • a =%> a
extractF mon c = id <! (\x, y => mon.unit.component (c.pos x) y)

extractNT : (mon : Monad Set) -> FFunctor mon.endo =>> Functor.idF Cont
extractNT mon = MkNT (extractF mon) (\a, b, x => cong2Dep (<!) Refl
                  (funExtDep $ \vx => sym (mon.unit.commutes _ _ (x.bwd vx))))

comultF : (mon : Monad Set) -> (a : Container) -> mon.endo.mapObj • a =%> mon.endo.mapObj • (mon.endo.mapObj • a)
comultF mon c = id <! (\x, y => mon.mult.component (c.pos x) y)

comultNT : (mon : Monad Set) -> FFunctor mon.endo =>> (FFunctor mon.endo !*> FFunctor mon.endo)
comultNT mon = MkNT (comultF mon) (\a, b, mor => cong2Dep (<!) Refl
                  (funExtDep $ \vx => sym $ mon.mult.commutes _ _ (mor.bwd vx)))

0
comonadSquare : (com : Monad Set) -> (x : Container) -> let

              func : Functor Cont Cont
              func = FFunctor com.endo

              top : func.mapObj (func.mapObj x) =%>
                    func.mapObj (func.mapObj (func.mapObj x))
              top = func.mapHom (func.mapObj x) (func.mapObj (func.mapObj x)) (comultF com x)

              bot, right : func.mapObj x =%> func.mapObj (func.mapObj x)
              bot = comultF com x
              right = comultF com x

              left : com.endo.mapObj • (com.endo.mapObj • x) =%>
                     com.endo.mapObj • (com.endo.mapObj • (com.endo.mapObj • x))
              left = comultF com (func.mapObj x)

              0 arm2, arm1 : com.endo.mapObj • x =%>
                             com.endo.mapObj • (com.endo.mapObj • (com.endo.mapObj • x))
              arm1 = (top • right) {cat = Cont}
              arm2 = (left • bot) {cat = Cont}
              in arm1 === arm2
comonadSquare com x = cong2Dep (<!) Refl (funExtDep $ \vx => com.square (x.pos vx))

0
comonadIdLeft : (com : Monad Set) -> (x : Container) -> let
              func : Functor Cont Cont
              func = FFunctor com.endo

              0 compose : func.mapObj x =%> func.mapObj x
              compose = ((extractF com) (func.mapObj x) • (comultF com) (x)) {cat = Cont}
              0 straight : func.mapObj x =%> func.mapObj x
              straight = identity _
              in compose === straight
comonadIdLeft com x = cong2Dep (<!) Refl (funExtDep $ \vx => com.identityLeft (x.pos vx))

0 comonadIdRight : (com : Monad Set) -> (x : Container) -> let
      func : Functor Cont Cont
      func = FFunctor com.endo
      0 compose : func.mapObj x =%> func.mapObj x
      compose = (func.mapHom _ _ (extractF com x) • comultF com x) {cat = Cont}
      0 straight : func.mapObj x =%> func.mapObj x
      straight = identity _
      in compose === straight
comonadIdRight com x = cong2Dep (<!) Refl (funExtDep $ \vx => com.identityRight (x .pos vx))

FComonad : (mon : Monad Set) -> Monad.Comonad Cont
FComonad mon = Monad.MkComonad
    (FFunctor mon.endo)
    (extractNT mon)
    (comultNT mon)
    (comonadSquare mon)
    (comonadIdLeft mon)
    (comonadIdRight mon)


------------------------------------------------------------------------------------------
-- We get a functor from the category of monads in Set to the category of comonads in Cont
------------------------------------------------------------------------------------------


fromID : Functor.idF (Cont).op =>> FFunctorOp (Functor.idF Set)
fromID = MkNT (\x => id <! (\_ => id)) (\x, y, m2 => Refl)

fromComp : (mon : Monad Set) -> (FFunctorOp mon.endo !*> FFunctorOp mon.endo) =>> (FFunctorOp (mon.endo !*> mon.endo))
fromComp mon = MkNT (\x => identity _) (\x, y, m => Refl)

extractNTOp : (mon : Monad Set) -> Functor.idF (Cont).op =>> FFunctorOp mon.endo
extractNTOp mon = fromID !!> FNatTransOp (idF Set) mon.endo mon.unit

comultNTOp : (mon : Monad Set) -> (FFunctorOp mon.endo !*> FFunctorOp mon.endo) =>> FFunctorOp mon.endo
comultNTOp mon = fromComp mon !!> FNatTransOp _ _ mon.mult

FComonad' : (mon : Monad Set) -> Monad.Monad (Cont).op
FComonad' mon = Monad.MkMonad
    (FFunctorOp mon.endo)
    (extractNTOp mon)
    (comultNTOp mon)
    (\x => cong2Dep' (<!) Refl (funExtDep $ \vx => mon.square (x.pos vx)))
    (\x => cong2Dep' (<!) Refl (funExtDep $ \vx => mon.identityLeft (x.pos vx)))
    (\x => cong2Dep' (<!) Refl (funExtDep $ \vx => mon.identityRight (x.pos vx)))

FComonadMor : (a, b : Monad Set) -> MonadMor a b -> MonadMor (FComonad' a) (FComonad' b)
FComonadMor a b m = MkMonadMor
    nt
    preservesPure
    preservesJoin
    where
      nt : (FComonad' a).endo =>> (FComonad' b).endo
      nt = (FNatTransOp _ _ m.monadMap)

      0 preservesPure : ((FComonad' a).unit !!> nt) `NTEq` (FComonad' b).unit
      preservesPure = let 0 nq = m.presPure
                          pp : (a.unit !!> m.monadMap) `NTEq` b.unit
                          pp = m.presPure
                          MkNTEq px := pp
                          -- MkNTEq py = m.presJoin

                      in MkNTEq $ \nx => let nt1 := nt.commutes nx nx (identity _)
                      in cong2Dep' (<!) ?aww
                        (funExtDep $ \vz => rewrite sym (px (nx .pos vz)) in ?blue)

      preservesJoin : ((FComonad' a).mult !!> nt) `NTEq` ((nt -*- nt) !!> (FComonad' b).mult)
      preservesJoin = ?preservesJoin_rhs

-- FComonadPresID : FComonadMor a a (MonId a)

monadComonadFunctor : Functor (MonCat Set) (MonCat (Cont).op)
monadComonadFunctor = MkFunctor
    FComonad'
    FComonadMor
    ?presId
    ?presCOmp

