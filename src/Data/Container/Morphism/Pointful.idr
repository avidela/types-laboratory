||| Pointful lenses
module Data.Container.Morphism.Pointful

import Data.Product
import Data.Container
import Data.Container.Morphism
import Data.Coproduct
import Data.Value

public export
Pointful : (a, b : Container) -> Type
Pointful a b =  (x : a.shp) -> Value b (a.pos x)

export infixr 1 $-

export
($-) : Pointful a b -> {0 r : Type} -> Value a r -> Value b r
($-) cont (MkVal arg fn) =
  let v' = cont arg in
      MkVal v'.arg (fn . v'.fn)

bind : Value a r -> ((x : a.shp) -> Value b (a.pos x)) -> Value b r
bind x f = f $- x

-- bindMor: Pointful a b -> ValueMorphism r a b
-- bindMor f = MkVMor $ \v => bind v f

export
(|>) : Pointful a b -> Pointful b c -> Pointful a c
(|>) x y z = y $- x $- MkVal z id

tensor : Pointful a b -> Pointful x y -> Pointful (a ⊗ x) (b ⊗ y)
tensor f g c = And (f c.π1) (g c.π2)

choice : Pointful a b -> Pointful x y -> Pointful (a + x) (b + y)
choice f g (<+ x) = MkVal (<+ arg (f x)) (fn (f x))
choice f g (+> x) = MkVal (+> arg (g x)) (fn (g x))

p1 : Pointful (Const String) (Const Int)

p2 : Pointful (Const Nat) (Const (List Nat))

op2 : Pointful (Const (List Nat)) (Const Nat)

op1 : Pointful (Const Int) (Const (Maybe Int))

test : Const (String * Nat) `Pointful` Const (Maybe Int * Nat)
test arg = let t = tensor {a = Const String, b = Const Int, x = Const Nat, y = Const (List Nat)} p1 p2
               (s1 && s2) = splitup {a = Const Int, b = Const (List Nat)} $ t  arg
               r1 = op1 $- s1
               r2 = op2 $- s2
               rboth = pairup r1 r2
        in rboth

server1 : Pointful (Const Int) (Const Int)

server2 : Pointful (Const (List Nat)) (Const Nat)

combined : Pointful (Const Int + Const (List Nat))
                    (Const Int + Const Nat)
combined = choice {a = Const Int, b = Const Int, x = Const (List Nat), y = Const Nat} server1 server2


testCombined : Pointful (Const Int + Const (List Nat)) (Const (Maybe Int) + Const Nat)
testCombined x =
  let c' = (combined $- (pure x)) {a = Const Int + Const (List Nat)}
  in pairup' {a = Const (Maybe Int), b = Const Nat}
   $ case splitup' {a = Const Int, b = Const Nat} c' of
          (+> a) => let v  = p2 $- a
                        v' = op2 $- v
                    in +> v'
          (<+ b) => let v  = server1 $- b
                        v' = op1 $- v
                    in <+ v'
