module Data.Container.Morphism.Closed

import Data.Container
import Data.Container.Morphism
import Data.Container.Lift
import Data.Sigma
import Data.Coproduct
import Data.Product
import Data.Iso
import Proofs.Extensionality
import Proofs.Sigma

||| Closed dependent lenses, previously known as linear but called
||| Closed in reference to the closed base category and to avoid
||| confusing it with "proper" linear lenses
public export
record Closed (c1, c2 : Container) where
  constructor MkClosed
  fn : (x : c1.shp) -> Σ c2.shp (\y => c2.pos y -> c1.pos x)

compose : Closed a b -> Closed b c -> Closed a c
compose x y = MkClosed $ \z => let a = x.fn z
                                   b = y.fn a.π1
                                in b.π1 ## a.π2 . b.π2

combined : {0 c1, c2 : Container} -> c1 =%> c2 -> Closed c1 c2
combined (fwd <! bwd) = MkClosed $ \x => fwd x ## bwd x

dup2 : Closed a (a ⊗ a)
dup2 = MkClosed $ \arg => dup arg ## fst

dup1 : Closed a (a ⊗ a)
dup1 = MkClosed $ \arg => dup arg ## snd

dupMon : (mult : {x : a.shp} -> a.pos x * a.pos x -> a.pos x) ->
         Closed a (a ⊗ a)
dupMon mult = MkClosed $ \arg => dup arg ## mult

choiceLin : Closed a b -> Closed x y -> Closed (a + x) (b + y)
choiceLin z w = MkClosed $ \arg =>
    ?choiceLin_rhs

toClosed : a =%> b -> Closed a b
toClosed mor = MkClosed $
  \x => mor.fwd x  ## (\y => mor.bwd x y)

export
fromClosed : Closed a b -> a =%> b
fromClosed (MkClosed g) =
  (\x => (g x).π1) <!
  (\x, y => (g x).π2 y)

closedIso : Closed a b `Iso` (a =%> b)
closedIso = MkIso
  fromClosed
  toClosed
  (\(x <! y) => Refl)
  (\(MkClosed fn) => cong MkClosed (funExtDep $ \nx => dpairUniq _))

-- generalised closed dependent lenses parameterised by two morphisms, one for
-- the forward part and one for the backward part
-- THis is the wrong approach since we want the forward morphism to be non-dependent
-- and keep the dependency in the Σ-type, rather than in the morphism.
-- Maybe there is a notion of dependent-copara, this reminds me of how
-- dependent optics are designed by jules & co
-- It could be that we are looking at the same thing where the primitive object
-- is the adapter, and the generalisation is adding dependency between the forward
-- and backward morphism.
private infixr 0 `arr1`
private infixr 0 `arr2`

-- record DepOptic (arr1, arr2 : Type -> Type -> Type) (c1, c2 : Container) where
--   constructor MkDepOpt
--   fwd : c1.shp `arr1` c2.shp
--   bwd : (0 x : c1.shp) -> c2.pos (fwd x) `arr2` c1.pos x

record ClosedArr (arr1 : (a : Type) -> (a -> Type) -> Type) (arr2 : Type -> Type -> Type) (c1, c2 : Container) where
  constructor MkClosedArr
  continuation : arr1 c1.shp (\x => Σ c2.shp (\y => c2.pos y `arr2` c1.pos x))

-- alias for dependent kleisli
DepKleisli : (m : Type -> Type) -> (a : Type) -> (a -> Type) -> Type
DepKleisli m a f = (x : a) -> m (f x)

-- alias for kleisli
Kleisli : (m : Type -> Type) -> (Type) -> (Type) -> Type
Kleisli m a b = a -> (m b)

-- A closed monadic lens is a generalised closed lens
ClosedM : (m : Type -> Type) -> (c1, c2 : Container) -> Type
ClosedM m = ClosedArr (DepKleisli m) (Kleisli m)

-- they compose in the expected way
closedComp : Monad m => ClosedM m a b -> ClosedM m b c -> ClosedM m a c
closedComp m1 m2 = MkClosedArr $ \x => do
  (r1 ## r2) <- m1.continuation x
  (o1 ## o2) <- m2.continuation r1
  pure (o1 ## (r2 <=< o2))

-- one can "pull" the monad from the backward morphism of a traditional dependent lens
-- into a generalised closed dependent lens.
toClosedM : (app : Applicative m) => a =%> m • b -> ClosedM m a b
toClosedM (f <! bwd) = MkClosedArr $ \x =>
                       pure $ f x ## (\vx : b.pos (f x) => let g = bwd x (pure vx) in pure @{app} g)

-- the codomain of a depdendent lens lifted by a monad is compatible with a closed monadic lens
compClose : Monad m => a =%> m • b -> ClosedM m b c -> ClosedM m a c
compClose m1 m2 = toClosedM m1 `closedComp` m2

-- we can run closed monadic lenses
runM : Monad m => ClosedM m a CUnit -> (x : a.shp) -> m (a.pos x)
runM m x = do r <- m.continuation x
              r.π2 ()

{-
record MClosed (m : Type -> Type) (c1, c2 : API) where
  constructor MkMClosed
  continuation : (input : c1.message) -> m (Σ c2.message (\output => c2.response output -> c1.response input))

compose : Monad m => MClosed m a b -> MClosed m b c -> MClosed m a c
compose x y = MkMClosed $ \i' => do
  (r1 ## r2) <- x.continuation i'
  (o1 ## o2) <- y.continuation r1
  pure (o1 ## r2 . o2)

record MClosed' (m : Type -> Type) (c1, c2 : API) where
  constructor MkMClosed'
  continuation : (input : c1.message) -> m (Σ c2.message (\output => c2.response output -> m (c1.response input)))

compose' : Monad m => MClosed' m a b -> MClosed' m b c -> MClosed' m a c
compose' x y = MkMClosed' $ \i' => do
  (r1 ## r2) <- x.continuation i'
  (o1 ## o2) <- y.continuation r1
  pure (o1 ## (r2 <=< o2))

toLens : MClosed m a b -> m $- a =%>
         (x : m b.message) !> Σ
           ((y : b.message) -> b.response y)
           (\f => Σ (Σ b.message b.response)
                    (\values => map f x === values.π2)
           )
