module Data.Container.Morphism.Copara

import Data.Product
import Data.Coproduct
import Data.Container
import Data.Container.Morphism

record CoparaArg where
  constructor (=|)
  domain : Container
  parameter : Container

infixl 4 =|

infixl 3 |=>
(|=>) : CoparaArg -> Container -> Type
(|=>) i codomain = i.domain =%> i.parameter ⊗ codomain

-- composition of copara morphisms
(|>) : a =|k|=> b -> b =|l|=> c -> a =|(k ⊗ l)|=> c
(|>) x y =
  (\v => let v1 = x.fwd v
             z = y.fwd v1.π2 in (v1.π1 && z.π1) && z.π2) <!
  (\v, q => x.bwd v (q.π1.π1 && y.bwd (x.fwd v).π2 (q.π1.π2 && q.π2)))


