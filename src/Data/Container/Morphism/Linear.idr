module Data.Container.Morphism.Linear

import Data.Container
import Data.Linear
import Data.Linear.Bifunctor
import Data.Product
import Data.Coproduct

-- linear sigma
export infix 7 @@

export typebind infixr 0 =@

public export
0 (=@) : (x : Type) -> (x -> Type) -> Type
x =@ f = (1 e : x) -> f e


public export
record Σ1 (a : Type) (f : (0 _ : a) -> Type) where
  constructor (@@)
  1 π1 : a
  1 π2 : f π1

||| Linear lenses are closed lenses where the codomain is linear
||| This is a restriction on closed lenses
public export
record Linear (c1, c2 : Container) where
  constructor MkSLin
  1 fn : (x : c1.shp) =@ Σ1 c2.shp (\y => c2.pos y -@ c1.pos x)

compose : Linear a b -> Linear b c -> Linear a c
compose (MkSLin fn) (MkSLin gn) = MkSLin $ \arg =>
    let (arg2 @@ kn) = fn arg
        (arg3 @@ ln) = gn arg2
    in arg3 @@ (\1 x => kn (ln x))

identity : Linear a a
identity = MkSLin $ \arg => arg @@ id

fromConst1 : (y : b) =@ ((x : a) =@ b' y -@ a' x) -@ Linear (MkCont a a') (MkCont b b')
fromConst1 y f = MkSLin $ \1 arg => y @@ f arg

fromConst2 : (fwd : a -@ b) =@ (((0 x : a) -> b' (fwd x) -@ a' x) -> Linear (MkCont a a') (MkCont b b'))
fromConst2 fwd bwd = MkSLin $ \arg =>
  fwd arg @@ bwd arg

fstLens : Linear (Const2 (LPair a a) (LPair b a)) (Const2 a b)
fstLens = MkSLin weirdFst
  where
    weirdFst : ((1 x : LPair a a) -> Σ1 a (\y => b -@ LPair b a))
    weirdFst (fst # snd) = fst @@ (# snd)

export infixl 7 ⊗@

-- linear tensor
public export
(⊗@) : (c1, c2 : Container) -> Container
(⊗@) c1 c2 = (x : LPair c1.shp c2.shp) !> case x of (x1 # x2) => LPair (c1.pos x1) (c2.pos x2)

data LEither : Type -> Type -> Type where
  (<+) : a -@ LEither a b
  (+>) : b -@ LEither a b

-- linear coproduct
(+) : (c1, c2 : Container) -> Container
(+) c1 c2 = (x : LEither c1.shp c2.shp) !> case x of
                                                (<+ x1) => c1.pos x1
                                                (+> x2) => c2.pos x2

parallel : Linear a b -@ Linear x y -@ Linear (a ⊗@ x) (b ⊗@ y)
parallel (MkSLin fn) (MkSLin gn) = MkSLin $ \(a1 # a2) =>
    let (bv @@ fn') = fn a1
        (yv @@ gn') = gn a2
    in (bv # yv) @@ (bimap fn' gn')


