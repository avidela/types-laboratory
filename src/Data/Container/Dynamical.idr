module Data.Container.Dynamical

import Data.Container
import Data.Container.Morphism
import Data.Coproduct
import Data.Product
import Data.Category.Ops

data State = S0 | S1 | S2

data Message = Orange | Green


transitionSimple : Message -> State -> Maybe State
transitionSimple Orange S0 = Just S1
transitionSimple Orange S1 = Just S2
transitionSimple Orange S2 = Nothing
transitionSimple Green S0 = Just S0
transitionSimple Green S1 = Just S0
transitionSimple Green S2 = Nothing

data Allowed : Message -> State -> Type where
  S0S1 : Allowed Orange S0
  S0S0 : Allowed Green S0
  S1S2 : Allowed Orange S1
  S1S0 : Allowed Green S1

transitionDependent : (st : State) -> (m : Message) -> Allowed m st -> State
transitionDependent S0 Orange S0S1 = S1
transitionDependent S0 Green S0S0 = S0
transitionDependent S1 Orange S1S2 = S2
transitionDependent S1 Green S1S0 = S0

isLegal : (m : Message) -> (st : State) -> Maybe (Allowed m st)
isLegal Orange S0 = Just S0S1
isLegal Orange S1 = Just S1S2
isLegal Orange S2 = Nothing
isLegal Green S0 = Just S0S0
isLegal Green S1 = Just S1S0
isLegal Green S2 = Nothing

asLens : Const State =%> (() :- Message) + (() :- Void)
asLens = (\case S0 => <+ (); S1 => <+ () ; S2 => +> ()) <!
         (\case S0 => \case Green => S0
                            Orange => S1
                S1 => \case Green => S0
                            Orange => S2
                S2 => absurd)

data MouseEvent = MouseDown | MouseUp | MouseMove

record MooreMachine where
  constructor MkMoore
  State : Type
  Output : Type
  Input : Type
  return : State -> Output
  update : State -> Input -> State

MooreAsLens : (m : MooreMachine) ->
    -- the state of the machine
    Const m.State =%>
    -- The interface of the machine
    (m.Output :- m.Input)
MooreAsLens m = m.return <! m.update

data MouseAction = Click | Drag
data MouseState = WaitingUp | WaitingDown | Clicked | Dragged

updateMouseState : MouseState -> MouseEvent -> MouseState
updateMouseState WaitingUp MouseDown = WaitingDown
updateMouseState WaitingUp MouseUp = WaitingUp -- impossible
updateMouseState WaitingUp MouseMove = Dragged
updateMouseState WaitingDown MouseDown = WaitingDown -- impossible
updateMouseState WaitingDown MouseUp = Clicked
updateMouseState WaitingDown MouseMove = Dragged
updateMouseState Clicked MouseDown = WaitingDown -- same as WaitingUp
updateMouseState Clicked MouseUp = WaitingUp -- impossible
updateMouseState Clicked MouseMove = WaitingUp -- moving after click
updateMouseState Dragged MouseDown = Dragged -- impossible
updateMouseState Dragged MouseUp = WaitingUp -- end dragging
updateMouseState Dragged MouseMove = Dragged -- still dragging

MouseController : MooreMachine
MouseController = MkMoore
  { State = MouseState
  , Output = Maybe MouseAction
  , Input = MouseEvent
  , return = \case WaitingUp => Nothing ; WaitingDown => Nothing ; Clicked => Just Click ; Dragged => Just Drag
  , update = updateMouseState
  }

mouseLensMachine : ? =%> ?
mouseLensMachine = MooreAsLens MouseController

record DFA where
  constructor MkDFA
  State : Type
  Input : Type
  update : State -> Input -> State
  initial : State
  accept : List State

firstMap : (dfa : DFA) -> CUnit =%> Const dfa.State
firstMap dfa = (const dfa.initial) <! (\_, _ => ())

secondMap : (dfa : DFA) -> Eq dfa.State => Const dfa.State =%> (Bool :- dfa.Input)
secondMap dfa = (`elem` dfa.accept) <! dfa.update

to2 : Bool -> () + ()
to2 True = <+ ()
to2 False = +> ()

Interface : (input, output : Type) -> Container
Interface i o = o :- i

-- This system represents a fibonacci system
fibProcess : (Const Nat ⊗ Const Nat) =%> Const Unit
fibProcess = (const ()) <!
  (flip (const $ fork f g))
  where
    f, g : Nat * Nat -> Nat
    f (x && _) = x     -- forward p to q
    -- whut? f (x && y) = x + y -- combine p and q to p

-- The state with an initial state and a running system
initFib : Const Unit =%> Const Unit
initFib = collapse             -- Close off the inputs
       ⨾ (psi `parallel` phi) -- Initialises the process
       ⨾ fibProcess           -- the process itself
  where
    -- Initial states phi's initial state is 0, and psi's 1
    phi, psi : Const Unit =%> Const Nat
    phi = (const 0) <! (flip (const id))
    psi = (const 1) <! (flip (const id))

    -- Map a unit to a product of units
    collapse : Const Unit =%> Const Unit ⊗ Const Unit
    collapse = dup <! (\x => const x)

generalisedDFA : (dfa : DFA) -> Eq dfa.State => Const dfa.State =%> Interface dfa.Input Unit + CNeutral
generalisedDFA dfa = (to2 . (`elem` dfa.accept)) <! updt
  where
    updt : (x : dfa .State) -> choice (\value => dfa .Input) (\value => Void) (to2 (x `elem` dfa.accept)) -> dfa .State
    updt x y with (to2 (x `elem` dfa.accept))
      updt x y | (<+ ()) = dfa.update x y
      updt x y | (+> z) = void y


