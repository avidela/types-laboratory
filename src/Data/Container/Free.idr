module Data.Container.Free

import Data.Container
import Data.Container.W
import Data.Sigma

starC : Container -> Container -> Container
starC c x = x + c


data Star : Container -> Container -> Type where
  Pure : Star c x
  Impure : Ex c (Star c x) -> Star c x

Kleene : Container -> Container
