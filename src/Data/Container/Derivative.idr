module Data.Container.Derivative

import Data.Fin
import Data.Vect
import Data.List
import Data.Sigma
import Data.Container
import Data.Container.Descriptions.List

δ : Container -> Container
δ c = (x : Σ c.shp c.pos) !>
      Σ (c.pos x.π1) (\p => Not (p === x.π2))


toSum : Ex (δ c1 + δ c2) x -> Ex (δ (c1 + c2)) x
toSum y = ?toSum_rhs

fromSum : Ex (δ (c1 + c2)) x -> Ex (δ c1 + δ c2) x

fromCompose : Ex (δ (c1 ○ c2)) x -> Ex (δ c1 * (δ c2 ○ c1)) x

toCompose : Ex (δ c1 * (δ c2 ○ c1)) x -> Ex (δ (c1 ○ c2)) x

record ContLens  where
  constructor MkContLens
  cont : Container

fwd : ContLens -> Type -> Type
fwd c = Ex c.cont

bwd : ContLens -> Type -> Type
bwd c = Ex (δ c.cont)

record ListZip (a : Type) where
  constructor MkZip
  lefts : List a
  rights : List a

listCtx : Type -> Type
listCtx a = (List a, ListZip a)

listCtxEx :  Type -> Type
listCtxEx a = (Ex ListCont a, Ex (δ ListCont) a)

split2 : {a, b : Nat} -> (f : Fin (a + b)) -> Either (Fin a) (Fin b)
split2 f {a = 0} = Right f
split2 FZ {a = (S k)} = Left FZ
split2 (FS x) {a = (S k)} = mapFst FS (split2 x)

testSplit : split2 (the (Fin (5 + 5)) 3) === Left (the (Fin 5) 3)
testSplit = Refl

testSplit2 : split2 {a = 5, b = 5} (the (Fin (5 + 5)) 7) === Right (the (Fin 5) 2)
testSplit2 = Refl

testSplit3 : split2 {a = 5, b = 5} (the (Fin (5 + 5)) 0) === Left (the (Fin 5) 0)
testSplit3 = Refl

testSplit4 : split2 {a = 5, b = 5} (the (Fin (5 + 5)) 9) === Right (the (Fin 5) 4)
testSplit4 = Refl

-- Split a fin three ways
splitFin : {a, b : Nat} -> (f : Fin (S (a + b))) -> Either (Fin a) (Either (Fin b) (f === weakenN b (last {n = a})))
splitFin {a = 0} FZ = Right (Right Refl)
splitFin {a = 0} (FS x) = Right (Left x)
splitFin {a = (S k)} FZ = Left FZ
splitFin {a = (S k)} (FS x) = bimap FS (mapSnd (\x => cong FS x)) (splitFin x)

-- Obtain a value from two lists given a fin thats either from the left list
-- the right list, or an element that does not exist in the middle
getFromLists :
   (lefts : List a) ->
   (rights : List a) ->
   (ax : Fin (S (plus (length lefts) (length rights)))) ->
   (bx : ax === weakenN (length rights) (Fin.last {n = length lefts}) -> Void) ->
   a
getFromLists lefts rights ax bx = case splitFin ax of
    Left x => index' lefts x
    (Right (Left x)) => index' rights x
    (Right (Right x)) => void (bx x)

fromListZip : ListZip a -> Ex (δ ListCont) a
fromListZip (MkZip lefts rights) =
  MkEx (S (length lefts) + length rights ## weakenN (length rights) last)
      (\(ax ## bx) => getFromLists lefts rights ax bx)

vecToListRefl : Vect n a -> (ls : List a ** length ls === n)
vecToListRefl [] = ([] ** Refl)
vecToListRefl (x :: xs) = let (ys ** prf) = vecToListRefl xs in (x :: ys ** cong S prf)

tabulateList : {n : Nat} -> (Fin n -> a) -> (ls : List a ** length ls === n)
tabulateList f = vecToListRefl (Fin.tabulate f)

toListZip : Ex (δ ListCont) a -> ListZip a
toListZip (MkEx ex1 ex2) =
  let indicies : Vect ex1.π1 (Fin ex1.π1) = range
      t = tabulateList {n = ex1.π1} (\fn => ex2 (fn ## ?arg))
  in MkZip (fst t) ?aa
  where
    fn :
      (ex2 : Σ (Fin ex1.π1) (\p => p === ex1.π2 -> Void) -> a) ->
      (indicies : Vect ex1.π1 (Fin ex1.π1)) ->
      (List a, List a)
    fn ex2 indicies = ?fn_rhs


