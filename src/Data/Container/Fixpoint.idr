module Data.Container.Fixpoint

import Data.Container
import Data.Vect

||| W types are fixpoints of containers
public export
record W (c : Container) where
  constructor MkW
  InW : (x : c.shp) -> c.pos x -> W c

data C : Nat -> Type where
  Var : Fin n -> C n
  Embed : Container -> C n
  I : C n
  Plus : C n -> C n -> C n
  Mult : C n -> C n -> C n
  Tensor : C n -> C n -> C n
  Comp : C n -> C n -> C n
  Mu : C (S n) -> C n

run : (ctx : Vect n Container) -> (contCtx : Container) -> C n -> Container
run ctx cont (Embed x) = x
run ctx cont I = CUnit
run ctx cont (Plus x y) = run ctx cont x + run ctx cont y
run ctx cont (Mult x y) = run ctx cont x * run ctx cont y
run ctx cont (Tensor x y) = run ctx cont x ⊗ run ctx cont y
run ctx cont (Comp x y) = run ctx cont x ○ run ctx cont y
run ctx cont (Var x) = index x ctx
run ctx cont (Mu x) = run (cont :: ctx) ?wht x

{-
KleeneStar : Container -> Container
KleeneStar x = run [] (Mu $ I `Plus` (Var 0 `Comp` Embed x))
