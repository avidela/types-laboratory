module Data.Container.Cartesian.ActionLaws

import Data.Category
import Data.Category.Ops
import Data.Category.Action
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.Monoid
import Data.Category.Product
import Data.Category.NaturalTransformation

import Data.Container
import Data.Container.Morphism
import Data.Container.Category
import Data.Container.Cartesian
import Data.Container.Cartesian.Category
import Data.Container.Cartesian.Monoidal
import Data.Container.Cartesian.Action
import Data.Container.Morphism.Cartesian.Bifunctor.Cont
import Data.Container.Morphism.Cartesian.Bifunctor.Seq

import Data.Iso
import Data.Product
import Data.Sigma

import Proofs

CartActionLaws : LaxActionLaws ContCartCat Cont
CartActionLaws = MkLaxActionLaws
    { act = CartAction
    , pentagon = Action.pentagon2
    , idRight = (\a, b => let gg = triangleTwo a b in ?end)
    , idLeft = ?bluh -- (\(a !> a'), (b !> b') => triangleOne ((!>) a a') ((!>) b b'))
    }
