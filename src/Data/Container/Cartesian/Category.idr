module Data.Container.Cartesian.Category

import Data.Category
import Data.Category.NaturalTransformation
import Data.Category.Monoid

import Data.Container.Definition
import Data.Container.Cartesian
import Data.Iso

import Data.Product

import Proofs
import Proofs.Sum

%hide Prelude.Ops.infixl.(|>)
--------------------------------------------------------------------------------
-- Cartesian lenses form a category
--------------------------------------------------------------------------------

0 identityRight :
    (w, v : Container) -> (f : w =#> v) ->
    Cartesian.identity w |#> f `CartDepLensEq` f
identityRight w v (MkCartDepLens fwd bwd) = MkCartDepLensEq (\_ => Refl)
    (\_ => MkIsoEq (\_ => Refl) (\_ => Refl))

0 identityLeft :
    (w, v : Container) ->
    (f : w =#> v) ->
    f |#> Cartesian.identity v `CartDepLensEq` f
identityLeft w v (MkCartDepLens f1 bwd) = MkCartDepLensEq (\_ => Refl)
    (\_ => MkIsoEq (\_ => Refl) (\_ => Refl))

0 proofComposition :
    (f : a =#> b) -> (g : b =#> c) -> (h : c =#> d) ->
    f |#> (g |#> h) `CartDepLensEq` (f |#> g) |#> h
proofComposition (MkCartDepLens fwd1 bwd1) (MkCartDepLens fwd2 bwd2) (MkCartDepLens fwd3 bwd3) =
  MkCartDepLensEq (\_ => Refl) (\_ => MkIsoEq (\_ => Refl) (\_ => Refl))

||| Cartesian containers category, where objects are containers and morphisms are cartesian lense
public export
ContCartCat : Category Container
ContCartCat = MkCategory
  (=#>)
  (\_ => identity _)
  Cartesian.(|#>)
  (\_, _, f => cartEqToEq (identityLeft _ _ f))
  (\_, _, f => cartEqToEq (identityRight _ _ f))
  (\_, _, _, _, f, g, h => cartEqToEq (proofComposition f g h))

