module Data.Container.Cartesian.Action

import Data.Iso

import Data.Category
import Data.Category.Ops
import Data.Category.Action
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.Monoid
import Data.Category.Product
import Data.Category.NaturalTransformation

import Data.Container
import Data.Container.Sequence
import Data.Container.SeqAll
import Data.Container.Morphism
import Data.Container.Morphism.Cartesian.Bifunctor.Cont
import Data.Container.Category
import Data.Container.Cartesian
import Data.Container.Cartesian.Category
import Data.Container.Cartesian.Monoidal

import Proofs

%hide Prelude.Ops.infixl.(|>)
%hide Syntax.PreorderReasoning.Ops.infixl.(~=)

-- (#>) form a lax action of ContCart on Cont
public export
neutralActionComponent : (a : Container) -> CUnit #> a =%> a
neutralActionComponent a =
  (\x => x.ex2 ()) <!
  (\x, y, z => replace {p = \v => a.pos (x.ex2 v)} (unitUniq z) y)

export
neutralActionFwd : (a : Container) -> (x : Ex CUnit a.shp) ->
                   (neutralActionComponent a).fwd x === x.ex2 ()
neutralActionFwd a x = Refl

-- (x : ()) -> m .bwd (vx .ex2 ()) vy = m .bwd (vx .ex2 x) vy
-- funiq : (b : Type) -> (f : Unit -> b) -> (x : Unit) -> f () === f x


%unbound_implicits off
public export
neutralActionCommutativity :
    (Bifunctor.applyBifunctor CUnit ContinuationBifunctor {a = ContCartCat}) =>> idF Cont
neutralActionCommutativity = MkNT
  neutralActionComponent
  (\x, y, m => depLensEqToEq $ MkDepLensEq
      (\vx => Refl)
      (\vx, vy => funExtDep $ ?thuing
          -- (replace {p = \yy => y .pos (m .fwd (vx .ex2 yy))} (unitUniq xx) vy))
      )
  )

public export
actComp : (0 a, b, c : Container) -> a #> b #> c =%> a ○ b #> c
actComp a b c =
    (\x => MkEx (MkEx x.ex1 (ex1 . x.ex2)) (\y => ex2 (x.ex2 y.π1) y.π2)) <!
    (\x, y, z, w => y (z ## w))

-- (I ○ x) |> a <───────── I |> (x |> a)
--            ╲           ╱
--             ╲         ╱
--              ╲       ╱
--               ╲     ╱
--                ╲   ╱
--                 ▼ ▼
--                x |> a
export
0 triangleOne : (0 x, a : Container) ->
                neutralActionComponent (x #> a) ≡ (actComp CUnit  x a ⨾ fromUnitLeft x ~▷~ identity a)
triangleOne _ _ = depLensEqToEq $ MkDepLensEq
    (\v => exEqToEq $ MkExEq Refl (\_ => Refl))
    (\(MkEx v1 v2), fn => funExtDep $ \MkUnit => Refl)

-- (x ○ I) |> a <───────── x |> (I |> a)
--            ╲           ╱
--             ╲         ╱
--              ╲       ╱
--               ╲     ╱
--                ╲   ╱
--                 ▼ ▼
--                x |> a
export
0 triangleTwo : (0 x, a : Container) ->
                identity x ~▷~ neutralActionComponent a ≡ (actComp x CUnit a ⨾ fromUnitRight x ~▷~ identity a)
triangleTwo _ _ = depLensEqToEq $ MkDepLensEq
    (\_ => Refl)
    (\va, w2 => funExtDep $ \z => Refl)

-- Functor that map from x * (y * z) to x #> y #> z
0 f1 : (ContCartCat * (ContCartCat * Cont)) -*> Cont
f1 = Bifunctor.pair (idF ContCartCat) ContinuationBifunctor !*> ContinuationBifunctor

-- Functor that maps from x * (y * z) to (x ○ y) #> z
0 f2 : (ContCartCat * (ContCartCat * Cont)) -*> Cont
f2 = assocR {a = ContCartCat, b = ContCartCat, c = Cont}
    !*> Bifunctor.pair (ContCartMon).mult (idF Cont)
    !*> ContinuationBifunctor


0 actCompSquare :
    {0 a, b, c, a', b', c' : Container} ->
    (m1 : a =#> a') -> (m2 : b =#> b') -> (m3 : c =%> c') -> let
        0 comp1 : a #> b #> c =%> a' ○ b' #> c'
        comp1 = (m1 ~▷~ (m2 ~▷~ m3)) ⨾ actComp a' b' c'
        0 comp2 : a #> b #> c =%> a' ○ b' #> c'
        comp2 = actComp a b c ⨾ ((m1 ~○#~ m2) ~▷~ m3)
        in comp2 ≡ comp1
actCompSquare m1 m2 m3 = depLensEqToEq $ MkDepLensEq
    (\vx => Refl)
    (\vx, vy => Refl)

-- Natural transformation from x #> y #> z to x ○ y #> z
public export
actNT : f1 =>> f2
actNT = MkNT
    (\x => actComp x.π1 x.π2.π1 x.π2.π2)
    (\x, y, m => actCompSquare m.π1 m.π2.π1 m.π2.π2)

public export
0 univLUnitSquare :
    (0 a, a' : Container) -> (m : a =%> a') -> let
    0 c1 : CUnit #> a =%> a'
    c1 = univLUnit {c = a} ⨾ m
    0 c2 : CUnit #> a =%> a'
    c2 = (identity CUnit ~▷~ m) ⨾ univLUnit {c = a'}
    in c1 ≡ c2
univLUnitSquare a a' m = depLensEqToEq $ MkDepLensEq
    (\vx => Refl)
    (\vx, vy => funExtDep $ ?huh)

public export
actUnit : applyBifunctor {a = ContCartCat} CUnit ContinuationBifunctor =>> idF Cont
actUnit = MkNT
    (\_ => univLUnit)
    univLUnitSquare

export
0 pentagon2 :
 (0 x, y, z, w : Container) -> let
         %hint
         contCat : Category Container
         contCat = Cont

         0 map1 : x #> y #> z #> w =%> ((x ○ y) ○ z) #> w
         map1 = Start (x #> y #> z #> w -< (identity x ~▷~ actComp y z w) >-
                      (x #> y ○ z #> w) -< actComp x (y ○ z) w >-
                      (x ○ (y ○ z) #> w) -< (alpha_component x y z ~▷~ identity w) >-
                  End (x ○ y ○ z #> w))
         0 map2 : x #> y #> z #> w =%> ((x ○ y) ○ z) #> w
         map2 = actComp x y (z #> w) ⨾ actComp (x ○ y) z w
      in map1 ≡ map2
pentagon2 x y z w = Refl

export
0 pentagon : (0 x, y, z, a : Container) ->
    let 0 f1 : x #> (y #> (z #> a)) =%> x #> ((y ○ z) #> a)
        f1 = identity x ~▷~ actComp y z a

        0 f2 : x #> ((y ○ z) #> a) =%> (x ○ (y ○ z)) #> a
        f2 = actComp x (y ○ z) a

        0 f3 : (x ○ (y ○ z)) #> a =%> ((x ○ y) ○ z) #> a
        f3 = alpha_component x y z ~▷~ identity a

        0 g1 : x #> (y #> (z #> a)) =%> (x ○ y) #> (z #> a )
        g1 = actComp x y (z #> a)

        0 g2 : (x ○ y) #> (z #> a ) =%> ((x ○ y) ○ z) #> a
        g2 = actComp (x ○ y) z a

        0 f13 : x #> y #> z #> a =%> ((x ○ y) ○ z) #> a
        f13 = f1 ⨾ (f2 ⨾ f3)

        0 g12 : x #> y #> z #> a =%> ((x ○ y) ○ z) #> a
        g12 = g1 ⨾ g2
     in f13 ≡ g12
pentagon x y z w = cong2Dep (<!) Refl Refl

public export
CartAction : LaxAction {o1 = Container, o2 = Container} ContCartCat Cont
CartAction = MkLaxAction
    ContCartMon
    ContinuationBifunctor
    actNT
    actUnit



