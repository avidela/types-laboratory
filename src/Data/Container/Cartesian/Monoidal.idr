module Data.Container.Cartesian.Monoidal

import Data.Category.NaturalTransformation
import Data.Category.Monoid
import Data.Category.Bifunctor
import Data.Category.Product

import Data.Product
import Data.Iso

import Data.Container
import Data.Container.Morphism
import Data.Container.Cartesian
import Data.Container.Cartesian.Category

import Proofs
import Proofs.Sum

%hide Prelude.Ops.infixl.(|>)
%unbound_implicits off


--------------------------------------------------------------------------------
-- Cartesian lenses are monoidal wrt to ○
--------------------------------------------------------------------------------

-- Cartesian lenses are monoidal with CUnit as the neutral element and ○ as the
-- bifunctor on containers.
-- This proof is used in the proof that #> is an action since an action ▷ : c -> d -> d
-- requires c to be monoidal. In particular #> is an action with Cont# monoidal on ○
-- A monoidal category $C$ is one with:
-- - a neutral element i ∈ C, we can also represent it as a functor $I : 1 → C$.
-- - a bifunctor $⊗ : C × C → C$
-- - a left-unit natural isomorphism $λ : i \otimes x \approx x$
-- - a right-unit natural isomorphism $ρ : x \otimes i \approx x$
-- - an associativity natural isomorphism $α : x ⊗ (y ⊗ z) ≈ (x ⊗ y) ⊗ z$
--
--

-- To build the associativity natural isomorphism, we define associativity of ○
-- in the $Cont^{\#}$ category, thie will then be the component of the natural
-- isomorphism.
public export
alpha_component : (0 x, y, z : Container) -> x ○ (y ○ z) =#> (x ○ y) ○ z
alpha_component x y z = MkCartDepLens
    (\x => MkEx (MkEx x.ex1 (ex1 . x.ex2)) (\z => ex2 (x.ex2 z.π1) z.π2))
    (\x => MkIso
        (\y => y.π1.π1 ## y.π1.π2 ## y.π2)
        (\y => (y.π1 ## y.π2.π1) ## y.π2.π2)
        (\y => sigEqToEq $ MkSigEq Refl (dpairUniq _))
        (\y => sigEqToEq $ MkSigEq (dpairUniq _) Refl))

-- x ○ (y ○ z)
public export
0 compRight: (ContCartCat * (ContCartCat * ContCartCat)) -*> ContCartCat
compRight = (idF ContCartCat `pair` ComposeContCartBifunctor)
    !*> ComposeContCartBifunctor

-- (x ○ y) ○ z
public export
0 compLeft : (ContCartCat * (ContCartCat * ContCartCat)) -*> ContCartCat
compLeft = assocR {a = ContCartCat, b = ContCartCat, c = ContCartCat}
    !*> ((ComposeContCartBifunctor `pair` idF ContCartCat)
    !*> ComposeContCartBifunctor)

-- inline implementation of `compRight`'s map on morphisms
0
compTriple : {0 x, x', y, y', z, z' : Container} ->
         (m1 : x =#> x') -> (m2 : y =#> y') -> (m3 : z =#> z') ->
         x ○ (y ○ z) =#> x' ○ (y' ○ z')
compTriple m1 m2 m3 = m1 ~○#~ (m2 ~○#~ m3)

-- inline implementation of compLeft's map on morphisms
public export 0
compTripleSwap : {0 x, x', y, y', z, z' : Container} ->
         (m1 : x =#> x') -> (m2 : y =#> y') -> (m3 : z =#> z') ->
         (x ○ y) ○ z =#> (x' ○ y') ○ z'
compTripleSwap m1 m2 m3 = (m1 ~○#~ m2) ~○#~ m3

-- Given a morphism (m1, (m2, m3)) : Cont# * (Cont# * Cont#)
--
--
--                 assoc
-- x ○ (y ○ z) ------------> (x ○ y) ○ z
--     |                          |
--     |                          |
--     | m1 ○ (m2 ○ m3)           | (m1 ○ m2) ○ m3
--     |                          |
--     |                          |
--     V                          V
-- x' ○ (y ○ z) ------------> (x' ○ y') ○ z'
--                  assoc
0
alpha_commutes : {0 x, x', y, y', z, z' : Container} ->
    (m1 : x =#> x') -> (m2 : y =#> y') -> (m3 : z =#> z') -> let
          0 nx : x ○ (y ○ z) =#> (x ○ y) ○ z
          nx = alpha_component x y z

          0 ny : x' ○ (y' ○ z') =#> (x' ○ y') ○ z'
          ny = alpha_component x' y' z'

          0 Fm : x ○ (y ○ z) =#> x' ○ (y' ○ z')
          Fm = compTriple {x, x', y, y', z, z'} m1 m2 m3

          0 Gm : (x ○ y) ○ z =#> (x' ○ y') ○ z'
          Gm = compTripleSwap {x, x', y, y', z, z'} m1 m2 m3

          0 comp1 : x ○ (y ○ z) =#> (x' ○ y') ○ z'
          comp1 = nx |#> Gm

          0 comp2 : x ○ (y ○ z) =#> (x' ○ y') ○ z'
          comp2 = Fm |#> ny

      in comp1 `CartDepLensEq` comp2
alpha_commutes m1 m2 m3 = MkCartDepLensEq
    (\(MkEx x1 x2) => Refl)
    (\(MkEx x1 x2) => MkIsoEq
      (\xn => Refl)
      (\xm => Refl))

-- The two ways of mapping x ○ y ○ z are naturaly isomorphic
public export
alphaFwd : compRight =>> compLeft
alphaFwd = MkNT
    (\x => alpha_component x.π1 x.π2.π1 x.π2.π2)
    (\x, y, m => cartEqToEq $ MkCartDepLensEq
        (\xn => Refl)
        (\xn => MkIsoEq (\_ => Refl) (\_ => Refl)))

-- Associator going in the opposite direction
alpha_reverse : (0 x, y, z : Container) -> (x ○ y) ○ z =#> x ○ (y ○ z)
alpha_reverse x y z = MkCartDepLens
    (\x=> MkEx x.ex1.ex1 (\y => MkEx (x.ex1.ex2 y) (\z => x.ex2 (y ## z))))
    (\x => MkIso
      (\z => (z.π1 ## z.π2.π1) ## z.π2.π2)
      (\((z1 ## z2) ## z3) => z1 ## (z2 ## z3))
      (\((z1 ## z2) ## z3) => sigEqToEq $ MkSigEq Refl Refl)
      (\(z1 ## (z2 ## z3)) => Refl))

-- composition of associators is identity
public export 0
alpha_rev_alpha :
    (0 x, y, z : Container) ->
    alpha_reverse x y z |#> alpha_component x y z `CartDepLensEq` identity ((x ○ y) ○ z)
alpha_rev_alpha x y z = MkCartDepLensEq
    (\x => exEqToEq $ MkExEq
        (exEqToEq $ MkExEq Refl (\_ => Refl))
        (\(y1 ## y2) => Refl))
    (\(MkEx x1 x2) => MkIsoEq
        (\y => sigEqToEq $ MkSigEq (dpairUniq y.π1) Refl)
        (\((y1 ## y3) ## y2) => sigEqToEq $ MkSigEq Refl Refl))

-- composition of associators is identity, the other way around
0 alpha_alpha_rev :
    (0 x, y, z : Container) ->
    alpha_component x y z |#> alpha_reverse x y z `CartDepLensEq` identity (x ○ (y ○ z))
alpha_alpha_rev x y z = MkCartDepLensEq
    (\x => exEqToEq $ MkExEq
        Refl
        (\y => exEqToEq $ MkExEq Refl (\_ => Refl)))
    (\x => MkIsoEq
        (\y => sigEqToEq $ MkSigEq Refl (dpairUniq y.π2))
        (\y => sigEqToEq $ MkSigEq Refl (dpairUniq y.π2)))

-- Using the proof that the associator and its reverse form an isomorphism we can build
-- the natural isomorphism between the two functors compRight and compLeft
public export
alpha : compRight =~= compLeft
alpha = MkNaturalIsomorphism
  alphaFwd
  (\x => alpha_reverse x.π1 x.π2.π1 x.π2.π2)
  (\x => cartEqToEq $ alpha_alpha_rev x.π1 x.π2.π1 x.π2.π2)
  (\x => cartEqToEq $ alpha_rev_alpha x.π1 x.π2.π1 x.π2.π2)

public export
fromUnitLeft : (x : Container) -> CUnit ○ x =#> x
fromUnitLeft x = MkCartDepLens
    (\x => x.ex2 ())
    (\x => MkIso
        (\x => () ## x)
        proj2Unit
        (\y => sigEqToEq $ MkSigEq (unitUniq y.π1) (proj2UnitPi2 y))
        (\_ => Refl)
    )

public export
toUnitLeft : (x : Container) -> x =#> CUnit ○ x
toUnitLeft x = MkCartDepLens
    (\x => MkEx () (const x))
    (\x => MkIso
        proj2Unit
        (() ##)
        (\_ => Refl)
        (\y => sigEqToEq $ MkSigEq
            (unitUniq y.π1)
            (proj2UnitPi2 y))
    )

public export
fromUnitRight : (x : Container) -> x ○ CUnit =#> x
fromUnitRight x = MkCartDepLens
    ex1
    (\x => MkIso
        (\y => y ## ())
        π1
        (\y => sigEqToEq $ MkSigEq Refl (unitUniq y.π2))
        (\_ => Refl))

public export
toUnitRight : (x : Container) -> x =#> x ○ CUnit
toUnitRight x = MkCartDepLens
    (\x => MkEx x (const ()))
    (\x => MkIso
        π1
        (## ())
        (\_ => Refl)
        (\x => sigEqToEq $ MkSigEq Refl (unitUniq x.π2))
    )

public export
ContCartMon : Monoidal ContCartCat
ContCartMon = MkMonoidal
  ComposeContCartBifunctor
  CUnit
  alpha
  (MkNaturalIsomorphism
      (MkNT
          fromUnitLeft
          (\x, y, m => cartEqToEq $ MkCartDepLensEq
              (\a => Refl)
              (\z => MkIsoEq
                  (\_ => Refl)
                  (\(() ## w) => Refl)
              )
          )
      )
      toUnitLeft
      (\x => cartEqToEq $ MkCartDepLensEq
          (\y => exEqToEq $ MkExEq
              (unitUniq y.ex1)
              (\z => cong y.ex2 (unitUniq z))
          )
          (\y => MkIsoEq
              (\z => sigEqToEq $ MkSigEq (unitUniq _) (proj2UnitPi2 z))
              (\z => sigEqToEq $ MkSigEq (unitUniq z.π1) (proj2UnitPi2 z))
          )
      )
      (\x => cartEqToEq $ MkCartDepLensEq
          (\_ => Refl)
          (\y => MkIsoEq (\_ => Refl) (\_ => Refl))
      )
  )
  (MkNaturalIsomorphism
      (MkNT
          fromUnitRight
          (\x, y, m => cartEqToEq $ MkCartDepLensEq
              (\_ => Refl)
              (\z => MkIsoEq
                  (\_ => Refl)
                  (\_ => Refl)
              )
          )
      )
      toUnitRight
      (\x => cartEqToEq $ MkCartDepLensEq
          (\y => exEqToEq $ MkExEq
              Refl
              (\z => unitUniq _))
          (\y => MkIsoEq
              (\z => sigEqToEq $ MkSigEq Refl (unitUniq _))
              (\x => sigEqToEq $ MkSigEq Refl (unitUniq x.π2))
          )
      )
      (\x => cartEqToEq $ MkCartDepLensEq
          (\_ => Refl)
          (\y => MkIsoEq
              (\_ => Refl)
              (\_ => Refl)
          )
      )
  )

--------------------------------------------------------------------------------
-- Cartesian lenses are monoidal wrt to *
--------------------------------------------------------------------------------


--- Mapping of morphisms from Cont#(a, b)⊗Cont#(c, d) to Cont#(a * c, b * d)
(~*~) : {0 a, a', b, b' : Container} ->
        (a =#> a') -> (b =#> b') ->
        (a * b) =#> (a' * b')
(~*~) m1 m2 = MkCartDepLens {
  fwd = (bimap m1.fwd m2.fwd),
  bwd = (\y => IsoCoprod (m1.bwd y.π1) (m2.bwd y.π2))
  }

-- Functor preserves composition
0 prodPresComp : (a, b, c : Container * Container) ->
    (f : (a.π1 =#> b.π1) * (a.π2 =#> b.π2)) ->
    (g : (b.π1 =#> c.π1) * (b.π2 =#> c.π2)) ->
    ((f.π1 |#> g.π1) ~*~ (f.π2 |#> g.π2)) `CartDepLensEq` (f.π1 ~*~ f.π2 |#> g.π1 ~*~ g.π2)
prodPresComp _ _ _
    (MkCartDepLens fwd1 bwd1 && MkCartDepLens fwd2 bwd2)
    (MkCartDepLens fwd3 bwd3 && MkCartDepLens fwd4 bwd4) =
      MkCartDepLensEq (\_ => Refl) (\_ => MkIsoEq bimapCompose bimapCompose)

||| the (*) bifunctor preserves identities
public export
prodPresId : (v : Container * Container) ->
          CartDepLensEq (identity v.π1 ~*~ identity v.π2) (identity (uncurry (*) v))
prodPresId (v1 && v2) = MkCartDepLensEq
    projIdentity
    (\_ => MkIsoEq bifunctorId' bifunctorId')

||| Monoidal bifunctor on cartesian lense, defined by (*) on containers
public export
contcartBifunctor : Functor (ContCartCat * ContCartCat) ContCartCat
contcartBifunctor = MkFunctor
  (Product.uncurry (*))
  (\_, _, m => m.π1 ~*~ m.π2)
  (\f => cartEqToEq (prodPresId f))
  (\a, b, c, f, g => cartEqToEq (Monoidal.prodPresComp a b c f g))
