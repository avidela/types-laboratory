module Data.Container.Indexed.Doubly

import Data.Container
import Data.Sigma

record Container (i, o : Type) where
  constructor MkContainer
  cont : Container
  q : cont.shp -> o
  r : Σ cont.shp cont.pos -> i
