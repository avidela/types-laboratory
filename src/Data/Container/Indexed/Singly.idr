module Data.Container.Indexed.Singly

import Data.Coproduct
import Data.Product
import Data.Container
import Optics.Dependent

%hide Prelude.(&&)

-- Container indexed on the positions
namespace Positions

  record IxCont (index : Type) where
    constructor MkIxCont
    shape : Type
    positions : shape -> index -> Type

  public export
  record IxContMor {i, j : Type} (c : IxCont i) (d : IxCont j) where
    constructor MkIxContMor
    f : c.shape -> d.shape
    mapIdx : i -> j
    r : (s : c.shape) -> {x : i} -> d.positions (f s) (mapIdx x) -> c.positions s x

  export
  compose : {0 a : IxCont i} -> {0 b : IxCont j} -> {0 c : IxCont k} ->
            IxContMor a b -> IxContMor b c -> IxContMor a c
  compose (MkIxContMor f1 mapij b1) (MkIxContMor f2 mapjk b2) =
    MkIxContMor (f2 . f1)
               (mapjk . mapij)
               (\s, y => b1 s (b2 (f1 s) y))

-- Continuation containers
public export
record CContainer (c : Container) where
  constructor MkCContainer
  command  : c.shp -> Type
  response : (x : c.shp) -> command x -> Type
  next     : (x : c.shp) -> (cmd : command x) -> response x cmd -> c.pos x

-- record IndexedContainerMorphism
--   (ix1 : IndexedContainer i o)
--   (ix2 : IndexedContainer i o) where
--     constructor MkIxContMor
--     underlying : DLens ix1.cont ix2.cont
--     output : ix2.q . underlying.get = ix1.q
--     input : (s : ix1.cont.shp) ->
--             (p : ix2.cont.pos (underlying.get s)) ->
--             ix2.r (underlying.get s ** p) = ix1.r (s ** underlying.set s p)

-- Container indexed on the shapes
namespace Shapes
  public export
  record IxContainer (i : Type) where
    constructor MkIxCont
    shp : i -> Type
    pos : (x : i) -> shp x -> Type

  public export
  fromCont : Container -> IxContainer i
  fromCont x = MkIxCont (const x.shp) (\y, z => x.pos z)

  public export
  par : IxContainer a -> IxContainer b -> IxContainer (a * b)
  par a b = MkIxCont
    (\x => a.shp x.π1 * b.shp x.π2)
    (\x, arg => a.pos x.π1 arg.π1 * b.pos x.π2 arg.π2)

  public export
  tensor : IxContainer a -> IxContainer b -> IxContainer (a * b)
  tensor x y = MkIxCont
    (\w => x.shp w.π1 * y.shp w.π2)
    (\w, arg => x.pos w.π1 arg.π1 + y.pos w.π2 arg.π2)

  public export
  contChoice : IxContainer a -> IxContainer b -> IxContainer (a * b)
  contChoice x y = MkIxCont
    (\w => x.shp w.π1 + y.shp w.π2)
    (\w => choice (x.pos w.π1) (y.pos w.π2))

  public export
  record IxContMor {i, j : Type} (l : IxContainer i) (r : IxContainer j) where
    constructor MkIxContMor
    mapIdx : i -> j
    get : (x : i) -> (y : l.shp x) -> r.shp (mapIdx x)
    set : (x : i) -> (y : l.shp x) -> r.pos (mapIdx x) (get x y) -> l.pos x y

  public export
  IxMorSame : {i : Type} -> (l : IxContainer i) -> (r : IxContainer i) -> Type
  IxMorSame = IxContMor {i} {j=i}

  MkIxMorSame : {i : Type} -> {l, r : IxContainer i} ->
                (get : (x : i) -> (y : l.shp x) -> r.shp x) ->
                ((x : i) -> (y : l.shp x) -> r.pos x (get x y) -> l.pos x y) ->
                IxMorSame {i} l r
  MkIxMorSame f g = MkIxContMor id f g

  CIUnit : IxContainer x
  CIUnit = MkIxCont (const Unit) (\x, _ => Unit)

  public export
  mapIndex : (j -> i) -> IxContainer i -> IxContainer j
  mapIndex f x = MkIxCont (x.shp . f) (\v => x.pos (f v))


-- Type theory in containers with terms depending on containers
-- source?
namespace TT
  public export
  record TermMorphism (l : Container) (r : IxContainer l.shp) where
    constructor MkTerm
    get : (x : l.shp) -> r.shp x
    set : (x : l.shp) -> r.pos x (get x) -> l.pos x

