module Data.Container.Indexed.NFunctors

import Data.Coproduct
import Data.Product

public export
record Container (i : Type) where
  constructor MkContainer
  shape : Type
  positions : shape -> i -> Type


public export
choice : Container i -> Container i -> Container i
choice x y = MkContainer (x.shape + y.shape)
                         (choice x.positions y.positions)

public export
dirichlet : Container i -> Container i -> Container i
dirichlet x y = MkContainer (x.shape * y.shape) (\s, idx => x.positions s.π1 idx + y.positions s.π2 idx)

public export
parallel : Container i -> Container i -> Container i
parallel x y = MkContainer (x.shape * y.shape) (\s, idx => x.positions s.π1 idx  * y.positions s.π2 idx)
