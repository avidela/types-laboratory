module Data.Container.Indexed.IO

-- Command containers
public export
record IOContainer (i, o : Type) where
  constructor MkIOContainer
  command  : (x : i) -> Type
  response : (x : i) -> (c : command x) -> Type
  next     : (x : i) -> (c : command x) -> response x c -> o

