module Data.Container.Cartesian

import Data.Container.Category
import Data.Container.Extension
import Data.Container.Definition

import Data.Coproduct
import Data.Iso
import Data.Sigma

import Control.Relation

import Proofs

%default total

||| Cartesian dependent lenses. Their forward part is the same as a dependent lens
||| But their backward part is an isomorphism.
public export
record (=#>) (a, b : Container) where
  constructor MkCartDepLens
  fwd : a.shp -> b.shp
  bwd : (x : a.shp) -> Iso (b.pos (fwd x)) (a.pos x)

export infixl 0 `CartDepLensEq`

public export
toLens : a =#> b -> a =%> b
toLens x = x.fwd <! (\y => to (x.bwd y))

public export
record CartDepLensEq (a, b : dom =#> cod) where
  constructor MkCartDepLensEq
  fwdEq : (x : dom.shp) -> a.fwd x === b.fwd x
  bwdEq : (x : dom.shp) ->
          let 0 p1 : Iso (cod.pos (a.fwd x)) (dom.pos x)
              p1 = a.bwd x
              0 p2 : Iso (cod.pos (a.fwd x)) (dom.pos x)
              p2 = replace {p = \arg => Iso (cod.pos arg) (dom.pos x)} (sym $ fwdEq x) (b.bwd x)
          in IsoEq p1 p2

export
0 cartEqToEq : CartDepLensEq a b -> a === b
cartEqToEq {a = (MkCartDepLens fwd1 bwd1)} {b = (MkCartDepLens fwd2 bwd2)} (MkCartDepLensEq fwdEq bwdEq) =
  cong2Dep' MkCartDepLens (funExt fwdEq) (funExtDep $ \x => fromIsoEq _ _ (bwdEq x))

||| Composition of cartesian lenses
public export
(|#>) : {0 a, b, c : Container} -> a =#> b -> b =#> c -> a =#> c
(|#>) m1 m2 =
  let f1 : a.shp -> b.shp
      f1 = m1.fwd
      b1 : (x : a.shp) -> Iso (b.pos (f1 x)) (a.pos x)
      b1 = m1.bwd
      f2 : b.shp -> c.shp
      f2 = m2.fwd
      b2 : (x : b.shp) -> Iso (c.pos (f2 x)) (b.pos x)
      b2 = m2.bwd
  in MkCartDepLens (f2 . f1)
    (\x => transIso (b2 (f1 x)) (b1 x))

export
fwdEq : (f : a =#> b) -> (g : b =#> c) -> (x : Ex a a'.shp) ->
        (f |#> g) .fwd (x .ex1) = g .fwd (f .fwd (x .ex1))
fwdEq f g x = Refl

export
bwdEq : (f : a =#> b) -> (g : b =#> c) -> (x : Ex a a'.shp) ->
        (y : c .pos ((f |#> g) .fwd (x .ex1))) ->
        ((f |#> g).bwd (x .ex1)).to y === (f.bwd x.ex1).to ((g.bwd (f.fwd x.ex1)).to y)
bwdEq f g x y = Refl

public export
identity : (0 v : Container) -> v =#> v
identity v = MkCartDepLens id (\x => identityIso (v.pos x))


