module Data.Container.Category

import public Data.Category
import Data.Category.Indexed

import public Data.Container.Definition
import public Data.Container.Morphism.Definition

------------------------------------------------------------------------------------
-- Containers and their morphisms form a category
------------------------------------------------------------------------------------

composeIdLeft : (f : a =%> b) -> f ⨾ identity b = f
composeIdLeft (fwd <! bwd) = Refl

composeIdRight : (f : a =%> b) -> identity a ⨾ f = f
composeIdRight (fwd <! bwd) = Refl

proveAssoc : (f : a =%> b) -> (g : b =%> c) -> (h : c =%> d) ->
             f ⨾ (g ⨾ h) = (f ⨾ g) ⨾ h
proveAssoc (fwd0 <! bwd0) (fwd1 <! bwd1) (fwd2 <! bwd2) = Refl

-- The category of containers with dependent lenses as morphisms
public export
Cont : Category Container
Cont = MkCategory
  (=%>)
  (\x => identity x)
  (⨾)
  (\_, _ => composeIdLeft)
  (\_, _ => composeIdRight)
  (\_, _, _, _ => proveAssoc)

------------------------------------------------------------------------------------
-- Containers and their morphisms form a category
------------------------------------------------------------------------------------
%unbound_implicits off

record GeneralisedContainer (0 o1 : Type) (0 o2 : o1 -> Type) where
  constructor MkGenCont
  proj1 : o1
  proj2 : o2 proj1

{-
record GeneralisedContainerMor
  {0 o1 : Type} {0 o2 : o1 -> Type}
  (c1 : Category o1) (c2 : IxCategory o1 o2)
  (cont1, cont2 : GeneralisedContainer o1 o2) where
    constructor MkGenConMor
    fwd : {x, y : o1} -> x ~> y
    bwd : {x : o1} -> (0 v : o1) -> c1.mor {i =

