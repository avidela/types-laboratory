module Data.Container.Continuation

import Data.Container.Definition
import Data.Container.Morphism
import Data.Category.Monad
import Data.Category.Functor
import Data.Category.NaturalTransformation

import Data.Container.Category

public export
C : Container -> Container
C a = (x : a.shp) !> ((y : a.shp) -> a.pos y)

public export
unit : c =%> C c
unit = MkMorphism id (\x, f => f x)

public export
mult : C (C c) =%> C c
mult = MkMorphism id (\x, f, y, z =>  f z)

public export
functor : (a =%> b) -> C a =%> C b
functor m = MkMorphism m.fwd (\x, f, y => m.bwd y (f (m.fwd y)))

%unbound_implicits off
functorId : functor identity `DepLensEq` identity
functorId = MkDepLensEq (\_ => Refl) (\a,b => Refl)

functorComp : {x, y, z : _} -> {a : x =%> y} -> {b : y =%> z} -> functor (a |> b) `DepLensEq` functor a |> functor b
functorComp = MkDepLensEq (\_ => Refl) (\x, y => Refl)

--      functor mult
-- C (C (C x)) --> C (C x)
--     │            │
--     │            │
--     V            V
-- mult (C x) --> C x
diagram1 : (x : Container) -> let
  top : C (C (C x)) =%> C (C x)
  top = functor mult
  right : C (C x) =%> C x
  right = mult
  left : C (C (C x)) =%> C (C x)
  left = mult {c = C x}
  bot : C (C x) =%> C x
  bot = mult
  in top |> right `DepLensEq` left |> bot
diagram1 x = MkDepLensEq
    (\_ => Refl)
    (\a, b => Refl)

-- functor Unit
-- C x ---> C (C x)
--     ╲     │
--      ╲    │
--       ╲   V
--          C x
triangle1 : (x : Container) -> let
    top : C x =%> C (C x)
    top = functor unit
    right : C (C x) =%> C x
    right = mult
    dia : C x =%> C x
    dia = identity
    in top |> right `DepLensEq` dia
triangle1 x = MkDepLensEq (\_ => Refl) (\a, b => Refl)

-- Unit (C x)
-- C x ---> C (C x)
--     ╲     │
--      ╲    │
--       ╲   V
--          C x
triangle2 : (x : Container) -> let
    top : C x =%> C (C x)
    top = unit {c = C x}
    right : C (C x) =%> C x
    right = mult
    dia : C x =%> C x
    dia = identity
    in top |> right `DepLensEq` dia
triangle2 x = MkDepLensEq (\_ => Refl) (\a, b => Refl)

ContinuationFunctor : Functor Cont Cont
ContinuationFunctor = MkFunctor
    C
    (\x, y => functor)
    (\x => depLensEqToEq functorId)
    (\x, y, z, f, g => depLensEqToEq functorComp)

unitNT : idF Cont ==>> ContinuationFunctor
unitNT = MkNT
    (\_ => unit)
    (\x, y, m => depLensEqToEq $ MkDepLensEq (\_ => Refl) (\_, _ => Refl))

multNT : (ContinuationFunctor *> ContinuationFunctor) ==>> ContinuationFunctor
multNT = MkNT
    (\_ => mult)
    (\x, y, (MkMorphism m1 m2) => depLensEqToEq $ MkDepLensEq (\z => Refl) (\_, _ => Refl))

-- Continuation is a monad
ContinuationMonad : Monad Cont
ContinuationMonad = MkMonad
  ContinuationFunctor
  unitNT
  multNT
  (\x => depLensEqToEq (diagram1 x))
  (\x => depLensEqToEq (triangle2 x))
  (\x => depLensEqToEq (triangle1 x))
