module Data.Container.Suspended

import Data.Container
import Data.Container.Indexed
import Data.Coproduct
import Data.Product
import Data.Product.Dependent
import Optics.Dependent
import Data.Category

%default total

--  p.shp
--    ╱╲
--   ╱  ╲
--  ╱    ╲
-- ╱      ╲
-- l       r

-- Morphisms between suspended things
--  p.shp    <---------   q.shp
--    ╱╲                    ╱╲
--   ╱  ╲                  ╱  ╲
--  ╱    ╲                ╱    ╲
-- ╱      ╲              ╱      ╲
-- l       r             l       r

-- composition of suspended things
--
--  p.shp                 p.shp
--    ╱╲                    ╱╲
--   ╱  ╲                  ╱  ╲
--  ╱    ╲                ╱    ╲
-- ╱      ╲              ╱      ╲
-- l       x   ------->  x       r
-- `Suspended` is a container morphism between two indexed containers both depending on a top-level container
-- This is just a span
public export
record Suspended (p : Container) (l : IxContainer p.shp) (r : IxContainer p.shp) where
  constructor MkSuspended
  get : (x : Σ p.shp l.shp) -> r.shp x.π1
  set : (x : Σ p.shp l.shp) ->
        r.pos x.π1 (get x) ->
        l.pos x.π1 x.π2 * p.pos x.π1

identitySus : Suspended p x x
identitySus = MkSuspended π2
  (\(x ## y), z => z && ?identitySus_rhs)

composition : Suspended p a b -> Suspended p b c -> Suspended p a c
composition a b = MkSuspended
  (\w => b.get (w.π1 ## a.get w))
  (\w, arg => let v2 = b.set (w.π1 ## a.get w ) arg in
                  a.set w v2.π1)

morphism : (ls : q =%> p) -> Suspended p a b ->
           Suspended q (mapIndex ls.fwd a) (mapIndex ls.fwd b)
morphism ls ss = MkSuspended
  (\x => ss.get (ls.fwd x.π1 ## x.π2))
  (\x, w => let result = ss.set (ls.fwd x.π1 ## x.π2) w
             in result.π1 && ls.bwd x.π1 result.π2)

parameters {p : Container}

  SusCat : Category (IxContainer p.shp)
  SusCat = MkCategory
    (Suspended p)
    ?id
    ?comp
    ?idL
    ?idR
    ?ass
