module Data.Container.Tensor

import Data.Container.Definition
import Data.Container.Category
import Data.Container.Morphism.Definition
import Data.Container.Cartesian
import Data.Container.Cartesian.Category

import Data.Iso
import Data.Product

import Data.Category.Bifunctor
import Data.Category.Monoid
import Data.Category.NaturalTransformation
import Data.Category.Bicategory

import Proofs


||| The tensor on containers
||| Idris does not support unicode operator but we can use unicode identifiers with infix notation
public export
(⊗) : (c1, c2 : Container) -> Container
(⊗) c1 c2 = (x : c1.shp * c2.shp) !> c1.pos x.π1 * c2.pos x.π2

-- General things about tensors
public export
swap : (x ⊗ y) =%> (y ⊗ x)
swap =
  swap <!
  (\_ => swap)

public export
dupUnit : CUnit =%> CUnit ⊗ CUnit
dupUnit = dup <! (\x, _ => x)

public export
joinUnit : CUnit ⊗ CUnit =%> CUnit
joinUnit =
  (\_ => ()) <!
  (\_, _ => () && ())

---------------------------------------------------------------------------------------------------
||| ⊗ is a bifunctor in cont
---------------------------------------------------------------------------------------------------

public export
(~⊗~) : (a =%> b) -> (a' =%> b') -> a ⊗ a' =%> b ⊗ b'
(~⊗~) x y = (bimap x.fwd y.fwd) <!
            (\v, w => x.bwd v.π1 w.π1 && y.bwd v.π2 w.π2)

public export
parallel : (a =%> b) -> (a' =%> b') -> a ⊗ a' =%> b ⊗ b'
parallel x y = (bimap x.fwd y.fwd) <!
               (\v, w => x.bwd v.π1 w.π1 && y.bwd v.π2 w.π2)

public export
TensorBifunctor : Bifunctor Cont Cont Cont
TensorBifunctor = MkFunctor
    (uncurry (⊗))
    (\x, y, m => m.π1 ~⊗~ m.π2)
    (\v => cong2Dep (<!) (funExt $ \(v1 && v2) => Refl)
                         (funExtDep $ \v => funExt $ \(v1 && v2) => Refl))
    (\a, b, c, f, g => Refl)

---------------------------------------------------------------------------------------------------
||| Cont is monoidal in ⊗
---------------------------------------------------------------------------------------------------
public export
TensorMonoidal : Monoidal Cont
TensorMonoidal = MkMonoidal
    TensorBifunctor
    CUnit
    (MkNaturalIsomorphism
        (MkNT
            (\v => assocR <! (\x, y => assocL y))
            (\a, b, m => Refl))
        (\v => assocL <! (\x, y => assocR y))
        (\v => cong2Dep
                 (<!)
                 (funExt $ \(a && (b && c)) => Refl)
                 (funExtDep $ \v => funExt $ \(g1 && (g2 && g3)) => Refl))
        (\v => cong2Dep
                 (<!)
                 (funExt $ \((x1 && x2) && x3) => Refl)
                 (funExtDep $ \v => funExt $ \((x1 && x2) && x3) => Refl)))
    (MkNaturalIsomorphism
        (MkNT
          (\_ =>
            π2 <!
            (\a, b => MkUnit && b))
          (\a, b, m => Refl))
        (\_ =>
          (MkUnit &&) <!
          (\_ => π2))
        (\c => cong2Dep
          (<!)
          (funExt $ \(() && a) => Refl)
          (funExtDep $ \x => funExt $ \(() && b) => Refl))
        (\_ => Refl))
    (MkNaturalIsomorphism
      (MkNT
        (\v => π1 <! (\_, x => x && ()))
        (\a, b, m => Refl))
      (\v => (&& ()) <! (\_ => π1))
      (\v => cong2Dep
        (<!)
        (funExt $ \(x1 && ()) => Refl)
        (funExtDep $ \v => funExt $ \(w && ()) => Refl))
      (\_ => Refl)
      )

---------------------------------------------------------------------------------------------------
||| Parameterised dependent lenses
---------------------------------------------------------------------------------------------------

public export
ParaDLens : Bicategory Container
ParaDLens = ParaConstr Cont TensorMonoidal

---------------------------------------------------------------------------------------------------
||| ⊗ is a bifunctor in contCart
---------------------------------------------------------------------------------------------------

public export
(~⊗#~) : (a =#> b) -> (a' =#> b') -> a ⊗ a' =#> b ⊗ b'
(~⊗#~) x y = MkCartDepLens
    (bimap x.fwd y.fwd)
    (\v => IsoProd (x.bwd v.π1) (y.bwd v.π2))

public export
TensorBifunctorCart : Bifunctor ContCartCat ContCartCat ContCartCat
TensorBifunctorCart = MkFunctor
    (uncurry (⊗))
    (\x, y, m => m.π1 ~⊗#~ m.π2)
    (\v => cartEqToEq $ MkCartDepLensEq
        prodUniq
        (\vx => MkIsoEq prodUniq prodUniq))
    (\a, b, c, f, g => cartEqToEq $ MkCartDepLensEq
        (\_ => Refl) (\_ => MkIsoEq (\_ => Refl) (\_ => Refl)))

