module Data.Container.Functor

import Data.Container.Morphism
import Data.Container.Category
import Data.Container
import Data.Product
import Data.Sigma

import Data.Category.Functor

private infixr 1 -!>

parmap : {0 f : Container -> Container} ->
         {pure : forall a. a =%> f a} ->
         {join : forall a. f (f a) =%> f a} ->
         ((a ⊗ b) -!> c) =%> ((f a ⊗ f b) -!> f c)
parmap = MkMorphism (bimap pure.fwd pure.fwd)
                    (\vx, vy => ?adad ## ?parmap_rhs)


parmap' : {f : Functor Cont Cont} ->
          (a ⊗ b =%> c) -> f.F a ⊗ f.F b =%> f.F c
parmap' (MkMorphism fwd bwd) = MkMorphism ?DFadad ?parmap'_rhs_0

