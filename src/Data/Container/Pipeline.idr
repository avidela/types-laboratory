module Data.Container.Pipeline

import public Pipeline.Category
import Data.Container.Definition
import Data.Container.Morphism.Definition
import public Control.Category
import Data.Vect

public export
Category (=%>) where
  id = identity _
  (.) a b = b ⨾ a

public export 0
ImplCont : Vect (2 + n) Container -> Type
ImplCont = ImplCat {arr = (=%>)}

public export
runCont : forall n.
          (p : Vect (2 + n) Container) -> ImplCat {arr = (=%>)} p ->
          Vect.head p =%> Vect.last p
runCont = RunCat

