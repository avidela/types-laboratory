module Data.Container.Lift

import Data.Container.Definition
import Data.Container.Coproduct
import Data.Container.Sequence
import Data.Container.Morphism.Definition
import Data.Coproduct
import Data.Container.Extension
import Interface.Comonad
import Data.Sigma

-- functorial Lift
public export
(•) : (f : Type -> Type) -> Container -> Container
(•) f c = (x : c.shp) !> f (c.pos x)

namespace Coproduct
  public export
  distrib6 : {0 a, b, c, d, e, f : Container} ->
             Functor m =>
             m • (a + b + c + d + e + f) =%> m • a + m • b + m • c + m • d + m • e + m • f
  distrib6 = id <! \case (<+ (<+ (<+ (<+ (<+ x))))) => id
                         (<+ (<+ (<+ (<+ (+> x))))) => id
                         (<+ (<+ (<+ (+> x)))) => id
                         (<+ (<+ (+> x))) => id
                         (<+ (+> x)) => id
                         (+> x) => id

namespace Composition
  public export
  distrib : Comonad m =>
            m • (a ○ b) =%> (m • a) ○ (m • b)
  distrib = (\(MkEx x y) => MkEx x (\v => y (extract v)))
         <! \(MkEx x y), (z ## w) => map (\ bx => extract z ## bx) w

  public export
  distrib' : Monad m =>
            (m • a) ○ (m • b) =%> m • (a ○ b)
  distrib' = (\(MkEx x y) => MkEx x (\v => y (pure v)))
          <! \(MkEx x y), z => map π1 z ## do
              z' <- z
              let mapPureSame : map π1 z === pure z'.π1
                  mapPureSame = believe_me () -- We assume the monad is lawful
              pure $ rewrite__impl (b.pos . y) mapPureSame z'.π2

  left : m • (a ○ b) =%> m • a ○ b
  left = (\(MkEx x y) => MkEx x ?adda) <! ?left_rhs


