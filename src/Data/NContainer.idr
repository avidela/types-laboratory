||| An n-ary container
module Data.NContainer

import Data.Fin
import Data.Vect
import Data.Func

public export
record IContainer (i : Nat) where
  constructor MkIContainer
  shape : Type
  pos : Fin i -> shape -> Type

public export
BigProd : sh -> (Fin n -> sh -> Type) -> Vect n Type -> Type
BigProd x f [] = Void
BigProd x f (y :: []) = f FZ x -> y
BigProd x f (y :: (z :: xs)) = (f FZ x -> y , BigProd x (f . FS) (z :: xs))

public export
F' : {0 i : Nat} -> IContainer i -> Vect i Type -> Type
F' (MkIContainer shape pos) types = (ctx : shape ** BigProd {n=i} ctx pos types)

public export
Func : {i : Nat} -> IContainer i -> Tn i
Func = curryVect . F'

prefix 5 @|
infixl 5 |@

public export
(|@) : IContainer i -> Vect i Type -> (IContainer i, Vect i Type)
(|@) = (,)

public export
(@|) : {i : Nat} -> (IContainer i, Vect i Type) -> Type
(@|) x = F' (fst x) (snd x)
