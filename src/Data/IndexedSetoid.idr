module Data.IndexedSetoid

import Control.Relation
-- Heterogeneous indexed relation

public export
0 HIRel : {i1, i2 : Type} ->
      (i1 -> Type) -> (i2 -> Type) -> Type
HIRel a1 a2 = {0 i1, i2 : _ } -> a1 i1 -> a2 i2 -> Type

public export
HRel : Type -> Type -> Type
HRel a b = a -> b -> Type

export infix 7 ≡>
public export
0 (≡>) : HRel a b -> HRel a b -> Type
p ≡> q = {x, y : _} -> p x y -> q x y

-- _⇔_ : REL A B ℓ₁ → REL A B ℓ₂ → Set _
-- P ⇔ Q = P ⇒ Q × Q ⇒ P
-- Homogeneous indexed relation

public export
0 IRel : {i : Type} -> (i -> Type) -> Type
IRel a = HIRel a a

public export
0 Reflexive : Rel a -> Type
Reflexive rel = {0 x : a} -> rel x x

public export
0 IReflexive : (a : i -> Type) -> IRel a -> Type
IReflexive a rel = {0 x : i} -> Reflexive (\x1 : a x, x2 => rel x1 x2)

public export
record IsIndexedEquivalence {i : Type} (a : i -> Type) (eq : IRel {i} a) where
  constructor MkIEquiv
  refl : {0 x : i} -> {0 y : a x} -> eq y y
  sym : {0 x : i} -> {0 y, z: a x}  -> eq y z -> eq z y
  trans : {0 x : i} -> {0 y, z, w : a x}  -> eq y z -> eq z w -> eq y w

-- Symmetric : (A : I → Set a) → IRel A ℓ → Set _
-- Symmetric _ _∼_ = ∀ {i j} → B.Sym (_∼_ {i} {j}) _∼_
--
-- Transitive : (A : I → Set a) → IRel A ℓ → Set _
-- Transitive _ _∼_ = ∀ {i j k} → B.Trans _∼_ (_∼_ {j}) (_∼_ {i} {k})

export infix 4 ~=
public export
record IndexedSetoid (i : Type) where
  constructor MkISetoid
  carrier : i -> Type
  (~=) : IRel carrier
  isEquivalence : IsIndexedEquivalence carrier (~=)

-- record IsIndexedPreorder {i : Type} (a : i -> Type) (eq : IRel {i} a) (rel : IRel a)  where
--   constructor MkIsIPreorder
--   isEquivalence : IsIndexedEquivalence a eq
--   -- reflexive     : --∀ {i j} → (_≈_ {i} {j}) ⟨ _⇒_ ⟩ _≲_
--   reflexive : {x : i} -> {y, z : _} -> rel y z `eq` rel z y
  -- trans         : rel a b -> rel b c -> rel a c


record IndexedPreorder (i : Type) where
  constructor MkIPreorder
  carrier    : i -> Type
  eq         : IRel carrier -- The underlying equality.
  rel        : IRel carrier -- The relation.
  -- isPreorder : IsIndexedPreorder Carrier _≈_ _≲_

