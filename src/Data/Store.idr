module Data.Store

import Data.Iso
import Proofs.Extensionality

record Store (b, a : Type) where
  constructor MkStore
  position : b
  retreival : b -> a

mapStore : (a -> b) -> Store x a -> Store x b
mapStore f (MkStore p r) = MkStore p (f . r)

record VFunctor (m : Type -> Type) where
  constructor MkVFunc
  vmap : {0 a, b : Type} -> (a -> b) -> m a -> m b
  0 mapId : {0 a : Type} -> (v : m a) -> vmap {a, b = a} (\x : a => x) v === v
--   0 mapComp : (f : b -> c) -> (g : a -> b) ->
--               vmap {a, b = c} (f . g) === vmap {a = b, b = c} f . vmap {a, b} g

-- [storeFunc] Functor (Store b) where
--    map f (MkStore p r) = MkStore p (f . r)

0 Lens : (a, b : Type) -> Type
Lens a b = a -> Store b a

idLens : Lens x x
idLens y = MkStore y id

0 presIdFunc : {0 x, a : Type} -> (v : Store x a) -> Store.mapStore (\v : a => v) v === v
presIdFunc (MkStore x1 x2) = Refl

storeFunc : VFunctor (Store x)
storeFunc = MkVFunc mapStore
    presIdFunc

0 FStore : (a, b : Type) -> Type
FStore a b = (0 f : Type -> Type) ->
             VFunctor f ->
             (b -> f b) -> f a

toStore : FStore b a -> Store a b
toStore f = f (Store a) storeFunc idLens

fromStore : Store a b -> FStore b a
fromStore store f functor x = functor.vmap store.retreival (x store.position)

0 toFrom : (x : FStore b a) -> fromStore (toStore x) === x
toFrom x with (toStore x) proof pr
  toFrom x | store with (fromStore store) proof qr
    toFrom x | store | fstore = trans (sym qr) $
      rewrite sym pr in funExtDep0 $ \arg =>
        funExt $ \xn =>
        funExt $ \yn => ?ass

StoreFuncIso : FStore b a `Iso` Store a b
StoreFuncIso = MkIso
    toStore
    fromStore
    (\(MkStore x y) => Refl)
    toFrom

record IStore (a, b, c : Type) where
  position : a
  retreival : c -> b

