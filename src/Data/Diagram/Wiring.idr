module Data.Diagram.Wiring

import Data.Container
import Data.Container.Morphism
import Data.Product
import Data.List.Elem
import Data.Category.Ops

fold1' : a -> (a -> a -> a) -> List a -> a
fold1' x f [] = x
fold1' x f (y :: []) = y
fold1' x f (y :: (z :: xs)) = f y (fold1' x f (z :: xs))

data CableKind
  -- exposed to the outside but also available insice
  = External
  -- Only internal cable not exposed to the outside
  | Internal

Eq CableKind where
  External == External = True
  Internal == Internal = True
  _ == _ = False

record Port a where
  constructor MkPort
  internal : a
  external : a

Functor Port where
  map f p = MkPort (f p.internal) (f p.external)

record Box ty where
  constructor MkBox
  inputs : Port (List ty)
  outputs : Port (List ty)

Functor Box where
  map f b = MkBox (map (map f) b.inputs) (map (map f) b.outputs)

-- A list of Containers with lenses in between them
data LensList : List Container -> Type where
  Empty : LensList []
  Cons : {b1, b2 : Container} -> (c : b1 =%> b2) -> LensList cs -> LensList (b2 :: cs)

codomains : LensList cs -> List Container
codomains Empty = []
codomains (Cons {b2} c xs) = b2 :: codomains xs

domains : LensList cs -> List Container
domains Empty = []
domains (Cons {b1} c xs) = b1 :: domains xs

giveDynamics : (sys : List Container) -> (lenses : LensList sys) -> fold1' CUnit (⊗) (domains lenses) =%> fold1' CUnit (⊗) (codomains lenses)
giveDynamics ([]) Empty = identity _
giveDynamics (y :: []) (Cons c Empty) = c
giveDynamics (y :: (z :: xs)) (Cons c (Cons x w)) =
  c `parallel` giveDynamics (z :: xs) (Cons x w)

record System where
  constructor MkSys
  boxes : List (Box Type)


interface Universe ty where
  U : ty -> Type

-- connect the output of a to the input of b
connect : (x, y : Box ty) -> (wire : ty) -> (Box ty, Box ty)
connect x y wire = ({inputs.internal $= (wire ::)} x, {outputs.internal $= (wire ::)} y)

-- connect a box to itself
selfConnect : Box ty -> (wire : ty) -> Box ty
selfConnect x wire = {inputs.internal $= (wire ::)}
                    ({outputs.internal $= (wire ::)} x)

test : Wiring.fold1' Unit (*) [] === Unit
test = Refl

test2 : Wiring.fold1' Unit (*) [Int] === Int
test2 = Refl

test3 : Wiring.fold1' Unit (*) [Int, String] === (Int * String)
test3 = Refl

test4 : Wiring.fold1' Unit (*) [Int, String, Double] === (Int * (String * Double))
test4 = Refl

toContainer : Universe ty => Box ty -> Container
toContainer x = fold1' Unit (*) (U <$> x.outputs.external) :- (fold1' Unit (*) (U <$> x.inputs.external))

exposeOutputWire : Universe ty => (box : Box ty) -> Elem x box.outputs.internal -> (box' : Box ty ** Elem x box'.outputs.external)
exposeOutputWire (MkBox inputs (MkPort (x :: xs) external)) Here = (MkBox inputs (MkPort xs (x :: external)) ** Here)
exposeOutputWire (MkBox inputs (MkPort (y :: xs) external)) (There z) =
  let (MkBox inputs' (MkPort xs exts) ** r') = exposeOutputWire (MkBox inputs (MkPort xs external)) z in
      ?end -- (MkBox inputs' (MkPort (y :: xs) exts) ** There r')



keep : Eq b => b -> List (a, b) -> List a
keep x = map fst . filter ((== x) . snd)

foldTypes : List Type -> Type
foldTypes = fold1' Unit (*)

boxSignature : Universe ty => Box ty -> Container
boxSignature x = foldTypes ((U <$> x).outputs.external) :- (foldTypes ((U <$> x).inputs.external))

systemBoundary : Universe ty => (Box ty -> Port (List ty)) -> List (Box ty) -> Type
systemBoundary f = foldTypes . concat . map (map U . external . f)

systemInputs : Universe ty => List (Box ty) -> Type
systemInputs = systemBoundary inputs

systemOutputs : Universe ty => List (Box ty) -> Type
systemOutputs = systemBoundary outputs

SystemSignature : Universe ty => List (Box ty) -> Container
SystemSignature boxes = systemOutputs boxes :- systemInputs boxes

InternalSignature : Universe ty => List (Box ty) -> Container
InternalSignature boxes = fold1' CUnit (⊗) (map boxSignature boxes)

-- examples

data A : Type where
data B : Type where
data C : Type where

Universe Type where
  U = id

plant : Box Type
plant = MkBox { inputs = MkPort {internal = [B], external = [A]}
              , outputs = MkPort {internal = [C], external = [C]}}

controller : Box Type
controller = MkBox { inputs = MkPort {internal = [C], external = []}
                   , outputs = MkPort {internal = [B], external = []}}

system : List (Box Type)
system = [plant, controller]

systemAsLens : InternalSignature Wiring.system =%> SystemSignature Wiring.system
systemAsLens = π1 <! (const (&& ()))

asLens : (system : List (Box Type)) -> InternalSignature system =%> SystemSignature system
asLens [] = id <! (const id)
asLens [MkBox (MkPort intInp intOut) (MkPort extInp extOut)] = id <! (const id)
asLens (MkBox int (MkPort [] extOut) :: (y :: xs)) = ?asLens_rhs_4 (⊗) asLens (y :: xs)
asLens (MkBox int (MkPort (ex :: exs) extOut) :: (y :: xs)) = ?asLens_rhs_1 (⊗) asLens (y :: xs)
asLens (MkBox int (MkPort extInp extOut) :: (y :: xs)) = ?asLens_rhs_3 (⊗) asLens (y :: xs)

withSemantics : s1 =%> (boxSignature Wiring.plant) -> s2 =%> (boxSignature Wiring.controller) -> s1 ⊗ s2 =%> InternalSignature Wiring.system
withSemantics (get1 <! set1) (get2 <! set2) =
  ((&& ()) . get1 . π1) <!
  (\x => bimap (set1 x.π1) (set2 x.π2))

wholeSystem : s1 =%> boxSignature Wiring.plant -> s2 =%> boxSignature Wiring.controller -> s1 ⊗ s2 =%> SystemSignature Wiring.system
wholeSystem a b = withSemantics a b ⨾ systemAsLens
