module Data.Tri

public export
data Tri a b c =
  One a | Two b | Three c
