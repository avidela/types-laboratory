module Data.Sigma


||| Dependent pairs
public export
record Σ (a : Type) (b : a -> Type) where
  constructor (##)
  ||| First projection of sigma
  π1 : a
  ||| Second projection of sigma
  π2 : b π1

public export
(.proj2) : (x : Σ a b) -> b x.π1
(.proj2) x = π2 x

namespace Erased
  export infix 7 *#
  public export
  record Σ0 (a : Type) (b : a -> Type) where
    constructor (*#)
    ||| First projection of sigma
    0 π1 : a
    ||| Second projection of sigma
    π2 : b π1


%name Σ p1,p2
