module Data.Monoid

import Data.Nat

public export
record Monoid where
  constructor MkMonoid
  T : Type -- the carrier type
  unit : T
  mult : T -> T -> T
  unitLeft : (x : T) -> mult x unit = x
  unitRight : (x : T) -> mult unit x = x
  assoc : (x, y, z : T) -> x `mult` (y `mult` z) = (x `mult` y) `mult` z


NatPlusMonoid : Monoid
NatPlusMonoid = MkMonoid Nat
  Z
  (+)
  (plusZeroRightNeutral)
  (\_ => Refl)
  plusAssociative

NatMultMonoid : Monoid
NatMultMonoid = MkMonoid Nat
  1
  (*)
  multOneRightNeutral
  multOneLeftNeutral
  multAssociative
