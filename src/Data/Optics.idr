module Data.Optics

import Data.Container
import Data.Product
import Data.Sigma
import Data.Container.Morphism

record Σ0 (0 a : Type) (0 f : a -> Type) where
  constructor (##)
  π1 : a
  π2 : f π1

record Optic (a, b : Container) where
  constructor MkOptic
  0 residual : a.shp -> Type
  top : (x : a.shp) -> b.shp * residual x
  bot : (0 x : a.shp) -> residual x -> b.pos (top x).π1 -> a.pos x

deval : {0 a : Type} -> {0 f : a -> Type} -> ((x : a) -> f x) -> (y : a) -> f y
deval f y = f y

eval : (a -> b) -> a -> b
eval f x = f x

eval0 : (0 x : c) -> {0 f, g : c -> Type} -> (f x -> g x) -> f x -> g x
eval0 x = eval

evald : (0 x : c) -> {0 f, g : c -> Type} -> (f x -> g x) -> f x -> g x
evald c = ?asd

linearToOptic : Linear a b -> Optic a b
linearToOptic (MkLin fn) = MkOptic
  (\xa => b.pos (fn xa).π1 -> a.pos xa)
  (\x => (fn x).π1 && (fn x).π2)
  (\xv, res, bv => res bv)

depToOptic : a =%> b -> Optic a b
depToOptic (MkMorphism fwd bwd) = MkOptic
    (\x => Σ a.shp (\y => x === y))
    (\x => fwd x && x ## Refl)
    (\xv, (xv ## Refl), bv => bwd xv bv)

