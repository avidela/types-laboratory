module Data.Category.RelativeMonad

import Data.Category
import Data.Category.Functor

import Data.Fin

{-
%unbound_implicits off
record RelativeMonad {0 a, b : Type} {c1 : Category a} {c2 : Category b} (f : Functor c1 c2) where
  constructor MkRelMon
  mapObj : a -> b
  unit : (val : a) -> f.F val ~> mapObj val
  ext : (v, y : a) -> (k : f.F v ~> mapObj y) -> mapObj v ~> mapObj y

  0 pres_compose : {0 v1, v2, v3 : a} ->
                   (k : f.F v1 ~> mapObj v2) ->
                   (l : f.F v2 ~> mapObj v3) ->
                     let 0 m1, m2 : (~:>) c2 (mapObj v1) (mapObj v3)
                         m1 = ext v1 v3 ((|:>) c2 k (ext v2 v3 l))
                         m2 = (|:>) c2 (ext v1 v2 k) (ext v2 v3 l)
                      in Equal { a = mapObj v1 ~> mapObj v3
                               , b = mapObj v1 ~> mapObj v3} m1 m2

  0 unit_mult : {v1, y : a} -> (k : f.F v1 ~> mapObj y) ->
                let 0 m1 : f.F v1 ~> mapObj y
                    m1 = unit v1 |> ext v1 y k
                in m1 === k

  0 unit_id : {val : a} -> ((ext val val (unit val)) === (c2.id (mapObj val)))


{-
data Lam : Nat -> Type where
  App : Lam n -> Lam n -> Lam n
  Abs : Lam (S n) -> Lam n
  Var : Fin n -> Lam n
