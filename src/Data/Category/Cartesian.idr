module Data.Category.Cartesian

import Data.Category
import Data.Category.Terminal
import Data.Category.Product
import Data.Category.Exponential

public export
record IsCartesianClosed {0 o : Type} (cat : Category o)  where
  constructor MkCCC
  prod : HasProduct cat
  exp : HasExp cat prod
  term : HasTerminal cat

