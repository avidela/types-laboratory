module Data.Category.Indexed

import Data.Category
import Data.Sigma

public export
record CarryCategory (ix : Type) (m : ix -> Type) where
  constructor MkCarryCat
  i : ix
  mor : m i -> m i -> Type
  id : (0 a : m i) -> mor a a
  comp : {0 a, b, c : m i} -> mor a b -> mor b c -> mor a c
  idL : {0 a, b : m i} -> {0 f : mor a b} -> comp (id {a}) f === f
  idR : {0 a, b : m i} -> {0 f : mor a b} -> comp f (id {a=b}) === f
  assoc : {0 a, b, c, d : m i} ->
          {f : mor a b} ->
          {g : mor b c} ->
          {h : mor c d} ->
          comp f (comp g h) === comp (comp f g) h

public export
record IxCategory (ix : Type) (m : ix -> Type) where
  constructor MkIxCat
  mor : {i : ix} -> m i -> m i -> Type
  id : {i : ix} -> (0 a : m i) -> mor a a
  comp : {i : ix} -> {0 a, b, c : m i} -> mor a b -> mor b c -> mor a c
  idL : {i : ix} -> {0 a, b : m i} -> {0 f : mor a b} -> comp (id {a}) f === f
  idR : {i : ix} -> {0 a, b : m i} -> {0 f : mor a b} -> comp f (id {a=b}) === f
  assoc : {i : ix} -> {0 a, b, c, d : m i} ->
          {f : mor a b} ->
          {g : mor b c} ->
          {h : mor c d} ->
          comp f (comp g h) === comp (comp f g) h

{-
toCat : (icat : CarryCategory index map) -> Category (map icat.i)
toCat icat = MkCategory
    icat.mor
    (\v => icat.id v)
    (\f, g => icat.comp f g)
    (\a => icat.idR)
    (\a => icat.idL)
    (\f, g, h => icat.assoc {f, g, h})

toMap : (icat : IxCategory index map) -> (idx : index) -> Category (map idx)
toMap icat idx = MkCategory
    icat.mor
    (\iv => icat.id {i=idx} iv)
    (icat.comp {i=idx})
    ?groth_rhs4444
    ?groth_rhs5
    ?groth_rhs6

