module Data.Category.Set

import Data.Category
import Data.Category.Monoid
import Data.Category.Bifunctor
import Data.Category.Functor
import Data.Category.Product
import Data.Category.NaturalTransformation
import Data.Category.Initial
import Data.Category.Bicategory

import Data.Product

import Proofs.Extensionality
import Proofs.Product

public export
Fn : Type-> Type -> Type
Fn a b = a -> b

public export
Set : Category Type
Set = MkCategory
  Fn
  (\_ => id)
  (\f, g => g . f)
  (\_, _, _ => Refl)
  (\_, _, _ => Refl)
  (\_, _, _, _, _, _, _ => Refl)

---------------------------------------------------------------------------------
-- set is monoidal with cartesian product
---------------------------------------------------------------------------------
SetMonoidal : Monoidal Set
SetMonoidal = MkMonoidal
  multFunctor
  Unit
  alpha
  leftUnitor
  rightUnitor
  where
    multFunctor : Bifunctor Set Set Set
    multFunctor = MkFunctor
      (uncurry (*))
      (\_, _ => uncurry bimap )
      (\x => funExt $ \(x && y) => Refl)
      (\_, _, _, f, g => Refl)

    psi : (v : Type * (Type * Type)) ->
          v.π1 * (v.π2.π1 * v.π2.π2) -> (v.π1 * v.π2.π1) * v.π2.π2
    psi v x = (x.π1 && x.π2.π1) && x.π2.π2

    phi : (v : Type * (Type * Type)) ->
          (v .π1 * (v .π2) .π1) * (v .π2) .π2 ->
          v .π1 * ((v .π2) .π1 * (v .π2) .π2)
    phi v x = x.π1.π1 && (x.π1.π2 && x.π2)

    alpha : let 0 f1, f2 : (Set * (Set * Set)) -*> Set
                f1 = (pair {a = Set, b = Set, c = Set * Set, d = Set} (idF _) multFunctor )
                   !*> multFunctor
                f2 = assocR {a = Set, b = Set, c = Set}
                   !*> ((pair {a = Set * Set} multFunctor (idF Set)) !*> multFunctor)
            in f1 =~= f2
    alpha = MkNaturalIsomorphism (MkNT psi (\a, b, m => Refl))
              phi
              (\xv => funExt $ \(ys && (ys2 && ys3)) => Refl)
              (\xn => funExt $ \((y1 && y2) && y3) => Refl)

    leftUnitor :
        let 0 leftAppliedMult : Set -*> Set
            leftAppliedMult = applyBifunctor {a = Set} Unit multFunctor
        in leftAppliedMult =~= idF Set
    leftUnitor = MkNaturalIsomorphism
      (MkNT (\_ => π2) (\x, y, m => Refl))
      (\x, y => MkUnit && y)
      (\_ => funExt $ \(MkUnit && v2) => Refl)
      (\_ => Refl)

    rightUnitor :
        let 0 rightAppliedMult : Set -*> Set
            rightAppliedMult = applyBifunctor' {b = Set} Unit multFunctor
        in rightAppliedMult =~= idF Set
    rightUnitor = MkNaturalIsomorphism
        (MkNT (\_ => π1) (\x, y, m => Refl))
        (\_, x => x && MkUnit)
        (\_ => funExt $ \(v && ()) => Refl)
        (\_ => Refl)

---------------------------------------------------------------------------------
-- para construction on Set
---------------------------------------------------------------------------------
setPara : Bicategory Type
setPara = ParaConstr Set SetMonoidal

---------------------------------------------------------------------------------
-- para construction on Set
---------------------------------------------------------------------------------
public export
SetProducts : HasProduct Set
SetProducts = MkProd
    (*)
    π1
    π2
    (\f, g, x => f x && g x)
    (\f, g => Refl)
    (\f, g => Refl)
    (\Refl, Refl => funExt $ \x => prodUniq _)

