module Data.Category.Bicategory.Set

import Data.Category.Bicategory
import Data.Category.Set
import Data.Category.Functor
import Data.Category.FunctorCat
import Data.Category.NaturalTransformation
import Data.Sigma
import Data.Product

import Syntax.PreorderReasoning
%hide Relation.Isomorphism.infix.(~=)
%hide Prelude.Ops.infixl.(|>)
%hide Prelude.(|>)

horzId : {o1, o2, o3 : Type} ->
    {a : Category o1} -> {b : Category o2} -> {c : Category o3} ->
    (f1 : Functor a b) -> (f2 : Functor b c) ->
    identity {f = f1} -*- identity {f = f2} `NTEq` identity {f = f1 !*> f2}
horzId (MkFunctor fo1 fm1 fi1 fc1) (MkFunctor fo2 fm2 fi2 fc2) = MkNTEq $ \x =>  Calc $
    |~ (|:>) c (fm2 (fo1 x) (fo1 x) (b.id (fo1 x))) (fm2 (fo1 x) (fo1 x) (fm1 x x (a.id x)))
    ~~ (|:>) c (c.id (fo2 (fo1 x))) (fm2 (fo1 x) (fo1 x) (fm1 x x (a.id x)))
        ...(cong (\xn => (|:>) c xn (fm2 (fo1 x) (fo1 x) (fm1 x x (a.id x))))
                (fi2 (fo1 x)))
    ~~ fm2 (fo1 x) (fo1 x) (fm1 x x (a.id x)) ...(c.idLeft _ _ _)

private infixl 1 ||>

horzComp :
    {o1, o2, o3 : Type} ->
    {c1 : Category o1} -> {c2 : Category o2} -> {c3 : Category o3} ->
    (a, b, c : (c1 -*> c2) * (c2 -*> c3)) ->
    (f : (a.π1 =>> b.π1) * (a.π2 =>> b.π2)) ->
    (g : (b.π1 =>> c.π1) * (b.π2 =>> c.π2)) ->
    let 0 h1, h2 : (a.π1 !*> a.π2) =>> (c.π1 !*> c.π2)
        h1 = (f.π1 !!> g.π1) -*- (f.π2 !!> g.π2)
        h2 = f.π1 -*- f.π2 !!> g.π1 -*- g.π2
    in h1 `NTEq` h2
horzComp a b c
    (p1@(MkNT p1c p1s) && p2@(MkNT p2c p2s))
    (f1@(MkNT f1c f1s) && f2@(MkNT f2c f2s))
    = MkNTEq $ \vn => let
      (||>) : {a, b, c : o3} -> a ~> b -> b ~> c -> a ~> c
      (||>) = (|:>) c3
      0 p2comp : (~:>) c3 (a.π2.mapObj (a.π1.mapObj vn)) (b.π2.mapObj (a.π1.mapObj vn))
      p2comp = p2.component (a.π1.mapObj vn)
      0 f1comp : (~:>) c2 (b.π1.mapObj vn) (c.π1.mapObj vn)
      f1comp = f1.component vn
      0 f2comp : (~:>) c3 (b.π2.mapObj (a.π1.mapObj vn)) (c.π2.mapObj (a.π1.mapObj vn))
      f2comp = f2.component (a.π1.mapObj vn)
      0 p1comp : (~:>) c2 (a.π1.mapObj vn) (b.π1.mapObj vn)
      p1comp = p1.component vn
      0 f1mapped : (~:>) c3 (c.π2.mapObj (b.π1.mapObj vn)) (c.π2.mapObj (c.π1.mapObj vn))
      f1mapped = c.π2.mapHom (b.π1.mapObj vn) (c.π1.mapObj vn) f1comp
    in Calc $
    |~
        ( p2comp
          ||>
          f2comp
        )
        ||>
        (c.π2.mapHom (a.π1.mapObj vn) (c.π1.mapObj vn)
          ((|:>) c2 p1comp f1comp)
        )
    ~~ (
         (
           p2comp
           ||>
           f2comp
         )
         ||>
         (
           c.π2.mapHom _ _ p1comp
           ||>
           c.π2.mapHom _ _ f1comp
         )
       )
        ...(cong ((p2comp ||> f2comp) ||>)
              (c.π2.presComp _ _ _ p1comp f1comp))
    ~~ (
         p2comp
         ||>
         (
           f2comp
           ||>
           (
             c.π2.mapHom _ _ p1comp
             ||>
             c.π2.mapHom _ _ f1comp
           )
         )
       ) ..<(c3.compAssoc _ _ _ _ p2comp f2comp
           (
             c.π2.mapHom _ _ p1comp
             ||>
             c.π2.mapHom _ _ f1comp
           ))
    ~~ (
         p2comp
         ||>
         (
           (
             f2comp
             ||>
             c.π2.mapHom _ _ p1comp
           )
           ||>
           f1mapped
         )
       ) ...(cong (p2comp ||>)
               (c3.compAssoc _ _ _ _
                  f2comp
                  (c.π2.mapHom _ _ p1comp)
                  f1mapped
               )
            )
    ~~ (
         p2comp
         ||>
         (
           (
             b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp
             ||>
             f2.component ((b .π1) .mapObj vn)
           )
           ||>
           f1mapped
         )
       )
        ...(cong
               (\xx : (~:>) c3 (b.π2.mapObj (a.π1.mapObj vn)) (c.π2.mapObj (b.π1.mapObj vn))
                    => p2comp ||> (xx ||> f1mapped))
               { a = f2comp ||> c.π2.mapHom _ _ p1comp
               , b = b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp ||> f2.component ((b .π1) .mapObj vn) }
               (f2s (a.π1.mapObj vn)((b .π1) .mapObj vn) (p1c vn))
           )
    ~~ (
         p2comp
         ||>
         (
           b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp
           ||>
           (
             f2.component ((b .π1) .mapObj vn)
             ||>
             c.π2.mapHom (b.π1.mapObj vn) (c.π1.mapObj vn) f1comp
           )
         )
       ) ..<(cong (p2comp ||>)
           (c3.compAssoc _ _ _ _
               (b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp)
               (f2.component (b.π1.mapObj vn))
               (c.π2.mapHom (b.π1.mapObj vn) (c.π1.mapObj vn) f1comp)
               ))
    ~~ (
         (
           p2comp
           ||>
           (b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp)
         )
         ||>
         (
           (f2.component ((b .π1) .mapObj vn))
           ||>
           (c.π2.mapHom (b.π1.mapObj vn) (c.π1.mapObj vn) f1comp)
         )
       )
         ...(c3.compAssoc _ _ _ _
                p2comp
                (b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp)
                (
                  (f2.component ((b .π1) .mapObj vn))
                  ||>
                  (c.π2.mapHom (b.π1.mapObj vn) (c.π1.mapObj vn) f1comp)
                )
            )
    ~= (|:>) c3
         (
           p2comp ||>
           (b.π2.mapHom (a.π1.mapObj vn) (b.π1.mapObj vn) p1comp))
         ((f1 -*- f2) .component vn)
    ~= (|:>) c3 ((p1 -*- p2) .component vn) ((f1 -*- f2) .component vn)


horizontalMorNT : (a, b, c : Σ Type Category) ->
                  Bifunctor (FunctorCat a.π2 b.π2) (FunctorCat b.π2 c.π2) (FunctorCat a.π2 c.π2)
horizontalMorNT (o1 ## c1) (o2 ## c2) (o3 ## c3) = MkFunctor
    (uncurry (!*>))
    (\f1, f2, m => m.π1 -*- m.π2)
    (\(f1 && f2) => ntEqToEq $ horzId f1 f2)
    (\a, b, c, f, g => ntEqToEq $ horzComp a b c f g)

SetBicategory : Bicategory (Σ Type Category)
SetBicategory = MkBiCat
    (\x, y => Functor x.π2 y.π2)
    (\x, y => FunctorCat x.π2 y.π2)
    horizontalMorNT

