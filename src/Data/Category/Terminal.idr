module Data.Category.Terminal

import Data.Category
import Relation.Isomorphism

public export
record HasTerminal {0 o : Type} (cat : Category o) where
  constructor MkTerminal

  -- The terminal object
  terminal : o

  -- Each object has a morphism to the terminal object
  toTerm : (v : o) -> v ~> terminal

  -- Each morphism to the terminal object is unique
  toTermUniq : (v : o) -> (m : v ~> terminal) -> m = toTerm v
