module Data.Category.Profunctor

import public Data.Category.Functor
import public Data.Category.Product
import public Data.Category.Set

export infixl 0 -/>

public export
(-/>) : {o : Type} -> (a, b : Category o) -> Type
(-/>) a b = Functor (b.op * a) Set

proComp : a -/> b -> b -/> c -> a -/> c
proComp (MkFunctor mapO1 _ _ _) (MkFunctor mapO2 _ _ _) = MkFunctor
    ?proComp_rhs1
    ?proComp_rhs2
    ?proComp_rhs3
    ?proComp_rhs4

-- idri arrows are profunctors
idrisPro : Set -/> Set
idrisPro = MkFunctor
    (uncurry Fn)
    (\a, b, f, g, x => f.π2 (g (f.π1 x)))
    (\x => Refl)
    (\a, b, c, f, g => Refl)

