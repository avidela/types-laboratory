module Data.Category.Double

import Data.Container
import Data.Container.Indexed
import Data.Category
import Optics.Dependent
import Optics.Parameterised
import Proofs.Extensionality
import Proofs.Congruence
import Relation.Isomorphism

record Morphism (obj : Type) where
  constructor MkMorphism
  m : obj -> obj -> Type
  id : {o : obj} -> m o o
  compose : {a, b, c : obj} -> m a b -> m b c -> m a c
  0 compose_id : {a, b : obj} ->
                 (f : m a b) -> (f `compose` id {o=b}) = f
  0 compose_assoc : {a, b, c, d : obj} ->
                  (f : m a b) -> (g : m b c) -> (h : m c d) ->
                  f `compose` (g `compose` h) = (f `compose` g) `compose` h


-- A double category
record DoubleCat where
  constructor MkDoubleCat
  obj : Type
  vert : Morphism obj
  horz : Morphism obj -- Those should only hold up to isomorphism, not equality

FunctionsAreMorphisms : Morphism Type
FunctionsAreMorphisms = MkMorphism
  (\x, y => x -> y)
  Prelude.id
  (\f, g, x => g (f x))
  (\f => Refl)
  (\f, g, h => Refl)

IOContainersAreMorphisms : Morphism Type
IOContainersAreMorphisms = MkMorphism
  IOContainer
  idIO
  composeIO
  composeIdCont
  compose_assoc
  where
    composeIdCont : {a, b : Type} ->
                    (f : IOContainer a b) ->
                    f `composeIO` Indexed.idIO = f
    composeIdCont (MkIOContainer c q r) =
      ?end

    compose_assoc : {a, b, c, d : Type} ->
                  (f : IOContainer a b) ->
                  (g : IOContainer b c) ->
                  (h : IOContainer c d) ->
                  f `composeIO` (g `composeIO` h) = (f `composeIO` g) `composeIO` h
    compose_assoc (MkIOContainer c1 q1 r1)
                  (MkIOContainer c2 q2 r2)
                  (MkIOContainer c3 q3 r3)
      = ?heuihi

ParaLensAreDoubleCat : DoubleCat
ParaLensAreDoubleCat = MkDoubleCat
  { obj = Type
  , vert = FunctionsAreMorphisms
  , horz = ?dIndexedContainer
  }


  {-

--       k
--   A ----> B
--   |       |
-- f |       | g
--   |       |
--   V       V
--   C ----> D
--       l
  one_cell : (a, b, c, d : obj) ->
             (f : vert a c) ->
             (g : vert b d) ->
             (k : horz a b) ->
             (l : horz c d) ->
             Type
--       k                  k'
--   A ----> B          B ----> E
--   |       |          |       |
-- f |       | g  =>  g |       | h
--   |       |          |       |
--   V       V          V       V
--   C ----> D          D ----> F
--       l                  l'
  composeHorizontal : one_cell a b c d f g k l ->
                      one_cell b e d f g h k' l' ->
                      one_cell a e c f

