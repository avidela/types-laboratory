module Data.Category.Monoid.Symmetric

import Data.Category.Monoid
import Data.Category
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation

%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

record SymmetricMon (c : Category o) where
  mon : Monoidal c
  swap : (x, y : o) -> mon.mult.mapObj (x && y) ~> mon.mult.mapObj (y && x)

  -- check that swapping twice is the identity
  -- x ⊗ y ================= x ⊗ y
  --       ╲                ↗︎
  --        ╲              ╱
  --         ╲            ╱
  --     swap ╲          ╱ swap
  --           ╲        ╱
  --            ╲      ╱
  --             ↘︎    ╱
  --             x ⊗ y
  isIso : (x, y : o) -> let
    0 swap2 : mon.mult.mapObj (x && y) ~> mon.mult.mapObj (x && y)
    swap2 = swap x y |> swap y x
    0 cid : mon.mult.mapObj (x && y) ~> mon.mult.mapObj (x && y)
    cid = c.id _
    in swap2 === cid

  -- coherence diagram with unit
  --              unitR
  -- x ⊗ I ────────────────> x
  --       ╲                ↗︎
  --        ╲              ╱
  --         ╲            ╱
  --     swap ╲          ╱ unitL
  --           ╲        ╱
  --            ╲      ╱
  --             ↘︎    ╱
  --             I ⊗ x
  unitOK : (x : o) -> let
      0 swapMor : mon.mult.mapObj (x && mon.I) ~> mon.mult.mapObj (mon.I && x)
      swapMor = swap x mon.I
      0 leftMor: mon.mult.mapObj (x && mon.I) ~> x
      leftMor = mon.rightUnitor.nat.component x
      0 rightMor : mon.mult.mapObj (mon.I && x) ~> x
      rightMor = mon.leftUnitor.nat.component x
      0 compMor : mon.mult.mapObj (x && mon.I) ~> x
      compMor = swapMor |> rightMor
      in leftMor === compMor

  -- pentagonal diagram
  -- (x ⊗ y) ⊗ z ───────> (y ⊗ x) ⊗ z
  --      │                    │
  --      ▼                    ▼
  -- x ⊗ (y ⊗ z)          y ⊗ (x ⊗ z)
  --      │                    │
  --      ▼                    ▼
  -- (y ⊗ z) ⊗ x ───────> y ⊗ (z ⊗ x)
  assocOk : (x, y, z : o) -> let
      0 (⊗) : o -> o -> o
      (⊗) x y = mon.mult.mapObj (x && y)
      0 p1 : (x ⊗ y) ⊗ z ~> (y ⊗ x) ⊗ z
      0 p2 : (y ⊗ x) ⊗ z ~> y ⊗ (x ⊗ z)
      0 p3 : y ⊗ (x ⊗ z) ~> y ⊗ (z ⊗ x)
      0 p4 : (x ⊗ y) ⊗ z ~> x ⊗ (y ⊗ z)
      0 p5 : x ⊗ (y ⊗ z) ~> (y ⊗ z) ⊗ x
      0 p6 : (y ⊗ z) ⊗ x ~> y ⊗ (z ⊗ x)
      0 comp1, comp2 : (x ⊗ y) ⊗ z ~> y ⊗ (z ⊗ x)
      comp1 = p1 |> p2 |> p3
      comp2 = p4 |> p5 |> p6
      p1 = mon.mult.mapHom _ _ (swap x y && c.id z)
      p2 = mon.alpha.φ (y && (x && z))
      p3 = mon.mult.mapHom _ _ (c.id y && swap x z)
      p4 = mon.alpha.φ (x && (y && z))
      p5 = swap x (y ⊗ z)
      p6 = mon.alpha.φ (y && (z && x))
   in comp1 === comp2
