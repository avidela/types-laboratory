module Data.Category.Pullback

import Data.Sigma
import Data.Category

basic : (x, y : Type) -> (f : x -> z) -> (g : y -> z) -> Type
basic x y f g = Σ x (\v => Σ y (\w => f v === g w))

record Pullback (cat : Category o) (a, b, c : o) (f : a ~> c) (g : b ~> c) where
  constructor MkPullback
  apex : o
  p1 : apex ~> a
  p2 : apex ~> b
  commutes : let 0 left : apex ~> c
                 left = p1 |> f
                 0 right : apex ~> c
                 right = p2 |> g
             in left === right
  univ : (x : o) -> (q1 : x ~> a) -> (q2 : x ~> b) ->
         Σ (x ~> apex) (\u =>
         let 0 left1 : x ~> c
             left1 = u |> (p1 |> f)
             0 left2 : x ~> c
             left2 = q1 |> f
             0 right1 : x ~> c
             right1 = u |> (p2 |> g)
             0 right2 : x ~> c
             right2 = q2 |> g
         in (left1 === left2, right1 === right2))
