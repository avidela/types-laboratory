module Data.Category.Bicategory

import Data.Sigma
import public Data.Category
import Data.Category.Monoid
import public Data.Category.Bifunctor
import Data.Category.NaturalTransformation

%unbound_implicits off
public export
record Bicategory (o : Type {- 0-cells -}) where
  constructor MkBiCat

  -- 1-cells
  0 m : o -> o -> Type

  -- vertical morphisms which morphisms are the 2-cells
  vert : (a, b : o) -> Category (m a b)

  -- horizontal morphisms
  horz : (a, b, c : o) -> Bifunctor (vert a b) (vert b c) (vert a c)

%unbound_implicits on

||| A helper to extract 2-cells out of a bicategory
public export 0
two_cell : forall o. (bc : Bicategory o) => {0 a, b : o} -> bc.m a b -> bc.m a b -> Type
two_cell x y = (~:>) (bc.vert a b) x y


private infixr 0 ~@>
private infixr 1 |@>
private infixl 8 *@*

-- The para construction forms a bicategory
public export
ParaConstr : forall o. (cat : Category o) -> Monoidal cat -> Bicategory o
ParaConstr
  (MkCategory m i c idl idr ass)
  (MkMonoidal
    (MkFunctor combine combineMap combId combComp )
    _
    (MkNaturalIsomorphism _ phi _ _)
    _
    _
  ) =
  MkBiCat
    (\x, y => Σ o (\mx => combine (mx && x) `m` y) )
    (\x, y => MkCategory
        (one_cell x y)
        (\x => i x.π1) c
        (\_, _, _ => idl _ _ _)
        (\_, _, _ => idr _ _ _)
        (\a, b, c, d, f, g, h => ass _ _ _ _ _ _ _))
    (\_, _, _ => MkFunctor
      (\x => x.π2.π1 *@* x.π1.π1 ##
                    (phi (x.π2.π1 && (x.π1.π1 && _)) |@>
                    combineMap _ _ (i x.π2.π1 && x.π1.π2) |@>
                    x.π2.π2))
      (\f, g, h => combineMap _ _ (swap h))
      (\f => combId _)
      (\a, b, c, f, g => combComp _ _ _ _ _)
      )

    where
      0 (~@>) : o -> o -> Type
      (~@>) = m
      (*@*) : o -> o -> o
      (*@*) a b = combine (a && b)

      (|@>) : {a, b, c : o} -> m a b -> m b c -> m a c
      (|@>) = c
      0 one_cell : (a, b : o) ->
                   Σ o (\mx => mx *@* a ~@> b) ->
                   Σ o (\mx => mx *@* a ~@> b) -> Type
      one_cell a b m1 m2 = m1.π1 ~@> m2.π1


