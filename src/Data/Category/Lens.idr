module Data.Category.Lens

import Data.Category.FiniteProduct
import Data.Product

import Syntax.PreorderReasoning

%unbound_implicits off

%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

proofProd : {0 a, b : Type} -> {x, y : a * b} ->
            (x.π1 === y.π1) -> (x.π2 === y.π2) -> x === y
proofProd Refl Refl {x = (fst && snd)} {y = (((fst && snd) .fst) && ((fst && snd) .snd))} =
  Refl

parameters {0 o : Type} {cat : Category o} {prod : HasProduct cat}

  -- The morphisms are pairs of morphisms in C, given two objects in C
  -- (a, a') and (b, b'), a morphism in lens is pair
  -- C(a, b) and C(a × b', a')
  -- the first one is called the "forward" part and the second the "backward"
  0 LensMor : (a, b : o * o) -> Type
  LensMor a b = (a.π1 ~> b.π1) * (a.π1 >< b.π2 ~> a.π2)

  -- The identity is given by the identity morphism on the first function
  -- and the second projection in the second, this is obvious once the types
  -- are spelt out: a -> a and a × a' -> a' (second projection)
  lensId : (x : o * o) -> LensMor x x
  lensId x = cat.id x.π1 && prod.pi2

  -- Composition is not entirely obvious, on the forwards morphism it is
  -- composition in C, but in the backward part, the composition
  -- between three objects (a, a'), (b, b'), (c, c'), given morphisms
  -- f : a -> b, f' : a × b' -> a'
  -- g : b -> c, g' : b × c' -> b'
  -- is given by the map a × c' -> a':
  -- λ x : a, y : c' => f' (x, g' (f(x), y))
  compose : {a, b, c : o * o} -> LensMor a b -> LensMor b c -> LensMor a c
  compose f g = f.π1 ⨾ g.π1
            && (prod.pi1 -*- (Product.mapFst cat prod _ _ _ f.π1 ⨾ g.π2)) ⨾ f.π2
-- ((prod.pi1 -*- (Product.mapFst cat prod _ _ _ g.π1 ⨾ h.π2)) ⨾ g.π2)
  0
  composeAssoc : (a, b, c, d : o * o) ->
                 (f : LensMor a b) -> (g : LensMor b c) -> (h : LensMor c d) ->
                 (f `compose` (g `compose` h) {a = b, b = c, c = d}) {a, b, c = d}
                 === ((f `compose` g) {a, b, c} `compose` h) {a, b = c, c = d}
  composeAssoc a b c d f g h = proofProd (cat.compAssoc _ _ _ _ _ _ _) $ Calc $
    |~ (compose f (compose g h)).π2

    ~~ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ f.π1 ⨾ (compose g h).π2))
        ⨾ f.π2 ...(Refl)

    ~~ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ f.π1
        ⨾ ((prod.pi1 -*- (Product.mapFst cat prod _ _ _ g.π1 ⨾ h.π2)) ⨾ g.π2)))
        ⨾ f.π2 ...(Refl)

    ~~ ((prod.pi1 -*- (Product.mapFst cat prod _ _ _ (f.π1 ⨾ g.π1) ⨾ h.π2))
        ⨾ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ f.π1 ⨾ g.π2)))
        ⨾ f.π2
      ...(cong (\x => (|:>) cat x f.π2) $ Calc $

          |~ prod.pi1
              -*-
             (Product.mapFst cat prod _ _ _ f.π1
               ⨾ ((prod.pi1 -*- (Product.mapFst cat prod _ _ _ g.π1 ⨾ h.π2)) ⨾ g.π2))

          ~~ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ (f.π1 ⨾ g.π1) ⨾ h.π2))
             ⨾ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ f.π1 ⨾ g.π2))
             ...(?rororo)
          )

    ~~ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ (f.π1 ⨾ g.π1) ⨾ h.π2))
        ⨾ ((prod.pi1 -*- (Product.mapFst cat prod _ _ _ f.π1 ⨾ g.π2)) ⨾ f.π2)
      ..<(cat.compAssoc _ _ _ _ _ _ _)

    ~~ (prod.pi1 -*- (Product.mapFst cat prod _ _ _ (f.π1 ⨾ g.π1) ⨾ h.π2)) ⨾ (compose f g).π2
      ...(Refl)

    ~~ (compose (compose f g) h).π2 ...(Refl)

-- The category of lenses is given by a category C with products.
-- Its objects are pairs of objects from C and the morphisms
-- are given by a pair of morphisms in C. Looking at the types, this
-- looks like the product category C * C^op but the morhpisms are very
-- different. Given a domain (a, a') and codomain (b, b') the forward
-- morphism is given by C(a, b) and the backward morphism is given by
-- C(a × b', a')
LensCat : forall o. (c : Category o)  -> HasFiniteProduct c -> Category (o * o)
LensCat c (MkHasFiniteProducts prod term) = MkCategory
    LensMor
    lensId
    compose
    (\a, b, w => proofProd
        (c.idLeft _ _ w.π1)
            (Calc $ |~ (prod .prod (prod .pi1)
                         ((mapFst c prod _ _ _ w.π1) |> prod.pi2)) |> w.π2
                    ~~ c.id (a.π1 >< b.π2) |> w.π2
                        ...(cong2 ((|:>) c) (Calc $
                            |~ prod.prod prod.pi1
                                (mapFst c prod a.π1 b.π2 b.π1 w.π1 |> prod.pi2)
                            ~~ prod.prod prod.pi1 prod.pi2
                              ...(cong (prod.prod prod.pi1)
                                (mapFstSnd {cat = c, a = a.π1, b = b.π2, c = b.π1, f = w.π1}
                                ))
                            ~~ c.id (a.π1 >< b.π2)
                              ...(prod.uniq {a = a.π1, b = b.π2, c = a.π1 >< b.π2}
                                   (c.idRight _ _ prod.pi1)
                                   (c.idRight _ _ prod.pi2))
                            ) Refl)
                    ~~ w.π2 ...(c.idRight (a.π1 >< b.π2) a.π2 _)
            ))
    (\a, b, w => proofProd
      (c.idRight a.π1 b.π1 w.π1)
      (Calc $
        |~ ((pi1 prod {a=a.π1, b = b.π2} -*- (mapFst c prod a.π1 b.π2 a.π1 (id c a.π1) ⨾ w.π2))
          ⨾ prod.pi2)
        ~~ (pi1 prod {a=a.π1, b = b.π2} -*- w.π2) ⨾ prod.pi2 {a = a.π1, b = a.π2}
          ...(cong2 ((|:>) c) (cong (prod.prod prod.pi1)
              (Calc $
                |~ (mapFst c prod a.π1 b.π2 a.π1 (id c a.π1) ⨾ w .π2)
                ~~ (id c (a.π1 >< b.π2) ⨾ w.π2)
                    ...(cong2 ((|:>) c) (mapFstId {cat = c}) Refl )
                ~~ w.π2 ...(c.idRight _ _ _)
              ))
              Refl)
        ~~ w.π2 ...(prod.prodRight prod.pi1 w.π2)))
    composeAssoc
