module Data.Category.Zoo.Eq

infixr 0 ~>
infixr 0 •

record Category where
  constructor MkCategory
  obj : Type
  (~>) : obj -> obj -> Type -- morphisms
  id : (x : obj) -> x ~> x  -- identities
  (•) : {0 a, b, c : obj} -> (b ~> c) -> (a ~> b) -> (a ~> c) -- composition
  compIdLeft : {0 x, y : obj} -> (f : x ~> y) -> (f • id x) = f
  compIdRight : {0 x, y : obj} -> (g : x ~> y) -> (id y • g) = g
  compAssoc : {0 x, y, z, w: obj} -> (f : z ~> w) -> (g : y ~> z) -> (h : x ~> y) ->
              (f • (g • h)) === ((•) ((•) f g {a=y, b=z, c=w}) h {a=x, b=y, c=w})

Eq : Category
Eq = MkCategory
  { obj = Type
  , (~>) = (===)
  , id = (\_ => Refl)
  , (•) = \a, b => trans b a
  , compIdLeft = \Refl => Refl
  , compIdRight = \Refl => Refl
  , compAssoc = \Refl, Refl, Refl => Refl
  }

