module Data.Category.Unparameterised

import public Data.Sigma
import public Data.Category

import Data.Category.Functor

import Control.Order
import Control.Relation

import Data.Category.Product

public export
record Functor' (a, b : Category') where
  constructor MkFunctor'
  functor : Functor a.π2 b.π2

public export
record FunctorEq' (f, g : Functor' c d) where
  constructor MkFunctorEq'
  feq : FunctorEq f.functor g.functor

public export 0
functorEqToEq' : FunctorEq' f g -> f === g
functorEqToEq' {f = (MkFunctor' ff)} {g = MkFunctor' gg}
    (MkFunctorEq' feq) = cong MkFunctor' (functorEqToEq feq)

public export
(-*>) : (a, b : Category') -> Type
(-*>) = Functor'

public export
idF' : (x : Category') -> Functor' x x
idF' x = MkFunctor' (idF _)

public export
compose' : (x, y, z : Category') -> Functor' x y -> Functor' y z -> Functor' x z
compose' x y z f g =
  MkFunctor' (f.functor !*> g.functor )

public export
(!*>) : {x, y, z : Category'} ->
        Functor' x y -> Functor' y z -> Functor' x z
(!*>) f g = compose' _ _ _ f g

public export
[FuncRefl] Reflexive (Category') Functor' where
  reflexive = idF' _

public export
[FuncTrans] Transitive (Category') Functor' where
  transitive = compose' _ _ _

public export
[FuncPre] Preorder (Category') Functor' using FuncRefl FuncTrans where

public export
ProductCat' : Category' -> Category' -> Category'
ProductCat' c d = _ ## c.π2 * d.π2

public export
(*) : Category' -> Category' -> Category'
(*) = ProductCat'
