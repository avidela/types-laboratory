module Data.Category.Monoidal

import Data.Category.Monoid
import Data.Category.Functor
import Data.Category.Product
import Data.Category.Bifunctor
import Data.Category
import Data.Product

%hide Prelude.Interfaces.Bifunctor

-- A monoidal category is the monoid object in the monoidal category Cat
BigCat : {0 o : Type} -> Category (Category o)
BigCat = MkCategory
  Functor
  (\x => idF x)
  (!*>)
  (\_, _ => functorIdRight)
  (\_, _ => functorIdLeft)
  (\_, _, _, _ => functorComposeOk)

internalise : Category o -> Category o -> Category o
internalise c1 c2 = MkCategory
    (\x, y => (~:>) c1 x y * (~:>) c2 x y)
    (\x => c1.id x && c2.id x)
    (\f, g => (|:>) c1 f.π1 g.π1 && (|:>) c2 f.π2 g.π2 )
    (\_, _, (f && g) => cong2 (&&) (c1.idRight _ _ f) (c2.idRight _ _ g))
    (\_, _, (f && g) => cong2 (&&) (c1.idLeft _ _ f) (c2.idLeft _ _ g))
    (\_, _, _, _, f1, f2, f3 => cong2 (&&)
        (c1.compAssoc _ _ _ _ f1.π1 f2.π1 f3.π1)
        (c2.compAssoc _ _ _ _ f1.π2 f2.π2 f3.π2))

productBifunctor : {0 o : Type} -> Bifunctor (BigCat {o}) (BigCat {o}) (BigCat {o})
productBifunctor = MkFunctor
  (uncurry internalise)
  (\c1, c2, (MkFunctor mo1 mm1 mid1 mcp1 && f2) => MkFunctor
      mo1
      ?hqu
      ?mp0
      ?mp1)
  ?wch
  ?chico

{-
CatMonoidal : {0 o : Type} -> Monoidal (BigCat {o})
CatMonoidal = MkMonoidal
  { mult = productBifunctor
  , I = (DescreteCat {o})
  , alpha = ?bb
  , leftUnitor = ?ccc
  , rightUnitor = ?ddd
  }

