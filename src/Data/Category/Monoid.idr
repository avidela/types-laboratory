module Data.Category.Monoid

import Data.Category
import Data.Category.Bifunctor
import Data.Category.Iso
import Data.Category.Initial
import Data.Category.Product
import Data.Category.NaturalTransformation

%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

%default total

||| A monoidal category
public export
record Monoidal {0 o : Type} (c : Category o) where
  constructor MkMonoidal
  mult : Bifunctor c c c
  I : o
  alpha : let f1, f2 : (c * (c * c)) -*> c
              f1 = ((idF _) `pair` mult) !*> mult
              f2 = assocR !*> ((mult `pair` idF _) !*> mult)
          in f1 =~= f2
  -- apply unit to the left
  leftUnitor :
      let 0 leftAppliedMult : c -*> c
          leftAppliedMult = applyBifunctor I mult
      in leftAppliedMult =~= idF c
  -- apply unit to the right
  rightUnitor :
      let 0 rightAppliedMult : c -*> c
          rightAppliedMult = applyBifunctor' I mult
      in rightAppliedMult =~= idF c

public export
(⊗)  : {auto 0 cat : Category o} -> (mon : Monoidal cat) => o -> o -> o
(⊗) a b = mon.mult.mapObj (a && b)

public export
record MonoidObject {cat : Category o} (mon : Monoidal cat) where
  constructor MkMonObj
  obj : o
  η : mon.I ~> obj
  μ : obj ⊗ obj ~> obj
  l : let 0 m1, m2 : mon.I ⊗ obj ~> obj
          0 func   : mon.I ⊗ obj ~> obj ⊗ obj
          func = mon.mult.mapHom (mon.I && obj) (obj && obj) (η && cat.id obj)
          m1 = func |> μ
          m2 = mon.leftUnitor.nat.component obj
       in m1 === m2
  r : let 0 m1, m2 : obj ⊗ mon.I  ~> obj
          0 func   : obj ⊗ mon.I  ~> obj ⊗ obj
          func = mon.mult.mapHom (obj && mon.I) (obj && obj) (cat.id obj && η)
          m1 = func |> μ
          m2 = mon.rightUnitor.nat.component obj
       in m1 === m2

public export
record HasMonoidalProduct {0 o : Type} (cat : Category o) where
  constructor MkMonProd
  prod : HasProduct {o} cat
  init : HasInitial {o} cat
  (<>) : {a, b, c : o} -> (>:<) prod a b ~> c

--                  mapSnd (<>)
--   a >< (b >< c) ─────────────> a >< x
--        │                          │
--        │ assoc                    │
--        │                          │
--        V                          │
--  (a >< b) >< c                    │ (<>)
--        │                          │
--        │ mapFst (<>)              │
--        │                          │
--        V                          V
--      y >< c ────────────────────> z
--                   (<>)

  pentagon : {a, b, c, x, y, z : o} ->
             (|:>) cat {a = a><(b><c), b = a><x, c = z}
                        (mapSnd cat prod a (b><c) (x) ((<>) {a = b, b = c, c = x}))
                        ((<>) {a = a, b=x, c=z})
             =
                (|:>) cat {a = a><(b><c), b= y><c, c = z}
                  ((|:>) cat {a = a><(b><c), b = (a><b)><c, c = y >< c}
                    (associative cat prod a b c)
                    (mapFst cat prod (a><b) c y ((<>) {a=a,b=b,c=y} )))
                  ((<>) {a = y, b=c, c=z})

--        mapFst unit
-- I >< b ———————> a >< b
--       ╲        │
--        ╲       │
--         ╲      │
--      snd ╲     │ (<>)
--           ╲    │
--            ╲   │
--             ╲  │
--              ╲ │
--               V
--               b


  unitLeft : {a, b : o} ->
             (|:>) cat {a = init.initial >< b, b = a >< b, c = b}
               (mapFst cat prod {right = b, source = init.initial, target = a, f = init.unit a})
               ((<>) {a=a,b=b, c=b})
             =
               prod.pi2 {a=init.initial} {b}

--        mapSnd unit
-- a >< I ———————> a >< b
--       ╲        │
--        ╲       │
--         ╲      │
--      fst ╲     │ (<>)
--           ╲    │
--            ╲   │
--             ╲  │
--              ╲ │
--               V
--               a
  unitRight : {a, b : o} ->
             (|:>) cat {a = a >< init.initial , b = a >< b, c = a}
               (mapSnd cat prod {source = init.initial, left = a, target = b, f = init.unit b})
               ((<>) {a=a,b=b, c=a})
             =
               prod.pi1 {a} {b=init.initial}

