module Data.Category.Semantics.CategoricalSemantics

import Language.STLC
import Data.Category
import Data.Category.Cartesian
import Data.Category.Terminal
import Data.Category.Exponential
import Data.Category.Product
import Data.Category.Product
import Control.Relation

%default total
%hide Prelude.Z
%hide Prelude.S
%hide Language.Ops.infixr.(~>)
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
%hide Data.Category.Ops.infixr.(=>>)

parameters {objects, base : Type}
           {cat : Category objects}
           {prod : HasProduct cat}
           {exp : HasExp cat prod}
           {term : HasTerminal cat}
           {interp : base -> objects}

  namespace Types
    public export 0
    pure : Ty base -> objects
    pure (x =>> y) = [| x |] -^> [| y |]
    pure (Base x) = interp x

  namespace Contexts
    public export 0
    pure : Ctxt base -> objects
    pure [] = term.terminal
    pure (xs &. x) = [| xs |] >< [| x |]

  namespace Variables
    public export 0
    pure : gam ∋ t -> [| gam |]  ~> [| t |]
    pure Z = prod.pi2
    pure (S _ a) = prod.pi1 |> Variables.pure a

  namespace Judgements
    public export 0
    pure : {t : Ty base} -> {gam : Ctxt base} ->
           gam |- t ->
           [| gam |] ~> [| t |]

    pure (Var x) = [| x |]

    pure (Lam x {a} {b}) {gam} =
        exp.uncurryλ [|gam|] [|a|] [|b|] [|x|]

    pure (App x y {a} {b=t}) =
       (Judgements.pure x -*- Judgements.pure y) |> exp.eval (Types.pure t) (Types.pure a)

  namespace Renamings
    public export 0
    pure : {gam, del : Ctxt base} ->
             del `Rename` gam ->
             [| del |] ~> [| gam |]
    pure {gam = []} {del} f = term.toTerm [| del |]
    pure {gam = gs &. g} f =
      pure {gam = gs} (shift f) -*- Variables.pure (f g Z)

  0
  renameVar : (gam, del : Ctxt base) ->
              (t : Ty base) ->
              (sub : del `Rename` gam ) ->
              (var : gam ∋ t) ->
              Compose [|del|] [|gam|] [|t|] [|sub|] [|var|] ===
              Variables.pure (sub _ var)
  renameVar (gam &. t) del t sub Z =
    prodRight prod (Renamings.pure (shift sub))
                   (Variables.pure (sub t Z))
--
--                    [|(sub (S n))|]
--                 Δ ──────────────→ t
--                 │ ⟍               ↑
--                 │   ⟍             │
--                 │     ⟍           │
--  prod ([|sub|]) │       ⟍         │ [|n|]
--       ([|n|])   │         ⟍       │
--                 │           ⟍     │
--                 │             ⟍   │
--                 ↓               ↘︎ │
--                Γ×g ─────────────→ Γ
--                         pi1
  renameVar (gam &. g) del t sub (S _ n) =
    let rec = renameVar gam del t (shift sub) n
        interpCong = cong Variables.pure (prfShift {sub} n)
        topRight = trans rec interpCong in
        glueTriangles (prod.prodLeft _ _)
                      topRight

  0
  renameTerm : (gam, del : Ctxt base) -> (t : Ty base) ->
               (sub : del `Rename` gam) -> (term : gam |- t) ->
               Compose [|del|] [|gam|] [|t|] [|sub|] [|term|] ===
               pure (rename sub t term)

  renameTerm gam del t sub (Var x) = renameVar gam del t sub x

  renameTerm gam del (a =>> b) sub (Lam {a} body)
         = let rec = renameTerm (gam &. a) (del &. a) b (extend sub) body
               nLam = sym (exp.naturalLam (Renamings.pure sub) (Judgements.pure body))
           in nLam `trans`
              (cong (exp.uncurryλ _ _ _)
                    ((congMor1 (sym $ prod.uniq (prod.prodLeft  _ _ `trans` pi1Sub gam del sub)
                                               (prod.prodRight _ _ `trans` (cat.idRight _ _ _)))
                              (Judgements.pure body))
                    `trans` rec))
    where

      --  Δ × a
      --    │ ⟍
      --    │   ⟍  [[ forget extend sub ]]
      --  π₁│     ⟍
      --    │       ⟍
      --    ↓         ↘︎
      --    Δ ────────→ Γ
      --      [[ sub ]]
      0
      pi1Sub : {val : Ty base} -> (gam, del : Ctxt base) -> (sub : del `Rename` gam) ->
               Start ([|del|] >< [|val|] -< prod.pi1 {a = [|del|], b= [|val|]} >-
                      [|del|]            -< [|sub|] >-
                 End (Contexts.pure gam)) ===
               Renamings.pure {gam} {del = del &. val} (shift (extend sub))
      pi1Sub [] del sub = term.toTermUniq _ _
      pi1Sub (gs &. g) del sub {val} =
        let rec = pi1Sub gs del (shift sub)
        -- rec : π1 |> [[ forget sub ]] === [[ forget extend forget sub ]]
        --  Δ × a
        --    │ ⟍
        --    │   ⟍  [[ forget extend forget sub ]]
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ────────→  gs
        --      [[ forget sub ]]
        --
        -- goal : π1 |> prod [[ forget sub ]] [[ sub Z ]]
        --    === prod [[ forget forget extend sub ]] (π₁ |> [[ sub Z ]])
        --
        --  Δ × a
        --    │ ⟍
        --    │   ⟍  (prod [[ forget forget extend sub ]] (π₁ |> [[ sub Z ]])
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ──────→  gs × g
        --      (prod [[forget sub ]] [[ sub Z ]])
         in natPairing (trans rec
                       (cong (Renamings.pure {gam=gs} {del = del &. val})
                       (shiftTwice {sub})))
                       Refl

  renameTerm gam del b sub (App {a} {b} x y) =
    let f = renameTerm gam del (a =>> b) sub x
        v = renameTerm gam del a sub y
        prf2 = prfAppTerm {x=rename sub (a =>> b) x} {y=rename sub a y}
     in ?tetra -- tetrahedron (natPairing f v)
               --      prfAppTerm
               --      prf2
    where
      0
      prfAppTerm :
          {gam : Ctxt base} ->
          {a, b : Ty base} ->
          {x : gam |- a =>> b} ->
          {y : gam |- a} ->
          Compose [| gam |]
                  ([| a |] -^> [| b |] >< [| a |])
                  [|b|]
                  (prod.prod {c = [|gam|]}
                             {a = [|a|] -^> [|b|]}
                             {b = [|a|]}
                             [|x|]
                             [|y|])
                  (exp.eval [|b|] [|a|])
          === (pure (App x y))
      prfAppTerm {b=Base b} = Refl
      prfAppTerm {b=a =>> b} = Refl

  namespace Substitutions
    public export 0
    pure : {gam, del : Ctxt base} ->
              del `Subst` gam ->
              [| del |] ~> [| gam |]
    pure {gam=[]} f = term.toTerm _
    pure {gam=gs &. g} f =
      pure {gam=gs} (\v, xsub => f v (S g xsub)) -*- Judgements.pure (f g Z)

  0
  substVar : {gam, del : Ctxt base} -> {t : Ty base} -> (var : gam ∋ t) ->
             {sub : del `Subst` gam} ->
             Compose [|del|] [|gam|] [|t|] (Substitutions.pure sub) (Variables.pure var) ===
             Judgements.pure (sub t var)
  substVar {gam=gs &. t} {sub} Z = prod.prodRight _ (Judgements.pure (sub t Z))
  substVar {gam=gs &. g} (S g var) =
    let rec = substVar {gam=gs} var in glueTriangles (prod.prodLeft _ _) rec

  forgetSIsPi1Pi1 : (gam : Ctxt base) ->
                    (t, g : Ty base) ->
                    Renamings.pure (shift (\v => S {gam = gam &. t} g)) =
                    Compose (([|gam|]>< [|t|])>< [|g|])
                            (Contexts.pure gam >< Types.pure t)
                            (Contexts.pure gam)
                            (prod.pi1 {a = ([|gam|]>< [|t|]), b = [|g|]})
                            (prod.pi1 {a = [|gam|], b = [|t|]})
  forgetSIsPi1Pi1 gam t g = ?forgetSIsPi1Pi1_rhs

  0
  substTerm : (gam, del : Ctxt base) -> (t : Ty base) ->
              (sub : del `Subst` gam) -> (term : gam |- t) ->
              Start ([|del|] -< [| sub  |] >-
                     [|gam|] -< [| term |] >-
                End (Types.pure t))
             = Judgements.pure (subst sub _ term)
  substTerm gam del t sub (Var x) = substVar x
  substTerm gam del (s =>> t) sub (Lam x) =
    let rec = substTerm (gam &. s) (del &. s) t (extendJ s sub) x in
        sym (exp.naturalLam _ _)
            `trans`
        (cong (exp.uncurryλ _ _ _) $
        (congMor1 (prod.uniq (prod.prodLeft _ _ `trans` renameSub {val=s} gam del sub)
                            (prod.prodRight _ _ `trans` sym (cat.idRight _ _ _)))
                       (Judgements.pure x))
              `trans`
              rec)
      where



        0
        sIsPi : (gam : Ctxt base) -> {g : Ty base} ->
                Renamings.pure (\_ => S {gam} g) === prod.pi1 {a = [|gam|], b = (Types.pure g)}
        sIsPi [] = sym $ term.toTermUniq _ _
        sIsPi (gs &. t) {g}=
          let rec = sIsPi (gs )
              f = sym (forgetSIsPi1Pi1 gs t g)
           in prod.uniq f Refl

        -- missing:
        --  Δ × val
        --    │ ⟍
        --    │   ⟍  [[rename S (sub Z)]]
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ───────→ g
        --      [[sub Z]]
        0
        missing : (val, g : Ty base) -> (del : Ctxt base) ->
                  (term : del |- g) ->
                  Judgements.pure (rename (\v' => S {gam=del} val) g term) ===
                  Compose ([|del|] >< [|val|]) [|del|] [|g|]
                          (prod.pi1 {a = [|del|], b = [|val|]})
                          (Judgements.pure term)
        missing val g del term =
          let interm = sym $ renameTerm _ _ _ (\v => S {gam=del} val) term in
              trans interm (congMor1 (sIsPi _) _)

          -- missing val g gam del sub | (T x) = ?rest
          -- missing val g gam del sub | ((\\) x) = ?rest
          -- missing val g gam del sub | (App x y) = ?rest

        0
        renameSub : {val : Ty base} -> (Γ, Δ : Ctxt base) -> (sub : Δ `Subst` Γ) ->
                   Substitutions.pure {gam = Γ} {del = Δ &. val}
                     (\v, var => rename (\v' => S {gam=Δ} val) v (sub v var)) ===
                   Compose ([|Δ|] >< [|val|]) [|Δ|] [|Γ|]
                           (prod.pi1 {a = [|Δ|], b = [|val|]})
                           (Substitutions.pure sub)
        renameSub [] del sub = sym $ term.toTermUniq _ _
        renameSub (gs &. g) del sub {val} =
          let rec = sym $ renameSub {val} gs del (\ty, var => sub ty (S g var))
              r1 = rename (\v' => S val) g (sub g Z)
              r2 = ((rename (\v' => S val) g (sub g Z)))
              l1 = (\ty, var => sub ty (S g var))
              l2 = sub g Z
        -- missing:
        --  Δ × val
        --    │ ⟍
        --    │   ⟍  r1
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ───────→ g
        --         l1
        --
        -- rec:
        --  Δ × val
        --    │ ⟍
        --    │   ⟍  r2
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ──────→  gs
        --         l2
        --
        -- goal:
        --  Δ × val
        --    │ ⟍
        --    │   ⟍  r1 * r2
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ──────→  gs × g
        --       l1 * l2
           in sym $ natPairing (rec ) (sym $ missing _ _ del (sub g Z))

  substTerm gam del t sub (App x y) = ?substTerm_rhs_3

{-
  0
  ZeroNotSucc : {gam : Ctxt base} -> {t' : Ty base} -> {x : gam >: t'} ->
                Not (Variables.pure (the (gam &. t' >: t') Z) = Variables.pure (S t' x))
  ZeroNotSucc prf = ?wot

  0
  iVarInj : {gam : Ctxt base} -> {x : gam >: t} -> {y : gam >: t} ->
            [|x|] = [|y|] -> x = y
  iVarInj {x=Z} {y=Z} prf = Refl
  iVarInj {x=Z} {y=S t' y} prf = absurd (ZeroNotSucc prf)
  iVarInj {x=S _ x} {y} prf = ?iVarInj_rhs_3

  0
  iTermInj : {t1 : Ty base} -> {x, y : gam |- t1} ->
             [|x|] = [|y|] -> x = y
  iTermInj {t1} {gam=gam} {x=T x} {y=T y} prf = cong T (iVarInj prf)
  iTermInj {t1=a=>>b} {gam=gam} {x=T x} {y=(\\) y} prf = ?wo
  iTermInj {t1} {gam=gam} {x=T x} {y=App y y'} prf = ?iTermInj_rhs_1
  iTermInj {t1=a=>>b} {gam=gam} {x=(\\) x} {y=y} prf = ?iTermInj_rhs_2
  iTermInj {t1} {gam=gam} {x=App x x'} {y=y} prf = ?iTermInj_rhs_3

  combine : {x, y : gam |- s =>> t} ->
            {m, n : gam |- s} ->
            [| x |] = [| y |] ->
            [| m |] = [| n |] ->
            Judgements.pure (App x m) = Judgements.pure (App y n)
  combine prf prf1 = ?combine_rhs

  0
  reduceStep  : {gam : Ctxt base} -> {t : Ty base} -> {t1, t2 : gam |- t} ->
                (Debrujn.(~>) t1 t2) ->
                [| t1 |] === [| t2 |]
  reduceStep (Xi1 x {m} {l} {l'}) =
    let rec = reduceStep x
        ref : [|m|] === [|m|] = Refl in combine rec ref
  reduceStep (Xi2 x y) = ?reduceStep_rhs_2
  reduceStep {t1=App (\\ f) (_)} {t2=subst _ b' f} (BetaLam v) {gam} = ?end

  0
  reduceSame : {gam : Ctxt base} -> {a : Ty base} -> {t1, t2 : gam |- a} ->
               (t1 ->> t2) -> [| t1 |] === [| t2 |]
  reduceSame (Step x) {a} {gam} = reduceStep x
  reduceSame (ReduceRefl x) = Refl
  reduceSame (ReduceTrans x y) = trans (reduceSame x) (reduceSame y)

