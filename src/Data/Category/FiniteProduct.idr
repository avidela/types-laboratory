module Data.Category.FiniteProduct

import public Data.Category
import public Data.Category.Product
import public Data.Category.Terminal

%hide Data.Category.Product.HasProduct.prod

public export
record HasFiniteProduct {o : Type} (c : Category o) where
  constructor MkHasFiniteProducts
  prod : HasProduct c
  term : HasTerminal c

%pair HasFiniteProduct prod term
