module Data.Category.Exponential

import Data.Category
import Data.Category.Product
import Relation.Isomorphism

private infixr 8 -^:>
export infixr 8 -^>

public export
record HasExp {0 o : Type} (cat : Category o) (prod : HasProduct cat) where
  constructor MkExp
  (-^:>) : o -> o -> o
  eval : (z, y : o) -> ((y -^:> z) >< y) ~> z
  uncurryλ : (x, y, z : o) ->
        (g : (x >< y) ~> z) ->
        x ~> (y-^:>z)

  --          uncurryλ g × id
  -- x × y ──────────────────┐
  --   │                     │
  --   │                     │
  --   │                     V
  -- g │                  y->z × y
  --   │                     │
  --   │                     │
  --   V         eval        │
  --   z <───────────────────┘
  --
  ||| proof that ((uncurryλ g) -.- id) |> eval = g
  uncurryλComp : (x, y, z : o) ->
            (g : (x >< y) ~> z) ->
               Start (
                    (x >< y)
                 -< ((-.-) {a=x, prod, b = y-^:>z, c = y, d = y}
                           (uncurryλ x y z g)
                           (cat.id {v=y})) >-
                    ((y -^:> z) >< y)
                 -< (eval z y) >-
                 End z)
                === g

  ||| (arrow : x ~> x') -> uncurryλ (arrow -.- id |> g) = arrow |> uncurryλ g
  naturalLam : {x, x', y, z : o} ->
               (arrow : x ~> x') ->
               (g : (x' >< y) ~> z) ->
               let 0 (*&*) : x ~> x' -> y ~> y -> (x >< y) ~> (x' >< y)
                   (*&*) = (-.-)
                   private infixl 7 *&*
               in (uncurryλ x y z
                     (Start $ x >< y -< arrow *&* cat.id {v=y} >-
                             x' >< y -< g >- End z))
               ===
                 Start (x -< arrow >- x' -< uncurryλ x' y z g >- End (y -^:> z))

public export
0 (-^>) : (cat : Category obj) => (prod : HasProduct cat) => (exp : HasExp cat prod) =>
        obj -> obj -> obj
(-^>) = (-^:>) exp

