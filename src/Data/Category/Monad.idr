module Data.Category.Monad

import Data.Category
import Data.Category.NaturalTransformation
import Data.Category.Functor

import Data.Iso
import Proofs.Extensionality
import Control.Relation
import Control.Order
import Syntax.PreorderReasoning
import Syntax.PreorderReasoning.Generic

import Proofs.Congruence
import Proofs.UIP

%hide Prelude.Functor
%hide Prelude.Monad
%hide Prelude.(*)
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
%hide Data.Category.Ops.infixl.(<~)

public export
record Monad (c : Category o) where
  constructor MkMonad
  endo : Functor c c
  unit : idF c =>> endo
  mult : (endo !*> endo) =>> endo

  --             F mult
  -- F (F (F x)) ───────> F (F x)
  --    │                   │
  --    │ mult (F x)        │ mult
  --    │                   │
  --    V                   V
  --  F (F x)  ──────────> F x
  --              mult
  0 square : (x : o) -> let
      top : endo.mapObj (endo.mapObj (endo.mapObj x)) ~> endo.mapObj (endo.mapObj x)
      top = endo.mapHom _ _ (mult.component x)
      bot, right : endo.mapObj (endo.mapObj x) ~> endo.mapObj x
      right = mult.component x
      bot = mult.component x
      left : endo.mapObj (endo.mapObj (endo.mapObj x)) ~> endo.mapObj (endo.mapObj x)
      left = mult.component (endo.mapObj x)
      0 arm2, arm1 : endo.mapObj (endo.mapObj (endo.mapObj x)) ~> endo.mapObj x
      arm1 = (top |> right) {cat = c}
      arm2 = left |> bot
      in arm1 === arm2

  -- identity triangles
  --           unit (F x)
  --         F x ──> F (F x)
  --          │  ╲     │
  --          │   ╲    │
  --   F unit │    =   │ mult
  --          │     ╲  │
  --          V      ╲ V
  --       F (F x) ──> F x
  --              mult
  0 identityLeft : (x : o) -> let
      0 compose : endo.mapObj x ~> endo.mapObj x
      compose = (unit.component (endo.mapObj x) |> mult.component x) {cat = c}
      0 straight : endo.mapObj x ~> endo.mapObj x
      straight = c.id (endo.mapObj x)
      in compose === straight

  0 identityRight : (x : o) -> let
      0 compose : endo.mapObj x ~> endo.mapObj x
      compose = (endo.mapHom _ _ (unit.component x) |> mult.component x) {cat = c}
      0 straight : endo.mapObj x ~> endo.mapObj x
      straight = c.id (endo.mapObj x)
      in compose === straight

public export
record MonadMor {0 o : Type} {0 c : Category o} (m1, m2 : Monad c) where
  constructor MkMonadMor
  monadMap : m1.endo =>> m2.endo
  0 presPure : (m1.unit !!> monadMap) `NTEq` m2.unit
  0 presJoin : (m1.mult !!> monadMap) `NTEq` ((monadMap -*- monadMap) !!> m2.mult)

public export 0
NT_UIP : (n1, n2 : NTEq a b) -> n1 === n2
NT_UIP (MkNTEq n1) (MkNTEq n2) = congDep0 MkNTEq (funExtDep $ \vx => UIP _ _)

public export
record MonadMorEq (m1, m2 : MonadMor t s) where
  constructor MkMonadMorEq
  mapSame : m1.monadMap `NTEq` m2.monadMap

public export 0
MonadMorEqToEq : MonadMorEq m1 m2 -> m1 === m2
MonadMorEqToEq {m1 = MkMonadMor map1 p1 j1} {m2 = MkMonadMor map2 p2 j2} (MkMonadMorEq same) with (ntEqToEq same)
  MonadMorEqToEq {m1 = MkMonadMor map1 p1 j1} {m2 = MkMonadMor map1 p2 j2} (MkMonadMorEq same) | Refl = cong2Dep (\x, y => MkMonadMor map1 x y) (NT_UIP _ _) (NT_UIP _ _)

public export
IdMor : {0 o : Type} -> {c : Category o} -> (m : Monad c) -> MonadMor m m
IdMor mon = MkMonadMor
  { monadMap = identity
  , presPure = MkNTEq $ \vx => CalcWith @{EP} $
            |~ (mon.unit.component vx) |> (mon.endo.mapHom vx vx (c.id vx))
            ~~ (mon.unit.component vx) |> (c.id (mon.endo.mapObj vx)) ...(cong (mon.unit.component vx |>) (mon.endo.presId vx))
            ~~ mon.unit.component vx                             ...(c.idRight _ _ (mon.unit.component vx))
  , presJoin = MkNTEq $ \vx => CalcWith @{EP} $
            |~ (mon.mult.component vx) |> (mon.endo.mapHom vx vx (c.id vx))
            ~~ (mon.mult.component vx) |> (c.id (mon.endo.mapObj vx)) ...(cong (mon.mult.component vx |>) (mon.endo.presId _))
            ~~ (mon.mult.component vx)                        ...(c.idRight _ _ (mon.mult.component vx))

            ~~  (((c.id (mon.endo.mapObj (mon.endo.mapObj vx)))))
             |> (mon.mult.component vx)                       ..<(c.idLeft _ _ (mon.mult.component vx))

            ~~  (mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) ((c .id (mon.endo.mapObj vx))))
             |> (mon.mult.component vx)                       ..<(cong (|> mon.mult.component vx ) (mon.endo.presId (mon.endo.mapObj vx)))

            ~~  (mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (mon.endo.mapHom vx vx (c .id vx)))
             |> (mon.mult.component vx)                       ..<(cong (\x => mon.endo.mapHom _ _ x |> mon.mult.component vx) (mon.endo.presId _))

            ~~  (c.id (mon.endo.mapObj (mon.endo.mapObj vx)))
             |>((mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (mon.endo.mapHom vx vx (c .id vx)))
             |> (mon.mult.component vx))                      ..<(c.idLeft _ _ _)

            ~~  (mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (c .id (mon.endo.mapObj vx)))
             |>((mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (mon.endo.mapHom vx vx (c .id vx)))
             |> (mon.mult.component vx))                      ..<(cong (|> (mon.endo.mapHom _ _ (mon.endo.mapHom vx vx (c.id vx)) |> mon.mult.component vx)) (mon.endo.presId _))

            ~~ ((mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (c .id (mon.endo.mapObj vx)))
             |> (mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (mon.endo.mapHom vx vx (c .id vx))))
             |> (mon.mult.component vx)
                                                    ...(c.compAssoc _ _ _ _ (mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (c .id (mon.endo.mapObj vx))) (mon.endo.mapHom (mon.endo.mapObj vx) (mon.endo.mapObj vx) (mon.endo.mapHom vx vx (c .id vx))) (mon.mult.component vx))
  }

public export
MonComp : {c : Category _} -> {m1, m2, m3 : Monad c} ->
          MonadMor m1 m2 -> MonadMor m2 m3 -> MonadMor m1 m3
MonComp mor1 mor2 =
    let sym = NTESym
        trans = NTETrans
        refl = NTERefl
    in MkMonadMor
    (mor1.monadMap !!> mor2.monadMap)
    (ntAssoc _ _ _ `ntTrans` (ntProd mor1.presPure ntRefl `ntTrans` mor2.presPure))
    (CalcSmart $
        |~ m1.mult !!> (mor1.monadMap !!> mor2.monadMap)
        <~ ((m1.mult !!> mor1.monadMap) !!> mor2.monadMap)                                         ...(ntAssoc _ _ _)
        <~ (((mor1.monadMap -*- mor1.monadMap) !!> m2.mult) !!> mor2.monadMap)                     ...(ntProd mor1.presJoin ntRefl)
        <~ ((mor1.monadMap -*- mor1.monadMap) !!> (m2.mult !!> mor2.monadMap))                     ..<(ntAssoc _ _ _)
        <~ ((mor1.monadMap -*- mor1.monadMap) !!> ((mor2.monadMap -*- mor2.monadMap) !!> m3.mult)) ...(ntProd ntRefl mor2.presJoin)
        <~ (((mor1.monadMap -*- mor1.monadMap) !!> (mor2.monadMap -*- mor2.monadMap)) !!> m3.mult) ...(ntAssoc _ _ _)
        <~ (((mor1.monadMap !!> mor2.monadMap) -*- (mor1.monadMap !!> mor2.monadMap)) !!> m3.mult) ...(ntProd distribute ntRefl))

public export
monIdentityRight : {0 o : Type} -> {0 c : Category o} -> {0 a, b : Monad c} ->
                   (vx : MonadMor a b) -> MonadMorEq (MonComp vx (IdMor b)) vx
monIdentityRight {b = MkMonad (MkFunctor endoObj endoMor endoId endoComp)
                              (MkNT unitComp unitsq)
                              (MkNT multComp multsq) sq le ri} (MkMonadMor v1 v2 v3)
    = MkMonadMorEq (MkNTEq $ \vy => Calc $
      |~ (v1.component vy) |> (endoMor vy vy (c.id vy))
      ~~ (v1.component vy) |> (c.id (endoObj vy)) ...(cong (v1.component vy |>) (endoId vy))
      ~~ (v1.component vy)                        ...(c.idRight _ _ (v1.component vy)))

public export
monIdentityLeft : {0 a, b : Monad c} -> (vx : MonadMor a b) -> MonadMorEq (MonComp (IdMor a) vx) vx
monIdentityLeft vx = MkMonadMorEq $ MkNTEq $ \vy => Calc $
                     |~ (a.endo.mapHom vy vy (c.id vy)) |> (vx.monadMap.component vy)
                     ~~ c.id (a.endo.mapObj vy) |> vx.monadMap.component vy ...(cong (|> vx.monadMap.component vy) (a.endo.presId vy))
                     ~~ vx.monadMap.component vy                       ...(c.idLeft _ _ (vx.monadMap.component vy))

public export
monCompAssoc : {0 cat : Category _} -> {0 a, b, c, d : Monad cat} -> (f : MonadMor a b) -> (g : MonadMor b c) -> (h : MonadMor c d) ->
               MonadMorEq (MonComp f (MonComp g h)) (MonComp (MonComp f g) h)
monCompAssoc f g h = MkMonadMorEq $ MkNTEq $ \vx => Calc $
                     |~ (f.monadMap.component vx) |> ((g.monadMap.component vx) |> (h.monadMap.component vx))
                     ~~ ((f.monadMap.component vx) |> (g.monadMap.component vx)) |> (h.monadMap.component vx)
                     ...(cat.compAssoc _ _ _ _ _ _ _ )

-- Category of monads in C
public export
MonCat : {0 o : Type} -> (c : Category o) -> Category (Monad c)
MonCat c = MkCategory
    MonadMor
    IdMor
    MonComp
    (\_, _, vx => MonadMorEqToEq (monIdentityRight vx))
    (\_, _, vx => MonadMorEqToEq (monIdentityLeft vx))
    (\_, _, _, _, f, g, h => MonadMorEqToEq (monCompAssoc f g h))

public export
record Comonad (c : Category o) where
  constructor MkComonad
  endo : Functor c c
  counit : endo =>> idF c
  comult : endo =>> (endo !*> endo)

  --             F comult
  -- F (F (F x)) <─────── F (F x)
  --    ^                   ^
  --    │ comult (F x)      │ comult
  --    │                   │
  --    │                   │
  --  F (F x)  <────────── F x
  --              comult
  0 square : (x : o) -> let
      top : endo.mapObj (endo.mapObj (endo.mapObj x)) <~ endo.mapObj (endo.mapObj x)
      top = endo.mapHom _ _ (comult.component x)
      bot, right : endo.mapObj (endo.mapObj x) <~ endo.mapObj x
      right = comult.component x
      bot = comult.component x
      left : endo.mapObj (endo.mapObj (endo.mapObj x)) <~ endo.mapObj (endo.mapObj x)
      left = comult.component (endo.mapObj x)
      0 arm2, arm1 : endo.mapObj (endo.mapObj (endo.mapObj x)) <~ endo.mapObj x
      arm1 = (top • right) {cat = c}
      arm2 = left • bot
      in arm1 === arm2

  -- identity triangles
  --               extract (F x)
  --            F x <── F (F x)
  --             ^  ╲     ^
  --             │   ╲    │
  --   F extract │    =   │ comult
  --             │     ╲  │
  --             │      ╲ │
  --          F (F x) <── F x
  --            comult
  0 identityLeft : (x : o) -> let
      0 compose : endo.mapObj x <~ endo.mapObj x
      compose = (counit.component (endo.mapObj x) • comult.component x) {cat = c}
      0 straight : endo.mapObj x <~ endo.mapObj x
      straight = c.id (endo.mapObj x)
      in compose === straight

  0 identityRight : (x : o) -> let
      0 compose : endo.mapObj x <~ endo.mapObj x
      compose =  endo.mapHom _ _ (counit.component x) • comult.component x
      0 straight : endo.mapObj x <~ endo.mapObj x
      straight = c.id (endo.mapObj x)
      in compose === straight

fromMonad : Monad c -> Comonad c.op
fromMonad (MkMonad endo eta mu square identityLeft identityRight)
  = MkComonad
    ?beli
    ?fromMonad_rhs_1
    ?fromMonad_rhs_2
    ?fromMonad_rhs_3
    ?fromMonad_rhs_4
    ?fromMonad_rhs_5

fromComonad : Comonad c.op -> Monad c

monadComonad : Monad c `Iso` Comonad c.op
monadComonad = MkIso
    fromMonad
    fromComonad
    ?cc
    ?monadComonad_rhs


