module Data.Category.CatExp

import Proofs
import Data.Sigma
import Data.Category
import Data.Category.Product
import Data.Category.Exponential
import Data.Category.FunctorCat
import Data.Category.Functor
import Data.Category.NaturalTransformation
import Data.Category.Set

import Control.Relation
import Control.Order

import Syntax.PreorderReasoning
import Syntax.PreorderReasoning.Generic

public export
CatCat : Category Category'
CatCat = NewCat
  { objects = Category'
  , morphisms = \x, y => Functor x.π2 y.π2
  , identity = \x => idF x.π2
  , composition = (!*>)
  , identity_right = functorIdRight
  , identity_left = functorIdLeft
  , compose_assoc = functorComposeOk
  }

CatCat' : Category Category'
CatCat' = NewCat
  { objects = Category'
  , morphisms = Functor'
  , identity = idF'
  , composition = (!*>)
  , identity_right = \f => let ff = functorIdRight in ?be
  , identity_left = \f => let ff = functorIdLeft in ?ben
  , compose_assoc = \f, g, h => let ff = functorComposeOk in ?bem
  }

ProjFunctor1 : Functor (ProductCat c d) c
ProjFunctor1 = MkFunctor
  { mapObj = π1
  , mapHom = \x, y => π1
  , presId = \_ => Refl
  , presComp = \x, y, z, f, g => Refl
  }

ProjFunctor2 : Functor (ProductCat c d) d
ProjFunctor2 = MkFunctor
  { mapObj = π2
  , mapHom = \x, y => π2
  , presId = \_ => Refl
  , presComp = \x, y, z, f, g => Refl
  }

ProjFunctor1' : Functor' (c * d) c

ProjFunctor2' : Functor' (c * d) d

FunctorProd : Functor c a -> Functor c b -> Functor c (ProductCat a b)
FunctorProd f g = MkFunctor
  (\v => f.mapObj v && g.mapObj v)
  (\x, y, m => f.mapHom _ _ m && g.mapHom _ _ m)
  (\x => cong2 (&&) (f.presId _) (g.presId _))
  (\x, y, z, m1, m2 =>
       cong2 (&&) (f.presComp _ _ _ m1 m2) (g.presComp _ _ _ m1 m2))

FunctorProd' : Functor' c a -> Functor' c b -> Functor' c (ProductCat' a b)
FunctorProd' f g = MkFunctor' (FunctorProd f.functor g.functor)

FunctorPair' : {a, a', b, b' : Category'} ->
               Functor' a a' -> Functor' b b' -> Functor' (a * b) (a' * b')
FunctorPair' x y = FunctorProd' (ProjFunctor1' !*> x)
                                (ProjFunctor2' !*> y)

CatHasProduct : HasProduct CatCat
CatHasProduct = MkProd
  (\c, d => c * d)
  ProjFunctor1
  ProjFunctor2
  FunctorProd
  ?CatHasProduct_rhs4
  ?CatHasProduct_rhs3
  ?CatHasProduct_rhs9

CatHasProduct' : HasProduct CatCat'
CatHasProduct' = MkProd
  (\c, d => c * d)
  ProjFunctor1'
  ProjFunctor2'
  FunctorProd'
  ?CatHasProduct_rhs4'
  ?CatHasProduct_rhs'3
  ?CatHasProduct_rhs9'

%hide Prelude.Ops.infixl.(|>)
%hide Prelude.(|>)

FunctorEval : {0 o1, o2 : Type} ->
              (c : Category o1) ->
              (d : Category o2) ->
              FunctorCat c d * c -*> d
FunctorEval c d = MkFunctor
  (\fo => fo.π1.mapObj fo.π2)
  (\f@(MkFunctor x1 x1' _ _ && x2), g@(MkFunctor y1 y1' _ _ && y2),
    m@(MkNT m1 _ && m2) => let
      goal : x1 x2 ~> y1 y2
      goal = m1 x2 |> y1' x2 y2 m2
      -- this also works
      -- goal2 : x1 x2 ~> y1 y2
      -- goal2 = x1' _ _ m2 |> m1 y2
   in goal)
  (\f@(MkFunctor f1 f2 pid _  && x2) => Calc $
      |~ (f2 x2 x2 (c .id x2) |> f2 x2 x2 (c.id x2))
      ~~ d.id (f1 x2) |> d.id (f1 x2) ...(cong2 (|>) (pid x2) (pid x2))
      ~~ d.id (f1 x2) ...(d.idLeft {} )
  )

  (\(MkFunctor f1 f2 fpid fcmp && fval),
    (MkFunctor g1 g2 gpid gcmp && gval),
    (MkFunctor h1 h2 hpid hcmp && hval),
    (MkNT mcomp msqr && mMor),
    (MkNT ncomp nsqr && nMor) => Calc $
      |~ ((mcomp fval |> ncomp fval)) |> (h2 fval hval (mMor |> nMor))
      ~~ ((mcomp fval |> ncomp fval)) |> (h2 fval gval mMor |> h2 gval hval nMor)
         ...(cong ((mcomp fval |> ncomp fval) |>) (hcmp {}))
      ~~ mcomp fval |> ((ncomp fval) |> (h2 fval gval mMor |> h2 gval hval nMor))
         ..<(d.compAssoc _ _ _ _ (mcomp fval) (ncomp fval) (h2 fval gval mMor |> h2 gval hval nMor))
      ~~ mcomp fval |> (((ncomp fval) |> h2 fval gval mMor) |> h2 gval hval nMor)
         ...(cong (mcomp fval |>) (d.compAssoc {}))
      ~~ mcomp fval |> ((g2 fval gval mMor |> ncomp gval) |> h2 gval hval nMor)
         ...(cong (\morphism => mcomp fval |> (morphism |> h2 gval hval nMor)) (nsqr {}))
      ~~ mcomp fval |> (g2 fval gval mMor |> (ncomp gval |> h2 gval hval nMor))
         ..<(cong (mcomp fval |>) (d.compAssoc {}))
      ~~ ((mcomp fval |> g2 fval gval mMor)) |> ((ncomp gval |> h2 gval hval nMor))
        ...(d.compAssoc {})
      )

FunctorEval' : (c : Category') ->
               (d : Category') ->
               (FunctorCat' c d `ProductCat'` c) -*> d
FunctorEval' c d = MkFunctor' (FunctorEval c.π2 d.π2)

%hide Relation.Isomorphism.infix.(~=)
%hide Data.Category.Ops.infixl.(<~)

%unbound_implicits off
uncurryEval :
  (x, y, z : Category') ->
  (g : x * y -*> z) ->
  let 0 composed : (x * y) -*> z
      composed = CalcWith @{FuncPre} $
                 |~ ProductCat' x y
                 <~ (FunctorCat' y z * y)
                     ...(FunctorPair' (uncurryFunctor' g) (idF' y))
                 <~ z ...(FunctorEval' y z)
  in composed ≡ g
uncurryEval x y z g = ?uncurryEval_rhs

naturalUncurry :
  {0 x, x', y, z : Category'} ->
  (m : Functor' x x') ->
  (g : x' * y -*> z) ->
  uncurryFunctor' ((m `FunctorPair'` idF' y) !*> g) {a = x, b = y, c = z}
  ≡ m !*> uncurryFunctor' g {a = x', b = y, c = z}
naturalUncurry m g = ?naturalUncurry_rhs

SetHasExp : HasExp CatCat' CatHasProduct'
SetHasExp = MkExp
            (\dom, cod => FunctorCat' dom cod)
            (\cod, dom => FunctorEval' dom cod)
            (\x, y, z => uncurryFunctor')
            uncurryEval
            naturalUncurry

