module Data.Category.Setoid

import Data.Container.Distrib
import Data.Category.Ops
import Data.Setoid.List
import Data.Setoid
import Data.IndexedSetoid

import Syntax.PreorderReasoning.Setoid

import Control.Relation

%hide Data.Category.Ops.infixr.(~>)


record Container where
  constructor (!>)
  shp : Setoid
  pos : IndexedSetoid shp.U

record ContMor (c1 , c2 : Container) where
  constructor MkContMor
  fwd : c1.shp ~> c2.shp

infix 0 ≡
infixr 1 ⨾

revA : List a -> List a -> List a
revA [] acc = acc
revA (x :: xs) acc = revA xs (x :: acc)

rev : List a -> List a
rev xs = revA xs []

revOne : (xs : List a) -> {x : a} -> {ys : _} -> (x :: revA (revA xs ys) []) === revA (revA xs (x :: ys)) []
revOne [] {ys = []} = Refl
revOne [] {ys = (y :: xs)} = let rec = revOne [] {x, ys = xs} in ?huw_1
revOne (y :: xs) = let rec = revOne xs in ?revOne_rhs_1

revSame : {a : Setoid} -> (ls : List a.U) ->  a.ListEquality (rev (rev ls)) ls
revSame [] = []
revSame (x :: xs) = let rec = revSame xs
                        blw = a.equivalence.reflexive x :: rec
                     in CalcWith (ListSetoid a) $
                     |~ (revA (revA xs [x]) [])
                     ~~ (x :: revA (revA xs []) []) ..<(reflect (ListSetoid a) (revOne xs))
                     ~~ x :: xs ...(blw)

