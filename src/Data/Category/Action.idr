module Data.Category.Action

import Data.Product
import Data.Category
import Data.Category.Product
import Data.Category.Monoid
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.NaturalTransformation
import Syntax.PreorderReasoning

%hide Prelude.(&&)
%hide Prelude.(*)
%hide Data.Category.Product.(*)

%unbound_implicits off

public export
record Action {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkAction
  mon : Monoidal c
  action : Bifunctor c d d
  m : let 0 f1 : (c `ProductCat` (c `ProductCat` d)) -*> d
          f1 = Bifunctor.pair (idF c) action !*> action
          0 f2 : (c `ProductCat` (c `ProductCat` d)) -*> d
          f2 = assocR !*> Bifunctor.pair mon.mult (idF d) !*> action
       in f1 =~= f2
  u : idF d =~= Bifunctor.applyBifunctor {a=c} mon.I action

public export
(.assocM) : {0 o1, o2 : Type} -> (c : Category o1) => (d : Category o2) => (act : Action c d) ->
            (x : o1) -> (y : o1) -> (z : o2) ->
               let 0 f1 : (c `ProductCat` (c `ProductCat` d)) -*> d
                   f1 = Bifunctor.pair (idF c) act.action !*> act.action
                   0 f2 : (c `ProductCat` (c `ProductCat` d)) -*> d
                   f2 = assocR !*> Bifunctor.pair act.mon.mult (idF d) !*> act.action
               in (~:>) d (f1.mapObj (x && (y && z)))
                          (f2.mapObj (x && (y && z)))
act.assocM x y z = act.m.nat.component (x && y && z)

private infixr 6 @>
private infixl 2 <⊗>
private infixl 2 *>>
private infixl 2 ~@>
private infixl 2 ~⊗>
private infixl 2 |@>

public export
record LaxAction {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkLaxAction
  mon : Monoidal c
  action : Bifunctor c d d
  m : let
        0 f1 : (c `ProductCat` (c `ProductCat` d)) -*> d
        f1 = Bifunctor.pair (idF c) action !*> action
        0 f2 : (c `ProductCat` (c `ProductCat` d)) -*> d
        f2 = assocR !*> Bifunctor.pair mon.mult (idF d) !*> action
     in f1 =>> f2
  u : Bifunctor.applyBifunctor {a=c} mon.I action =>> idF d

private infixl 7 ⊗⊗
public export
record LaxActionLaws {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkLaxActionLaws
  act : LaxAction c d
  0 pentagon : (0 x, y, z : o1) -> (0 w : o2) -> let

        0 (@>) : o1 -> o2 -> o2
        (@>) x1 x2 = act.action.mapObj (x1 && x2)

        0 (~@>) : o2 -> o2 -> Type
        (~@>) = (~:>) d

        0 (~⊗>) : o1 -> o1 -> Type
        (~⊗>) = (~:>) c

        0 (*>>) : {0 a, a' : o1} -> {0 b, b' : o2} ->
                  a ~⊗> a' -> b ~@> b' -> (a @> b) ~@> (a' @> b')
        (*>>) m1 m2 = act.action.mapHom (a && b) (a' && b') (m1 && m2)

        0 (⊗⊗) : o1 -> o1 -> o1
        (⊗⊗) a b = act.mon.mult.mapObj (a && b)

        0 mm : (v1, v2 : o1) -> (v3 : o2) -> (v1 @> (v2 @> v3)) ~@> (v1 ⊗⊗ v2) @> v3
        mm v1 v2 v3 = act.m.component (v1 && (v2 && v3))

        0 map1 : x @> y @> z @> w ~@> ((x ⊗⊗ y) ⊗⊗ z) @> w
        map1 =
            ((c.id x *>> mm y z w)
              {a = x, a' = x, b = y @> z @> w, b' = y ⊗⊗ z @> w}
              ⨾ (mm x (y ⊗⊗ z) w
                ⨾ (act.mon.alpha.nat.component (x && (y && z)) *>> d.id w)
                  {a = x ⊗⊗ (y ⊗⊗ z), a' = (x ⊗⊗ y) ⊗⊗ z, b = w, b' = w}
                )
                { a =  x @> (y ⊗⊗ z) @> w
                , b =  x ⊗⊗ (y ⊗⊗ z) @> w
                , c = (x ⊗⊗  y) ⊗⊗ z @> w }
            )
            { a = x @> y @> z @> w
            , b = x @> (y ⊗⊗ z) @> w
            , c = x ⊗⊗ y ⊗⊗ z @> w
            }

        0 map2 : x @> y @> z @> w ~@> ((x ⊗⊗ y) ⊗⊗ z) @> w
        map2 = (|:>) d (mm x y (z @> w)) (mm (x ⊗⊗ y) z w)
            {a = x @> y @> z @> w, b = x ⊗⊗ y @> z @> w, c = x ⊗⊗ y ⊗⊗ z @> w}
     in map1 === map2

    -- a |> I |> b -----> a |> b
    --      |               ||
    --      |               ||
    --      V               ||
    -- a ⊗ I |> b ------> a |> b
  0 idRight : (0 a : o1) -> (0 b : o2) -> let

        0 (@>) : o1 -> o2 -> o2
        (@>) x1 x2 = act.action.mapObj (x1 && x2)

        0 (~@>) : o2 -> o2 -> Type
        (~@>) = (~:>) d

        0 (|@>) : {0 x, y, z : o2} -> x ~@> y -> y ~@> z -> x ~@> z
        (|@>) = (|:>) d

        0 (~⊗>) : o1 -> o1 -> Type
        (~⊗>) = (~:>) c

        0 (*>>) : {0 x, x' : o1} -> {0 y, y' : o2} ->
                  x ~⊗> x' -> y ~@> y' -> (x @> y) ~@> (x' @> y')
        (*>>) m1 m2 = act.action.mapHom (x && y) (x' && y') (m1 && m2)

        0 (⊗⊗) : o1 -> o1 -> o1
        (⊗⊗) a b = act.mon.mult.mapObj (a && b)

        0 mm : (v1, v2 : o1) -> (v3 : o2) -> (v1 @> (v2 @> v3)) ~@> (v1 ⊗⊗ v2) @> v3
        mm v1 v2 v3 = act.m.component (v1 && (v2 && v3))

        0 mapid1 : a @> act.mon.I @> b ~@> a @> b
        mapid1 = c.id a *>> act.u.component b

        0 mapid2 : a @> act.mon.I @> b ~@> a @> b
        mapid2 = (mm a act.mon.I b |@> (act.mon.rightUnitor.nat.component a *>> d.id b))
          {x = a @> act.mon.I @> b, y = a ⊗⊗ act.mon.I @> b, z = a @> b}
      in mapid1 === mapid2

    -- I |> a |> b -----> a |> b
    --      |               ||
    --      |               ||
    --      V               ||
    -- I ⊗ a |> b ------> a |> b
  0 idLeft : (0 a : o1) -> (0 b : o2) -> let

        0 (~@>) : o2 -> o2 -> Type
        (~@>) = (~:>) d

        0 (|@>) : {0 x, y, z : o2} -> x ~@> y -> y ~@> z -> x ~@> z
        (|@>) = (|:>) d

        0 (~⊗>) : o1 -> o1 -> Type
        (~⊗>) = (~:>) c

        0 (*>>) : {0 x, x' : o1} -> {0 y, y' : o2} ->
                  x ~⊗> x' -> y ~@> y' -> act.action.mapObj (x && y) ~@> act.action.mapObj (x' && y')
        (*>>) m1 m2 = act.action.mapHom (x && y) (x' && y') (m1 && m2)

        0 (⊗⊗) : o1 -> o1 -> o1
        (⊗⊗) a b = act.mon.mult.mapObj (a && b)

        0 mm : (v1, v2 : o1) -> (v3 : o2) ->
               act.action.mapObj (v1 && (act.action.mapObj (v2 && v3))) ~@>
               act.action.mapObj ((v1 ⊗⊗ v2) && v3)
        mm v1 v2 v3 = act.m.component (v1 && (v2 && v3))

        0 mapid1 : act.action.mapObj (act.mon.I && act.action.mapObj (a && b)) ~@> act.action.mapObj (a && b)
        mapid1 = act.u.component (act.action.mapObj (a && b))

        0 mapid2 : act.action.mapObj (act.mon.I && act.action.mapObj (a && b)) ~@> act.action.mapObj (a && b)
        mapid2 = (mm act.mon.I a b |@> (act.mon.leftUnitor.nat.component a *>> d.id b))
          {x = act.action.mapObj (act.mon.I && act.action.mapObj (a && b)),
           y = act.action.mapObj ((act.mon.I ⊗⊗ a) && b),
           z = act.action.mapObj (a && b)}
      in mapid1 === mapid2

{-
public export
getMor : {0 o1, o2 : Type} -> {c : Category o1} -> {d : Category o2}
    -> (act : Action c d) -> (x : o1) -> (y : o1) -> (z : o2)
    -> (~:>) d (act.action.mapObj (x && (act.action.mapObj (y && z))))
               (act.action.mapObj (act.mon.mult.mapObj (x && y) && z))
getMor (MkAction
       (MkMonoidal (MkFunctor m m' _ _) i _ _ _)
       (MkFunctor f f' _ _)
       n _)
       x y z = n.nat.component (x && (y && z))

monoidSelfAction : forall o. (c : Category o) -> Monoidal c -> Action c c
monoidSelfAction c x = ?monoidSelfAction_rhs

