module Data.Category.Functor

import Data.Sigma
import public Data.Category
import Proofs.Congruence
import Proofs.Extensionality
import Proofs.Preorder
import Proofs.UIP

import Syntax.PreorderReasoning

import Control.Order
import Control.Relation

%hide Prelude.Functor
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

%unbound_implicits off

public export
record Functor {0 o1, o2 : Type} (c : Category o1) (d : Category o2) where
  constructor MkFunctor

  -- a way to map objects
  mapObj : o1 -> o2

  -- a way to map morphisms
  -- For each morphism in cat1 between objects a and b
  -- we get a morphism in cat2 between the corresponding objects a and b
  -- but mapped to their counterparts in cat2
  mapHom : (x, y : o1) -> x ~> y -> mapObj x ~> mapObj y

  0 -- mapping the identity morphism in cat1 results in the identity morphism in cat2
  presId : (v : o1) -> mapHom v v (c.id v) = d.id (mapObj v)

  0 -- Preserves associativity of composition
  presComp :
    (x, y, z : o1) ->
    (f : x ~> y) ->
    (g : y ~> z) ->
    let 0 h1, h2 : mapObj x ~> mapObj z
        h1 = mapHom x z (f |> g)
        h2 = mapHom x y f |> mapHom y z g
    in h1 === h2

public export
record FunctorEq
  {0 o1, o2 : Type} {c : Category o1} {d : Category o2}
  (f1, f2 : Functor c d) where
  constructor MkFunctorEq
  sameMapObj : (0 x : o1) -> f1.mapObj x === f2.mapObj x
  sameMapHom : {0 x, y : o1} ->
               (m1 : (~:>) c x y) ->
               f1.mapHom x y m1 ===
                 replace {p = \vx => (~:>) d vx (f1.mapObj y)} (sym $ sameMapObj x)
                   (replace {p = (~:>) d (f2.mapObj x)} (sym $ sameMapObj y) (f2.mapHom x y m1))


public export
(-*>) : {0 o1, o2 : Type} -> Category o1 -> Category o2 -> Type
(-*>) = Functor

-- composition of functors
public export
(!*>) : {0 o1, o2, o3 : Type} -> {0 a : Category o1} -> {0 b : Category o2} -> {0 c : Category o3} ->
      Functor a b -> Functor b c -> Functor a c
(!*>) f1 f2 = MkFunctor
  (f2.mapObj . f1.mapObj)
  (\a, b, m => f2.mapHom (f1.mapObj a) (f1.mapObj b) (f1.mapHom a b m))
  (\x => cong (f2.mapHom (f1.mapObj x) (f1.mapObj x)) (f1.presId x) `trans` f2.presId (f1.mapObj x))
  (\a, b, c, h, j => cong (f2.mapHom (f1.mapObj _) (f1.mapObj _)) (f1.presComp _ _ _ h j) `trans`
                          f2.presComp _ _ _ (f1.mapHom _ _ h) (f1.mapHom _ _ j))

public export
idF : {0 o : Type} -> (0 c : Category o) -> Functor c c
idF c = MkFunctor id (\_, _ => id) (\_ => Refl) (\_, _, _, _, _ => Refl)

-- functorial mapping of morphisms
public export
F_m : {0 o1, o2 : Type} ->
      (c1 : Category o1) => (c2 : Category o2) => (f : Functor c1 c2) =>
      {a, b : o1} ->
      a ~> b -> f.mapObj a ~> f.mapObj b
F_m x = mapHom f a b x

public export
FromUnit : {0 o : Type} -> (c : Category o) -> o -> Functor UnitCat c
FromUnit (MkCategory m i c idl idr ass) x = MkFunctor
  (\_ => x)
  (\_, _, _ => i x)
  (\_ => Refl)
  (\_, _, _, _, _ => sym (idr x (id x) (i x)))

---------------------------------------------------------------------------------
-- proofs about functors
---------------------------------------------------------------------------------
0 functorCong : {o1, o2 : Type} ->
                {c1 : Category o1} ->
                {c2 : Category o2} ->
                {f, g : o1 -> o2} ->
                {fm : (x, y : o1) -> (~:>) c1 x y -> (~:>) c2 (f x) (f y)} ->
                {gm : (x, y : o1) -> (~:>) c1 x y -> (~:>) c2 (g x) (g y)} ->
                {mi : (v : o1) -> fm v v (id c1 v)  = id c2 (f v)} ->
                {ni : (v : o1) -> gm v v (id c1 v)  = id c2 (g v)} ->
                {mh : (a, b, c : o1) ->
                      (fn : a ~> b) ->
                      (gn : b ~> c) ->
                      fm a c ((|:>) c1 {a, b, c} fn gn)
                    === ((|:>) c2) {a=f a, b=f b, c=f c} (fm a b fn) (fm b c gn)} ->

                {nh : (a, b, c : o1) ->
                      (fn : (~:>) c1 a b) ->
                      (gn : (~:>) c1 b c) ->
                      gm a c ((|:>) c1 {a, b, c} fn gn)
                    === ((|:>) c2) {a=g a, b=g b, c=g c} (gm a b fn) (gm b c gn)} ->
                (p1 : f = g) ->
                (p2 : (replace {p = \fn =>
                      (x, y : o1) -> (~:>) c1 x y -> (~:>) c2 (fn x) (fn y)} p1 fm)
                        === gm) ->
                (p3 : mi ~=~ ni) ->
                (p4 : mh ~=~ nh) ->
                (let f1, f2 : Functor {o1, o2} c1 c2
                     f1 = MkFunctor {c=c1, d=c2} f fm mi mh
                     f2 = MkFunctor {c=c1, d=c2} g gm ni nh
                     in f1 === f2)
functorCong Refl Refl Refl Refl = Refl

export 0
functorEqToEq : {0 o1, o2 : Type} ->
                {c1 : Category o1} -> {c2 : Category o2} ->
                {f1, f2 : Functor c1 c2} ->
                FunctorEq f1 f2 -> f1 === f2
functorEqToEq (MkFunctorEq sameMapObj sameMapHom) {f1 = MkFunctor mo mh ci cc} {f2 = MkFunctor mo' mh' ci' cc'}
  = let p1 = (funExtDep0' sameMapObj)
        p2 = (funExtDep $ \a => funExtDep $ \b => funExtDep (\vx => sameMapHom {x=a, y=b} vx))
    in functorCong p1 (rewrite sym p1 in p2)
        (rewrite p1 in rewrite p2 in funExtDep $ \vx => UIP _ _)
        (rewrite p1 in rewrite p2 in
          funExtDep $ \vx => funExtDep $ \vy => funExtDep $ \vz => funExtDep $ \vw => funExtDep $ \ww => UIP _ _)

public export 0
functorIdLeft : {0 o1, o2 : Type} -> {0 a : Category o1} -> {0 b : Category o2} ->
                (f : Functor a b) -> ((Functor.idF a) !*> f) === f
functorIdLeft (MkFunctor mapo mapf presId presComp) =
  functorEqToEq $ MkFunctorEq (\_ => Refl)
    (\vx => Refl)

public export 0
functorIdRight : {0 o1, o2 : Type} -> {0 a : Category o1} -> {0 b : Category o2} ->
                 (f : Functor a b) -> (f !*> (Functor.idF b)) === f
functorIdRight (MkFunctor mapo mapf presId presComp) =
  functorEqToEq $ MkFunctorEq (\_ => Refl) (\_ => Refl)

public export 0
functorComposeOk :
    {0 o1, o2, o3, o4 : Type} ->
    {0 a : Category o1} -> {0 b : Category o2} ->
    {0 c : Category o3} -> {0 d : Category o4} ->
    (f : Functor a b) -> (g : Functor b c) -> (h : Functor c d) ->
    (f !*> (g !*> h)) === ((f !*> g) !*> h)
functorComposeOk
    (MkFunctor mapo1 mapf1 presId1 presComp1)
    (MkFunctor mapo2 mapf2 presId2 presComp2)
    (MkFunctor mapo3 mapf3 presId3 presComp3)
  = functorEqToEq $ MkFunctorEq (\_ => Refl) (\_ => Refl)

-- op functor
public export
(.op) : {0 a, b : _} -> Functor a b -> Functor a.op' b.op'
(.op) func = MkFunctor
    func.mapObj
    (\a, b => func.mapHom b a)
    func.presId
    (\x, y, z, f, g => func.presComp z y x g f)

0
opOpSame :
  {0 o1, o2 : Type} ->
  {0 c1 : Category o1} -> {0 c2 : Category o2} ->
  (func : Functor c1 c2) ->
  func.op.op ===
    replace {p = \x => Functor x c2.op'.op'}
      (sym $ Category.opOpSame' c1)
      (replace {p = Functor c1} (sym $ Category.opOpSame' c2) func)
opOpSame _ = functorEqToEq $ MkFunctorEq (\_ => Refl) (\_ => Refl)

{-
namespace Groth
  public export
  record Functor' (a, b : Category') where
    constructor MkFunctor'
    functor : Functor a.π2 b.π2

  public export
  (-*>) : (a, b : Category') -> Type
  (-*>) = Functor'

  export
  idF' : (x : Category') -> Functor' x x
  idF' x = MkFunctor' (idF _)

  export
  compose' : (x, y, z : Category') -> Functor' x y -> Functor' y z -> Functor' x z
  compose' x y z (MkFunctor' f) (MkFunctor' g) =
    MkFunctor' (f !*> g)

  public export
  (!*>) : {x, y, z : Category'} ->
          Functor' x y -> Functor' y z -> Functor' x z
  (!*>) f g = compose' _ _ _ f g

  public export
  [FuncRefl] Reflexive (Category') Functor' where
    reflexive = idF' _

  public export
  [FuncTrans] Transitive (Category') Functor' where
    transitive = compose' _ _ _

  public export
  [FuncPre] Preorder (Category') Functor' using FuncRefl FuncTrans where
