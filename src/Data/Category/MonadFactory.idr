module Data.Category.MonadFactory

import Data.Category
import Data.Category.Functor
import Data.Category.Bifunctor
import Data.Category.Monoid
import Data.Category.Monad
import Data.Category.Action
import Data.Category.NaturalTransformation
import Data.Container.Descriptions
import Data.Sigma
import Decidable.Equality

import Proofs
import Proofs.Congruence
import Syntax.PreorderReasoning

%hide Prelude.Monad
%hide Prelude.(&&)

%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

%unbound_implicits off

appBifunctorNT :
     {0 o1, o2, o3 : Type}
  -> {a : Category o1}
  -> {b : Category o2}
  -> {c : Category o3}
  -> {f : Bifunctor a b c}
  -> {x, y : o1}
  -> (m : (~:>) a x y)
  -> Bifunctor.applyBifunctor {a, b, c} x f =>> Bifunctor.applyBifunctor {a, b, c} y f
appBifunctorNT m {f} = let
    component : (v : o2) ->
                (Bifunctor.applyBifunctor {a, b, c} x f).mapObj v ~> (Bifunctor.applyBifunctor {a, b, c} y f).mapObj v
    component v = f.mapHom _ _ (m && b.id v)
    0 compProof : (0 z, w : o2) -> (mor : (~:>) b z w) ->
                let 0 n1, n2 : (Bifunctor.applyBifunctor {a, b, c} x f).mapObj z ~>
                               (Bifunctor.applyBifunctor {a, b, c} y f).mapObj w
                    n1 = component z |> (Bifunctor.applyBifunctor {a, b, c} y f).mapHom _ _ mor
                    n2 = (Bifunctor.applyBifunctor {a, b, c} x f).mapHom _ _ mor |> component w
                in n1 === n2
    compProof z w mor = let 0 ppp = f.presComp (x && z) (x && w) (y && w) (a.id x && mor) (m && b.id w)
                            0 qqq = f.presComp (x && z) (y && z) (y && w) (m && b.id z) (a.id y && mor)
                         in Calc $ |~ (f.mapHom (x && z) (y && z) (m && b.id z)) |> (f.mapHom (y && z) (y && w) (a.id y && mor))
                                   ~~ f.mapHom (x && z) (y && w) ((m |> a.id y) && (b.id z |> mor))
                                   ...(sym qqq)
                                   ~~ f.mapHom (x && z) (y && w) ((a.id x |> m) && (mor |> b.id w))
                                   ...(cong (f.mapHom (x && z) (y && w)) $
                                       cong2 (&&)
                                           (a.idRight x y m `trans` sym (a.idLeft x y m))
                                           (b.idLeft z w mor `trans` sym (b.idRight z w mor)))
                                   ~~ (f.mapHom (x && z) (x && w) (a.id x && mor)) |> (f.mapHom (x && w) (y && w) (m && b.id w))
                                   ...(ppp)

  in MkNT
      component
      compProof


%hide Prelude.Ops.infixr.($>)
private infixr 5 $>

parameters {0 o1, o2 : Type} {c : Category o1} {d : Category o2} (act : Action c d) (m : MonoidObject act.mon)
  public export
  mAct : d -*> d
  mAct = applyBifunctor m.obj act.action

  (⊗) : o1 -> o1 -> o1
  (⊗) x y = act.mon.mult.mapObj (x && y)

  μComponent : (x : o2) -> (mAct !*> mAct).mapObj x ~> mAct.mapObj x
  μComponent x = let aaa : ((mAct !*> mAct).mapObj x) ~> (act.action.mapObj ((m.obj ⊗ m.obj) {mon=act.mon} && x))
                     aaa = act.m.nat.component (m.obj && (m.obj && x))
                     bbb : (act.action.mapObj ((m.obj ⊗ m.obj) {mon=act.mon} && x)) ~> (mAct.mapObj x)
                     bbb = act.action.mapHom ((m.obj ⊗ m.obj) {mon=act.mon} && x) (m.obj && x) (m.μ && d.id x)
                  in aaa |> bbb

  μNTProof : (0 x, y : o2) -> (mor : (~:>) d x y) ->
              let 0 f1 : (mAct !*> mAct).mapObj x ~> mAct.mapObj y
                  f1 = μComponent x |> mAct.mapHom x y mor
                  0 f2 : (mAct !*> mAct).mapObj x ~> mAct.mapObj y
                  f2 = (mAct !*> mAct).mapHom x y mor |> μComponent y
              in f1 === f2
  μNTProof x y mor =
      let (*>) : {a1, b1 : o1} -> {a2, b2 : o2} ->
                  a1 ~> b1 ->
                  a2 ~> b2 ->
                  act.action.mapObj (a1 && a2) ~> act.action.mapObj (b1 && b2)
          (*>) f g = act.action.mapHom _ _ (f && g)
          ($>) : (a1 : o1) -> (a2 : o2) -> o2
          ($>) x y = act.action.mapObj (x && y)
      in Calc $
             |~ Start (m.obj $> m.obj $> x -< (act.m.nat.component (m.obj && m.obj && x) |> (m.μ *> d.id x)) >-
                       m.obj $> x          -< (c.id m.obj *> mor) >-
                  End (m.obj $> y))
             ~~ Start (m.obj $> m.obj $> x -< (c.id m.obj *> (c.id m.obj *> mor)) >-
                       m.obj $> m.obj $> y -< act.m.nat.component (m.obj && m.obj && y) >-
                       m.obj ⊗  m.obj $> y -< m.μ *> d.id y >-
                  End (m.obj $> y))
             ...(?ntp)

  μNT : (mAct !*> mAct) =>> mAct
  μNT = MkNT
    μComponent
    μNTProof

  ηNT : idF d =>> mAct
  ηNT = let n1 : idF d =>> applyBifunctor {a= c} act.mon.I act.action
            n1 = act.u.nat
            n2 : applyBifunctor {a=c} act.mon.I act.action =>> mAct
            n2 = appBifunctorNT m.η
        in n1 !!> n2

  (-**-) : {a1, b1 : o1} -> {a2, b2 : o2} ->
          a1 ~> b1 ->
          a2 ~> b2 ->
          act.action.mapObj (a1 && a2) ~> act.action.mapObj (b1 && b2)
  (-**-) f g = act.action.mapHom _ _ (f && g)

  -- monadLeftUnit : (x : o1) ->
  --         (Start (m.obj ⊗ (m.obj ⊗ (m.obj ⊗ x))) -< ((c.id m.obj -**- (act.assocM m.obj m.obj x |> (m.μ -**- d.id x)))
  --            |> (act.assocM m.obj m.obj x |> (m.μ -**- d.id x))))
  --         ≡
  --         (((act.assocM m.obj m.obj (act.action.mapObj (m.obj && x)) |>
  --           (m.μ -**- d.id (act.action.mapObj (m .obj && x))))
  --         |> (act.assocM m.obj m.obj x |> (m.μ -**- d.id x))))

  monadFromMonoidAction : Monad d
  monadFromMonoidAction =
    let (-*-) : {a1, b1 : o1} -> {a2, b2 : o2} ->
                a1 ~> b1 ->
                a2 ~> b2 ->
                act.action.mapObj (a1 && a2) ~> act.action.mapObj (b1 && b2)
        (-*-) f g = act.action.mapHom _ _ (f && g)
    in MkMonad
      mAct
      ηNT
      μNT
      (\x => Calc $
          |~ (c.id m.obj -**- (act.assocM m.obj m.obj x |> (m.μ -**- d.id x)))
             |> (act.assocM m.obj m.obj x |> (m.μ -**- d.id x))

          ~~ (act.assocM m.obj m.obj (act.action.mapObj (m.obj && x)) |>
            (m.μ -**- d.id (act.action.mapObj (m .obj && x))))
          |> (act.assocM m.obj m.obj x |> (m.μ -**- d.id x))
          ...(?proof))
      (\vx => Calc $
           |~
              (act.u.nat.component (act.action.mapObj (m.obj && vx)) |>
                ((appBifunctorNT m.η).component (act.action.mapObj (m.obj && vx))))
              |> (act.assocM m.obj m.obj vx
                |> act.action.mapHom (act.mon.mult.mapObj (m.obj && m.obj) && vx) (m.obj && vx) (m.μ && d.id vx))
           ~~ ((act.u.nat.component (act.action.mapObj (m.obj && vx)))
                 |> (act.action.mapHom
                       (act.mon.I && act.action.mapObj (m.obj && vx))
                       (m.obj && act.action.mapObj (m.obj && vx))
                       (m.η && d.id (act.action.mapObj (m.obj && vx)))))
               |> (act.assocM m.obj m.obj vx
                  |> (act.action.mapHom (act.mon.mult.mapObj (m.obj && m.obj) && vx) (m.obj && vx) (m.μ && d.id vx)))
                  ...(Refl)
           ~~ d.id (act.action.mapObj (m.obj && vx)) ...(?profof))
      ?ccc

