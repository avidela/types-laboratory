module Data.Location

import Language.STLC

record Location where
  constructor MkLoc
  line : Int
  col : Int


