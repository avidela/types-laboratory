module Data.Product.Dependent

public export
record Σ (x : Type) (f : x -> Type) where
  constructor (##)
  π1 : x
  π2 : f π1

