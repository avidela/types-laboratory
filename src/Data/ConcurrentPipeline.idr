module Data.ConcurrentPipeline

import Data.Pipeline

import System.Concurrency
import Data.Vect

-- Channel-based communication between stages
data PipeStage : Type -> Type -> Type where
  MkStage : Channel a -> Channel b -> (a -> IO b) -> PipeStage a b

-- Modified Pipeline implementation for concurrent execution
ConcurrentPipeline : Pipeline (2 + n) -> Type
ConcurrentPipeline (x :: y :: []) = PipeStage x y
ConcurrentPipeline (x :: y :: z :: xs) =
  Pair (PipeStage x y) (ConcurrentPipeline (y :: z :: xs))

-- Function to create a single pipeline stage
makeStage : Monad m => (a -> IO b) -> IO (PipeStage a b)
makeStage f = do
  inChan <- makeChannel
  outChan <- makeChannel
  pure $ MkStage inChan outChan f

covering
forever : Monad m => m () -> m ()
forever p = p >> forever p

-- Worker function that runs in each thread
stageWorker : PipeStage a b -> IO ()
stageWorker (MkStage inChan outChan f) = do
  forever $ do
    input <- channelGet inChan
    result <- f input  -- Execute the stage function
    channelPut outChan result

-- Function to connect and start the pipeline
startPipeline :
  (p : Pipeline (2 + n)) ->
  ConcurrentPipeline p ->
  IO ()
startPipeline [x, y] stage =
  fork (stageWorker stage)
  *> pure ()
startPipeline (x :: y :: z :: xs) (stage, rest) = do
  fork (stageWorker stage) *> startPipeline (y :: z :: xs) rest

record X (1 n : Nat) where
  constructor MkX

{-
getn : (0 _ : Nat) -> Type
getn n = X n -> Nat

-- getn (MkX {n}) = n
{-
-- Function to feed input into the pipeline
feedPipeline : (p : Pipeline (2 + n)) ->
  ConcurrentPipeline p ->
  Vect.head p ->
  IO (Vect.last p)
feedPipeline [x, y] (MkStage inChan outChan _) input = do
  channelPut inChan input
  channelGet outChan
feedPipeline (x :: y :: z :: xs) (stage, rest) input = do
  let MkStage inChan _ _ = stage
  channelPut inChan input
  feedPipeline (y :: z :: xs) rest
