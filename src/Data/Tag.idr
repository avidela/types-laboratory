module Data.Tag

import public Data.List.Quantifiers
import public Data.List.Elem
import public Decidable.Equality
import public Data.Singleton

%hide Builtin.infixr.(#)

export prefix 3 #

public export
data Label : String -> Type where
  MkLabel : (str : String) -> Label str

export
(.label) : Label s -> String
(.label) (MkLabel s) = s

export
Show (Label s) where
  show = (.label)

public export
data OneOf : List String -> Type where
  ||| A proof that the satisfying element is the first one in the `List`
  Here  : {0 xs : List String} -> Label x -> OneOf (x :: xs)
  ||| A proof that the satisfying element is in the tail of the `List`
  There : {0 xs : List String} -> OneOf xs -> OneOf (x :: xs)

public export
elimConcat :
   (xs, ys : List String) ->
   (t1 : OneOf xs -> a) ->
   (t2 : OneOf ys -> a) ->
   OneOf (xs ++ ys) -> a
elimConcat [] ys t1 t2 x = t2 x
elimConcat (x :: xs) ys t1 t2 (Here v) = t1 (Here v)
elimConcat (x :: xs) ys t1 t2 (There v) = elimConcat xs ys (t1 . There) t2 v

export
Uninhabited (OneOf Nil) where
  uninhabited (Here _) impossible
  uninhabited (There _) impossible

-- A possible tag is any one instance of a singleton
-- picked from the list of candidates
public export
(#) : List String -> Type
(#) xs = OneOf xs

-- Show instances for singleton and any
public export
Show (Singleton {a = String} x) where
  show (Val x) = x

export
(xs : List String) => Show (OneOf xs) where
  show {xs = a :: as} (Here x) = show x
  show {xs = a :: as} (There x) = show x

-- Evidence that a decidable property is satisfied
public export
data IsYes : Dec a -> Type where
  Witness : (w : a) -> IsYes (Yes w)

export
Uninhabited (IsYes (No x)) where
  uninhabited _ impossible

-- Given that we have a witness for a decidable property, extract it
public export
extract : {x : Dec a} -> IsYes x -> a
extract (Witness w) = w

-- Given a string, and evidence that the string belongs to a list of strings `xs`
-- return the membership proof as an instance of OneOf
public export
mkE : (s : String) -> Elem s xs -> #xs
mkE s Here = Here (MkLabel s)
mkE s (There x) = There $ mkE s x

-- Convert string literals by computing the proof that the given string actually
-- exists in the list of candidates. If that is the case, extract the inhabitant
-- from the proof.
public export
fromString : (s : String) -> {xs : List String} ->
             (e : IsYes (s `isElem` xs)) => #xs
fromString s {e} = mkE s (extract e)

