module Data.Value

import Data.Container
import Data.Category
import Data.Product
import Data.Coproduct
import Proofs.Extensionality
import Data.Category.Functor
import Data.Container
import Data.Container.Morphism
import Data.Iso
import Data.Sigma
import Data.Category.Set

public export
record Value (0 a : Container) (0 r : Type) where
  constructor MkVal
  arg : a.shp
  fn : a.pos arg -> r

-- vertical value morphisms
public export
0 VMor : (a, b : Type) -> Type
VMor a b = (0 c : Container) -> Value c a -> Value c b

-- values as a functor in Types
vmap : (a -> b) -> VMor a b
vmap f c (arg `MkVal` fn) = MkVal arg (f . fn)

-- Values are the same as the _extension_ of a container, aka the interpretation
-- of a container
ValueIsExtension : Value a x `Iso` Ex a x
ValueIsExtension = MkIso
    (\x => MkEx x.arg x.fn)
    (\x => MkVal x.ex1 x.ex2)
    (\(MkEx x y) => Refl)
    (\(MkVal x y) => Refl)

-- Values are a functor, well known since the extension of a functor maps
-- containers to type morphisms
[valFunc] Functor (\r => Value c r) where
  map f (MkVal x y) = MkVal x (f . y)

-- Values are not comonoidal because we cannot map from
-- (x : a.shp) to (a.pos x). However, we can with directed containers
extract : Value a x -> x
extract aaa = ?impossible_bc_we_dont_have_ashp_to_apos

-- extend is ok though
extend : Value a x -> Value a (Value a x)
extend (arg `MkVal` fn) = MkVal arg (\y => MkVal arg fn)


mapVert : a =%> b -> Value a x -> Value b x
mapVert lens y = MkVal (lens.fwd y.arg) (\vn => y.fn (lens.bwd y.arg vn))

public export
pure : {0 a : Container} -> (x : a.shp) -> Value a (a.pos x)
pure x = MkVal x id

public export
unit : v -> Value CUnit v
unit x = MkVal () (const x)

Vlh : (c : Container) -> (r : Type) -> Type
Vlh c r = (f : Type -> Type) -> (fn : Functor f) ->
          ((x : c.shp) -> f (c.pos x)) -> f r

Vlh2Val : Vlh c r -> Value c r
Vlh2Val f = f (\r => Value c r) valFunc pure

Val2Vlh : Value c r -> Vlh c r
Val2Vlh x f (MkFunctor m) g = m x.fn (g x.arg)

toFromVLH : {0 c : Container} ->
            (f : (Type -> Type)) -> (fn : Functor f) ->
            (m : (x : c .shp) -> f (c .pos x)) ->
            (v : Vlh c r) ->
            Val2Vlh (Vlh2Val {c} v) f fn m === v f fn m
toFromVLH f (MkFunctor mm) g v with (v (\r => Value c r) valFunc pure) proof p
  toFromVLH f (MkFunctor mm) g v | (MkVal arg fn) with (mm fn (g arg)) proof q
    toFromVLH f (MkFunctor mm) g v | (MkVal arg fn) | with_pat = let rec = (mm (v (\r => Value c r) valFunc pure).fn (g (v (\r => Value c r) valFunc pure).arg)) in ?toFromVLH_rhs_0_rhs0_rhs0

fromToVal : (x : Value a r) -> Vlh2Val (Val2Vlh x) === x
fromToVal (arg `MkVal` fn) = Refl

HorizontalMorphism : (a, b : Container) -> Type
HorizontalMorphism a b = (r : Type) -> Value a r -> Value b r

Vid : HorizontalMorphism x x
Vid _ = id

compose : HorizontalMorphism a b -> HorizontalMorphism b c -> HorizontalMorphism a c
compose x y r = y r . x r

CoContCat : Category Container
CoContCat = MkCategory
    (HorizontalMorphism)
    (\_, _ => id)
    compose
    (\_, _, a => funExtDep $ \r => Refl)
    (\_, _, _ => funExtDep $ \r => Refl)
    (\_, _, _, _, f, g, h => funExtDep $ \r => Refl)

KontCat : Category Type
KontCat = MkCategory
    (VMor)
    (\_, c => id)
    (\f, g, c => g c . f c)
    (\_, _, a => Refl)
    (\_, _, a => Refl)
    (\_, _, _, _, a, b, c => Refl)

KontFun : Functor Set KontCat
KontFun = MkFunctor
    id
    (\_, _, f => vmap f)
    (\x => funExtDep0 $ \c => funExt $ \(MkVal a b) => Refl)
    (\a, b, f, g, h => funExtDep0 $ \c => funExt $ \(MkVal w z) => Refl)

export autobind infixr 0 `MkVal`

-------------------------------------------------------------------------
-- Linear values
-------------------------------------------------------------------------
public export
record Value1 (a : Container) (r : Type) where
  constructor MkVal1
  1 arg : a.shp
  1 fn : (1 _ : a.pos arg) -> r

public export
And : Value a r1 -> Value b r2 -> Value (a ⊗ b) (r1 * r2)
And x y = MkVal (x.arg && y.arg) (bimap x.fn y.fn)

export
pairup : Value a (s -> r) -> Value b s -> Value (a ⊗ b) r
pairup x y = MkVal (x.arg && y.arg) (\(x1 && x2) => x.fn x1 (y.fn x2))

export
splitup : (v : Value (a ⊗ b) r)
       -> Value a (b.pos v.arg.π2 -> r)
        * Value b (b.pos v.arg.π2)
splitup x = MkVal x.arg.π1 (\f, a => x.fn (f && a)) && MkVal x.arg.π2 id

public export
splitup' : (v : Value (a + b) r)
        -> Value a r + Value b r
splitup' (MkVal (<+ x) fn) = <+ MkVal x fn
splitup' (MkVal (+> x) fn) = +> MkVal x fn

public export
pairup' : Value a r + Value b r -> Value (a + b) r
pairup' (<+ MkVal l f) = MkVal (<+ l) f
pairup' (+> MkVal r f) = MkVal (+> r) f
