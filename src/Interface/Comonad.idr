module Interface.Comonad

public export
interface Functor m => Comonad m where
  extract : m a -> a
  comult : m a -> m (m a)
