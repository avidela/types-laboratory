module Proofs

import public Proofs.Extensionality
import public Proofs.Congruence
import public Proofs.Preorder
import public Proofs.Sigma
import public Proofs.UIP
import public Proofs.Unit
import public Proofs.Void
import public Proofs.Product

export infix 0 ≡

||| An alternative operator name for propositional equality
||| with the lowest precedence to match behaviour expected by `=`
public export
(≡) : {a : Type} -> (e1, e2 : a) -> Type
(≡) = (===)

-- The fiber of sigma should know about the eta-rules of ()
public export
proj2Unit : {0 p : Unit -> Type} -> (x : Σ Unit p) -> p ()
proj2Unit (() ## y) = y

public export
proj2UnitPi2 : {0 p : Unit -> Type} -> (x : Σ Unit p) ->
               proj2Unit x === replace {p} (sym $ unitUniq x.π1) x.π2
proj2UnitPi2 (() ## z) = Refl

