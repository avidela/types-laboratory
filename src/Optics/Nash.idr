module Optics.Nash


import Data.Boundary
import Optics.Dependent
import Optics.Lens
import Data.Container
import Data.Product
import Optics.Parameterised

||| The nashator on regular lenses
public export
nashator : {0 x, x' : Type} -> {0 s : x -> Type} -> {0 s' : x' -> Type}
    -> Lens (MkB (x * x') (((v : x) -> s v) * ((v' : x') -> s' v')))
            (MkB (x * x') (((vp : x * x') -> (s vp.π1 * s' vp.π2))))
nashator = MkLens
  { get = id
  , set = \(vx && wx), f => (\y1 => π1 (f (y1 && wx))) && \y2 => π2 (f (vx && y2))
  }

||| The reverse derivative on dependent parameterised lenses
public export
revDPara : DPara l p r -> DPara (RC l) (RC p) (RC r)
revDPara (MkDPara (play <! coplay)) = MkDPara $
  MkDLens play
    (\(p && x), payoff
      => (\p' => π1 (coplay (p' && x) (payoff (play (p' && x)))))
      && (\x' => π2 (coplay (p && x') (payoff (play (p && x'))))))

||| The reverse-derivative as a mapping from dependent to non-dependent parameterised lenses
public export
revPara : DPara l p r -> Para (RC' l) (RC' p) (RC' r)
revPara (MkDPara (play <! coplay)) = MkDPara $
  MkDLens play
    (\(p && x), payoff
      => (\p' => π1 (coplay (p' && x) (payoff (play (p' && x)))))
      && (\x' => π2 (coplay (p && x') (payoff (play (p && x'))))))
