module Optics.CoPara

import Data.Container
import Optics.Dependent
import Data.Coproduct

public export
CoPara : (l, p, r : Container) -> Type
CoPara l p r = l `DLens` r + p

assocl : a + (b + c) -> (a + b) + c
assocl (<+ x) = <+ <+ x
assocl (+> (<+ x)) = <+ +> x
assocl (+> (+> x)) = +> x

assocr : (a + b) + c -> a + (b + c)
assocr (<+ (<+ x)) = <+ x
assocr (<+ (+> x)) = +> (<+ x)
assocr (+> x) = +> (+> x)

swap : a + b -> b + a
swap (<+ x) = +> x
swap (+> x) = <+ x

export
lemma : {l, r, p1, p2, x : Container} ->
      (g1 : (l .shp -> x.shp + p1.shp)) ->
      ((v : l .shp) -> choice (x.pos) (p1 .pos) (g1 v) -> l .pos v) ->
      (g2 : (x .shp -> r .shp + p2 .shp)) ->
      ((v : x .shp) -> choice (r .pos) (p2 .pos) (g2 v) -> x.pos v) ->
      (x : l .shp) ->
      choice
        (r .pos)
        (choice (p1.pos) (p2.pos))
        (Prelude.mapSnd CoPara.swap (CoPara.assocr (Prelude.mapFst g2 (g1 x)))) ->
      l .pos x
lemma g1 f g2 g v w with (g1 v) proof p
  lemma g1 f g2 g v w | (<+ z) with (g2 z) proof q
    lemma g1 f g2 g v w | (<+ z) | (<+ z2) = f v (rewrite p in g z
                                                 (rewrite q in w))
    lemma g1 f g2 g v w | (<+ z) | (+> z2) = f v (rewrite p in g z
                                                 (rewrite q in w))
  lemma g1 f g2 g v w | (+> z) = f v (rewrite p in w)

public export
compose : {l, r, x, p1, p2 : Container} ->
          CoPara l p1 x -> CoPara x p2 r -> CoPara l (p1 + p2) r
compose (g1 <! s1) (g2 <! s2) = MkDLens
  (mapSnd swap . assocr . mapFst g2 . g1)
  (lemma g1 s1 g2 s2)


