module Optics.FullPara

import Data.Coproduct
import Data.Product
import Data.Container
import Optics.Dependent
import Optics.CoPara
import Optics.Parameterised

public export
FullPara : (l, p, r, q : Container) -> Type
FullPara l p r q = l ⊗ p `DLens` r + q

lemma : {l, r, p, q, d, b, x : Container} ->
        {act : (x : q.shp) -> q.pos x} ->
        (g1 : l.shp * p.shp -> x.shp + b.shp) ->
        ((v : l.shp * p.shp) -> choice x.pos b.pos (g1 v) -> l.pos v.π1 * p.pos v.π2) ->
        (g2 : x.shp * q.shp -> r.shp + d.shp) ->
        ((v : x.shp * q.shp) -> choice r.pos d.pos (g2 v) -> x.pos v.π1 * q.pos v.π2) ->
        (x : l.shp * (p.shp * q.shp)) ->
        choice r.pos (choice b.pos d.pos)
          (choice (\z => Prelude.mapSnd (+>) (g2 (z && x.π2.π2)))
                  (\x => +> (<+ x))
                  (g1 (x.π1 && x.π2.π1))) ->
        l.pos x.π1 * (p.pos x.π2.π1 * q.pos x.π2.π2)
lemma g1 s1 g2 s2 (x1 && (x2 && x3)) y with (g1 (x1 && x2)) proof p1
  lemma g1 s1 g2 s2 (x1 && (x2 && x3)) y | (<+ z) with (g2 (z && x3)) proof p2
    lemma g1 s1 g2 s2 (x1 && (x2 && x3)) y | (<+ z) | (<+ z2) =
      let r2 = s2 (z && x3) (rewrite p2 in y)
          r1 = s1 (x1 && x2) (rewrite p1 in r2.π1)
      in r1.π1 && r1.π2 && r2.π2
    lemma g1 s1 g2 s2 (x1 && (x2 && x3)) y | (<+ z) | (+> z2) =
      let r2 = s2 (z && x3) (rewrite p2 in y)
          r1 = s1 (x1 && x2) (rewrite p1 in r2.π1)
      in r1.π1 && r1.π2 && r2.π2
  lemma g1 s1 g2 s2 (x1 && (x2 && x3)) y | (+> z) =
    let r1 = s1 (x1 && x2) (rewrite p1 in y)
    in r1.π1 && (r1.π2 && act x3)

compose : {l, r, p, q, d, b, x : Container} ->
          (act : (x : q.shp) -> q.pos x) ->
          FullPara l p x b ->
          FullPara x q r d ->
          FullPara l (p ⊗ q) r (b + d)
compose act (g1 <! s1) (g2 <! s2) = MkDLens
  (\w => choice
    (\z => mapSnd (+>) (g2 (z && w.π2.π2)))
    ((+>) . (<+))
    (g1 (w.π1 && w.π2.π1)))
    (lemma {l} {r} {p} {q} {d} {b} {x} {act} g1 s1 g2 s2)

