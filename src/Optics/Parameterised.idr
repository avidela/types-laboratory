module Optics.Parameterised

import Data.Alg
import Data.Container
import Data.Container.Product
import Data.Product
import Data.Coproduct
import Data.Category
import Data.Category.Graded
import Data.Boundary
import Data.Sigma

import Optics.Dependent

import Control.Relation

%hide Prelude.Ops.infixl.(|>)

||| A dependent parameterised lens defined as the para-construction on dependent lenses
public export
record DPara (p, l, r : Container) where
  constructor MkDPara
  lens : p ⊗ l `DLens` r

data MPara : {0 carrier : Type} -> (mor : Rel carrier) -> (m : carrier -> carrier -> carrier) -> Type where
  MkPara : {0 carrier : Type} ->
           {0 m : carrier -> carrier -> carrier} ->
           {0 mor : Rel carrier} ->
           (0 a : carrier) -> (0 b : carrier) -> (0 c : carrier) ->
           (m a b `mor` c) -> MPara mor m

data GPara : {0 carrier : Type} -> {0 group : Type} -> (mor : Rel carrier) -> (m : carrier -> group -> carrier) -> Type where
  MkGPara : {0 carrier, group: Type} ->
            {0 m : carrier -> group -> carrier} ->
            {0 mor : Rel carrier} ->
            (0 a : carrier) -> (0 b : group) -> (0 c : carrier) ->
            (m a b `mor` c) -> GPara mor m

data APara : {0 carrier : Type} -> {0 group : carrier -> Type} -> (mor : Rel carrier) -> (m : (x : carrier) -> group x -> carrier) -> Type where
  MkAPara : {0 carrier : Type} -> {0 group : carrier -> Type} ->
            {0 m : (x : carrier) -> group x -> carrier} ->
            {0 mor : Rel carrier} ->
            (0 a : carrier) -> (0 b : group a) -> (0 c : carrier) ->
            (m a b `mor` c) -> APara mor m

Fn : Type -> Type -> Type
Fn x y = x -> y

Sigmapara : (x : Type) -> (y : x -> Type) -> (z : Type) -> (DPair x y -> z) -> APara Fn DPair
Sigmapara x y z = MkAPara x y z

record SigPara (x : Type) (y : x -> Type) (z : Type) where
  constructor MkSig
  fn : DPair x y -> z

0 M : forall x, y. (x -> Type) -> (x * y -> Type) -> (x -> Type)
M f g x = f x * (v : y ** g (x && v))


combine : (p : a -> Type) -> (q : Σ a p -> Type) -> a -> Type
combine p q aa = Σ (p aa) (q . (aa ##))

pull : (f : x -> y) -> (q : y -> Type) -> x -> Type
pull f q xx = q (f xx)

compose : (p : a -> Type) -> (q : b -> Type) ->
          (f : Σ a p -> b) -> (g : Σ b q -> c) -> (Σ a (combine p (pull f q)) -> c)
compose p q f g (aa ## (pp ## qq)) = g ((f (aa ## pp)) ## qq)



-- compose : SigPara a b c -> SigPara c b' d -> SigPara a (\arg => b arg * Σ c b') d
-- compose (MkSig fn) (MkSig gn) = MkSig $ \(ax ** bx && cx ## dx)  => gn (fn (ax ** bx) ** ?bd)

||| A type-alias for non-dependent parameterised lenses
public export
Para : Boundary -> Boundary -> Boundary -> Type
Para (MkB x s) (MkB p q) (MkB y r) = DPara (Const2 x s) (Const2 p q) (Const2 y r)

||| Composition of parameterised lenses
public export
(||>) : DPara p l x -> DPara q x r -> DPara (p ⊗ q) l r
(||>) y z = MkDPara $
  (z.lens.fwd . mapSnd y.lens.fwd . assocL . mapFst swap) <!
  (\x, w => let
                b = z.lens.bwd (x.π1.π2 && y.lens.fwd (x.π1.π1 && x.π2)) w
                a = y.lens.bwd (x.π1.π1 && x.π2) b.π2
            in (a.π1 && b.π1) && a.π2)

public export
cornerRight : DPara p CUnit p
cornerRight = MkDPara $ π1 <! (\_, x => x && ())

public export
(.get) : DPara p l x -> p.shp * l.shp -> x.shp
(.get) y = y.lens.fwd

(.set) : (o : DPara p l r) -> (a : p.shp * l.shp) -> (b : r.pos (o.lens.fwd a)) -> p.pos a.π1 * l.pos a.π2
(.set) y x = y.lens.bwd x

export
reparam : DLens a b -> DPara b l r -> DPara a l r
reparam x y = MkDPara (parallel x (identity {a = l}) ⨾ y.lens)

export
comb : DLens l x -> DLens p q -> DLens y r -> DPara q x y -> DPara p l r
comb left top right para = MkDPara $ parallel top left ⨾ para.lens ⨾ right

export
paraLeft : DLens a b -> DPara p l r -> DPara p (a ⊗ l) (b ⊗ r)
paraLeft x y = MkDPara (((assocL . mapFst swap . assocR) <!
                       (\_ => assocL . mapFst swap . assocR)) ⨾ parallel x y.lens)

export
paraRight : DPara p l r -> DLens a b -> DPara p (l ⊗ a) (r ⊗ b)
paraRight x y = MkDPara ((assocR <! (\_ => assocL)) ⨾ parallel x.lens y)

export
preCompose : DLens a b -> DPara p b c -> DPara p a c
preCompose x y = MkDPara $ (identity {a = p} `parallel` x) ⨾ y.lens

export
postCompose : DPara p a b -> DLens b c -> DPara p a c
postCompose x y = MkDPara $ x.lens ⨾ y

export infixl 1 ||>

export
compSame : (mon : {x : p.shp} -> Semigroup (p.pos x)) => DPara p l x -> DPara p x r -> DPara p l r
compSame y z = reparam {b= p ⊗ p} (dup <! (\x, y => (y.π1 <+> y.π2) @{mon {x}})) (y ||> z)

public export
choice : DPara p l r -> DPara q x y -> DPara (p * q) (l + x)  (r + y)
choice z w = MkDPara $ ?aad ⨾ z.lens ~+~ w.lens

Reparam : {l, p, q, r : Container} -> DPara p l r -> DPara q l r -> Type
Reparam _ _ = DLens p q

idReparam : {l, p, r : Container} -> {v : DPara p l r} -> Reparam {l} {p} {q=p} v v
idReparam = identity p


idPara : {0 v : Container} -> DPara CUnit v v
idPara = MkDPara $ π2 <! (\x => (() &&))

public export
[SemiTensor] Semigroup Container where
  (<+>) = (⊗)

public export
[MonTensor] Monoid Container using SemiTensor where
  neutral = CUnit

public export
GradedCat MonTensor DPara using MonTensor SemiTensor where
  identity = idPara
  (#>) f g = (||>) f g

foldPara : List Container -> Container
foldPara [] = CUnit
foldPara [x] = x
foldPara (x :: y :: xs) = x ⊗ foldPara (y :: xs)

record FreePara (ps : List Container) (x, y : Container) where
  constructor MkFreePara
  para : DPara (foldPara ps) x y

composeFree : {p1, p2 : List Container} ->
              FreePara p1 x y -> FreePara p2 y z -> FreePara (p1 ++ p2) x z
composeFree {p1 = []} {p2 = []} w v = MkFreePara $ let pp = w.para ||> v.para in reparam dupUnit pp
composeFree {p1 = []} {p2 = [x]} w v = MkFreePara $ let pp = w.para ||> v.para in reparam ?aha22 pp
composeFree {p1 = []} {p2 = x :: y :: xs} w v = MkFreePara $ let pp = w.para ||> v.para in ?aha222
composeFree {p1 = (s :: xs)} w v = ?composeFree_rhs_1

GradedCat %search FreePara where
  identity = MkFreePara idPara
  (#>) f g = composeFree f g

{-

idPrfLeft : (f : DLens a b) -> f ||> Morphism.identity b = f
idPrfLeft {f = (MkMorphism get set)} = Refl

idPrfRight : (f : DLens a b) -> Morphism.identity a ||> f = f
idPrfRight {f = (MkMorphism get set)} = Refl

parameters {l, r : Container}
  Repar : (p ** DPara l p r) -> (p ** DPara l p r) -> Type
  Repar x y = DLens x.fst y.fst

  comp : {a, b, c : (p ** DPara l p r)} -> Repar a b -> Repar b c -> Repar a c
  comp = Morphism.(|>)

  prfAssoc2 : {a, b, c, d : (p ** DPara l p r)}  ->
              (f : Repar a b) -> (g : Repar b c) -> (h : Repar c d) ->
              (comp {a} {b} {c=d} f (comp {a=b} {b=c} {c=d} g h)) === (comp {a} {b=c} {c=d} (comp {a} {b} {c} f g) h)
  prfAssoc2 {f=MkMorphism fget fset} {g=MkMorphism gget gset} {h=MkMorphism hget hset} = Refl

  DParaCat : Category (p ** DPara l p r)
  DParaCat = MkCategory
    Repar
    (\_ => identity)
    (|>)
    idPrfLeft
    idPrfRight
    prfAssoc2


interleaving1 : {0 a, y, y', s : Type} -> {0 r : y -> Type} -> {0 r' : y' -> Type} ->
   DPara (MkCont (a * Maybe y') (const s)) p (MkCont y r) ->
   DPara (MkCont (a' * Maybe y) (const s')) q (MkCont y' r') ->
   DPara (MkCont a (const s) ⊗ MkCont a' (const s')) (p ⊗ q) (MkCont y r ⊗ MkCont y' r')
interleaving1 l1 l2 = first |> end
  where
      op : DPara (MkCont a (const s)) p (MkCont y r)
      op = MkMorphism (&& Nothing) (Product.curry snd) `preCompose` l1

      first : DPara (MkCont a (const s) ⊗ MkCont a' (const s')) p (MkCont y r ⊗ MkCont a' (const s'))
      first = (op `paraRight` identity {a = MkCont a' (const s')})

      end : DPara (MkCont y r ⊗ MkCont a' (const s')) q (MkCont y r ⊗ MkCont y' r')
      end = MkMorphism (\x => x.π1 && (x.π2 && Just x.π1)) (\_ => id) `preCompose` (identity {a = MkCont y r}  `paraLeft` l2)

interleaving2 : {0 a, y, y', s : Type} -> {r : y -> Type} -> {r' : y' -> Type} ->
   DPara (MkCont (a * Maybe y') (const s)) p (MkCont y r) ->
   DPara (MkCont (a' * Maybe y) (const s')) q (MkCont y' r') ->
   DPara (MkCont a (const s) ⊗ MkCont a' (const s')) (p ⊗ q) (MkCont y r ⊗ MkCont y' r')
interleaving2 l1 l2 = reparam (MkMorphism swap (\_ => swap)) (first |> end)
  where
      op : DPara (MkCont a' (const s')) q (MkCont y' r')
      op = MkMorphism (&& Nothing) (const id) `preCompose` l2

      first : DPara (MkCont a (const s) ⊗ MkCont a' (const s')) q (MkCont a (const s) ⊗ MkCont y' r')
      first = identity {a = MkCont a (const s)} `paraLeft` op

      end : DPara (MkCont a (const s) ⊗ MkCont y' r') p (MkCont y r ⊗ MkCont y' r')
      end = MkMorphism (\x => (x.π1 && Just x.π2) && x.π2) (\_ => id) `preCompose` (l1 `paraRight` identity {a = MkCont y' r'})

-- Move those two away, probably somewhere close to `Action`?
interface Semigroup a => Combine a (0 b : a -> Type) where
   combine : (x1, x2 : a) ->
             (y : b (x1 <+> x2)) ->
             b x1 * b x2

join : Combine a b => DLens (MkCont a b ⊗ MkCont a b) (MkCont a b)
join = MkMorphism (uncurry (<+>)) (\(x1 && x2), y => combine x1 x2 y)

||| Interleaving two lenses with the same signature
interleavingSame : {0 a, y, s : Type} -> {0 r : y -> Type} ->
   Semigroup s => Semigroup y => Combine y r =>
   (mon : {x : p.shp} -> Semigroup (p.pos x)) =>
   DPara (MkCont (a * Maybe y) (const s)) p (MkCont y r) ->
   DPara (MkCont (a * Maybe y) (const s)) p (MkCont y r) ->
   DPara (MkCont a (const s)) p (MkCont y r)
interleavingSame l1 l2 = (first `compSame` end) {mon}
  where
      op : DPara (MkCont a (const s)) p (MkCont y r)
      op = MkMorphism (&& Nothing) (Product.curry snd) `preCompose` l1

      first : DPara (MkCont a (const s)) p (MkCont y r ⊗ MkCont a (const s))
      first = MkMorphism dup (\_ => uncurry (<+>)) `preCompose` (op `paraRight` identity {a = MkCont a (const s)})

      end : DPara (MkCont y r ⊗ MkCont a (const s)) p (MkCont y r)
      end = (MkMorphism (\x => (x.π2 && Just x.π1) && x.π1) (\_ => swap) `preCompose` (l2 `paraRight` identity {a = MkCont y r})) `postCompose` join {a = y}

||| Precomposing a choice with the `x + x = 2 * x` isomorphism
chooseBool : DPara (MkCont l s) p r ->
             DPara (MkCont l s) q y ->
             DPara (MkCont (l * Bool) (s . π1)) (p * q) (r + y)
chooseBool l1 l2 = MkMorphism (\x => if x.π2 then <+ x.π1 else +> x.π1) (\case (x && True) => id
                                                                               (x && False) => id) `preCompose` (l1 `choice` l2)

||| give the choice of interleaving two lenses one way or the other
choiceInterleaving : Monoid s => Combine y r => (mon : {x : p.shp} -> Semigroup (p.pos x)) =>
                     DPara (Const2 (a * Maybe y) s) p (MkCont y r)
                  -> DPara (Const2 (a * Maybe y) s) p (MkCont y r)
                  -> DPara (Const2 (a * Bool) s)    p (MkCont y r + MkCont y r)
choiceInterleaving l1 l2 =
  let a1 = interleavingSame l1 l2 {mon}
      a2 = interleavingSame l2 l1 {mon}
  in reparam (MkMorphism dup (\_ => dia)) $ chooseBool a1 a2 {l = a} {s = const s} {p} {q=p} {r = MkCont y r} {y = MkCont y r}


public export
parallel : DPara l p r -> DPara x q y -> DPara (l ⊗ x) (p ⊗ q) (r ⊗ y)
parallel p1 p2 = MkDPara $ let p = parallel p1.lens p2.lens in
                               MkMorphism shuffle (\((a && a') && (c && c')) => shuffle) |> p

-- Containers monoid

-- Parameterised graded monad
