module Optics.Lens

import Algebra.Monoid

import Data.Product
import Data.Category
import Data.Boundary
import Data.Coproduct
import Data.Vect
import Data.Sigma

import Proofs.Extensionality
import Proofs.Sum

import Syntax.PreorderReasoning
import Relation.Isomorphism

%hide Prelude.Monoid
%hide Prelude.(&&)

public export
record Lens (a, b : Boundary) where
  constructor MkLens
  get : a.π1 -> b.π1
  set : a.π1 -> b.π2 -> a.π2

public export
WellBehaved : Type -> Type -> Type
WellBehaved a b = Lens (MkB a a) (MkB b b)

||| Lens composition
export
(|>) : Lens a b -> Lens b c -> Lens a c
(|>) l1 l2 = MkLens (l2.get . l1.get)
  (\x => l1.set x . l2.set (l1.get x))

||| Parallel composition
export
parallel : Lens a b -> Lens x y -> Lens (MkB (a.π1 * x.π1) (a.π2 * x.π2))
                                        (MkB (b.π1 * y.π1) (b.π2 * y.π2))
parallel l1 l2 = MkLens (bimap l1.get l2.get) (\x => bimap (l1.set (fst x)) (l2.set (snd x)))

||| identity on lenses
export
idLens : Lens x x
idLens = MkLens id (const id)

prfidleft : (f : Lens a b) ->
            MkLens (\x => f.get x) (\x => \y => f.set x y) = f
prfidleft (MkLens g s) = Refl

prfIdRight : (f : Lens a b) ->
             MkLens (\x => f .get x) (\x => \y => f .set x y) = f
prfIdRight (MkLens get set) = Refl

%hide Prelude.Ops.infixl.(|>)
prfAssoc : (f : Lens a b) -> (g : Lens b c) -> (h : Lens c d) ->
  (f |> (g |> h)) === ((f |> g) |> h)
prfAssoc {f = (MkLens fget fset)} {g = (MkLens gget gset)} {h = (MkLens hget hset)} = Refl

||| lenses form a category with composition
export
lensCategory : Category Boundary
lensCategory = MkCategory
  Lens
  (\_ => idLens)
  (|>)
  (\_, _ => prfidleft)
  (\_, _ => prfIdRight)
  (\_, _, _, _ => prfAssoc)

public export
record Traversal (a, b : Boundary) where
  constructor MkTraversal
  extract : a.π1 -> Σ Nat (\n => Vect n b.π1 * (Vect n b.π2 -> a.π2))

public export
record Affine (a, b : Boundary) where
  constructor MkAffine
  read : a.π1 -> a.π2 + (b.π1 * (b.π2 -> a.π2))

record AffineHackage (a, b : Boundary) where
  constructor MkAffineH
  match : a.π1 -> a.π2 + b.π1
  update : a.π1 -> b.π2 -> a.π2

fromHackage : AffineHackage a b -> Affine a b
fromHackage (MkAffineH match update) = MkAffine $ \x => mapSnd (\xn => xn && update x) (match x)

toHackage : Affine a b -> AffineHackage a b
toHackage (MkAffine read) = MkAffineH (mapSnd π1 . read) (\x, y => dia (mapSnd ((\f => f y) . π2) (read x)))

toFromHackage : (aff : Affine a b) -> fromHackage (toHackage aff) === aff
toFromHackage (MkAffine read) = cong MkAffine $ funExt $ \xn => Calc $
        |~ mapSnd (\xx : b.π1 => xx && (\y => dia (mapSnd (\x => x.π2 y) (read xn)))) (mapSnd π1 (read xn))
        ~~ mapSnd ((\xx : b.π1 => xx && (\y => dia (mapSnd (\x => x.π2 y) (read xn)))) . π1) (read xn) ..<(bimapCompose (read xn))
        ~~ read xn ...(?blheu)

fromToHackage : (aff : AffineHackage a b) -> toHackage (fromHackage aff) === aff
fromToHackage (MkAffineH mm uu ) =
  cong2 MkAffineH (funExt $ \xn => Calc $
        |~ mapSnd (.π1) (mapSnd (\xn => xn && ?) (mm xn))
        ~~ mapSnd (π1 . (\xn => xn && ?)) (mm xn) ..<(bimapCompose (mm xn))
        ~~ mapSnd id (mm xn) ...(Refl)
        ~~ mm xn ...(bifunctorId' (mm xn)))
        (funExt $ \xn => funExt $ \yn => Calc $
        |~ dia (bimap id (\x => π2 x yn) (bimap id (\xx : b.π1 => xx && uu xn) (mm xn)))
        ~~ dia (mapSnd (\x => π2 x yn) (mapSnd (\xx => xx && uu xn) (mm xn))) ...(Refl)
        ~~ dia (mapSnd ((\x : b.π1 * (b.π2 -> a.π2) => x.π2 yn) . (\xx : b.π1 => xx && uu xn)) (mm xn)) ..<(cong dia (bimapCompose (mm xn)))
        ~~ dia (mapSnd ((\_ => uu xn yn)) (mm xn)) ...(Refl)
        ~~ uu xn yn ...(?bleh))

public export
record Affine' (a, b : Boundary) where
  constructor MkAffine'
  read : a.π1 -> Maybe (b.π1 * (b.π2 -> a.π2))
