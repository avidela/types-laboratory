module Optics.Lawful

import Data.Iso
import Data.Sigma
import Data.Product
import Data.String

Lens : Type -> Type -> Type
Lens x y = Σ0 Type $ \r => Iso x (y * r)

HString : Type
HString = List Char

crazyFst : Lens (a, HString) a
crazyFst = HString *#
    MkIso fwd bwd bf fb

    where
      fwd : (a, HString) -> a * HString
      fwd x = fst x && 'b' :: 'a' :: 'n' :: 'a' :: 'n' :: 'a' :: snd x

      bwd : a * HString -> (a, HString)
      bwd (x && 'b' :: 'a' :: 'n' :: 'a' :: 'n' :: 'a' :: s) = (x, s)
      bwd (x && s) = (x, s)

      bf : (x : a * HString) -> fwd (bwd x) === x
      bf (fst && 'b' :: 'a' :: 'n' :: 'a' :: 'n' :: 'a' :: s) = Refl
      bf (fst && snd) = ?bf_rhs_0

      fb : (x : (a, HString)) -> bwd (fwd x) === x
      fb (x, y) = Refl

