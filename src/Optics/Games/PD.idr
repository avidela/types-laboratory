module Optics.Games.PD

import Data.Product
import Optics.Games.Bimatrix
import Optics.Game
import Data.Container

-- Prisoner's dilemma moves
data MovesPD = Cooperate | Defect

Show MovesPD where
  show Cooperate = "cooperate"
  show Defect = "defect"

Listable MovesPD where
  allValues = [Cooperate, Defect]

Listable a => Listable b => Listable (a * b) where
  allValues = [(x && y) | x <- allValues, y <- allValues]

payoffPD : Bimatrix MovesPD MovesPD
payoffPD (Cooperate && Cooperate) = (2 && 2)
payoffPD (Cooperate && Defect) = (0 && 3)
payoffPD (Defect && Cooperate) = (3 && 0)
payoffPD (Defect && Defect) = (1 && 1)

Default Double where
  def = 0

equilibriaPD : List (MovesPD * MovesPD)
equilibriaPD = equilibria (argmaxPlayer ## argmaxPlayer) (bimatrixContext payoffPD) bimatrixArena

eqFast : IO ()
eqFast = printLn equilibriaPD

