module Optics.Games.Bimatrix

import Data.Product
import Data.Boundary
import Optics.Game
import Data.Container
import Optics.Parameterised
import Optics.Dependent
import Optics.Lens

public export
TwoDoubles : Type
TwoDoubles = Double * Double

export
Eq TwoDoubles where
  (a && b) == (x && y) = a == x && b == y

export
Ord (TwoDoubles) where
  -- (<=) : TwoDoubles -> TwoDoubles -> Bool
  (x && y) <= (x' && y') = x <= x' && y <= y'
  -- (<) : TwoDoubles -> TwoDoubles -> Bool
  (x && y) < (x' && y') = x < x' && y < y'
  (>) _ _ = ?no

public export
argmaxPlayer : (Ord u, Eq u, Default u, Listable s) => Player s s u
argmaxPlayer = MkLens id (\p, k => argmax k)

-- Helpers for bimatrix games
public export
Bimatrix : (moves1, moves2 : Type) -> Type
Bimatrix moves1 moves2 = (moves1 * moves2) -> TwoDoubles

export
bimatrixArena : Arena CUnit (Const2 (moves1 * moves2) TwoDoubles) (Const2 (moves1 * moves2) TwoDoubles)
bimatrixArena = let v =  (cornerRight {p = Const2 moves1 Double} `parallel` cornerRight {p = Const2 moves2 Double})
  in MkDLens dup (\x, _ => x) `preCompose` v

export
bimatrixContext : Bimatrix moves1 moves2 -> Context CUnit (Const2 (moves1 * moves2) TwoDoubles)
bimatrixContext b = MkPair (scalarToState ()) (funToCostate b)

