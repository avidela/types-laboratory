module Optics.Profunctor

import Data.Category.Profunctor
import Optics.Lens
import Optics.Prism
import Optics.Adapter
import Data.Coproduct
import Data.Sigma
import Data.Boundary
import Data.Product
import Data.Vect
import Proofs.Extensionality
import Proofs.Sum
import Proofs.Sigma

adtIsProfunctor : (t : Boundary) -> Set .op -/> Set .op
adtIsProfunctor t = MkFunctor
    (\x => Adapter t (MkB x.fst x.snd))
    (\a, b, f, m => compose m
        (MkAdt f.π1 f.π2))
    (\x => funExt $ \(MkAdt f b) => Refl)
    (\x, y, z, f, g => Refl)


lensIsProfunctor : Boundary -> Set -/> Set
lensIsProfunctor t = MkFunctor
    (\x => Lens (MkB x.fst x.snd) t)
    (\a, b, m, f => MkLens (f.get . m.π1)
        (\x, y => m.π2 (f.set (m.π1 x) y)))
    (\f => funExt $ \(MkLens g s) => Refl)
    (\a, b, c, f, g => Refl)

prismIsProfunctor : Boundary -> Set -/> Set
prismIsProfunctor t = MkFunctor
    (\x => Prism (MkB x.fst x.snd) t)
    (\a, b, m, p => MkPrism
        (mapFst m.π2 . p.match . m.π1)
        (m.π2 . p.build))
    (\f => funExt $ \(MkPrism m b) =>
          cong2
              MkPrism
              (funExt $ \xn => bifunctorId' (m xn))
              Refl)
    (\a, b, c, f, g => funExt $ \(MkPrism mm bb) =>
        cong2
            MkPrism
            (funExt $ \xm => bimapCompose _ )
            Refl)

traversalIsProfunctor : Boundary -> Set -/> Set
traversalIsProfunctor t = MkFunctor
    (\x => Traversal (MkB x.fst x.snd) t)
    (\a, b, m, p => MkTraversal $ \tx =>
        (p.extract (m.π1 tx)).π1 ##
            ((p.extract (m.π1 tx)).π2.π1 && m.π2 . (p.extract (m.π1 tx)).π2.π2))
    (\x => funExt $ \(MkTraversal y) =>
          cong MkTraversal (funExt $ \xy =>
              sigEqToEq $ MkSigEq Refl
                  (projIdentity (y xy).π2)))
    (\a, b, c, f, g => Refl)
