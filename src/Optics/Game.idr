module Optics.Game

import Optics.Lens
import Optics.Nash
import Optics.Dependent
import Data.Container
import Optics.Parameterised
import Data.Product
import Data.Boundary

revDeriv : Boundary -> Boundary
revDeriv a = MkB a.π1 (a.π1 -> a.π2)


cart : Boundary -> Boundary -> Boundary
cart a b = (MkB (a.proj1 * b.proj1) (a.proj2 * b.proj2))

public export
Player : Type -> Boundary -> Type
Player profiles param
  = Lens (MkB profiles (profiles -> Bool)) (revDeriv param)

public export
fromLens : Lens a b -> DLens (fromB a) (fromB b)
fromLens x = MkDLens x.get x.set

public export
(##) : Player s (au) -> Player s' (au') -> Player (s * s') (cart au au')
player1 ## player2 = MkLens id (\_, (phi && psi), (a && b) => phi a && psi b) |> parallel player1 player2 |> nashator

public export
interface Listable x where
  allValues : List x

public export
interface Default t where
  def : t

public export
maximum : Ord a => a -> List a -> a
maximum x [] = x
maximum x (a :: b) = maximum (max a x) b

public export
argmax : Listable x => Default a => Ord a => Eq a
    => (x -> a) -> (x -> Bool)
argmax f x = f x == maximum def (map f allValues)

public export
CoState : Container -> Type
CoState x = DLens x CUnit

public export
State : Container -> Type
State = DLens CUnit

public export
Context : (a, b : Container) -> Type
Context a b = Pair (State a) (CoState b)

public export
Arena : ?
Arena = DPara

runitor : DLens (Const2 () (() -> ())) CUnit
runitor = MkDLens id (const const)

lunitor : DLens CUnit (Const2 () (() -> ()))
lunitor = MkDLens id const

packup : DPara l p r -> Context l r -> Para BUnit (RC' p) BUnit
packup p (st, co) = let v = revPara ((st `preCompose` p) `postCompose` co) in lunitor `preCompose` (v `postCompose` runitor)

export
fixpoints : (Listable x) => (x -> x -> Bool) -> List x
fixpoints f = filter (\x => f x x) allValues

export
paraStateTofun : DPara CUnit p CUnit -> (x : p.shp) -> p.pos x
paraStateTofun (MkDPara (MkDLens _ coplay)) p = fst (coplay (p && ()) ())

export
funToCostate : (x -> s) -> CoState (Const2 x s)
funToCostate f = MkDLens (const ()) (\x, _ => f x)

export
scalarToState : x -> State (Const2 x s)
scalarToState x = MkDLens (const x) const

-- Computes the equilibria of a game in a given context
public export
equilibria : (Listable profiles)
  => Player profiles au
  -> Context l r
  -> Arena l (fromB au) r
  -> List profiles
equilibria player ctx arena = let packed = packup arena ctx
                                  play = reparam (fromLens player) packed
                              in fixpoints (paraStateTofun play)

