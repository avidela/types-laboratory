module Optics.Adapter

import Data.Boundary

public export
record Adapter (a, b : Boundary) where
  constructor MkAdt
  fwd : a.π1 -> b.π1
  bwd : b.π2 -> a.π2
