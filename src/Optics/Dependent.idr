module Optics.Dependent

import public Data.Container
import public Data.Category
import public Data.Product
import public Data.Coproduct

||| Dpendent lenses are container morphisms
public export
DLens : (a, b : Container) -> Type
DLens = (=%>)

public export
MkDLens : {0 c1 : Container} -> {0 c2 : Container} ->
          (fwd : (c1 .shp -> c2 .shp)) ->
          ((x : c1 .shp) -> c2 .pos (fwd x) -> c1 .pos x) ->
          c1 =%> c2
MkDLens = (<!)
