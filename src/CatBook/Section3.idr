module CatBook.Section3

import CatBook.Section1

record HasInitial (cat : Category) where
  constructor MkInitial
  initial : cat.o
  init : {obj : cat.o} -> initial ~> obj

record HasTerminal (cat : Category) where
  constructor MkTerminal
  terminal : cat.o
  term : {obj : cat.o} -> obj ~> terminal

Terminal : (cat : Category) => (term : HasTerminal cat) => cat.o
Terminal = term.terminal

SetInitial : HasInitial Set
SetInitial = MkInitial Void absurd

SetTerminal : HasTerminal Set
SetTerminal = MkTerminal () (const ())

natInitial : HasInitial NatLTE
natInitial = MkInitial Z LeZ

record HasNNO (cat : Category) (term : HasTerminal cat) where
  constructor MkNNO
  elem : cat.o
  zero : Terminal ~> elem
  succ : elem ~> elem
  u : {a : cat.o} -> (Terminal ~> a) -> (a ~> a) -> elem ~> a

  rec0 : {a : cat.o} ->
         (q : Terminal ~> a) -> (f : a ~> a) ->
         Compose Terminal elem a zero (u {a} q f) = q

  recS : {a : cat.o} ->
         (q : Terminal ~> a) -> (f : a ~> a) ->
         Compose elem elem a succ (u {a} q f) =
         Compose elem a a (u {a} q f) f

public export 0
setNNO : HasNNO Set SetTerminal
setNNO = MkNNO Nat (const Z) S foldNat prfRec0 ?prfRecS
  where
    foldNat : (() -> a) -> (a -> a) -> Nat -> a
    foldNat f g 0 = f ()
    foldNat f g (S k) = g (foldNat f g k)

    0
    prfRec0 : (q : (() -> a)) -> (f : (a -> a)) -> andThen (\value => 0) (foldNat q f) = q
    prfRec0 q f = funExt $ \() => Refl

    0
    prfRecS : (q : (() -> a)) -> (f : (a -> a)) -> andThen S (foldNat q f) = andThen (foldNat q f) f
    prfRecS q f = funExt $ \case Z => Refl
                                 S n => Refl




