module CatBook.Section2

import CatBook.Section1

private infixl 4 >:<
private infixl 4 ><
private infix 2 -.-
private infix 2 -*-
private infix 2 *:*

%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)

record HasProduct (cat : Category) where
  constructor MkProduct
  (>:<) : cat.o -> cat.o -> cat.o
  pi1 : (a, b : cat.o) -> a >:< b ~> a
  pi2 : (a, b : cat.o) -> a >:< b ~> b

  (*:*) : {a, b, o: cat.o} ->
          o ~> a ->
          o ~> b ->
          o ~> (a >:< b)

  -- composing the unique morphism from c to a × b with pi1
  -- is the same as going through f1
  prodLeft : {a, b, o : cat.o} ->
             (f1 : o ~> a) -> (f2 : o ~> b) ->
             Compose o (a >:< b) a
             ((*:*) {a} {b} {o} f1 f2) (pi1 a b)
             === f1

  prodRight : {a, b, o : cat.o} ->
              (f1 : o ~> a) -> (f2 : o ~> b) ->
              Compose o (a >:< b) b
              ((*:*) {a} {b} {o} f1 f2) (pi2 a b)
              === f2

public export
(><) : (cat : Category) => (prod : HasProduct cat) => cat.o -> cat.o -> cat.o
(><) = (>:<) prod

public export 0
(-*-) : (cat : Category) => (prod : HasProduct cat) =>
        {a, b, d : cat.o} ->
        a ~> b -> a ~> d -> a ~> b >< d
(-*-) = (*:*) prod

public export 0
swap : (cat : Category) => (prod : HasProduct cat) => {a, b : cat.o} -> a >< b ~> b >< a
swap = (prod.pi2 a b) -*- (prod.pi1 a b)

public export 0
(-.-) : (cat : Category) => (prod : HasProduct cat) =>
        {a, b, c, d : cat.o} ->
        a ~> b -> c ~> d -> a >< c ~> b >< d
(-.-) m1 m2 = (prod.pi1 a c |> m1) -*- (prod.pi2 a c |> m2)

Prod : o Set -> o Set -> o Set
Prod x y = Pair x y

proj1 : (a, b : o Set) -> Prod a b -> a
proj1 a b (x, y) = x

proj2 : (a, b : o Set) -> Prod a b -> b
proj2 a b (x, y) = y

mkProd : {a, b, o : o Set} ->
         (o -> a) ->
         (o -> b) ->
         (o -> Prod a b)
mkProd f g x = (f x, g x)

0
mkProdRight : {a, b, o : o Set} ->
              (f1 : o -> a) -> (f2 : o -> b) ->
              andThen {a=o} {b=(Prod a b)} {c=b}
              (mkProd {a} {b} {o} f1 f2) (proj2 a b)
              === f2
mkProdRight f1 f2 = funExt $ \v => Refl

0
mkProdLeft : {a, b, o : o Set} ->
             (f1 : o -> a) -> (f2 : o -> b) ->
             andThen {a=o} {b=(Prod a b)} {c=a}
             (mkProd {a} {b} {o} f1 f2) (proj1 a b)
             === f1
mkProdLeft f1 f2 = funExt $ \v => Refl

0
SetHasProduct : HasProduct Set
SetHasProduct = MkProduct
  Prod
  proj1
  proj2
  mkProd
  mkProdLeft
  mkProdRight
