module CatBook.Section4Training

import Language.STLC
import Data.Category
import Data.Category.Cartesian
import Data.Category.Terminal
import Data.Category.Exponential
import Data.Category.Product
import Control.Relation
import Control.Order
import Relation.Equality

%default total
%hide Prelude.Z
%hide Prelude.S
%hide Language.Ops.infixr.(~>)
%hide Prelude.(|>)
%hide Prelude.Ops.infixl.(|>)
%hide Data.Category.Ops.infixr.(=>>)

parameters {0 objects, base : Type}
           {cat : Category objects}
           {prod : HasProduct cat}
           {exp : HasExp cat prod}
           {term : HasTerminal cat}
           {interp : base -> objects}

  namespace Types
    public export 0
    pure : Ty base -> objects
    pure (x =>> y) = [| x |] -^> [| y |]
    pure (Base x) = interp x

  namespace Contexts
    public export 0
    pure : Ctxt base -> objects
    pure [] = term.terminal
    pure (gam &. t) = [| gam |] >< [| t |]

  namespace Variables
    public export 0
    pure : gam ∋ t -> [| gam |] ~> [| t |]
    pure Z = prod.pi2
    pure (S _ a) = prod.pi1 |> Variables.pure a

  namespace Judgements
    public export 0
    pure : {t : Ty base} -> {gam : Ctxt base} ->
           gam |- t ->
           [| gam |] ~> [| t |]

    pure (Var x) = Variables.pure x

    pure (Lam x {a} {b}) {gam} =
        exp.uncurryλ [|gam|] [|a|] [|b|] [|x|]

    pure (App x y {a} {b=t}) = let
          f2 = [| y |]
          f1 = [| x |]
       in (f1 -*- f2) |> exp.eval [| t |] [| a |]

  namespace Renamings
    public export 0
    pure : {gam, del : Ctxt base} ->
             del `Rename` gam ->
             [| del |] ~> [| gam |]
    pure {gam = []} {del} ren = term.toTerm [| del |]



    pure {gam = gs &. g} {del} ren =
      let p1 : Rename del gs = shift ren
          p2 : del ∋ g = ren g Z
       in Renamings.pure p1 -*- Variables.pure p2

  namespace Substitutions
    public export 0
    pure : {gam, del : Ctxt base} ->
              del `Subst` gam ->
              [| del |] ~> [| gam |]
    pure {gam=[]} ren = term.toTerm _
    pure {gam=gs &. g} ren =
      prod.prod (Substitutions.pure (\v, var => ren v (S g var)))
                (Judgements.pure (ren g Z))



















  0
  renameVar : (gam, del : Ctxt base) ->
              (t : Ty base) ->
              (ren : del `Rename` gam ) ->
              (var : gam ∋ t) ->
              Compose [|del|] [|gam|] [|t|] [|ren|] [|var|] ===
              Variables.pure (ren _ var)
  renameVar (gam &. t) del t ren Z = prod.prodRight _ _



--                   [|(ren (S x))|]
--                 Δ ──────────────→ t
--                 │ ⟍               ↑
--                 │   ⟍             │
--                 │     ⟍           │
--  prod ([|ren|]) │       ⟍         │ [|x|]
--       ([|x|])   │         ⟍       │
--                 │           ⟍     │
--                 │             ⟍   │
--                 ↓               ↘︎ │
--                Γ×g ─────────────→ Γ
--                          π₁
  renameVar (gam &. s) del t ren (S s x) =
    let ind = renameVar gam del t (shift ren) x
        prf = prfShift x {sub = ren} in
        glueTriangles (prod.prodLeft _ _) (ind `trans` cong pure prf)


































  0
  renameTerm : (gam, del : Ctxt base) -> (t : Ty base) ->
               (ren : del `Rename` gam) -> (term : gam |- t) ->
               Compose [|del|] [|gam|] [|t|] [|ren|] [|term|] ===
               pure (rename ren t term)
































  renameTerm gam del t ren (Var x) = renameVar gam del t ren x




















  renameTerm gam del (a =>> b) ren (Lam x) = begin @{EP} (===) $
      (Renamings.pure ren |> exp.uncurryλ _ _ _ (Judgements.pure x))
         -< ?step1 >-
      exp.uncurryλ _ _ _ ((Renamings.pure ren -.- cat.id [|a|]) |> Judgements.pure x)
         -< ?step2 >-
      End (exp.uncurryλ _ _ _ (Judgements.pure (rename (extend ren) b x)))






















{-
    let extRenId = prod.uniq (prod.prodLeft _ _ `trans` sym (pi1Sub gam del ren))
                             (prod.prodRight _ _ `trans` sym cat.idRight) in
    begin (===) $
      (Renamings.pure ren |> exp.uncurryλ _ _ _ (Judgements.pure x))
         -< sym (exp.naturalLam _ _) >-
      exp.uncurryλ _ _ _ (Renamings.pure ren -.- cat.id [|a|] |> Judgements.pure x)
         -< cong (exp.uncurryλ _ _ _) (congMor1 extRenId _ `trans` renameTerm _ _ _ (extend ren) x) >-
      End (exp.uncurryλ _ _ _ (Judgements.pure (rename (extend ren) b x)))

-}





















  {-
         = let rec = renameTerm (gam &. a) (del &. a) b (extend ren) body
               nLam = sym (exp.naturalLam (Renamings.pure ren) (Judgements.pure body))
           in nLam `trans`
              (cong (exp.uncurryλ _ _ _)
                    ((congMor1 (sym $ prod.uniq (prod.prodLeft  _ _ `trans` pi1Sub gam del ren)
                                               (prod.prodRight _ _ `trans` cat.idRight))
                              (Judgements.pure body))
                    `trans` rec))
                    -}
    where

      --  Δ × a
      --    │ ⟍
      --    │   ⟍  [[ shift extend ren ]]
      --  π₁│     ⟍
      --    │       ⟍
      --    ↓         ↘︎
      --    Δ ────────→ Γ
      --      [[ ren ]]
      0
      pi1Sub : {val : Ty base} -> (gam, del : Ctxt base) -> (ren : del `Rename` gam) ->
               Start ([|del|] >< [|val|] -< prod.pi1 {a=[|del|]} {b=[|val|]} >-
                      [|del|]            -< [|ren|] >-
                 End (Contexts.pure gam)) ===
               Renamings.pure {gam} {del = del &. val} (shift (extend ren))
      pi1Sub [] del ren = term.toTermUniq _ _
      pi1Sub (gs &. g) del ren {val} =
        let rec = pi1Sub gs del (shift ren)
        -- rec : π1 |> [[ shift ren ]] === [[ shift extend shift ren ]]
        --  Δ × a
        --    │ ⟍
        --    │   ⟍  [[ shift extend shift ren ]]
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ────────→  gs
        --      [[ shift ren ]]
        --
        -- goal : π1 |> prod [[ shift ren ]] [[ ren Z ]]
        --    === prod [[ shift shift extend ren ]] (π₁ |> [[ ren Z ]])
        --
        --  Δ × a
        --    │ ⟍
        --    │   ⟍  (prod [[ shift shift extend ren ]] (π₁ |> [[ ren Z ]])
        --  π₁│     ⟍
        --    │       ⟍
        --    ↓         ↘︎
        --    Δ ──────→  gs × g
        --      (prod [[shift ren ]] [[ ren Z ]])
         in natPairing (trans rec
                       (cong (Renamings.pure {gam=gs} {del = del &. val})
                       (shiftTwice {sub=ren})))
                       Refl

  renameTerm gam del b ren (App {a} {b} f x) =
    let renFun = renameTerm gam del (a =>> b) ren f
        renArg = renameTerm gam del a ren x
     in ?renameTermApp

    -- let f = renameTerm gam del (a =>> b) ren x
    --     v = renameTerm gam del a ren y
    --     prf1 = cong Judgements.pure (sym (renApp {sub=ren} {a} {b} {x} {y}))
    --     prf2 = prfAppTerm {x=rename ren (a =>> b) x} {y=rename ren a y}
    --  in tetrahedron (natPairing f v)
    --                 prfAppTerm
    --                 (trans prf2 prf1)
    where
      0
      prfAppTerm :
          {gam : Ctxt base} ->
          {a, b : Ty base} ->
          {x : gam |- a =>> b} ->
          {y : gam |- a} ->
          Compose [| gam |]
                  ([| a |] -^> [| b |] >< [| a |])
                  [|b|]
                  (prod.prod {c = [|gam|]}
                             {a = [|a|] -^> [|b|]}
                             {b = [|a|]}
                             [|x|]
                             [|y|])
                  (exp.eval [|b|] [|a|])
          === (pure (App x y))
      prfAppTerm {b=Base b} = Refl
      prfAppTerm {b=a =>> b} = Refl

{-
-}
