module CatBook.Section1

export infixr 1 ~>
export infixr 1 ~:>
export infixl 5 |>
export infixl 5 |:>
export infix 3 <>

||| A category is defined by its objects (`o`)and morphisms (`m`)
||| It must provide an identity morphism for each object and a composition
||| operation for composing morphisms. It also must provide proofs that
||| the identity is neutral wrt to composition and that composition is
||| associative.
||| `a`, `b`, `c`, `d` are names for objects
||| `f`, `g`, `h` are names for morphisms
public export
record Category where
  constructor MkCategory

  0 o : Type
  -- The morphisms
  (~:>) : o -> o -> Type

  -- The identity
  0 id : (v : o) -> v ~:> v

  -- The composition
  0 (|:>) : {a, b, c : o} -> (a ~:> b) -> (b ~:> c) -> (a ~:> c)

  -- The proofs
  0 idLeft : {a, b : o} -> {f : a ~:> b} -> f |:> id b = f
  0 idRight : {a, b : o} -> {f : a ~:> b} -> id a |:> f = f
  0 compAssoc : {a, b, c, d : o} ->
              {f : a ~:> b} ->
              {g : b ~:> c} ->
              {h : c ~:> d} ->
              f |:> (g |:> h) = (f |:> g) |:> h

public export
0 (|>) : (cat : Category) => {start, middle, end : cat.o} ->
         (~:>) cat start middle -> (~:>) cat middle end -> (~:>) cat start end
(|>) = Category.(|:>) cat

public export 0
Compose : (cat : Category) => (start, middle, end : cat.o) ->
          (~:>) cat start middle ->
          (~:>) cat middle end ->
          (~:>) cat start end
Compose _ _ _ = Category.(|:>) cat

public export
0 (~>) : (cat : Category) =>  cat.o -> cat.o -> Type
(~>) = Category.(~:>) cat


public export 0
funExt : {f, g : a -> b} -> ((x : a) -> f x = g x) ->  f = g

namespace SetCat
    public export
    Function : Type -> Type -> Type
    Function a b = a -> b

    public export
    identity : (a : Type) -> a -> a
    identity _ v = v

    public export
    andThen : (a -> b) -> (b -> c) -> a -> c
    andThen f g x = g (f x)

    public export
    0 fAndThenId : {a, b : Type} -> {f : a -> b} -> andThen f (identity b) = f
    fAndThenId = funExt $ \v => Refl

    public export 0
    idAndThenF : andThen (identity a) f = f
    idAndThenF = funExt $ \v => Refl

    public export 0
    andThenAssoc : f `andThen` (g `andThen` h) = (f `andThen` g) `andThen` h
    andThenAssoc = funExt $ \v => Refl

    public export
    Set : Category
    Set = MkCategory
      Type
      Function
      identity
      andThen
      fAndThenId
      idAndThenF
      andThenAssoc


namespace LinearCat
  private infixr 0 #->
  private infix 2 #>
  private infixl 1 $$

  public export
  record (#->) (a: Type) (b : Type) where
    constructor MkLin
    fn : (1 _ : a) -> b

  public export
  ($$) : a #-> b -> (1 _ : a) -> b
  ($$) (MkLin f) y = f y

  -- composition of linear functions
  public export
  (#>) : a #-> b -> b #-> c -> a #-> c
  (#>) (MkLin f) (MkLin g) = MkLin (\v => g (f v))

  linIdentity : (a : Type) -> a #-> a
  linIdentity _ = MkLin (\x => x)

  0
  idRightNeutral : {f : a #-> b} -> f #> LinearCat.linIdentity b = f
  idRightNeutral {f=MkLin f} = Refl

  0
  idLeftNeutral : {f : a #-> b} -> LinearCat.linIdentity a #> f = f
  idLeftNeutral {f=MkLin f} = Refl

  0
  linCompAssoc : {f : a #-> b} -> {g : b #-> c} -> {h : c #-> d} ->
                 f #> (g #> h) = (f #> g) #> h
  linCompAssoc {f = (MkLin f)} {g = (MkLin g)} {h = MkLin h}
      = Refl

  public export
  linearCategory : Category
  linearCategory = MkCategory
    Type
    (#->)
    linIdentity
    (#>)
    idRightNeutral
    idLeftNeutral
    linCompAssoc

namespace LTECat
  public export
  data (<=) : (_, _: Nat) -> Type where
    LeZ : Z <= n
    LeS : a <= b -> S a <= S b

  public export
  lteRefl : (n : Nat) -> n <= n
  lteRefl Z = LeZ
  lteRefl (S k) = LeS (lteRefl k)

  public export
  lteIrrelevant : (p, q : m <= n) -> p === q
  lteIrrelevant LeZ LeZ = Refl
  lteIrrelevant (LeS p) (LeS q) = cong LeS (lteIrrelevant p q)

  public export
  lteSuccRight : n <= m -> n <= (S m)
  lteSuccRight LeZ     = LeZ
  lteSuccRight (LeS x) = LeS (lteSuccRight x)

  public export
  lteSuccLeft : (S n) <= m -> n <= m
  lteSuccLeft (LeS x) = lteSuccRight x

  public export
  lteTrans : n <= m -> m <= p -> n <= p
  lteTrans LeZ y = LeZ
  lteTrans (LeS x) (LeS y) = LeS (lteTrans x y)

  public export
  lteIdLeft : {lte : n <= m} -> lteTrans lte (lteRefl m) = lte
  lteIdLeft = irrelevantEq $ lteIrrelevant (lteTrans lte (lteRefl m)) lte

  public export
  lteIdRight : {lte : n <= m} -> lteTrans (lteRefl n) lte = lte
  lteIdRight = irrelevantEq $ lteIrrelevant (lteTrans (lteRefl n) lte) lte

  public export
  lteAssoc : {l1 : a <= b} -> {l2 : b <= c} -> {l3 : c <= d} ->
             l1 `lteTrans` (l2 `lteTrans` l3)
             = (l1 `lteTrans` l2) `lteTrans` l3
  lteAssoc = irrelevantEq $ lteIrrelevant (l1 `lteTrans` (l2 `lteTrans` l3))
                                          ((l1 `lteTrans` l2) `lteTrans` l3)

  public export
  NatLTE : Category
  NatLTE = MkCategory
    Nat
    (<=)
    lteRefl
    lteTrans
    lteIdLeft
    lteIdRight
    lteAssoc
