module Effects.Handlers

data FreeState s a
  = Ret a
  | Get () (s  -> FreeState s a)
  | Put () (() -> FreeState s a)

mapFree : (a -> b) -> FreeState s a -> FreeState s b
mapFree f (Ret x) = Ret (f x)
mapFree f (Get x g) = Get x (mapFree f . g)
mapFree f (Put x g) = Put x (const $ mapFree f (g ()))

Functor (FreeState s) where
  map = mapFree

Applicative (FreeState s) where
  pure = Ret
  (<*>) f x = ?bbb

Monad (FreeState s) where
  (Ret y) >>= f = f y
  (Get y g) >>= f = Get () (\x => g x >>= f)
  (Put y g) >>= f = Put y (\x => g x >>= f)

data Free : (f : Type -> Type) -> (a : Type) -> Type where
  Return : a -> Free f a
  Do : (f (Free f a)) -> Free f a

Functor f => Functor (Free f) where
  map f (Return x) = Return (f x)
  map f (Do x) = Do $ map (map f) x

Applicative f => Applicative (Free f) where
  pure = Return
  (<*>) (Return y) (Return x) = Return (y x)
  (<*>) (Do y) (Return x) = Do (map (<*> Return x)  y)
  (<*>) (Return y) (Do x) = Do (map (map y) x)
  (<*>) (Do y) (Do x) = Do (?buu <*> x)

covering
Applicative f => Monad (Free f) where
  (Return x) >>= g = g x
  (Do x) >>= g = Do (map (>>= g) x)
