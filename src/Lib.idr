module Lib

export infix 0 ≡
public export
(≡) : a -> a -> Type
(≡) = (===)

public export
Any : (x -> Type) -> Maybe x -> Type
Any p (Just x) = p x
Any p Nothing = Void

public export
All : (x -> Type) -> Maybe x -> Type
All p (Just x) = p x
All p Nothing = Unit

public export
mapAny : (f : a -> b) -> {0 p : b -> Type} -> {0 q : a -> Type} ->
         {x : Maybe a} ->
         (m : {x : a} -> p (f x) -> q x) ->
         Any p (map f x) -> Any q x
mapAny f {x = (Just x)} m y = m y

public export
cong2' :
  {0 t1 : Type} ->
  {0 t2 : t1 -> Type} ->
  (f : ((x : t1) -> t2 x -> t3)) ->
  {0 a, b : t1} ->
  {0 c : t2 a} ->
  {0 d : t2 b} ->
  (p : a === b) ->
  c === (rewrite p in d) ->
  f a c = f b d
cong2' f Refl Refl = Refl


public export 0
funExt : {a : Type} -> {b : a -> Type} -> {f, g : (x : a) -> b x} ->
         ((x : a) -> f x = g x) -> f ≡ g

public export
maybeTypePresComp : (x : Maybe a) -> map (g . f) x ≡ map g (map f x)
maybeTypePresComp Nothing = Refl
maybeTypePresComp (Just x) = Refl

public export
mJoin : Maybe (Maybe a) -> Maybe a
mJoin (Just x) = x
mJoin Nothing = Nothing

public export
data (+) : Type -> Type -> Type where
  InL : a -> a + b
  InR : b -> a + b

public export
elim : (a -> c) -> (b -> c) -> a + b -> c
elim f g (InL x) = (f x)
elim f g (InR x) = (g x)

public export
record Σ (x : Type) (i : x -> Type) where
  constructor (&&)
  π1 : x
  π2 : i π1

%name Lib.(&&) p1, p2

