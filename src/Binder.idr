import Data.Vect
import Data.Sigma
import Data.Ops

(>>=) : (x : Type) -> (x -> Type) -> Type
(>>=) = Σ

test : do x <- Nat ; Vect x Nat
test = 3 ## [1,2,3]

