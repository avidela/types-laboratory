module Proofs.Sum

import Proofs.Extensionality
import Data.Coproduct

public export
0 bifunctorId' : {0 a, b : Type} ->
                 (x : a + b) ->
                Prelude.Interfaces.bimap {f = (+)} (Prelude.id {a}) (Prelude.id {a=b}) x
                === x
bifunctorId' (<+ x) = Refl
bifunctorId' (+> x) = Refl

public export
0 bifunctorId : {0 a, b : Type} ->
                Prelude.Interfaces.bimap {f = (+)} (Prelude.id {a}) (Prelude.id {a=b})
                === Prelude.id {a = a + b}
bifunctorId = funExt bifunctorId'

public export 0
bimapCompose : (x : a + b) ->
               Prelude.Interfaces.bimap {f = (+)} (g . f) (k . l) x
           === Prelude.Interfaces.bimap {f = (+)} g k (Prelude.Interfaces.bimap f l x)
bimapCompose (<+ x) = Refl
bimapCompose (+> x) = Refl

