module Proofs.Unit

export
unitUniq : (x : Unit) -> MkUnit === x
unitUniq () = Refl

