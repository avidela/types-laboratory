module Proofs.Parametricity

0 TBool : Type
TBool = forall a. a -> a -> a

toTBool : Bool -> TBool
toTBool False y z = z
toTBool True y z = y

fromTBool : TBool -> Bool
fromTBool f = f {a = Bool} True False

toFrom : (v : Bool) -> fromTBool (toTBool v) === v
toFrom False = Refl
toFrom True = Refl


Rel : Type -> Type -> Type
Rel a b = a -> b -> Type


tbool_parametric : {a, b :Type} -> {r : Rel a b} -> {x1, x2, y1, y2 : _} -> r x1 x2 -> r y1 y2 -> r (TBool x1 x2) (TBool y1 y2)

fromTo : (v : TBool) -> toTBool (fromTBool v) === v
fromTo v = ?fromTo_rhs

