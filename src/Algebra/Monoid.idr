module Algebra.Monoid

public export
record Monoid (a : Type) where
  constructor MkMon
  neutral : a
  (<+>) : a -> a -> a
  neutralLeft : (v : a) -> neutral <+> v = v
  neutralRight : (v : a) -> v <+> neutral = v
  opAssoc : (v1, v2, v3 : a) -> v1 <+> (v2 <+> v3) = (v1 <+> v2) <+> v3

