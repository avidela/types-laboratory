
import Data.Vect

0 ImplM : {m : Type -> Type} -> Vect (2 + n) Type -> Type
ImplM (x :: y :: []) = x -> m y
ImplM (x :: y :: z :: xs) = Pair (x -> m y) (ImplM {m} (y :: z :: xs))

RunM : {m : Type -> Type} -> (mon : Monad m) => {n : _} -> (p : Vect (2 + n) Type) -> ImplM p -> Vect.head p -> m (Vect.last p)
RunM [x, y]  f = ?add
RunM (x :: y :: z :: xs)  f = ?add2
