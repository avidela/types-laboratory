module Language.OrnamentedSTLC

import Language.STLC
import Language.Scoped
import Language.Untyped
import Data.Ornaments
import Data.Fin
import Data.Product
import Data.Sigma

%hide Builtin.infixr.(#)
%hide Language.Ops.prefix.(#)

%default partial


0 Context : Type -> Type
Context ty = Σ NatT (Vect ty)

ElemIndex : {a : Type} -> Context a * a -> NatT
ElemIndex x = x.π1.π1

STLCO : (base : Type) ->
        Orn (Ctxt base * Ty base) (const ()) (Parameterised.untyped Unit)
STLCO base = Ornaments.Ornaments.Sig (#["var", "app", "lam"]) $ \case
    "var" => △ (Ctxt base) $ \ctx =>
             △ (Ty base) $ \ty =>
             △ (ctx ∋ ty) $ \prf =>
             Σ Unit (\xn => Say (Ok (ctx && ty)))

    "app" => △ (Ctxt base) $ \ctx =>
             △ (Ty base) $ \dom =>
             △ (Ty base) $ \cod =>
             Ask (Ok (ctx && dom =>> cod)) $
             Ask (Ok (ctx && dom)) $
             Say (Ok (ctx && cod))

    "lam" => △ (Ctxt base) $ \ctx =>
             △ (Ty base) $ \dom =>
             △ (Ty base) $ \cod =>
             Σ Unit $ \_ =>
             Ask (Ok (ctx &. dom && cod)) $
             Say (Ok (ctx && dom =>> cod))

-- future syntax
--     "lam" => △ (ctx : Ctxt base) |
--              △ (dom : Ty base) |
--              △ (cod : Ty base) |
--              Sigma (_ : Unit) |
--              Ask (Ok (ctx &. dom && cod)) $
--              Say (Ok (ctx && dom =>> cod))
--
--     "var" => Delta (ctx : Ctxt base) |
--              Delta (ty : Ty base) |
--              Delta (prf : ctx >: ty) |
--              Sigma (_ : Unit) |
--              Say (Ok (ctx && ty))
--
--     "app" => Delta (ctx : Ctxt base) |
--              Delta (dom : Ty base) |
--              Delta (cod : Ty base) |
--              Ask (Ok (ctx && dom =>> cod)) $
--              Ask (Ok (ctx && dom)) $
--              Say (Ok (ctx && cod))

-- buildEvidence : {0 n : NatT} -> (i : FinT n) -> (xs : Vect (Ty a) n) -> xs ∋ index i xs

index : Vect a n -> FinT n -> a
index (Val ("S" ## (a1 ## q2))) (Val ("Z" ## p2)) = a1
index (Val ("S" ## head ## m ## tail ## Refl)) (Val ("S" ## m ## pq ## Refl))
    = index tail pq

STLCFromScoped : (base : Type) -> Orn (Σ NatT (\xn => Vect (Ty base) xn * FinT xn))
                                      (π1) (ScopedD Unit)
STLCFromScoped base = Sig (#["var", "app", "lam"]) $ \case
    "var" => Sig NatT $ \variables =>
             Sig (FinT variables) $ \bound =>
             Del (Vect (Ty base) variables) $ \context =>
             Sig Unit $ \val =>
             Say (Ok (variables ## (context && bound)))
    "app" => Sig NatT $ \n =>
             Del (Vect (Ty base) n) $ \context =>
             Del (Ty base) $ \a =>
             Del (Ty base) $ \b =>
             Del (FinT n) $ \lam =>
             Del (index context lam === a =>> b) $ \prf =>
             Del (FinT n) $ \arg =>
             Del (index context arg === a) $ \prf2 =>
             Del (FinT n) $ \ret =>
             Del (index context ret === b) $ \prf3 =>
             Ask (Ok (n ## (context && lam))) $
             Ask (Ok (n ## (context && arg))) $
             Say (Ok (n ## (context && ret)))
    "lam" => Sig NatT $ \n =>
             Del (Vect (Ty base) n) $ \ctx =>
             Sig Unit $ \_ =>
             Del (Ty base) $ \a =>
             Del (Ty base) $ \b =>
             Del (FinT (S n)) $ \func =>
             Del (index (ConsV a ctx) func === a =>> b) $ \prf1 =>
             Del (FinT n) $ \ret =>
             Del (index ctx ret === b) $ \prf2 =>
             Ask (Ok (S n ## (ConsV a ctx && func))) $
             Say (Ok (n ## (ctx && ret)))

-- 0 TypedO : {base, name : Type} -> Orn (Ctxt base * Ty base)
--                                 (STLC.length . π1) (ScopedD name)
-- TypedO = Sig (#["var", "app", "lam"]) $ \case
--     "var" => Sig Nat (\xn => Sig (Fin xn) (\fn => Sig name (\nm => Del (Ty base)
--                      (\type => Say (Ok (?bbb && type))))))
--     "app" => ?TypedO_rhs_1
--     "lam" => ?TypedO_rhs_12

