module Language.Untyped

import Data.Ornaments

%hide Builtin.infixr.(#)

data Untyped : Type -> Type where
  Var : x -> Untyped x
  App : Untyped x -> Untyped x -> Untyped x
  Lam : x -> Untyped x -> Untyped x


subst : Eq x => Untyped x -> x -> Untyped x -> Untyped x
subst (Var str1) str y = if str == str1 then y else Var str1
subst (App x z) str y = App (subst x str y) (subst z str y)
subst (Lam str1 x) str y = if str1 == str then Lam str1 x else Lam str1 (subst x str y)


-- levitated definitions:

namespace Hardcoded
  public export
  untyped : Desc ()
  untyped = Sig (#["var", "app", "lam"]) $ \case
                "var" => Sig String (const $ Say ())
                "app" => Ask () (Ask () (Say ()))
                "lam" => Sig String (const $ Ask () (Say ()))

  public export
  untypedTy : Type
  untypedTy = Data untyped ()

  %unbound_implicits off

  public export
  var : String -> untypedTy
  var str = Val $ "var" ## str ## Refl

  public export
  app : untypedTy -> untypedTy -> untypedTy
  app x y = Val $ "app" ## x ## y ## Refl

  public export
  lam : String -> untypedTy -> untypedTy
  lam str rec = Val $ "lam" ## str ## rec ## Refl

  fromDesc : untypedTy -> Untyped String
  fromDesc (Val ("var" ## p2)) = Var p2.π1
  fromDesc (Val ("app" ## v)) = App (fromDesc v.π1) (fromDesc v.π2.π1)
  fromDesc (Val ("lam" ## p2)) = Lam p2.π1 (fromDesc p2.π2.π1)

  toDesc : Untyped String -> untypedTy
  toDesc (Var x) = var x
  toDesc (App x y) = app (toDesc x) (toDesc y)
  toDesc (Lam x y) = lam x (toDesc y)

namespace Parameterised
  public export
  untyped : Type -> Desc ()
  untyped x = Sig (#["var", "app", "lam"]) $ \case
                "var" => Sig x (const $ Say ())
                "app" => Ask () (Ask () (Say ()))
                "lam" => Sig x (const $ Ask () (Say ()))

  %unbound_implicits off

  public export
  untypedTy : Type -> Type
  untypedTy x = Data (untyped x) ()

  parameters {0 a : Type}
    var : a -> untypedTy a
    var str = Val $ "var" ## str ## Refl

    app : untypedTy a -> untypedTy a -> untypedTy a
    app x y = Val $ "app" ## x ## y ## Refl

    lam : a -> untypedTy a -> untypedTy a
    lam str rec = Val $ "lam" ## str ## rec ## Refl

    fromDesc : untypedTy a -> Untyped a
    fromDesc (Val ("var" ## p2)) = Var p2.π1
    fromDesc (Val ("app" ## v)) = App (fromDesc v.π1) (fromDesc v.π2.π1)
    fromDesc (Val ("lam" ## p2)) = Lam p2.π1 (fromDesc p2.π2.π1)

    toDesc : Untyped a -> untypedTy a
    toDesc (Var x) = var x
    toDesc (App x y) = app (toDesc x) (toDesc y)
    toDesc (Lam x y) = lam x (toDesc y)

namespace Metadata
  -- untyped -> metadata
  public export
  LamMDO : (t, s : Type) -> Orn Unit (const ()) (Parameterised.untyped s)
  LamMDO t s = Del t $ \_ => Sig (#["var", "app", "lam"]) $ \case
      "var" => Sig s (\xs => Say $ Ok ())
      "app" => Ask (Ok ()) (Ask (Ok ()) (Say (Ok ())))
      "lam" => Sig s (\_ => Ask (Ok ()) (Say (Ok ())))

  public export
  untypedFDesc : Type -> Type -> Desc ()
  untypedFDesc t s = extract (LamMDO t s)

  public export
  untypedF : Type -> Type -> Type
  untypedF t s = Data (untypedFDesc t s) ()

  export
  getMetaData : {0 md, str : Type} -> untypedF md str -> md
  getMetaData (Val (t ## _)) = t
