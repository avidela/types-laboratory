module Language.SparseMatrix

import Data.SortedMap
import Data.Fin
import Data.Fin.Util
import Data.Vect

import Language.Locations
import Language.Lambda

-- instance to manipulate values in sparse matrices
private
Num a => Num (Maybe a) where
  fromInteger x = Just (fromInteger x)
  Nothing + Nothing = Nothing
  Nothing + Just x = Just x
  Just x + Nothing = Just x
  Just x + Just y = Just (x + y)
  Nothing * Nothing = Nothing
  Nothing * Just x = Just x
  Just x * Nothing = Just x
  Just x * Just y = Just (x * y)

namespace Loc

  public export
  data LamLoc : (width : Nat) -> Type where

    -- variables
    Var : (nm : LocF String) -> LamLoc 1

    -- lambda abstraction
    Lam : (lam : Span) ->
          (nm : LocF String) ->
          (dot : Span) ->
          (body : LamLoc tkns) ->
          LamLoc (3 + tkns)

    -- applications
    App : LamLoc l ->
          LamLoc r ->
          LamLoc (4 + l + r)

namespace Let

  public export
  data LetLoc : (width : Nat) -> Type where

    -- variables
    Var : LocF String -> LetLoc 1

    -- lambda abstraction
    Lam : (lam : Span) ->
          (name : LocF String) ->
          (dot : Span) ->
          (body : LetLoc t) ->
          LetLoc (3 + t)

    -- applications
    App : LetLoc f -> LetLoc x -> LetLoc (4 + f + x)

    -- let
    Let : (let_ : Span) ->
          (name : LocF String) ->
          (eq : Span) ->
          (bound : LetLoc b) ->
          (in_ : Span) ->
          (body : LetLoc t) ->
          LetLoc (4 + b + t)

namespace Sparse

  export
  record Matrix (w, h : Nat) (a : Type) where
    constructor MkSparse
    content : SortedMap (Fin w, Fin h) a

  (.at) :  Matrix w h a -> Fin w -> Fin h -> Maybe a
  m.at x y = lookup (x, y) m.content

  export
  fromList : List (Fin w, Fin h, a) -> Matrix w h a
  fromList = MkSparse . fromList . map (\(x, (y, z)) => ((x, y), z))

  export
  fromMap : SortedMap (Fin w, Fin h) a -> Matrix w h a
  fromMap = MkSparse


  toVect' : (zero : a) -> {w, h : Nat} -> Matrix w h a -> Vect w (Vect h a)
  toVect' zero x = for (Fin.range {len = h}) $ \line =>
                   flip map (Fin.range {len = w}) $ \col =>
                   fromMaybe zero (lookup (col, line) x.content)

  toVect : Num a => {w, h : Nat} -> Matrix w h a -> Vect w (Vect h a)
  toVect x = toVect' 0 x

  Functor (Matrix w h) where
    map f m = MkSparse $ map f m.content

  {w, h : Nat} -> Show a => Show (Matrix w h a) where
    show m = show $ toVect' " " $ map show m


  zipWithIndex : Vect n a -> Vect n (Fin n, a)
  zipWithIndex [] = []
  zipWithIndex (x :: xs) = (FZ, x) :: map (mapFst FS) (zipWithIndex xs)

  fromVect : Vect w (Vect h (Maybe a)) -> Matrix w h a
  fromVect xs = fromMap $ foldl (\acc, (r, row) => foldl
                                  (\acc2, (c, col) =>
                                     maybe acc2 (\v => insert (r, c) v acc2) col
                                  ) acc (zipWithIndex row)
                                ) empty (zipWithIndex xs)

  fromVect' : Eq a => (zero : a) -> Vect w (Vect h a) -> Matrix w h a
  fromVect' zero xs = fromVect $ map (map (\x => if x == zero then Nothing else Just x)) xs

  matIdx : {w, h : Nat} -> ((Fin w, Fin h) -> Maybe a) -> Matrix w h a
  matIdx f = fromVect $
    for (Fin.range {len = h}) $ \line =>
    flip map (Fin.range {len = w}) $ \col =>
    f (col, line)

  getRow : {h : Nat} -> Fin h -> Matrix w h a -> Vect h (Maybe a)
  getRow idx m = map ?getRow_rhs Fin.range

  getCol : Fin w -> Matrix w h a -> Vect w (Maybe a)
  getCol idx m = ?getCol_rhs

  export
  mult : Num a => {w, h, x : Nat} -> Matrix w x a -> Matrix x h a -> Matrix w h a
  mult m1 m2 = matIdx $ \(col, lin) =>
    foldl {t = Vect x}
        (\acc, idx => do
            let row = getRow idx m1
          ; let col = getCol idx m2
          ; sum (zipWith (*) row col)
          )
        (the (Maybe a) Nothing)
        (Fin.range {len = x})

  diagonal : {s : Nat} -> (v : a) -> Matrix s s a
  diagonal v = matIdx (\(x,y) => if x == y then Just v else Nothing)

  identity : Num a => {s : Nat} -> Matrix s s a
  identity = diagonal 0

  testMatrix : Matrix 3 3 Nat
  testMatrix = fromList [(0,0,3), (0,1,3), (1,1,3)]

  export
  add : Num a => {w, h : Nat} -> Matrix w h a -> Matrix w h a -> Matrix w h a
  add x y = matIdx $ \(col, line) => do
     val1 <- lookup (col, line) x.content
     val2 <- lookup (col, line) y.content
     pure (val1 + val2)

Num Bool where
  fromInteger 0 = False
  fromInteger _ = True
  (*) x y = x && y
  (+) x y = x || y

offsetBloc : (x1, x2 : Nat) -> {y1, y2 : Nat} ->
             Matrix x2 y2 a -> Matrix (x1 + x2) (y1 + y2) a
offsetBloc x y m = matIdx $ \(col, lin) => do
  col' <- greater col
  lin' <- greater lin
  m.at col' lin'

desugar : LetLoc n -> (m ** (LamLoc m, Matrix n m Bool))
desugar (Var x) = (1 ** (Var x, identity))
desugar (Lam lam name dot body) =
  let (ln ** (bdy, mx)) = desugar body
  in (3 + ln ** (Lam lam name dot bdy, ?aasd))
desugar (App x y) = let
  (n1 ** (f, mx)) = desugar x
  (n2 ** (v, my)) = desugar y
  in (4 + n1 + n2 ** (App f v, ?hello))
desugar (Let let_ name eq bound in_ body) = let
  (n1 ** (bnd, mx)) = desugar bound
  (n2 ** (bdy, my)) = desugar body
  in (_ **
       (App (Lam ?ddda name ?ddd bnd) bdy,
       ?bleh)
     )

