module Language.Surface

data Syntax : Type where
  Variable : String -> Syntax
  Lambda : List String -> Syntax -> Syntax
  Let : String -> Syntax -> Syntax -> Syntax
  App : Syntax -> Syntax -> Syntax
