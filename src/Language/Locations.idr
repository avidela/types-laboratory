module Language.Locations

import Data.SortedMap
import Data.Fin
import Data.Vect


public export
record Coord where
  constructor MkLoc
  line : Nat
  col : Nat

public export
record Span where
  constructor MkSpan
  -- start location
  start : Coord
  -- how far right and down does the sequence span
  -- for example if the start location is 10:4
  -- and the delta is 0:4, then the span ranges from
  -- 10:4 to 10:8
  -- To span multiple lines, the delta needs a positive
  -- number of lines in the delta, for example a delta
  -- of 2:5 creates a range up to line 12 and column 13
  delta : Coord

public export
record LocF (a : Type) where
  constructor LocS
  span : Span
  value : a

