module Language.Pollack

import Data.Container
import Data.List
import Data.List.Elem
import Data.Fin
import Data.String
import Data.Sigma

data Token a
  = Lam a
  | Ident String a
  | Num Nat a
  | BracketL a
  | BracketR a
  | Space a
  | EqSign a
  | DotSign a
  | Let a
  | In a

{-
todo next:
- Handle line breaks
- prove that every prints can be parsed
- write this as a container morphism
- Use the followin data structure to rewrite the list of tokens and extend the language
data Enum : List String -> Type where
  V : (x : String) -> Elem x xs => Enum xs

TokenCont : Container
TokenCont = Const2 (Enum ["Lam", "Ident", "Num", "BracketL", "BracketR", "Space", "EqSign", "DotSign", "Let", "In"]) Unit

TokenFunctor : Type -> Type
TokenFunctor = Ex TokenCont

mkLam : a -> TokenFunctor a
mkLam x = V "Lam" ## const x
-}

textRepr : Token a -> String
textRepr (Lam x) = "lam"
textRepr (Ident str x) = str
textRepr (Num k x) = show k
textRepr (BracketL x) = "("
textRepr (BracketR x) = ")"
textRepr (Space x) = " "
textRepr (EqSign x) = "="
textRepr (DotSign x) = "."
textRepr (Let x) = "let"
textRepr (In x) = "in"

counit : Token a -> a
counit (Lam x) = x
counit (Ident str x) = x
counit (Num k x) = x
counit (BracketL x) = x
counit (BracketR x) = x
counit (Space x) = x
counit (EqSign x) = x
counit (DotSign x) = x
counit (Let x) = x
counit (In x) = x

record Location where
  constructor MkLoc
  position : Nat
  length : Nat

record WithLocation (a : Type) where
  constructor MkWithLoc
  val : a
  loc : Location

TToken : Type
TToken = Token ()

data TokenDiet = Allowed | Ending | Disallowed

spanToken : (a -> TokenDiet) -> List a -> Maybe (List a, List a)
spanToken f [] = Just ([], [])
spanToken f (x :: xs) = case f x of
                             Allowed => map (mapFst (x ::)) (spanToken f xs)
                             Disallowed => Nothing
                             Ending => Just ([], x :: xs)

alphanumToken : Char -> TokenDiet
alphanumToken x = if isAlphaNum x then Allowed
                  else if isSpace x || x == '(' || x == ')' then Ending
                  else Disallowed
State : Type
State = Nat

lexerFragment : List Char -> (state : Nat) -> (acc : List (Token Location)) -> Either String (List (Token Location))
lexerFragment ['l' , 'a' , 'm'] state acc
    = Right $ [Lam (MkLoc state 3)]
lexerFragment ['l' , 'e' , 't' ] state acc
    = Right $ [Let (MkLoc state 3)]
lexerFragment ('(' :: rest) state acc
    = lexerFragment rest (state + 1) (BracketL (MkLoc state 1) :: acc)
lexerFragment (')' :: rest) state acc
    = lexerFragment rest (state + 1) (BracketR (MkLoc state 1) :: acc)
lexerFragment ('.' :: rest) state acc
    = lexerFragment rest (state + 1) (DotSign (MkLoc state 1) :: acc)
lexerFragment ['i' , 'n'] state acc
    = Right $ [In (MkLoc state 2)]
lexerFragment ('=' :: rest) state acc
    = lexerFragment rest (state + 1) (EqSign (MkLoc state 1) :: acc)
lexerFragment input@(x :: xs) state acc
    = if isAlpha x then
         let Just (str, rest) = spanToken alphanumToken input
             | Nothing => Left "unexpected sequence: \{pack input}"
             len = length str
          in lexerFragment rest (state + len) (Ident (pack str) (MkLoc state len) :: acc)
      else if isDigit x then
         let (str, rest) = span isDigit input
             len = length str
             Just n = parsePositive (pack str)
             | Nothing => Left "unexpected sequence: \{pack input}"
          in lexerFragment rest (state + len) (Num n (MkLoc state len) :: acc)
      else Left "unexpected sequence: \{pack input}"
lexerFragment [] state acc = Right (acc)

LexerState : Type
LexerState = (List Char, State)

lexerWhitespace : List Char -> Either String (List (Token Location))
lexerWhitespace cs = let s1 = splitWhitespace (cs, 0) ([], Z) []
                         s2 = traverse lexAll s1
                     in concat <$> s2
  where
    splitWhitespace : (current, local : LexerState) ->
                      List LexerState -> List LexerState
    splitWhitespace ([], state) accLocal accAll = reverse (mapFst reverse accLocal :: accAll)
    splitWhitespace ((x :: xs), state) accLocal accAll
        = if isSpace x
             then splitWhitespace (xs, S state) ([], state) (mapFst reverse accLocal :: accAll)
             else splitWhitespace (xs, S state) (mapFst (x ::) accLocal) accAll

    lexAll : LexerState -> Either String (List (Token Location))
    lexAll (x, y) = lexerFragment x y []

lexer : String -> Either String (List (Token Location))
lexer str = lexerWhitespace (unpack str)

printExact : (lastLoc : State) -> List (Token Location) ->  String
printExact st [] = ""
printExact st (x :: xs) =
  let loc = counit x
  in textRepr x ++ replicate (S loc.position `minus` st) ' '
  ++ printExact (loc.position + loc.length) xs

print : List (Token Location) -> String
print [] = ""
print [x] = textRepr x
print (x :: xs) = textRepr x ++ " " ++ print xs
