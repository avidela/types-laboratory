module Language.Progress

import Data.Container
import Data.Container.Morphism
import Data.Coproduct
import Data.Product

data Term : Type where
  N : Nat -> Term
  Add : Term -> Term -> Term

mutual

  0 TTerm : Type
  TTerm = TermN + TermAdd

  data TermN : Type where
    TN : Nat -> TermN

  record TermAdd where
    constructor TAdd
    left : TTerm
    right : TTerm

data Choice : (f : a -> Type) -> (g : b -> Type) -> (x : a + b) -> Type where
  L : {0 x : a} -> {0 y : a + b} -> (<+ x) = y -> f x -> Choice f g y
  R : {0 x : b} -> {0 y : a + b} -> (+> x) = y -> g x -> Choice f g y

cop : Container -> Container -> Container
cop c1 c2 = (t : c1.req + c2.req) !> Choice c1.res c2.res t

diaF : cop a a =%> a
diaF = dia <!
    (\case (<+ x) => L Refl
          ; (+> x) => R Refl
          )

choiceF : (a =%> a') -> (b =%> b') -> cop a b =%> cop a' b'
choiceF m1 m2 = bimap m1.fwd m2.fwd <!
    (\case (<+ x) => \(L Refl y) => L Refl (m1.bwd x y)
         ; (+> x) => \(R Refl y) => R Refl (m2.bwd x y)
         )

data Val : Term -> Type where
  N' : (n : Nat) -> Val (N n)

data TVal : TTerm -> Type where
  TN' : (n : Nat) -> TVal (<+ TN n)

data TSmallStep : TTerm -> TTerm -> Type where
  STVal : TSmallStep (+> TAdd (<+ TN m) (<+ TN n)) (<+ TN (m + n))

  TAddL : TSmallStep a a' ->
          TSmallStep (+> TAdd a b)
                    (+> TAdd a' b)
  TAddR : TSmallStep b b' ->
          TSmallStep (+> TAdd (<+ TN m) b)
                     (+> TAdd (<+ TN m) b')

data SmallStep : Term -> Term -> Type where
  SVal : SmallStep (Add (N m) (N n)) (N (m + n))
  AddL : SmallStep a a' -> SmallStep (Add a b) (Add a' b)
  AddR : SmallStep b b' -> SmallStep (Add (N m) b) (Add (N m) b')


namespace Specific
  data TProgressArith : TTerm -> Type where
    TDone : {m : TTerm} -> TVal m -> TProgressArith m
    TStep : {m, e : TTerm} -> TSmallStep m e -> TProgressArith m

  data ProgressArith : Term -> Type where
    Done : Val m -> ProgressArith m
    Step : {e : Term} -> SmallStep m e -> ProgressArith m

  Prog : Container
  Prog = (t : Term) !> ProgressArith t

  ProgVal : Container
  ProgVal = (t : TermN) !> TProgressArith (<+ t)

  ProgAdd : Container
  ProgAdd = (t : TermAdd) !> TProgressArith (+> t)

  TProg : Container
  TProg = (t : TermN + TermAdd)
      !> Choice (TProgressArith . (<+)) (TProgressArith . (+>)) t

  splitAdd : ProgAdd =%> TProg ⊗ TProg
  splitAdd = (\tadd => tadd.left && tadd.right) <!
             (\(TAdd t s) => bwd t s)
    where
      fwd : TermAdd -> TTerm * TTerm
      fwd (TAdd l r) = l && r

      bwd : (t, s : TTerm ) -> (TProg ⊗ TProg).res (t && s) ->
            TProgressArith (+> TAdd t s)
      bwd (<+ TN t) (<+ TN s)
          (L {x = TN t} Refl (TDone {m = <+ TN t} (TN' t)) &&
           L {x = TN s} Refl (TDone {m = <+ TN s} (TN' s)))
           = TStep STVal
      bwd (<+ TN t) (+> TAdd s s')
          (L {x = TN t} Refl (TDone {m = <+ TN t} q1) &&
           R {x = TAdd s s'} Refl (TStep {m = +> TAdd s s', e} q2))
          = TStep (TAddR q2)
      bwd (+> t) (s)
          (R {x = t} Refl (TStep {m = +> t, e} q1) && q2)
          = TStep (TAddL q1)

--       bwd (<+ p1 && +> p2) (q1 && q2) = ?bwd_rhs_12
--       bwd (+> p1 && <+ p2) (q1 && q2) = ?bwd_rhs_1
--       bwd (+> p1 && +> p2) (q1 && q2) = ?bwd_rhs_13

  %hide Prelude.Ops.infixl.(|>)
  %hide Prelude.(|>)
  mutual
    progressVal : Costate ProgVal
    progressVal = costate (\(TN n) => TDone (TN' n))

    progressAdd : Costate ProgAdd
    progressAdd = splitAdd |%> (progressAll `parallel` progressAll) |%> joinUnit

    progressAll : Costate TProg
    progressAll = (progressVal `choiceF` progressAdd) |%> diaF


  -- progress : (t : Term) -> ProgressArith t
  -- progress (N n) = Done (N' n)
  -- progress (Add t1 t2) with (progress t1)
  --   progress (Add _ e1) | Done v with (progress e1)
  --     progress (Add (N m) (N n)) | Done (N' m) | Done (N' n) = Step {e = N (m + n)} SVal
  --     progress (Add (N m) e1) | Done (N' m) | (Step {e} s) = Step {e = Add (N m) e } (AddR s)
  --   progress (Add e0 e1) | (Step {e} s) = Step {e = Add e e1} (AddL s)

  total
  progress : (t : Term) -> ProgressArith t
  progress (N n) = Done (N' n)
  progress (Add t1 t2) with (progress t1) | (progress t2)
    progress (Add (N m) (N n)) | Done (N' m) | Done (N' n) = Step {e = N (m + n)} SVal
    progress (Add (N m) e1)    | Done (N' m) | (Step {e} s) = Step {e = Add (N m) e } (AddR s)
    progress (Add e0 e1)       | (Step {e} s) | _ = Step {e = Add e e1} (AddL s)


  progressLens : Costate Prog
  progressLens = costate progress

{-
namespace General
  progress : (tm : Term) -> Progress Val SmallStep tm
  progress (N n) = Done (N' n)
  progress (Add t1 t2) with (progress t1)
    progress (Add _ e1) | (Done v) with (progress e1)
      progress (Add (N m) (N n)) | Done (N' m) | Done (N' n) = Step {e = N (m + n)} SVal
      progress (Add (N m) e1) | Done (N' m) | (Step {e} s) = Step {e = Add (N m) e } (AddR s)
    progress (Add e0 e1) | (Step {e} s) = Step {e = Add e e1} (AddL s)

