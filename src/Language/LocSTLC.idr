module Language.LocSTLC

import Language.Untyped
import Data.Ornaments

%hide Builtin.infixr.(#)

LetLam : Desc ()
LetLam = Sig (#["let", "lam"]) $ \case
    "let" => Sig String $ \_ =>
             Ask () $ Ask () (Say ())
    "lam" => Hardcoded.untyped

LetLamTy : Type
LetLamTy = Data LetLam ()

record Loc where
  constructor MkLoc
  line : Nat
  col : Nat

LocLetLam : Type
LocLetLam = Data (Sig Loc (const LetLam)) ()

namespace Location

  public export
  localLam : Orn () (\x => x) (Hardcoded.untyped)
  localLam =
      (△ Loc (\loc => IdO Hardcoded.untyped
             )
      )

  public export
  localDesc : Desc ()
  localDesc = extract localLam

  public export
  LocLam : Type
  LocLam = Data localDesc ()

  MkApp : Loc -> LocLam -> LocLam -> LocLam
  MkApp loc fn x = Val
      (loc ## "app" ## fn ## x ## Refl)

  MkVar : Loc -> String -> LocLam
  MkVar loc str = Val (loc ## "var" ## str ## Refl)

  MkAbs : Loc -> String -> LocLam -> LocLam
  MkAbs loc bound body = Val (loc ## "lam" ## bound ## body ## Refl)

desugar : LetLamTy -> Hardcoded.untypedTy
desugar (Val ("let" ## p1))
  = app (lam p1.π1 (desugar p1.π2.π1))
        (desugar p1.π2.π2.π1)
desugar (Val ("lam" ## p2)) with (p2)
  desugar (Val ("lam" ## p2)) | ("var" ## p3)
    = var p3.π1
  desugar (Val ("lam" ## p2)) | ("app" ## p3)
    = app (desugar p3.π1) (desugar p3.π2.π1)
  desugar (Val ("lam" ## p2)) | ("lam" ## p3)
    = lam p3.π1 (desugar p3.π2.π1)

desugarFold : LetLamTy -> Hardcoded.untypedTy
desugarFold x = let
    xx = fold {x = \arg => Hardcoded.untypedTy}
        (\case ("let" ## ys) => app ys.π2.π1 ys.π2.π2.π1
               ("lam" ## ys) => Val (?bah)) x
  in xx

desugar' : LocLetLam -> LocLam
desugar' (Val (p1 ## p2)) = Val (p1 ## ?Huia)

