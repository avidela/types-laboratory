||| A lambda calculus parser
module Language.Parser

import public Text.Parse.Manual
import Derive.Prelude
import Data.String
import Data.Matrix
import Data.Vect
import Data.Ornaments

%default total
%language ElabReflection

%hide Ornaments.List

-- syntax:
--
-- number := ...
-- identifier := ...
--
-- application := term term
--
-- binder := identifier ":" term | identifier
--
-- lambda := "lam" binder "|" term
--
-- term := "(" term ")" | application | lambda | number | identifier
--
-- defn := identifier ":" term
--
-- clause := application "=" term
-- topLevel := defn | clause

namespace Desc
  -- TInt String
  public export
  TIntD : Desc ()
  TIntD = labelType "TInt" Integer

  -- TStr String
  public export
  TStrD : Desc ()
  TStrD = labelType "TStr" String

  -- TSpace Nat | TNewLine
  public export
  TWhitespaceD : Desc ()
  TWhitespaceD = ("TSpaceN" ::= liftType Nat) + ("TNewLine" ::= Say ())

  -- Base tokens
  public export
  TBaseD : Desc ()
  TBaseD = enumeration
      [ "LParen"
      , "RParen"
      , "TLam"
      , "TPipe"
      , "TColon"
      , "TDot"
      ]

  public export
  TNoWhitespace : Desc ()
  TNoWhitespace = flatPlus TBaseD
                $ flatPlus TIntD TStrD

  public export
  TAllTokens : Desc ()
  TAllTokens
    = flatPlus TBaseD
    $ flatPlus TIntD
    $ flatPlus TStrD TWhitespaceD

  public export
  TRemoveWhitespace : Orn () (\_ => ()) TAllTokens
  TRemoveWhitespace = Sig
    (OneOf (["LParen", "RParen", "TLam", "TPipe", "TColon", "TDot", "TInt", "TStr", "TSpaceN", "TNewLine"]))
    $ \case "LParen" => Say (Ok ())
            "RParen" => Say (Ok ())
            "TDot" => Say (Ok ())
            "TLam" => Say (Ok ())
            "TPipe" => Say (Ok ())
            "TColon" => Say (Ok ())
            "TInt" => Sig Integer (\_ => Say (Ok ()))
            "TStr" => Sig String (\_ => Say (Ok ()))
            "TSpaceN" => Del Void absurd
            "TNewLine" => Del Void absurd

%default covering

public export
Token : Type
Token = Data TAllTokens ()

AllTokensRemoveWS : Type
AllTokensRemoveWS = Data (extract TRemoveWhitespace) ()

public export
TokNoWhitespace : Type
TokNoWhitespace = Data TNoWhitespace ()

covering
export
Eq TokNoWhitespace where
  (==) (Val $ "LParen" ## _) (Val $ "LParen" ## _) = True
  (==) (Val $ "RParen" ## _) (Val $ "RParen" ## _) = True
  (==) (Val $ "TDot" ## _) (Val $ "TDot" ## _) = True
  (==) (Val $ "TLam" ## _) (Val $ "TLam" ## _) = True
  (==) (Val $ "TPipe" ## _) (Val $ "TPipe" ## _) = True
  (==) (Val $ "TColon" ## _) (Val $ "TColon" ## _) = True
  (==) (Val $ "TInt" ## x) (Val $ "TInt" ## y) = x.π1 == y.π1
  (==) (Val $ "TStr" ## x) (Val $ "TStr" ## y) = x.π1 == y.π1
  (==) _ _ = False

covering export
Show TokNoWhitespace where
  show (Val $ "LParen" ## _) = "LParen"
  show (Val $ "RParen" ## _) = "RParen"
  show (Val $ "TDot" ## _) = "TDot"
  show (Val $ "TLam" ## _)  = "TLam"
  show (Val $ "TPipe" ## _)  = "TPipe"
  show (Val $ "TColon" ## _) = "TColon"
  show (Val $ "TInt" ## x) = "TInt \{show x.π1}"
  show (Val $ "TStr" ## x) = "TStr \{x.π1}"
  show x = ""

covering
Show (Data TWhitespaceD ()) where
  show (x) = ?blehu

------------------------------------------------------------------------------------
-- Smart constructors
------------------------------------------------------------------------------------

public export
LParen : {xs : List String} ->
          (prf : IsYes ("LParen" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "LParen" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
LParen = Val $ "LParen" ## rewrite checkTy in Refl

public export
RParen : {xs : List String} ->
          (prf : IsYes ("RParen" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "RParen" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
RParen = Val $ "RParen" ## rewrite checkTy in Refl

public export
TDot : {xs : List String} ->
          (prf : IsYes ("TDot" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TDot" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
TDot = Val $ "TDot" ## rewrite checkTy in Refl

public export
TStr : {xs : List String} ->
          (prf : IsYes ("TStr" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TStr" (Tag.extract prf))) === Sig String (\_ => Say ())) =>
          String ->
          Data (Sig (OneOf xs) fn) ()
TStr n = Val $ "TStr" ## rewrite checkTy in n ## Refl

public export
TInt : {xs : List String} ->
          (prf : IsYes ("TInt" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TInt" (Tag.extract prf))) === Sig Integer (\_ => Say ())) =>
          Integer ->
          Data (Sig (OneOf xs) fn) ()
TInt n = Val $ "TInt" ## rewrite checkTy in n ## Refl

public export
TSpaceN : {xs : List String} ->
          (prf : IsYes ("TSpaceN" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TSpaceN" (Tag.extract prf))) === Sig Nat (\_ => Say ())) =>
          Nat ->
          Data (Sig (OneOf xs) fn) ()
TSpaceN n = Val $ "TSpaceN" ## rewrite checkTy in n ## Refl

public export
TLineRet : {xs : List String} ->
          (prf : IsYes ("TNewLine" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TNewLine" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
TLineRet = Val $ "TNewLine" ## rewrite checkTy in Refl

public export
TPipe : {xs : List String} ->
          (prf : IsYes ("TPipe" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TPipe" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
TPipe = Val $ "TPipe" ## rewrite checkTy in Refl

public export
TColon : {xs : List String} ->
          (prf : IsYes ("TColon" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TColon" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
TColon = Val $ "TColon" ## rewrite checkTy in Refl

public export
TLam : {xs : List String} ->
          (prf : IsYes ("TLam" `isElem` xs)) =>
          {0 fn : OneOf xs -> Desc ()} ->
          (checkTy : (fn (Tag.mkE "TLam" (Tag.extract prf))) === Say ()) =>
          Data (Sig (OneOf xs) fn) ()
TLam = Val $ "TLam" ## rewrite checkTy in Refl

LParenBase : Data TBaseD ()
LParenBase = LParen

LParenWS : TokNoWhitespace
LParenWS = LParen

LParenAll : Token
LParenAll = LParen

spaceTok : Token -> Bool
spaceTok (Val ("TSpaceN" ## p2)) = True
spaceTok (Val ("TNewLine" ## p2)) = True
spaceTok x = False

export
Interpolation Token where
  interpolate (Val $ "LParen" ## _) = "("
  interpolate (Val $ "RParen" ## _) = ")"
  interpolate (Val $ "TDot" ## _) = "."
  interpolate (Val $ "TStr" ## p) = p.π1                  -- Issue 2200
  interpolate (Val $ "TInt" ## p) = show p.π1             -- Issue 2200
  interpolate (Val $ "TSpaceN" ## p) = replicate p.π1 ' ' -- Issue 2200
  interpolate (Val $ "TNewLine" ## _) = "__new_line__"
  interpolate (Val $ "TLam" ## _) = "lam"
  interpolate (Val $ "TPipe" ## _) = "|"
  interpolate (Val $ "TColon" ## _) = ":"

covering
export
Show Token where
  show (Val $ "TNewLine" ## _) = "\n"
  show (Val $ "TDot" ## _) = "DOT"
  show (Val $ "TSpaceN" ## p) = "SPACES:\{show p.π1}"
  show x = interpolate x


export
tokFromChar : Char -> Maybe Token
tokFromChar '('   = Just LParen
tokFromChar ')'   = Just RParen
tokFromChar ' '   = Just (TSpaceN 1)
tokFromChar '\n'  = Just TLineRet
tokFromChar '\t'  = Just (TSpaceN 1)
tokFromChar '|'   = Just TPipe
tokFromChar ':'   = Just TColon
tokFromChar n     = Nothing

export
fromString : String -> Token
fromString "("   = LParen
fromString ")"   = RParen
fromString " "   = TSpaceN 1
fromString "\n"  = TLineRet
fromString "\t"  = TSpaceN 1
fromString "lam" = TLam
fromString "|"   = TPipe
fromString ":"   = TColon
fromString n = case parseInteger n of
                    Just i => TInt i
                    Nothing => TStr n

export
[LIST] Show a => Show (List (Bounded a)) where
  show x = show (map val x)

public export
0 Err' : Type -> Type
Err' t = ParseError t Void

public export
0 Err : Type
Err = Err' Token

lit :
     (st : SnocList (Bounded Token))
  -> (start,cur : Position)
  -> (acc : Integer)
  -> (rest : List Char)
  -> Either (Bounded Err) (List $ Bounded Token)

tok :
     (st : SnocList (Bounded Token))
  -> (cur : Position)
  -> (rest : List Char)
  -> Either (Bounded Err) (List $ Bounded Token)

ident :
     (st : SnocList (Bounded Token))
  -> (start, cur : Position)
  -> (acc : List Char) -- current string we are building for the identifier
  -> (rest : List Char)
  -> Either (Bounded Err) (List $ Bounded Token)
ident st start cur acc [] = Right $ st <>> [bounded (TStr (pack acc)) start cur]
ident st start cur acc (x :: xs) =
  if isSpace x
     then tok (st :< bounded (TStr (pack (reverse acc))) start cur) cur xs
     else if isAlphaNum x
     then ident st start (incCol cur) (x :: acc) xs
     else Left $ (oneChar (Unknown . Left $ show x) cur)



lit st s c n []        = Right $ st <>> [bounded (TInt n) s c]
lit st s c n (x :: xs) =
  if isDigit x
     then lit st s (incCol c) (n*10 + cast (digit x)) xs
     else tok (st :< bounded (TInt n) s c) c (x::xs)

tok st c ('l' :: 'a' :: 'm' :: xs) = tok (st :< bounded TLam c (addCol 3 c)) (addCol 3 c) xs
tok st c ('.' :: xs) = tok (st :< bounded TDot c (incCol c)) (incCol c) xs
tok st c ('\n' :: xs) =
  tok (st :< oneChar TLineRet c) (incLine c) xs
tok st c (x :: xs) =
  if isSpace x then tok (st :< oneChar (TSpaceN 1) c) (incCol c) xs
  else if isDigit x then lit st c (incCol c) (cast $ digit x) xs
  else case tokFromChar x of
            Nothing => ident st c (incCol c) [x] xs
            Just t => tok (st :< oneChar t c) (incCol c) xs
tok st c []        = Right $ st <>> []

export
tokenise : List Char -> Either (Bounded Err) (List (Bounded Token))
tokenise = tok [<] begin

export
printRes : String -> Either (Bounded Err) (List $ Bounded Token) -> String
printRes _ (Right ts) = unlines $ (\(B t bs) => "\{bs}: \{t}") <$> ts
printRes s (Left b) =
  uncurry (printParseError s) (virtualFromBounded b)

export
lexAndPrint : String -> IO ()
lexAndPrint s = putStrLn $ printRes s (tok [<] begin $ unpack s)

contractWhitespace : (tok : Token) -> (spaceTok tok === False) -> TokNoWhitespace
contractWhitespace (Val $ "TSpaceN" ## n ## p) prf = absurd prf
contractWhitespace (Val $ "TNewLine" ## p) prf = absurd prf
contractWhitespace (Val $ "LParen" ## p) prf = LParen
contractWhitespace (Val $ "RParen" ## p) prf = RParen
contractWhitespace (Val $ "TDot" ## p) prf = TDot
contractWhitespace (Val $ "TStr" ## p) prf = TStr p.π1
contractWhitespace (Val $ "TInt" ## p) prf = TInt p.π1
contractWhitespace (Val $ "TLam" ## p) prf = TLam
contractWhitespace (Val $ "TPipe" ## p) prf = TPipe
contractWhitespace (Val $ "TColon" ## p) prf = TColon

export
filterWhitespace : List (Bounded Token) -> List (Bounded TokNoWhitespace)
filterWhitespace [] = []
filterWhitespace (B tok bounds :: xs) with (spaceTok tok) proof p
  filterWhitespace (B tok bounds :: xs) | True = filterWhitespace xs
  filterWhitespace (B tok bounds :: xs) | False = B (contractWhitespace tok p) bounds :: filterWhitespace xs

-- reversible lexing?
export
relex  : Vect n (Bounded Token) -> (m ** v : Vect m (Bounded TokNoWhitespace) ** Matrix n m Bool)
relex [] = (0 ** [] ** [])
relex ((B tok bounds) :: xs) with (spaceTok tok) proof p
  relex ((B tok bounds) :: xs) | True =
    let (n ** v ** mx) = relex xs
    in (n ** v ** replicate _ False :: mx)
  relex ((B tok bounds) :: xs) | False =
    let (n ** v ** mx) = relex xs
        contracted = contractWhitespace tok p
    in (S n ** B contracted bounds :: v ** (True :: replicate n False) :: map (False ::) mx)
    where

relex2  : Vect n (Bounded Token) -> (m ** v : Vect m (Bounded AllTokensRemoveWS) ** Matrix n m Bool)
relex2 [] = (0 ** [] ** [])
relex2 ((B tok bounds) :: xs) with (spaceTok tok) proof p
  relex2 ((B tok bounds) :: xs) | True =
    let (n ** v ** mx) = relex2 xs
    in (n ** v ** replicate _ False :: mx)
  relex2 ((B tok bounds) :: xs) | False =
    let (n ** v ** mx) = relex2 xs
        contracted = contract tok p
    in (S n ** B contracted bounds :: v ** (True :: replicate n False) :: map (False ::) mx)
    where
      contract : (tok : Token) -> (spaceTok tok === False) -> AllTokensRemoveWS
      contract (Val $ "TSpaceN" ## n ## p) prf = absurd prf
      contract (Val $ "TNewLine" ## p) prf = absurd prf
      contract (Val $ "TDot" ## p) prf = TDot
      contract (Val $ "LParen" ## p) prf = LParen
      contract (Val $ "RParen" ## p) prf = RParen
      contract (Val $ "TStr" ## p) prf = TStr p.π1
      contract (Val $ "TInt" ## p) prf = TInt p.π1
      contract (Val $ "TLam" ## p) prf = TLam
      contract (Val $ "TPipe" ## p) prf = TPipe
      contract (Val $ "TColon" ## p) prf = TColon




{-

parameters (targetLength : Nat)
    covering
    padLengthL, padLengthR : (currentLength : Nat) -> String -> String
    padLengthR currentLength str =
      if currentLength == targetLength
         then str
         else padLengthL (S currentLength) (str ++ " ")
    padLengthL currentLength str =
      if currentLength == targetLength
         then str
         else padLengthR (S currentLength) (" " ++ str)
covering
printTokenRow : Vect n String -> (Nat, String)
printTokenRow xs =
  let withLength = map (\x => (length x, x)) xs
      maxWord = foldl (\x, y => max x (fst y)) Z withLength
      maxLength = 2 + maxWord
      paddedLength = map (uncurry (padLengthR maxLength)) withLength
      labels = concat $ intersperse "│" paddedLength
      leftMargin = replicate (maxLength) ' ' ++ "│"
      header = leftMargin ++ labels
  in (maxLength, header
  ++ "\n"
  ++ replicate maxLength '─' ++ (concat $ map (\(n, _) => "┼" ++ replicate (maxLength) '─') withLength))

covering
printMatrixRow : (len : Nat) -> Vect n Bool -> Vect n String
printMatrixRow len xs = map ( (++ "│") . padLengthR len 1 . printBool) xs
  where
    printBool : Bool -> String
    printBool True = "1"
    printBool False = "0"

partial
relexAndPrint : String -> String
relexAndPrint input =
  let Right lexed = tokenise (unpack input)
      (m ** out ** mx) = relex (fromList lexed)
      (cellWidth, headerLine) = printTokenRow (map (show . val) (fromList lexed))
      rows = map (printMatrixRow cellWidth) (Data.Vect.transpose mx)
      columnLabels = map (\tkn => let strTkn = show (val tkn)
                                  in padLengthR cellWidth (length strTkn) strTkn ++ "│")
                         out
  in headerLine ++ (concat $ map (("\n" ++) . concat) (zipWith (::) columnLabels rows))

public export
data BlockToken
  = Single Token
  | Block (List BlockToken)

data Binder : Type

data Term : Type

public export
record Identifier where
  constructor MkIdentifier
  idValue : String

public export
record Application  where
  constructor MkApp
  fn : Term
  arg : Term

public export
data Term : Type where
  SLit : String -> Term
  SNum : Nat -> Term
  SIdent : Identifier -> Term
  SApp : Application -> Term
  SLam : Binder -> Term -> Term
  SParens : Term -> Term

public export
data Binder : Type where
  BNm : Identifier -> Binder
  BTy : Identifier -> Term -> Binder


public export
data TopLevel : Type where
  SClause : Application -> Term -> TopLevel
  SDef : Identifier -> Term -> TopLevel

