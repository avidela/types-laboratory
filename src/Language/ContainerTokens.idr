module Language.ContainerTokens

import Data.Coproduct
import Data.Product
import Data.List
import Data.List1
import Data.List.Elem
import Data.Fin
import Data.String
import Data.Sigma
import Data.Tag
import Data.Levitation
import Text.Lex

import Proofs.Extensionality

%hide Builtin.infixr.(#)

%default total

add : Desc -> Desc -> Desc
add One One = One
add One (Sig s d) = Sig (Unit + s) (\case (<+ _) => One ; (+> x) => d x)
add (Sig s d) One = Sig (Unit + s) (\case (<+ _) => One ; (+> x) => d x)
add (Sig s f) (Sig t g) = Sig (s + t) (\case (<+ y) => f y ; (+> y) => g y)
add y (Ind x) = Ind (add y x)
add (Ind x) y = Ind (add x y)

tlam : Desc
tlam = Sig (Singleton "lam") (\_ => Ind One)

tnum : Desc
tnum = Sig (Singleton "num") (\_ => Sig Integer (\_ => Ind One))

plus : Desc
plus = tlam `add` tnum

mkLam : [| ContainerTokens.plus |] ()
mkLam = <+ (Val "lam") ## (() && ())

mkNum : [| ContainerTokens.plus |] ()
mkNum = +> (Val "num") ## 3 ## (() && ())

sumDesc : Desc
sumDesc = Sig (#["lam", "num"]) (\case
              "lam" => Ind One
              "num" => Sig Integer (\_ => Ind One))

to : [| ContainerTokens.plus |] () -> [|ContainerTokens.sumDesc |] ()
to (<+ (Val "lam") ## p2) = Here (Val "lam") ## p2
to (+> (Val "num") ## p2) = There (Here (Val "num")) ## p2

from : [|ContainerTokens.sumDesc |] () -> [| ContainerTokens.plus |] ()
from ((Here (Val "lam")) ## p2) = <+ Val "lam" ## p2
from ((There (Here $ Val "num")) ## p2) = +> Val "num" ## p2


TokenDesc : Desc
TokenDesc = Sig
  (#["lam", "(", ")", " ", "=", ".", "let", "in", "Ident", "Num", ":"])
  (\case "Ident" => Sig String (const $ Ind One)
         "Num" => Sig Integer (const $ Ind One)
         _ => Ind One
         )

Token : Type -> Type
Token = [| TokenDesc |]

Lamtest : Token ()
Lamtest = "lam" ## (() && ())

Intest2 : Token ()
Intest2 = "in" ## (() && ())

Identtest3 : Token ()
Identtest3 = "Ident" ## "hello" ## (() && ())

data HemiDecidable : {t : Type} -> (x , y : t) -> Type where
  Yess : (0 prf : x === y) -> HemiDecidable x y
  Oops : HemiDecidable x y

checkSame : DecEq a => {n : Nat} -> (f : Fin n -> a) -> (g : Fin n -> a) -> HemiDecidable f g
checkSame {n = 0} f g = Oops
checkSame {n = (S k)} f g with (decEq (f FZ) (g FZ)) proof p1
    checkSame {n = S k} f g | (Yes prf) =
      case checkSame (f . FS) (g . FS) of
           Yess rest => Yess (funExt $ \case FZ => prf
                                           ; (FS n) => applySame n rest)
           Oops => Oops
    checkSame {n = S k} f g | (No contra) = Oops

Petrinet : Type -> Type
Petrinet places  = (s : places -> Nat) ->(t: places -> Nat) -> Type

-- a Petri net with a single transition a + b -> 2 a

--source arking
M : Fin 2 -> Nat
M FZ = 1
M (FS FZ) = 1

--target marking
N : Fin 2 -> Nat
N FZ = 2
N (FS FZ) = 0

Net : Petrinet (Fin 2)
Net m n = case (checkSame m M, checkSame n N) of
               (Yess proof1, Yess proof2) => Fin 1
               _ => Fin 0
               where
    M : Fin 2 -> Nat
    M FZ = 1
    M (FS FZ) = 1
    N : Fin 2 -> Nat
    N FZ = 2
    N (FS FZ) = 0

flattenList : List (List a) -> List a
flattenList [] = []
flattenList (x :: xs) = x ++ flattenList xs

testFlat : flattenList [[1,2,3], [4,5]] === [1,2,3,4,5]
testFlat = Refl

showDesc : Show x => (desc : Desc) => ([|desc|] x) -> String
showDesc {desc = One} a = "1"
showDesc {desc = (Sig s d)} (v ## a) = "Σ " ++ show v ++ " . " ++ showDesc a
showDesc {desc = (Ind y)} (v && a) = show v ++ " " ++ showDesc {desc = y} a

Show x => (desc : Desc) => Show ([|desc|] x)  where
  show = showDesc

descMap : {desc : Desc} -> (a -> b) -> [|desc|] a -> [|desc|] b
descMap {desc = One} f d = ()
descMap {desc = (Sig s g)} f (d ## d2) = d ## descMap f d2
descMap {desc = (Ind x)} f d = bimap f (descMap f) d

{desc : Desc} -> Functor [|desc|] where
  map = descMap

printCon : [| TokenDesc |] a -> String
printCon (a ## b) = show a

{-
todo next:
- Handle line breaks
- prove that every prints can be parsed
- write this as a container morphism
- Use the followin data structure to rewrite the list of tokens and extend the language
data Enum : List String -> Type where
  V : (x : String) -> Elem x xs => Enum xs

TokenCont : Container
TokenCont = Const2 (Enum ["Lam", "Ident", "Num", "BracketL", "BracketR", "Space", "EqSign", "DotSign", "Let", "In"]) Unit

TokenFunctor : Type -> Type
TokenFunctor = Ex TokenCont

mkLam : a -> TokenFunctor a
mkLam x = V "Lam" ## const x
-}

textRepr : Token a -> String
textRepr ("Ident" ## str ## x) = str
textRepr ("Num" ## k ## x) = show k
textRepr x = printCon x

counit : Token a -> a
counit ("Ident" ## p2) = p2.π2.π1
counit ("Num" ## p2) = p2.π2.π1
counit ("in" ## p2) = p2.π1
counit ("lam" ## p2) = p2.π1
counit ("(" ## p2) = p2.π1
counit (")" ## p2) = p2.π1
counit (" " ## p2) = p2.π1
counit ("=" ## p2) = p2.π1
counit ("." ## p2) = p2.π1
counit ("let" ## p2) = p2.π1
counit (":" ## p2) = p2.π1

record Location where
  constructor MkLoc
  position : Nat
  length : Nat

LocationD : Desc
LocationD = Sig Nat (const $ Sig Nat (const One))

Loc : Type
Loc = [| LocationD |] ()

(.position) : Loc -> Nat
(.position) (p1 ## p2 ## _) = p1

(.length) : Loc -> Nat
(.length) (p1 ## p2 ## _) = p2

Show Location where
  show loc = "\{show loc.position}:\{show loc.length}"

TToken : Type
TToken = Token ()

data TokenDiet = Allowed | Ending | Disallowed

spanToken : (a -> TokenDiet) -> List a -> Maybe (List a, List a)
spanToken f [] = Just ([], [])
spanToken f (x :: xs) = case f x of
                             Allowed => map (mapFst (x ::)) (spanToken f xs)
                             Disallowed => Nothing
                             Ending => Just ([], x :: xs)

alphanumToken : Char -> TokenDiet
alphanumToken x = if isAlphaNum x then Allowed
                  else if isSpace x || x == '(' || x == ')' then Ending
                  else Disallowed
State : Type
State = Nat

BracketR : a -> Token a
BracketR x = ")" ## (x && ())

DotSign : a -> Token a
DotSign x = "." ## (x && ())

In : a -> Token a
In x = "in" ## (x && ())

EqSign : a -> Token a
EqSign x = "=" ## (x && ())

Ident : String -> a -> Token a
Ident str x = "Ident" ## str ## (x && ())

Num : Integer -> a -> Token a
Num int x = "Num" ## int ## (x && ())


-- genericLexer : (constructors : List String) ->
--                (check : NonEmpty constructors) =>
--                (values : #constructors -> Desc) ->
--                Tokenizer String (pure (Sig (#constructors) values) ())
-- genericLexer (x :: []) values = Direct (\xn =>
--     case prefixOfBy (\a, b => toMaybe (a == b) a)
--                     (unpack x) xn of
--          Just (e, xs) => Succ (Here (MkS x) ##
--               the (pure (values (Here $ MkS x)) ()) ?res) xs
--          Nothing => ?eer)
-- genericLexer (x :: (y :: xs)) values = ?genericLexer_rhs_3

covering
lexerFragment : List Char -> (state : Nat) -> (acc : List (Token Location)) -> Either String (List (Token Location))
lexerFragment ['l' , 'a' , 'm'] state acc
    = Right $ [ "lam" ## (MkLoc state 3 && ()) ]
lexerFragment ['l' , 'e' , 't' ] state acc
    = Right $ [ "let" ## (MkLoc state 3 && ())]
lexerFragment ('(' :: rest) state acc
    = lexerFragment rest (state + 1) (("(" ## (MkLoc state 1 && ())) :: acc)
lexerFragment (')' :: rest) state acc
    = lexerFragment rest (state + 1) (BracketR (MkLoc state 1) :: acc)
lexerFragment ('.' :: rest) state acc
    = lexerFragment rest (state + 1) (DotSign (MkLoc state 1) :: acc)
lexerFragment ['i' , 'n'] state acc
    = Right $ [In (MkLoc state 2)]
lexerFragment ('=' :: rest) state acc
    = lexerFragment rest (state + 1) (EqSign (MkLoc state 1) :: acc)
lexerFragment input@(x :: xs) state acc
    = if isAlpha x then
         let Just (str, rest) = spanToken alphanumToken input
             | Nothing => Left "unexpected sequence: \{pack input}"
             len = length str
          in lexerFragment rest (state + len) (Ident (pack str) (MkLoc state len) :: acc)
      else if isDigit x then
         let (str, rest) = span isDigit input
             len = length str
             Just n = parsePositive (pack str)
             | Nothing => Left "unexpected sequence: \{pack input}"
          in lexerFragment rest (state + len) (Num n (MkLoc state len) :: acc)
      else Left "unexpected sequence: \{pack input}"
lexerFragment [] state acc = Right (acc)

LexerState : Type
LexerState = (List Char, State)

covering
lexerWhitespace : List Char -> Either String (List (Token Location))
lexerWhitespace cs = let s1 = splitWhitespace (cs, 0) ([], Z) []
                         s2 = traverse lexAll s1
                     in concat <$> s2
  where
    splitWhitespace : (current, local : LexerState) ->
                      List LexerState -> List LexerState
    splitWhitespace ([], state) accLocal accAll = reverse (mapFst reverse accLocal :: accAll)
    splitWhitespace ((x :: xs), state) accLocal accAll
        = if isSpace x
             then splitWhitespace (xs, S state) ([], state) (mapFst reverse accLocal :: accAll)
             else splitWhitespace (xs, S state) (mapFst (x ::) accLocal) accAll

    lexAll : LexerState -> Either String (List (Token Location))
    lexAll (x, y) = lexerFragment x y []

covering
lexer : String -> Either String (List (Token Location))
lexer str = lexerWhitespace (unpack str)

{-
printExact : (lastLoc : State) -> List (Token Location) ->  String
printExact st [] = ""
printExact st (x :: xs) =
  let loc = counit x
  in textRepr x ++ replicate (S loc.position `minus` st) ' '
  ++ printExact (loc.position + loc.length) xs

print : List (Token Location) -> String
print [] = ""
print [x] = textRepr x
print (x :: xs) = textRepr x ++ " " ++ print xs

covering
testLexer : String
testLexer = case lexer "let n = lam x . x in n" of
                 Left err => err
                 Right val => print val
