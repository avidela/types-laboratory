module Language.XML

import Data.Container
import Data.Container.Morphism
import Data.Container.Descriptions.List
import Data.Container.Descriptions.Maybe

import Data.Product

import Proofs

record XMLNode where
  tag : String
  attributes : List String
  body : List (Either String XMLNode)

XMLParse : (XMLNode -> Type) -> Container
XMLParse a = (x : XMLNode) !> a x

fromMaybe : (f : a.shp -> Maybe b.shp) -> a =%> MaybeAny b
fromMaybe f = f <! ?buhe
  where
    maybeBwd : (x : a .shp) -> Any (b .pos) (f x) -> a.pos x
    maybeBwd x y with (f x) proof p
      maybeBwd x y       | Nothing  = absurd y
      maybeBwd x (Yep y) | (Just z) = ?maybeBwd_rhs_rhss_1

oneChild : XMLParse a =%> MaybeAny (XMLParse a)
oneChild = (getNode . body) <!
           (\vx, vy => let (vz ** (pq && vw)) = extractAny vy in ?bui)
  where
    getNode : List (Either String XMLNode) -> Maybe XMLNode
    getNode [Right x] = Just x
    getNode _ = Nothing

