module Language.Andrej.Driver

import Data.Category.Graded
import Pipeline.Basic
import Pipeline.Category
import Language.Andrej.Term
import Language.Parser
import Language.Lambda
import Text.Bounds
import Text.Parse.Manual
import Data.Ornaments

import System

%hide Data.Ornaments.Ornaments.List

public export
data CompilerError
  = TokenizingPhase (Bounded Parser.Err)
  | ParsingPhase String
  | TypecheckingPhase Term.Err

Show CompilerError where
  show (TokenizingPhase x) = "tokenizing error:" ++ show x
  show (ParsingPhase str) = "parsing error: " ++ str
  show (TypecheckingPhase x) = "typechecking error: " ++ show x

public export
CompilerPipeline : Vect ? Type
CompilerPipeline = [ String
                   , List Char
                   , List (Bounded Token)
                   , List (Bounded TokNoWhitespace)
                   , LC
                   -- scope check
                   -- , Expr
                   ]
toGr : (a -> Either e b) -> a -> Either (OneOf [e]) b
toGr f = mapFst Here . f

export
CompilerImpl : ImplGr {gr = List Type, obj = Type, arr = Graded.AnyErrGradeMor}
    CompilerPipeline
    [[], [Bounded Parser.Err], [], [String]]
CompilerImpl = pure . unpack
            :: toGr tokenise
            :: pure . filterWhitespace
            :: toGr parseLambda
            :: []

{-
main : IO ()
main = do
  line <- getLine
  let ls = LIST
  let c = RunTraceM (Either CompilerError) CompilerPipeline CompilerImpl line
  printLn c


