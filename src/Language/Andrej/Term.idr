module Language.Andrej.Term

import Data.Fin
import Data.Thin
import Data.List
import Data.Lookup
import Control.Monad.State
import Control.Monad.Error.Interface

liftMaybe : MonadError e m => e -> Maybe a -> m a
liftMaybe err Nothing = throwError err
liftMaybe _ (Just y) = pure y

public export
data Variable
  = Str String
  | Gensym String Nat
  | Dummy

export
Show Variable where
  show (Str s) = s
  show (Gensym s _) = s
  show Dummy = "_"

export
Eq Variable where
  Str x == Str y = x == y
  Gensym s n == Gensym t m = s == t && n == m
  Dummy == Dummy = True
  _ == _ = False


mutual
  public export
  record Abs where
    constructor MkAbs
    bound : Variable
    boundType : Expr
    body : Expr

  public export
  data Expr
    = Var Variable
    | Universe Nat
    | Pi Abs
    | Lam Abs
    | App Expr Expr

  export
  Show Expr where
    showPrec _ (Var v) = show v
    showPrec _ (Universe Z) = "Type"
    showPrec _ (Universe n) = "Type_\{show n}"
    showPrec n (Pi (MkAbs bound boundType body))
      = "(\{show bound} : \{show boundType}) -> \{show body}"
    --   = "((\{show bound} : \{show boundType}) -> \{showPrec (S n) body})"
    showPrec (User n) (Lam (MkAbs bound boundType body))
      = let str = "λ \{show bound} : \{show boundType}) . \{show body}"
        in if n > 0 then "(\{str})" else str
    showPrec _ (Lam (MkAbs bound boundType body))
      = "λ \{show bound} : \{show boundType}) . \{show body}"
    showPrec n (App x y) = "\{showPrec App x} \{showPrec App y}"

refresh : (count : Nat) -> Variable -> (Nat, Variable)
refresh k (Str x) = (S k, Gensym x k)
refresh k (Gensym x _) = (S k, Gensym x k)
refresh k Dummy = (S k, Gensym "_" k)

refreshS : (MonadState Nat m) => Variable -> m Variable
refreshS x = do count <- get
                let (newCount, var) = refresh count x
                put newCount
                pure var

public export
data Err
  = VarNotFound String
  | UnexpectedType String String

export
Show Err where
  show (VarNotFound str) = "Variable not found: \{str}"
  show (UnexpectedType str str1) = "Expected: \{str}\nGot instead: \{str1}"

----------------------------------------------------------------------------------
-- Substitution
----------------------------------------------------------------------------------

public export
record CtxVar where
  constructor MkCtxVar
  name : Variable
  ty : Expr
  -- def : Maybe Expr

export
HasKey CtxVar Variable where
  getKey = name

export
HasVal CtxVar Expr where
  getVal = ty

public export
Context : Type
Context = List CtxVar

parameters {auto merr : MonadError Err m} {auto mst : MonadState Nat m}
  subst : (ctx : Context) => (in_: Expr) -> m Expr

  subst_abstraction : (ctx : Context) => (in_ : Abs) -> m Abs

  subst_abstraction (MkAbs bound boundType body) = do
    xx <- refreshS bound
    newType <- subst boundType
    newVal <- subst {ctx = MkCtxVar bound (Var xx) :: ctx} body
    pure $ MkAbs xx newType newVal
  -- subst : (ctx : Context) => MonadError Err m => MonadState Nat m => (e, in_: Expr) -> m (Maybe Expr)
  subst (Var x) = do var <- liftMaybe (VarNotFound (show x)) (lookup x ctx)
                     pure $ Var x
  subst (Universe k) = pure $ Universe k
  subst (Pi x) = Pi <$> subst_abstraction x
  subst (Lam x) = Lam <$> subst_abstraction x
  subst (App x y) = [| App (subst x) (subst y) |]

----------------------------------------------------------------------------------
-- normalisation
----------------------------------------------------------------------------------
public export
record InferCtxVar where
  constructor MkInferCtxVar
  name : Variable
  ty : Expr
  def : Maybe Expr

export
HasKey InferCtxVar Variable where
  getKey = name

export
HasVal InferCtxVar (Expr, Maybe Expr) where
  getVal x = (x.ty, x.def)

public export
IContext : Type
IContext = List InferCtxVar

lookupTy : Variable -> IContext -> Maybe Expr
lookupTy var = lookupFst var

lookupDef : Variable -> IContext -> Maybe (Maybe Expr)
lookupDef var = lookupSnd var

extend : Variable -> Expr -> {default Nothing def : Maybe Expr} -> IContext -> IContext
extend var e = (MkInferCtxVar var e def ::)

parameters {auto merr : MonadError Err m} {auto mst : MonadState Nat m}

  export
  normalise : (ctx : IContext) => Expr -> m Expr

  normalise_abs : (ctx: IContext) => Abs -> m Abs
  normalise_abs (MkAbs bound boundType body) = do
    tt <- normalise boundType
    pure $ MkAbs bound tt !(normalise {ctx = extend bound tt ctx} body)

  normalise (Var x) = case !(liftMaybe (VarNotFound (show x)) (lookupSnd x ctx)) of
    Nothing => pure $ Var x
    Just val => normalise val

  normalise (Universe k) = pure $ Universe k
  normalise (Pi x) = Pi <$> normalise_abs x
  normalise (Lam x) = Lam <$> normalise_abs x
  normalise (App x y) = do
    arg <- normalise y
    Lam (MkAbs nm _ body) <- normalise x
    | e => pure $ App e arg
    (subst {ctx = [MkCtxVar nm arg]} body) >>= normalise

----------------------------------------------------------------------------------
-- Inference
----------------------------------------------------------------------------------

  export
  equals : (ctx : IContext) => Expr -> Expr -> m Bool
  equals x y = do e1 <- (normalise x)
                  e2 <- (normalise y)
                  eqTerms e1 e2
    where
      eqAbs : Abs -> Abs -> m Bool
      eqAbs (MkAbs nm ty b) (MkAbs nm' ty' b')
        = do xx <- Var <$> refreshS nm
             sameType <- equals ty ty'
             t1 <- subst {ctx = [MkCtxVar nm xx]} b
             t2 <- subst {ctx = [MkCtxVar nm' xx]} b'
             sameBody <- equals t1 t2
             pure (sameType && sameBody)

      eqTerms : Expr -> Expr -> m Bool
      eqTerms (Var x) (Var y) = pure $ x == y
      eqTerms (Universe k) (Universe s) = pure $ k == s
      eqTerms (Pi x) (Pi y) = eqAbs x y
      eqTerms (Lam x) (Lam y) = eqAbs x y
      eqTerms (App x z) (App a b) = do
        e1 <- equals x a
        e2 <- equals z b
        pure (e1 && e2)
      eqTerms _ _ = pure False

  export
  infer : (ctx : IContext) => Expr -> m Expr

  infer_pi : (ctx : IContext) => Expr -> m Abs

  infer_universe : (ctx : IContext) => Expr -> m Nat
  infer_universe x = case !(infer x) of
      Universe y => pure y
      y => throwError $ UnexpectedType (show y) "Type"

  -- infer : (ctx : IContext) => Expr -> m Expr
  infer (Var x) = liftMaybe (VarNotFound (show x)) (lookupTy x ctx)
  infer (Universe k) = pure $ Universe (S k)
  infer (Pi (MkAbs name arg ret)) = do
    u1 <- infer_universe arg
    u2 <- infer_universe {ctx = extend name arg ctx} ret
    pure $ Universe (max u1 u2)
  infer (Lam (MkAbs bound boundType body)) = do
    _ <- infer_universe boundType
    ret <- infer {ctx = extend bound boundType ctx} body
    pure $ Pi (MkAbs bound boundType ret)
  infer (App x y) = do
    (MkAbs bound ty ret) <- infer_pi x
    arg <- infer y
    if !(equals arg ty)
       then subst {ctx = [MkCtxVar bound y]} ret
       else throwError (UnexpectedType (show ty) (show arg))


-- let rec subst s = function
--   | Var x -> (try List.assoc x s with Not_found -> Var x)
--   | Universe k -> Universe k
--   | Pi a -> Pi (subst_abstraction s a)
--   | Lambda a -> Lambda (subst_abstraction s a)
--   | App (e1, e2) -> App (subst s e1, subst s e2)
