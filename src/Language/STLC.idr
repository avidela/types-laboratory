module Language.STLC

import Data.Nat
import Relation.Equality
import Decidable.Equality
import Control.Relation
import Proofs.Extensionality
import Language.Ops

%default total

%hide Builtin.(#)
-- %hide Data.List.(\\)


-- lambda type
public export
data Ty : (0 _ : Type) -> Type where
  (=>>) : Ty b -> Ty b -> Ty b
  Base : base -> Ty base

-- Context
public export
data Ctxt : Type -> Type where
  Nil : Ctxt b
  (&.) : Ctxt b -> Ty b -> Ctxt b

public export
length : Ctxt b -> Nat
length [] = 0
length (g &. _) = S (length g)

public export
data (∋) : {0 base : Type} -> Ctxt base -> Ty base -> Type where
  Z : gam &. a ∋ a
  S : (0 extra : Ty base) -> gam ∋ a -> gam &. extra ∋ a

public export
data (|-) : {0 base : Type} -> Ctxt base -> Ty base -> Type where

  Var : {Γ : _} ->
        Γ ∋ a ->
      ---------------
        Γ |- a

  Lam : {a, b : Ty base} -> {Γ : _} ->
        Γ &. a |- b ->
      -----------------
        Γ |- a =>> b

  App : {a, b : Ty base} -> {Γ : _} ->
         Γ |- a  =>> b ->
         Γ |- a ->
      ------------------
         Γ |- b

public export
lookup : (gam : Ctxt b) -> (n : Nat) -> (p : n `LT` length gam) -> Ty b
lookup [] 0 p = absurd p
lookup (_ &. y) 0 (LTESucc z) = y
lookup [] (S k) p = absurd p
lookup (x &. _) (S k) (LTESucc z) = lookup x k z

count : (n : Nat) -> (gam : Ctxt b) -> (p : n `LT` length gam) -> gam ∋ lookup gam n p
count 0 [] p = absurd p
count 0 (_ &. _) (LTESucc z) = Z
count (S k) [] p = absurd p
count (S k) (x &. y) (LTESucc z) = S _ (count k x z)

(#) :  (n : Nat) -> {gam : Ctxt b} -> {auto inGam : n `LT` length gam}
   -> gam |- lookup gam n inGam
(#) n = Var (count n gam inGam)

%hide Builtin.infixr.(#)

export
lam_I : {a, gam : _} -> gam |- a =>> a
lam_I = Lam (#0)

export
lam_K : {a, b, gam : _} -> gam |- a =>> b =>> a
lam_K = Lam (Lam (#1))

export
lam_S : {a, b, c, gam : _} -> gam |- (a =>> b =>> c) =>> (a =>> b) =>> a =>> c
lam_S = Lam (Lam (Lam (App (App (#2) (#0)) (App (#1) (#0)))))

public export 0
Subset : (f : a -> b -> Type) -> (g : a -> b -> Type) -> (x : a) -> (y : a) -> Type
Subset f g x y = (xsub : b) -> f x xsub -> g y xsub

public export 0
Rename, CtxtMap, Subst: Ctxt base -> Ctxt base -> Type
Rename  gam del = Subset (∋) (∋) del gam
CtxtMap gam del = Subset (|-) (|-) del gam
Subst   gam del = Subset (∋) (|-) del gam

public export
extend : {0 gam, del: Ctxt base}
      -> {b : Ty base}
      -> del      `Rename` gam
      -> del &. b `Rename` gam &. b
extend f b Z = Z
extend f xsub (S _ x) = S _ (f xsub x)

public export
rename : {gam, del: Ctxt base}
      -> del `Rename` gam
      -> del `CtxtMap` gam
rename f t (Var x) = Var (f t x)
rename f (a =>> b) (Lam x) = Lam (rename (extend f) b x)
rename f b (App x {a} y {b}) = App (rename f (a =>> b) x) (rename f a y)

public export
shift: del `Rename` gam &. g ->
       del `Rename` gam
shift f ren Z = f ren (S g Z)
shift f ren (S extra x) = f ren (S g (S extra x))

public export
prfShift : {0 sub : del `Rename` gam &. g } ->
           (n : gam ∋ t) ->
           (shift sub) t n === sub t (S g n)
prfShift Z = Refl
prfShift (S extra x) = Refl

public export 0
shiftTwice : {g : _} -> {gam, del : Ctxt base} ->
             {sub : del `Rename` gam &. g} ->
             STLC.shift (STLC.extend (STLC.shift sub)) = STLC.shift (STLC.shift (STLC.extend sub))
shiftTwice with (del `Rename` gam &. g)
  shiftTwice | with_pat = ?shiftTwice_rhs_rhss


0
renVar : {val : Ty base} -> {gam, del : Ctxt base} -> {sub : Subst del (gs &. g)} ->
         STLC.rename (\v' => S val) g (sub g Z) = Var (S val Z)
renVar {g} with (sub g Z) proof p
  renVar {g = g} | (Var Z) = ?renVar_0
  renVar {g = g} | (Var (S extra x)) = ?nanit_1

  renVar {g = (a =>> b)} | (Lam x) = ?renVar_rhs_2
  renVar {g = g} | (App x y) = ?renVar_rhs_3

public export 0
renApp : {gam, del : Ctxt base} -> {s, t : Ty base} ->
         {ren : Rename del gam} ->
         (f : gam |- s =>> t) -> (x : gam |- s) ->
         rename ren t (App f x) = App (rename ren (s =>> t) f) (rename ren s x)
renApp _ _ {t = (z =>> w)} = Refl
renApp _ _ {t = (Base z)} = Refl

public export
extendJ : {gam, del : Ctxt base}
       -> (b : Ty base)
       -> del      `Subst` gam
       -> del &. b `Subst` gam &. b
extendJ a sub a Z = Var Z
extendJ b sub a (S b x) =
    rename {del= del &. b} (\v => S {gam=del} {a=v} b) (_) (sub a x)

||| Simultaneous substitution
public export
subst : {gam, del : Ctxt base}
     -> del `Subst` gam
     -> del `CtxtMap` gam
subst f xsub (Var x) = f _ x
subst f (a =>> b) (Lam x) =
  Lam (subst (extendJ a f) _ x)
subst f xsub (App x y) =
  App (subst f _ x) (subst f _ y)

public export
Sigma : {x, b : Ty base}
     -> {gam : _}
     -> gam      |- b
     -> gam &. b ∋ x
     -> gam      |- x
Sigma y Z = y
Sigma y (S _ z) = Var z

public export
(:-) : {gam : _}
    -> {a, b : Ty base}
    -> gam &. b |- a
    -> gam      |- b
    -> gam      |- a
(:-) x y =
   STLC.subst (\c => Sigma y {x=c}) a x

-- does not hold by definition, should be isomorphism?
public export
proofApp : (b' : Ty base) ->
           (f : (gam &. a =>> b) |- b')->
           (n : (gam &. a) |- b) ->
           App (Lam f) (Lam n) = subst (\c => Sigma (Lam n)) b' f
proofApp (a =>> b) (Var Z) (Var x) = ?proofApp_rhs_9
proofApp (a =>> b) (Var (S _ w)) (Var x) = ?proofApp_rhs_9333
proofApp (Base z) (Var y) (Var x) = ?proofApp_rhs_7
proofApp (a =>> b) (Lam y) (Var x) = ?proofApp_rhs_4
proofApp b' (App y z) (Var x) = ?proofApp_rhs_5
proofApp b' f (Lam x) = ?proofApp_rhs_1
proofApp b' f (App x y) = ?proofApp_rhs_2


public export
data Value : gam |- a -> Type where
  VLam : Value (Lam n)

public export
data (~>) : (t1, t2 : gam |- a) -> Type where
  Xi1 : l ~> l' -> App l m ~> App l' m
  Xi2 : {v : gam |- a =>> b} -> {m, m' : gam |- a} ->
        Value v -> m ~> m' -> App v m ~> App v m'
  BetaLam : {f : gam &. b |- a} -> {v : gam |- b} ->
            Value v -> (App (Lam f) v) ~> (f :- v)

applyIdentity : {b : _} -> {gam : Ctxt base} -> {x : gam |- b} -> Value x -> (App (Lam (Var Z)) x) ~> x
applyIdentity = BetaLam

public export
data (->>) : (t1, t2 : gam |- a) -> Type where
  Step : m ~> n -> m ->> n
  ReduceRefl : (t : gam |- a) -> t ->> t
  ReduceTrans : l ->> m -> m ->> n -> l ->> n

export
implementation Reflexive (gam |- a) (->>) where
  reflexive = ReduceRefl _

export
implementation Transitive (gam |- a) (->>) where
  transitive ab bc = ReduceTrans ab bc

export
implementation Preorder (gam |- a) (->>) where

{-
twoIsTwo : (Debrujn.cTwo `App` Debrujn.cSuc) `App` Zero ->> Succ (Succ Zero)
twoIsTwo = begin (->>) $
           ((cTwo `App` cSuc) `App` Zero)
           -< Step (Xi1 (BetaLam VLam)) >-
           Lam ((cSuc `App` (cSuc `App` #0)) `App` Zero)
           -< Step (BetaLam VZero) >-
           (cSuc `App` (cSuc `App` Zero))
           -< Step (Xi2 VLam (BetaLam VZero)) >-
           (cSuc `App` Succ Zero)
           -< Step (BetaLam (VSucc VZero)) >-
           End (Succ (Succ Zero))

valuesWontReduce : Value v -> Not (v ~> v')
valuesWontReduce VLam (Xi1 _) impossible
valuesWontReduce VZero (Xi1 _) impossible
valuesWontReduce (VSucc x) (Xi1 _) impossible
valuesWontReduce VLam (Xi2 _ _) impossible
valuesWontReduce VZero (Xi2 _ _) impossible
valuesWontReduce (VSucc x) (Xi2 _ _) impossible
valuesWontReduce VLam (BetaLam _) impossible
valuesWontReduce VZero (BetaLam _) impossible
valuesWontReduce (VSucc x) (BetaLam _) impossible
valuesWontReduce (VSucc x) (XiSucc y) = valuesWontReduce x y
valuesWontReduce VLam (XiCase _) impossible
valuesWontReduce VZero (XiCase _) impossible
valuesWontReduce (VSucc x) (XiCase _) impossible
valuesWontReduce VLam BetaZero impossible
valuesWontReduce VZero BetaZero impossible
valuesWontReduce (VSucc x) BetaZero impossible
valuesWontReduce VLam (BetaSucc _) impossible
valuesWontReduce VZero (BetaSucc _) impossible
valuesWontReduce (VSucc x) (BetaSucc _) impossible
valuesWontReduce VLam BetaMu impossible
valuesWontReduce VZero BetaMu impossible
valuesWontReduce (VSucc x) BetaMu impossible

reductionsCantBeValues : v ~> v' -> Not (Value v)
reductionsCantBeValues x y = valuesWontReduce y x

data Progress : (m : [] |- a) -> Type where
  Step' : {n : _} -> m ~> n -> Progress m
  Done' : Value m -> Progress m

twoProof : 2 + 2 `Equal` 4
twoProof = Refl

progress : (m : [] |- a) -> Progress m
progress (^ x) impossible
progress (\\ x) = Done' VLam
progress (x |> y) with (progress x)
  progress (x |> y) | (Step' z) = Step' (Xi1 z)
  progress (x |> y) | (Done' z) with (progress y)
    progress (x |> y) | (Done' z) | (Step' w) = Step' (Xi2 z w)
    progress (\\ x |> y) | (Done' z) | (Done' w) = Step' (BetaLam w)
progress Zero = Done' VZero
progress (Succ x) with (progress x)
  progress (Succ x) | (Step' y) = Step' (XiSucc y)
  progress (Succ x) | (Done' y) = Done' (VSucc y)
progress (Case x y z) with (progress x)
  progress (Case x y z) | (Step' w) = Step' (XiCase w)
  progress (Case Zero y z) | (Done' VZero) = Step' BetaZero
  progress (Case Zero y z) | (Done' VLam) impossible
  progress (Case Zero y z) | (Done' _) = ?impossiblePLsFix
  progress (Case (Succ v) y z) | (Done' (VSucc w)) = Step' (BetaSucc w)
  progress (Case (Succ v) y z) | (Done' _) = ?impossiblePlsFixToo
progress (Mu x) = Step' BetaMu

Gas : Type
Gas = Nat

data Finished : (n : gam |- a) -> Type where
  Done : Value n -> Finished n
  OutOfGas : Finished n

data Steps : [] |- a -> Type where
  MkSteps : l ->> n -> Finished n -> Steps l

eval : Gas -> (l : [] |- a) -> Steps l
eval 0 l = MkSteps (ReduceRefl l) OutOfGas
eval (S k) l with (progress l)
  eval (S k) l | (Step' x {n}) with (eval k n)
    eval (S k) l | (Step' x) | (MkSteps y z) = MkSteps (ReduceTrans (Step x) y) z
  eval (S k) l | (Done' x) = MkSteps (ReduceRefl l) (Done x)


sucMu : [] |- NatType
sucMu = Mu (Succ (#0))

evalInf : (Debrujn.eval 3 Debrujn.sucMu) = MkSteps
    (ReduceTrans (Step BetaMu)
    (ReduceTrans (Step (XiSucc BetaMu))
    (ReduceTrans (Step (XiSucc (XiSucc BetaMu)))
    (ReduceRefl (Succ (Succ (subst (Sigma (Mu (Succ (#0)))) (Succ (#0))))))))) OutOfGas
evalInf = Refl


-- evalTwoPlusTwo : (Debrujn.eval 100 (Debrujn.cTwo |> Debrujn.cSuc |> Zero)) ~=~
--                  MkSteps
--                  (ReduceTrans (Step (Xi1 (BetaLam VLam)))
--                  (ReduceTrans (Step (BetaLam VZero))
--                  (ReduceTrans (Step (Xi2 VLam (BetaLam VZero)))
--                  (ReduceTrans (Step (BetaLam (VSucc VZero)))
--                  (ReduceRefl (Succ (Succ Zero))))))) (Done (VSucc (VSucc VZero)))
-- evalTwoPlusTwo = Refl
-}
