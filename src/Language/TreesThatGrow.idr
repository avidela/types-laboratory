module Language.TreesThatGrow

data Typ = TInt | TFun Typ Typ

data BaseExp
  = Lit Integer
  | Var String
  | Ann BaseExp Typ
  | Abs String BaseExp
  | App BaseExp BaseExp


data Con
  = CLit
  | CVar
  | CAnn
  | CAbs
  | CApp
  | CExp

data Exp_x : (f : Con -> Type -> Type) -> Type -> Type where
  Lit_x : {0 t : Type} -> f CLit t -> Integer -> Exp_x f t
  Var_x : {0 t : Type} -> f CVar t -> String  -> Exp_x f t
  Ann_x : {0 t : Type} -> f CAnn t -> Exp_x f t -> Typ -> Exp_x f t
  Abs_x : {0 t : Type} -> f CAbs t -> Exp_x f t -> Exp_x f t
  App_x : {0 t : Type} -> f CApp t -> Exp_x f t -> Exp_x f t -> Exp_x f t
  Exx_x : {0 t : Type} -> f CExp t -> Exp_x f t


BExp : Type
BExp = Exp_x (const $ const Unit) Void

fromBase : BaseExp -> BExp
fromBase (Lit i) = Lit_x () i
fromBase (Var str) = Var_x () str
fromBase (Ann x y) = Ann_x () (fromBase x) y
fromBase (Abs str x) = Abs_x () (fromBase x)
fromBase (App x y) = App_x () (fromBase x) (fromBase y)

