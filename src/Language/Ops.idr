module Language.Ops

export prefix 9 \\
export prefix 9 #

export infixr 0 ==> -- function application, like $
export infixr 4 ~>  -- Single Reduction step
export infixr 2 ->> -- "Reduces to"
export infixr 7 =>> -- Type in Lambda calculus

export infix 5 :-

export infixl 6 .:
export infixr 4 >:
export infixr 4 ∋
export infixl 5 &.
export infixl 5 |-
