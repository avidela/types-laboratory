module Language.Lambda

import Data.List
import Data.List1
import Text.Lexer
import Text.Parser
import Text.Parse.Core
import Language.Parser
import Data.Ornaments


%hide Ornaments.List


-- untyped lambda calculus
public export
data LC : Type where
  Var : String -> LC
  Abs : String -> LC -> LC
  App : LC -> LC -> LC

%default total

export
Show LC where
  showPrec d (App e1 e2) = showParens (d == App) (showPrec (User 0) e1 ++ " " ++ showPrec App e2)
  showPrec d (Abs v e) = showParens (d > Open) ("\\" ++ v ++ "." ++ show e)
  showPrec d (Var v) = v



identifier : Lexer
identifier = alpha <+> many alphaNum

public export
data LamErr = Msg String

export
Show LamErr where
  show (Msg x) = x

%default covering

0 Rule : Bool -> Type -> Type
Rule b t = Grammar b () TokNoWhitespace LamErr t

variable : Rule True LC

abstraction : Rule True LC

fullExpr : Rule True LC

application : LC -> Rule True LC

applicationHelp : LC -> Rule False LC

token : Eq a => a -> Grammar True () a e ()
token x = terminal (\y => if x == y then Just () else Nothing)

abstraction = do
  token TLam
  arg <- terminal (\case (Val ("TStr" ## x)) => Just x.π1
                         _ => Nothing)
  token TDot
  e <- fullExpr
  pure $ Abs arg e

parensExpr : Rule True LC
parensExpr = do
  token LParen
  e <- fullExpr
  token RParen
  pure e

variable = Var <$> terminal (\case (Val ("TStr" ## p)) => Just p.π1 ; _ => Nothing)

simpleExpr : Rule True LC
simpleExpr =
  abstraction <|> variable <|> parensExpr

application fn = do
  arg <- simpleExpr
  applicationHelp $ App fn arg

applicationHelp fn = application fn <|> pure fn

fullExpr = do
  e <- simpleExpr
  application e <|> pure e

export
parseLambda : List (Bounded TokNoWhitespace) -> Either String LC
parseLambda toks =
  case Core.parse (fullExpr ) () toks of
       (Left x) => Left (show x)
       (Right (x, y, [])) => pure y
       (Right (x, y, z)) => Left ("unconsumed input: " ++ show z)

{-
parse : String -> Either String LC
parse x =
  case lexLambda x of
    Just toks => parseLambda toks
    Nothing => Left "Failed to lex."
