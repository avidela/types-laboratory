module Language.Scoped

import Data.Fin
import Language.Untyped
import Data.Ornaments

%hide Builtin.infixr.(#)

%default partial

public export
data Scoped : Nat -> Type where
  Var : Fin n -> Scoped n
  App : Scoped n -> Scoped n -> Scoped n
  Lam : Scoped (S n) -> Scoped n


-- Untyped -> Scoped
public export
ScopedO : (x : Type) -> Orn NatT (const ()) (Parameterised.untyped x)
ScopedO x = Sig (#["var", "app", "lam"]) $ \case
    "var" => △ NatT (\xn =>
             △  (FinT xn) (\_ =>
             (Sig x (\yn => Say (Ok xn)))))
    "app" => △ NatT (\xn => Ask (Ok (xn)) (Ask (Ok xn) (Say $ Ok xn)))
    "lam" => △ NatT (\xn => Sig x (\_ => Ask (Ok (S xn)) (Say $ Ok xn)))

public export
0 ScopedD : Type -> Desc NatT
ScopedD x = extract (ScopedO x)

0 ScopedTy : Type -> NatT -> Type
ScopedTy ty n = Data (ScopedD ty) n

fromNat : Nat -> NatT
fromNat 0 = Z
fromNat (S k) = S (fromNat k)

public export
toNat : NatT -> Nat
toNat (Val $ "Z" ## Refl) = Z
toNat (Val $ "S" ## z ## Refl) = S (toNat z)

-- scoped -> metadata scoped
ScopedOF : (t, x : Type) -> Orn NatT (\arg => arg) (ScopedD x)
ScopedOF t x = Sig (#["var", "app", "lam"]) $ \case
    "var" => △ t $ \_ =>
             Sig NatT (\xn =>
             Sig (FinT xn) (\_ =>
             (Sig x (\yn => Say (Ok xn)))))
    "app" => △ t $ \_ => Sig NatT (\xn => Ask (Ok (xn)) (Ask (Ok xn) (Say $ Ok xn)))
    "lam" => △ t $ \_ => Sig NatT (\xn => Sig x (\_ => Ask (Ok (S xn)) (Say $ Ok xn)))

-- metadata -> Scoped metadata
ScopedFO : (t, s : Type) -> Orn NatT (const ()) (Metadata.untypedFDesc t s)
ScopedFO t s = △ NatT $ \xn => Sig t $ \_ => Sig (#["var", "app", "lam"]) $ \case
      "var" => △ (FinT xn) (\_ =>
               Sig s (\xs => Say $ Ok xn))
      "app" => Ask (Ok xn) (Ask (Ok xn) (Say (Ok xn)))
      "lam" => Sig s (\_ => Ask (Ok (S xn)) (Say (Ok xn)))

scopedFODesc : Type -> Type -> Desc NatT
scopedFODesc t s = extract (ScopedFO t s)

scopedFOTy : Type -> Type -> NatT -> Type
scopedFOTy t s n = Data (scopedFODesc t s) n

getscopedMD : {t, s : Type} -> {n : _} -> scopedFOTy t s n -> untypedF t s
getscopedMD x = forget (ScopedFO t s) x

getMetadata' : {t, s, n : _} -> scopedFOTy t s n -> t
getMetadata' x = getMetaData (getscopedMD x)

data Value : Scoped n -> Type where
  LVal : (x : Scoped (S n)) -> Value (Lam x)


ext : (Fin n -> Fin m) -> Fin (S n) -> Fin (S m)
ext f FZ = FZ
ext f (FS x) = FS (f x)

rename : (Fin n -> Fin m) -> Scoped n -> Scoped m
rename f (Var x) = Var (f x)
rename f (App x y) = App (rename f x) (rename f y)
rename f (Lam x) = Lam (rename (ext f) x)


exts : (Fin n -> Scoped m) -> Fin (S n) -> Scoped (S m)
exts f FZ = Var FZ
exts f (FS x) = rename FS (f x)

subst : (Fin n -> Scoped m) -> Scoped n -> Scoped m
subst f (Var x) = f x
subst f (App x y) = App (subst f x) (subst f y)
subst f (Lam x) = Lam (subst (exts f) x)

{-
fromScoped : {n : Nat} -> Scoped n -> ScopedTy () n
fromScoped (Var x) = Val $ "var" ## n ## x ## () ## Refl
fromScoped (App x y) = Val $ "app" ## n ## fromScoped x ## fromScoped y ## Refl
fromScoped (Lam x) = Val $ "lam" ## n ## () ## fromScoped x ## Refl

partial
toScoped : {n : Nat} -> ScopedTy () n -> Scoped n
toScoped (Val (sq ## p)) with (sq) proof p
  toScoped (Val $ sq ## p ## q ## z ## Refl) | "var" = Var q
  toScoped (Val $ sq ## p ## q ## z ## Refl) | "app" = App (toScoped q) (toScoped z)
  toScoped (Val $ sq ## p ## q ## z ## Refl) | "lam" = Lam (toScoped z)


