module DSL.Functor

-- import Data.List.Elem
import Data.Coproduct
import Data.Vect
import Data.Vect.Quantifiers
import Data.Vect.Elem
import Decidable.Equality
import Data.Fun

index' : {xs : Vect n a} ->  Elem x xs -> (ys : Vect n b) -> b
index' Here (y :: ys) = y
index' (There later) (y :: ys) = index' later ys

public export
data IsYes : Dec a -> Type where
  Witness : (w : a) -> IsYes (Yes w)

public export
extract : {x : Dec a} -> IsYes x -> a
extract (Witness w) = w

OneOf :  Vect (S n) (String, Type -> Type) -> Type -> Type
OneOf xs x = Any (\v => snd v x) xs

%unbound_implicits off
-- convert from Elem to Any
obtain : {0 n : Nat} -> {0 ty : Type} ->
         (xs : Vect (n) (String, Type -> Type)) ->
         (s : String) ->
         (e : Elem s (map fst xs)) ->
         (i : index' e (map snd xs) ty) ->
         Any (\v => snd v ty) xs
obtain (x :: xs) (fst x) Here i = Here i
obtain (x :: xs) s (There later) i = There ( obtain xs s later i )

prefix 1 >

(>) : {0 n : Nat} -> {0 ty : Type} ->
             (s : String) -> {xs : Vect (S n) (String, Type -> Type)} ->
             (e : IsYes (s `isElem` map fst xs)) =>
             (index' (extract e) (map snd xs) ty) -> OneOf xs ty
(>) s {e} {xs} i = obtain xs s (extract e) i
%unbound_implicits on

test : OneOf [("List", List), ("Maybe", Maybe), ("Id", \x => x)] Bool
test = (>"Maybe") (Just True)


