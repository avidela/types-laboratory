module Relation.Isomorphism

import Control.Relation
import Control.Order

infix 2 ~=
infix 2 ~/=

public export
record (~=) (a : Type) (b : Type) where
  constructor MkIso
  from : a -> b
  to : b -> a
  0 fromTo : {x : b} -> from (to x) = x
  0 toFrom : {x : a} -> to (from x) = x

export
Reflexive Type (~=) where
  reflexive = MkIso id id Refl Refl

export
Transitive Type (~=) where
  transitive (MkIso ab ba fromToAB toFromBA) (MkIso bc cb fromToBC toFromCB)
      = MkIso (\x => bc (ab x)) (\x => ba (cb x)) fn gn
      where
        0 fn : {arg : _} -> bc (ab (ba (cb arg))) = arg
        fn = rewrite fromToAB {x=cb arg} in fromToBC

        0 gn : {arg : _} -> ba (cb (bc (ab arg))) = arg
        gn = rewrite toFromCB {x = ab arg} in toFromBA

export
Symmetric Type (~=) where
  symmetric (MkIso from to fromTo toFrom) = MkIso
    to
    from
    toFrom
    fromTo

export
Preorder Type (~=) where
