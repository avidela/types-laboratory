module MSPUtils

import public Control.Category
import public Data.Morphisms
import public Data.Telescope.Telescope
import Data.Vect
import public Data.List.Quantifiers


public export
OneOf : List Type -> Type
OneOf = Any.Any id

%hide Data.Telescope.Telescope.infixr.(.-)
%hide Data.Telescope.Telescope.Left.Telescope
%hide Data.Telescope.Telescope.Tree.Telescope

export autobind infixr 0 |>>>
export typebind infixr 0 .-

public export
record DContPair (x, y : Type) (f : y -> Type)  where
  constructor (|>>>)
  fst : x
  cont : (v : y) -> f v

public export
DContPairM : (Type -> Type) -> (a : Type) -> (a -> Type) -> Type
DContPairM m a b = DContPair (m a) a b

public export
(!>) : Category arr => x `arr` y -> y `arr` z -> x `arr` z
(!>) f g = g . f

export infixr 0 !>

public export
Fn : Type -> Type -> Type
Fn a b = a -> b

public export
Category Fn where
  id = Basics.id
  (.) f g x = f (g x)

export typebind infixr 0 *

public export
(*) : (ty : Type) -> (ty -> Type) -> Type
(*) = DPair

public export
telHead : Right.Telescope (S n) -> Type
telHead (x .- _) = x

FSuc : (xs : Right.Telescope (S n)) -> {0 ty : Type} -> {0 tel : ty -> Right.Telescope n} ->
       xs === ((t : ty) .- tel t) -> telHead xs === ty
FSuc _ Refl = Refl

EnvZero : (xs : Right.Telescope 0) -> Right.Environment xs === Unit
EnvZero [] = Refl

public export
telTail : (xs : Right.Telescope (S n)) -> telHead xs -> Right.Telescope n
telTail (ty .- gamma) x = gamma x

0 telTailSuc : (xs : Right.Telescope (S n)) -> {ty : Type} -> {tel : ty -> Right.Telescope n} ->
  (p : xs === ((t : ty) .- tel t)) -> (v : ty) -> telTail xs (rewrite__impl Basics.id (FSuc xs p) v) === tel v
telTailSuc _ Refl v = Refl

parameters (m : Type -> Type) {auto mon : Monad m}

  public export
  ToSigmaM : Telescope n -> Type
  ToSigmaM [] = Unit
  ToSigmaM (ty .- gamma)
      = DContPairM m ty (ToSigmaM . gamma)
  -- (fst : m ty) * ((v : ty) -> f rec)

  public export
  ToImplM : Right.Telescope n -> Type
  ToImplM [] = Void
  ToImplM (ty .- gamma) = (x : ty) -> ToSigmaM (gamma x)
  ||| How to run the implementation of a telescope and obtain a
  ||| sigma-types with all the intermediate results
  export
  RunDepM : {n : _} -> (xs : Right.Telescope (S (S n)) ) ->
           (imp : ToImplM xs) -> (val : telHead xs) -> m (Environment (telTail xs val))
  RunDepM {n = 0} (ty .- gamma) imp val with (imp val)
    RunDepM {n = 0} (ty .- gamma) imp val | res with (gamma val)
      RunDepM {n = 0} (ty .- gamma) imp val | (fst |>>> cont) | (x .- f) = do
        vx <- fst
        pure (vx ** (rewrite EnvZero (f vx) in ()))
  RunDepM {n = S n} (ty .- gamma) imp val with (imp val)
    RunDepM (ty .- gamma) imp val | res with (gamma val) proof q
      RunDepM (ty .- gamma) imp val | (vx |>>> cont) | (ty' .- tel) = do
        vy <- vx
        gn <- RunDepM (gamma val) (rewrite q in cont) (rewrite q in vy)
        let gn' = replace {p = Environment} (telTailSuc _ q vy) gn
        pure (vy ** gn')

public export
pairUp : Vect (S n) a -> Vect n (a, a)
pairUp [a] = []
pairUp (x :: y :: xs) = (x, y) :: pairUp (y :: xs)

