module Control.Monad.Dijkstra

%hide Prelude.Monad

-- basic definition of monad, maybe replace this by the verified monad from
-- Data.Category.Monad
record Monad (f : Type -> Type) where
  constructor MkMon
  pure : {0 a : Type} -> a -> f a
  bind : {0 a, b : Type} -> f a -> (a -> f b) -> f b

record MonadMor (m1 : Monad f) (m2 : Monad g) where
  constructor MkMonMor

-- Basic definition of Dijkstra monad. Probably a good idea to lift this to
-- a categorical definition
record Dijkstra {m : Monad f} (a : Type) (w : f a) where
  constructor MkDijkstra
  D : (ty : Type) -> f ty -> Type
  pure : (x : a) -> D a (m.pure x)
  bind : {0 w1 : f a} -> {0 w2 : a -> f b} ->
         D a w1 -> ((x : a) -> D b (w2 x)) -> D b (m.bind w1 w2)

record DijkstraMor {f1, f2 : Type -> Type} {m1 : Monad f1} {m2 : Monad f2}
                   {w1 : f1 a} {w2 : f2 a}
                   (d1 : Dijkstra {m = m1} a w1) (d2 : Dijkstra {m = m2} a w2) where
  constructor MkDijkstraMor
  monadMor : MonadMor m1 m2

