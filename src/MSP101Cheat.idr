module MSP101

import Data.Vect
import MSPUtils

%hide Prelude.Ops.infixl.(*)
%hide Data.Telescope.Telescope.infixr.(.-)
%hide Data.Telescope.Telescope.Left.Telescope
%hide Data.Telescope.Telescope.Tree.Telescope

------------------------------------------------------------
-- A basic compiler infrastructure
------------------------------------------------------------

data Tree : Type where
data Token : Type where
data Sema : Type where
data Bytecode : Type where

lex : String -> List Token
parse : List Token -> Tree
typecheck : Tree -> Sema
codegen : Sema -> Bytecode

basicCompiler : String -> Bytecode
basicCompiler = codegen . typecheck . parse . lex

------------------------------------------------------------
-- Compiler Pipeline Implementation
------------------------------------------------------------

CompilerPipeline : Vect 5 Type
CompilerPipeline = [String, List Token, Tree, Sema, Bytecode]

CompilerPipelineImpl : (String -> List Token, List Token -> Tree, Tree -> Sema, Sema -> Bytecode)
CompilerPipelineImpl = (lex, parse, typecheck, codegen)

------------------------------------------------------------
-- Generic Pipeline Implementation
------------------------------------------------------------

Impl : Vect (2 + n) Type -> Type
Impl [x, y] = x -> y
Impl (x :: y :: z :: xs) = Pair (x -> y) (Impl (y :: z :: xs))

CompilerPipelineImpl' : Impl CompilerPipeline
CompilerPipelineImpl' = CompilerPipelineImpl

Run : (p : Vect (2 + n) Type) -> Impl p -> head p -> last p
Run [x, y] impl = impl
Run (x :: y :: z :: xs) (f , impl) = Run (y :: z :: xs) impl . f

runCompiler : String -> Bytecode
runCompiler = Run CompilerPipeline CompilerPipelineImpl'

------------------------------------------------------------
-- Monadic Compiler
------------------------------------------------------------

lexM : String -> Maybe (List Token)
parseM : List Token -> Maybe Tree
typecheckM : Tree -> Maybe Sema
codegenM : Sema -> Maybe Bytecode

------------------------------------------------------------
-- Monadic Pipeline
------------------------------------------------------------

parameters (m : Type -> Type)
  ImplM : Vect (2 + n) Type -> Type
  ImplM [x, y] = x -> m y
  ImplM (x :: y :: z :: xs) = Pair (x -> m y) (ImplM (y :: z :: xs))

  RunM : Monad m => (p : Vect (2 + n) Type) -> ImplM p -> head p -> m (last p)
  RunM [x, y] impl = impl
  RunM (x :: y :: z :: xs) (f , impl) = RunM (y :: z :: xs) impl <=< f

------------------------------------------------------------
-- Monadic Compiler Implementation
------------------------------------------------------------

compilerComplM : ImplM Maybe CompilerPipeline
compilerComplM = (lexM, parseM, typecheckM, codegenM)

runCompilerM : String -> Maybe Bytecode
runCompilerM = RunM Maybe CompilerPipeline compilerComplM

------------------------------------------------------------
-- Multicore Pipeline
------------------------------------------------------------

-- go to Multicore










------------------------------------------------------------
-- Categorical Pipeline
------------------------------------------------------------

parameters
  (o : Type) (arr : o -> o -> Type)
  {auto cat : Category arr}

  ImplCat : Vect (2 + n) o -> Type
  ImplCat [x, y] = x `arr` y
  ImplCat (x :: y :: z :: xs) =
    Pair (x `arr` y) (ImplCat (y :: z :: xs))

  RunCat : (p : Vect (2 + n) o) ->
           ImplCat p -> head p `arr` last p
  RunCat [x, y] impl = impl
  RunCat (x :: y :: z :: xs) (f , impl) =
    RunCat (y :: z :: xs) impl . f

------------------------------------------------------------
-- Both compilers as a categorical Pipeline
------------------------------------------------------------

runCompiler' : String -> Bytecode
runCompiler' =
  RunCat Type Fn CompilerPipeline CompilerPipelineImpl

runCompilerM' : Kleislimorphism Maybe String Bytecode
runCompilerM' =
  RunCat Type (Kleislimorphism Maybe) CompilerPipeline
         (Kleisli lexM,
          Kleisli parseM,
          Kleisli typecheckM,
          Kleisli codegenM)

------------------------------------------------------------
-- Dependent Compiler definitions
------------------------------------------------------------

data LC : Type where
data ScopedLC : (names : Vect n String) -> Type where
data Ty : Type where
data TypedLC : (context : Vect n (String, Ty)) -> Type where

namespace LambdaCalculus
  parseTree : List Token -> Maybe LC

  scopecheckM :
      LC ->
      Maybe ((n : Nat) * (vars : Vect n String) * ScopedLC vars)

  typecheckM :
      {n : Nat} ->
      (vars : Vect n String)  -> ScopedLC vars ->
      Maybe ((types : Vect n Ty) * TypedLC (zip vars types))

  codegenLC : TypedLC types -> Maybe Bytecode

------------------------------------------------------------
-- Dependent Compiler Telescope
------------------------------------------------------------
  CompilerTelescope : Telescope ?
  CompilerTelescope =
    (_ : String) .-
    (_ : List Token) .-
    (ls : LC) .-
    (scoped :
      (n : Nat) * (vars : Vect n String) * ScopedLC vars) .-
    (checkd :
      (types : Vect scoped.fst Ty) *
      TypedLC (zip scoped.snd.fst types)) .-
    (_ : Bytecode) .-
    []

------------------------------------------------------------
-- Dependent Compiler Implementation
------------------------------------------------------------

  CompilerImpl : ToImplM Maybe CompilerTelescope
  CompilerImpl inputFile =
    (tokens := lexM inputFile) |>>>
    (tree := parseTree tokens) |>>>
    ((n ** vars ** tree) := scopecheckM tree) |>>>
    ((types ** calc) := typecheckM vars tree) |>>>
    (out := codegenLC calc) |>>>
    ()

------------------------------------------------------------
-- Dependent Compiler Running
------------------------------------------------------------

  compilerRun : String -> Maybe Bytecode
  compilerRun str =
    case RunDepM Maybe CompilerTelescope CompilerImpl str of
         Just (token ** tree ** vars ** typed ** bytes ** ()) =>
             Just bytes
         Nothing => Nothing






------------------------------------------------------------
-- Graded Categories
------------------------------------------------------------

public export
interface GradedCat
  (mon : Monoid gr)
  (0 arr : gr -> obj -> obj -> Type) | arr where
  constructor MkGradedCat

  identity : forall x. arr Prelude.Interfaces.neutral x x

  (#>) : forall x, y, z.  {g1, g2 : _} -> arr g1 x y -> arr g2 y z -> arr (g1 <+> g2) x z

export infixr 1 #>

------------------------------------------------------------
-- Graded Pipelines
------------------------------------------------------------

public export
foldGrades : Monoid gr => Vect (S n) gr -> gr
foldGrades (x :: []) = x
foldGrades (x :: (y :: xs)) = x <+> foldGrades (y :: xs)

parameters
  {0 gr : Type}
  {0 obj : Type}
  {0 arr : gr -> obj -> obj -> Type}

  public export 0
  ImplGr : forall n.
           (layers : Vect (2 + n) obj) ->
           (grades : Vect (S n) gr) -> Type
  ImplGr [x, y] [g] = arr g x y
  ImplGr (x :: y :: z :: xs) (g :: gs) =
    Pair (arr g x y) (ImplGr (y :: z :: xs) gs)

  export
  RunGrCat : forall n.
             {auto mon : Monoid gr} ->
             (cat : GradedCat mon arr) ->
             (p : Vect (2 + n) obj) ->
             (grades : Vect (S n) gr) ->
             ImplGr p grades ->
             arr (foldGrades grades) (Vect.head p) (Vect.last p)
  RunGrCat cat [x, y] [gr] f = f
  RunGrCat cat (x :: y :: z :: xs)
               (gr1 :: (g :: gs)) (f, cs) =
    f #> RunGrCat cat (y :: z :: xs) (g :: gs) (cs)

------------------------------------------------------------
-- Graded Compiler
------------------------------------------------------------

record FreeEither (ls : List Type) (a, b: Type) where
  constructor GrMor
  mor : a -> Either (OneOf ls) b

GradedCat %search FreeEither where
  identity = GrMor Right
  (#>) (GrMor f) (GrMor g) = GrMor $ ?bb

lexG : String
