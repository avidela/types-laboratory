module Bug

record P {o : Type} where
  constructor MkP
  f : {x : o} -> Type

{-
%unbound_implicits off

data P : Type -> Type where
  MkP : {o : Type} ->
        (f :  (x : o) -> Type) -> P o
