module Interactive.Desugaring

import Data.List
import Data.Fin
import Data.Sigma
import Data.Coproduct
import Data.Vect
import Data.Product
import Data.Container.Morphism
import Data.Container.Descriptions
import Data.Container.Descriptions.Maybe
import Data.Container.Maybe

||| Raw syntax with let bindings
namespace Raw
  public export
  data Syntax : Type where
    Variable : String -> Syntax
    Lambda : List String -> Syntax -> Syntax
    Let : String -> Syntax -> Syntax -> Syntax
    App : Syntax -> Syntax -> Syntax

Parser : Type -> Type
Parser a = List Char -> Maybe a * List Char

parseIdentifier : Parser String
parseIdentifier ls@('λ' :: _) = Nothing && ls
parseIdentifier ls@('.' :: _) = Nothing && ls
parseIdentifier ls@('l' :: 'e' :: 't' :: _) = Nothing && ls
parseIdentifier ls@('i' :: 'n' :: _) = Nothing && ls
parseIdentifier str = Just (pack (takeWhile (/= ' ') str)) && dropWhile (/= ' ') str

Par : Container
Par = List Char :- Syntax

ParsedC : Container -> Container
ParsedC c = MaybeAny c ⊗ Par

Collapse : {c : Container} -> MaybeAny c ⊗ Par =%> c + Par

parseIdLens : Par =%> ParsedC (Const String)
parseIdLens =
    (\input => parseIdentifier input) <!
    (\x, (v && s) => Variable (extract' v).snd)

maybeMap : a =%> b -> ParsedC a =%> ParsedC b
maybeMap = ?composeCOntBifunctorFromCat

closeLoop : Const a =%> CUnit

-- parseLam will parse λ id+ . expr
parseLam : Parser (List String * Syntax)
parseLam cs = ?parseLam_rhs

private infixl 8 >?>
(>?>) : Par =%> ParsedC b -> b =%> ParsedC c -> Par =%> ParsedC c

parseLamLens : Par =%> ParsedC (Const (List String) ⊗ Const Syntax)
parseLamLens = (parseLam ) <! ?whui
  -- (\input, (part ## (res && rest)) => Lambda res.π1 res.π2)

adapt : (Const (List String) ⊗ Const Syntax) =%> Par

parseAll : Par =%> ParsedC CUnit
parseAll = parseLamLens
         ⨾  ?dad
         ⨾ ((adapt ⨾ parseAll) ~+~
            (parseIdLens ⨾ maybeMap {a = Const String, b = CUnit} closeLoop))
            { c1 = (Const (List String) ⊗ Const Syntax)
            , c3 = Par
            , c2 = ParsedC CUnit
            , c4 = ParsedC CUnit}
         ⨾ dia

-- name for extracting the arguments from the syntax nodes
record VarArg where
  constructor MkVarArg
  variable : String

record LamArgs  where
  constructor MkLamArgs
  args : List String
  body : Syntax

record LetArgs where
  constructor MkLetArgs
  name : String
  expr : Syntax
  body : Syntax

record AppArgs where
  constructor MkAppArgs
  fn : Syntax
  arg : Syntax

-- Target language, a lambda calculus without let binding
namespace LC
  public export
  data Lam : Type where
    Variable : String -> Lam
    Lambda : List String -> Lam -> Lam
    App : Lam -> Lam -> Lam

-- The syntax containers,
Syn : Container
Syn = Syntax :- Lam

-- Rewriting variables does not produce any more subgoals
rewriteVar : (VarArg :- Lam) =%> CUnit
rewriteVar = embed (Variable . variable)

-- Rewriting lambda generates a subgoal for the lambda body
rewriteLam : (LamArgs :- Lam) =%> Syn
rewriteLam =
  (\(MkLamArgs name body) => body) <!
  (\(MkLamArgs name body), body' => Lambda name body')

-- Rewriting application generates two subgoals, on for the functio
-- and one for its argument
rewriteApp : Const2 AppArgs Lam =%> Syn ⊗ Syn
rewriteApp =
  (\(MkAppArgs a b) => a && b) <!
  (\_, x => App x.π1 x.π2)

-- rewriting let generates two subgoals, one for the variable bound
-- and one for the program body
rewriteLet : Const2 LetArgs Lam =%> Syn ⊗ Syn
rewriteLet =
    (\(MkLetArgs n a b) => a && b) <!
    (\(MkLetArgs n _ _), x => App (Lambda [n] x.π2) x.π1)

-- Prism does case analysis on the input and returns a choice of
-- constructor
-- (Unit, const Void) + c ≅ MaybeCont #> c
prism : Syn =%> Const2 VarArg  Lam
              + Const2 LamArgs Lam
              + Const2 AppArgs Lam
              + Const2 LetArgs Lam
prism = match <! rebuild
  where
    match : Syntax -> VarArg + LamArgs + AppArgs + LetArgs
    match (Variable str) = <+ <+ <+ MkVarArg str
    match (Lambda strs x) = <+ <+ +> MkLamArgs strs x
    match (Let str x y) = +> MkLetArgs str x y
    match (App x y) = <+ +> MkAppArgs x y

    rebuild : (s : Syntax) ->
       pos (Const2 VarArg Lam
          + Const2 LamArgs Lam
          + Const2 AppArgs Lam
          + Const2 LetArgs Lam)
          (match s) ->
       Lam
    rebuild (Variable str) x = x
    rebuild (Lambda strs y) x = x
    rebuild (Let str y z) x = x
    rebuild (App y z) x = x


-- could simplify this by using the bifunctor instance (+)
dia4 : a + a + a + a =%> a
dia4 =
    (\case (<+ (<+ (<+ x))) => x
           (<+ (<+ (+> x))) => x
           (<+ (+> x)) => x
           (+> x) => x) <!
    (\case (<+ (<+ (<+ x))) => id
           (<+ (<+ (+> x))) => id
           (<+ (+> x)) => id
           (+> x) => id)

-- rewriting the whole language using recursion
rewriteAll : Syn =%> CUnit
rewriteAll = prism ⨾ rewriteNest ⨾ dia4
   where
     rewriteNest : Const2 VarArg  Lam
                 + Const2 LamArgs Lam
                 + Const2 AppArgs Lam
                 + Const2 LetArgs Lam =%> CUnit + CUnit + CUnit + CUnit
     rewriteNest = rewriteVar
                ~+~ (rewriteLam ⨾ rewriteAll)
                ~+~ (rewriteApp ⨾ (rewriteAll `parallel` rewriteAll) ⨾ joinUnit)
                ~+~ (rewriteLet ⨾ (rewriteAll `parallel` rewriteAll) ⨾ joinUnit)




