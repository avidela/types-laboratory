module Interactive.FS

import Data.Container
import Data.Container.Morphism
import Data.Container.Kleene
import Data.Coproduct

import Data.Sigma

import System.File

-- A file system example
data OpenFile = MkOpen String
data WriteFile = MkWrite String File
data CloseFile = MkClose File

-- filesystem monad
FSMonad : Type -> Type
FSMonad = IO . Either FileError

Functor FSMonad where
  map f = map (map f)

Applicative FSMonad where
  pure = pure . pure
  x <*> y = do x' <- x
               y' <- y
               pure (x' <*> y')

Monad FSMonad where
  x >>= f = do x' <- x
               r <- traverse f x'
               pure (join r)

OpenC : Container
OpenC = OpenFile :- File

WriteC : Container
WriteC = WriteFile :- Unit

CloseC : Container
CloseC = CloseFile :- Unit

-- interacting with the file system is a choice of those actions
FileAPI : Container
FileAPI = OpenC + WriteC + CloseC

writeMany : F FSMonad (WriteC).º =%> CUnit
writeMany = embed writeFS
  where
  writeFS : (x : StarShp WriteC) -> FSMonad (StarPos WriteC x)
  writeFS Done = pure (pure StarU)
  writeFS (More ((MkWrite content file) ## p2)) = do
    putStrLn "writing `\{content}` to file"
    Right () <- fPutStrLn {io = IO} file content
    | Left err =>  putStrLn "error \{show err}" *> pure (Left err)
    map (map (StarM ())) (writeFS (p2 ()))

-- The only valid way to interact with the file system however is with
-- this sequence of actions
FileSession : Container
FileSession = OpenC ○ (WriteC).º ○ CloseC

testWrite : F FSMonad WriteC =%> CUnit
testWrite = embed (\(MkWrite content fh) => fPutStr fh content)

testOpen : F FSMonad OpenC =%> CUnit
testOpen = embed (\(MkOpen filename) => openFile filename ReadWrite)

testClose : F FSMonad CloseC =%> CUnit
testClose = embed (\(MkClose fh) => map pure (closeFile fh))

testFS : F FSMonad FileAPI =%> CUnit
testFS = distrib_plus_3 {a = OpenC, b = WriteC, c = CloseC, f = FSMonad}
      |> (testOpen + testWrite + testClose |> dia3)


writeHello : (FileSession).req
writeHello = ((MkOpen "append"
             ## (\file => single (MkWrite "hello" file)))
             ## (\val => MkClose val.π1))

runSession : (FileSession).req -> IO Unit
runSession p1
  = let rr := runSequenceM3 p1 testOpen writeMany testClose
                 {a = OpenC, b = (WriteC).º, c = CloseC, m = FSMonad}
    in rr *> pure ()

main : IO Unit
main = runSession writeHello
