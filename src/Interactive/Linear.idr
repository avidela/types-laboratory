module Interactive.Linear

import Data.Linear.Notation
import Control.Linear.LIO

autobind infixr 0 !>>=
autobind infixr 0 @>>=
autobind infixr 0 ->>=

(!>>=) : LinearBind io =>
        L io {use=Unrestricted} a -@
        ContType io Unrestricted u_k a b -@ L io {use=u_k} b
(!>>=) = Control.Linear.LIO.(>>=)

(@>>=) : LinearBind io =>
        L1 io a -@
        ContType io Linear u_k a b -@ L io {use=u_k} b
(@>>=) = Control.Linear.LIO.(>>=)

(->>=) : LinearBind io =>
        L io {use=None} a -@
        ContType io None u_k a b -@ L io {use=u_k} b
(->>=) = Control.Linear.LIO.(>>=)

fn : L1 IO a -@
     L1 IO b -@
     L0 IO c -@
       L1 IO (LPair a b)
fn f g h =
  (x := f) @>>=
  (y := g) @>>=
  (z := h) ->>=
    pure1 (x # y)
