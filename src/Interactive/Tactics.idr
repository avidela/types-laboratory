module Interactive.Tactics

import Data.Container
import Data.Container.Category
import Data.Category.Functor
import Data.Container.Descriptions
import Data.Container.Descriptions.Maybe
import Data.Container.Morphism
import Data.Container.Morphism.Pointful
import Data.Container.Morphism.Linear
import Data.Product
import Data.Sigma
import Data.Value
import Proofs.Sigma

import Decidable.Equality

import Data.Linear
import Data.Linear.LVect

%hide Prelude.(&&)

%default total

data Term : Type where
  Zero : Term
  Succ : Term -> Term
  (+) : Term -> Term -> Term

data Eq : Term -> Term -> Type where
  Refl : Eq t t
  Trans : Eq r s -> Eq s t -> Eq r t
  Cong : Eq t s -> Eq (Succ t) (Succ s)
  CPlus : Eq t s -> Eq x y -> Eq (t + x) (s + y)
  Sym : Eq t s -> Eq s t
  Assoc : Eq t1 t2 -> Eq r1 r2 -> Eq s1 s2 ->
          Eq ( t1 + (r1  + s1))
             ((t2 +  r2) + s2)

partial
fromInteger : Integer -> Term
fromInteger 0 = Zero

EqProblem : Container
EqProblem = (!>) (Term * Term) (uncurry Eq)

--------------------------------------------------------------------------------
-- Symmetry tactic                                                             --
--------------------------------------------------------------------------------
sym : EqProblem =%> EqProblem
sym = swap <! (\_ => Sym)

--------------------------------------------------------------------------------
-- Refl tactic                                                                --
--------------------------------------------------------------------------------
eq : (t, s : Term) -> Maybe (t === s)
eq Zero Zero = Just Refl
eq (Succ x) (Succ y) = map (\w => cong Succ w) (eq x y)
eq (x + y) (a + b) = do Refl <- eq x a
                        Refl <- eq y b
                        Just Refl
eq _ _ = Nothing

isEq : (t, s : Term) -> Bool
isEq t s = isJust (eq t s)

decCheck : Term -> Term -> MaybeType ()
decCheck a b with (isEq a b)
  decCheck a b | False = Nothing
  decCheck a b | True = Just ()

refl : EqProblem =%> MaybeM CUnit
refl = (uncurry decCheck) <! (\v => backward v.π1 v.π2)
where
  backward : (x, y : Term) ->
             Σ (isTrue ((decCheck x y).ex1)) (\_ => ()) ->
             Eq x y
  backward x y z with (eq x y)
    backward x y z | Nothing = absurd z.π1
    backward x x z | (Just Refl) = Refl

--------------------------------------------------------------------------------
-- Transitivity tactic                                                        --
--------------------------------------------------------------------------------
trans : (term : Term) -> EqProblem =%> (EqProblem ⊗ EqProblem)
trans term =
    (\x => (x.π1 && term) && (term && x.π2)) <!
    (\_ => uncurry Trans)

--------------------------------------------------------------------------------
-- Associativity tactic                                                       --
--------------------------------------------------------------------------------

isAssoc : (t, s : Term) -> Maybe ((t1 : Term **  t2 : Term ** t3 : Term **
                                  (t1 + (t2 + t3) === t)) *
                                  (t1 : Term **  t2 : Term ** t3 : Term **
                                  (((t1 + t2) + t3) === s)))
isAssoc (x + (y + z)) ((a + b) + c)
  = Just ((x ** y ** z ** Refl) && (a ** b ** c ** Refl))
isAssoc _ _ = Nothing

assoc : EqProblem =%> MaybeM (EqProblem ⊗ (EqProblem ⊗ EqProblem))
assoc = isAssocCandidate <! isAssocSolution
  where
    isAssocCandidate : Term * Term -> MaybeType ((Term * Term) * ((Term * Term) * (Term * Term)))
    isAssocCandidate x with (isAssoc x.π1 x.π2)
      isAssocCandidate x | Nothing = Nothing
      isAssocCandidate x | (Just ((t1 ** t2 ** t3 ** p) && (s1 ** s2 ** s3 ** q)))
        = Just ((t1 && s1) && ((t2 && s2) && (t3 && s3)))

    isAssocSolution : (s : Term * Term) ->
      (MaybeCont ○ (EqProblem ⊗ (EqProblem ⊗ EqProblem))).pos (isAssocCandidate s) ->
      (pos EqProblem s)
    isAssocSolution s x with (isAssoc s.π1 s.π2)
      isAssocSolution s x | Nothing = absurd x.π1
      isAssocSolution (s && s') x | (Just ((t1 ** t2 ** t3 ** p) && (s1 ** s2 ** s3 ** q)))
        = replace {p = Eq s} q
        $ rewrite sym p in Assoc x.π2.π1 x.π2.π2.π1 x.π2.π2.π2

--------------------------------------------------------------------------------
-- Run custom tactic                                                          --
--------------------------------------------------------------------------------

combineUnits : MaybeM CUnit ⊗ MaybeM CUnit =%> MaybeM CUnit
combineUnits = strength {x = CUnit, y = MaybeM CUnit} ⨾ assocCont
  where
    assocCont : (MaybeM (CUnit ⊗ (MaybeM CUnit))) =%> MaybeM CUnit
    assocCont = let sw : CUnit ⊗ (MaybeM CUnit) =%> (MaybeM CUnit) ⊗ CUnit
                    sw = (swap {x = CUnit, y = MaybeM CUnit})

                    str : ((MaybeM CUnit) ⊗ CUnit) =%> MaybeM (CUnit ⊗ CUnit)
                    str = strength {x = CUnit, y = CUnit}

                    mp : MaybeM (CUnit ⊗ (MaybeCont ○ CUnit)) =%>
                          MaybeM (MaybeM (CUnit ⊗ CUnit))
                    mp = composeFunctor {a = MaybeCont
                                         , x = (CUnit ⊗ (MaybeCont ○ CUnit))
                                         , y = MaybeM (CUnit ⊗ CUnit)}
                                         (sw ⨾ str)
                 in mp
                 ⨾ join {x = CUnit ⊗ CUnit}
                 ⨾ composeFunctor {a = MaybeCont, x = CUnit ⊗ CUnit, y = CUnit} joinUnit

tripleRight : a ⊗ a =%> a -> a ⊗ (a ⊗ a) =%> a
tripleRight x = parallel (identity a) x ⨾ x

assocCompose : EqProblem =%> (MaybeCont ○ CUnit)
assocCompose
    = sym
    ⨾ assoc
    ⨾ composeFunctor { a = MaybeCont }
            ((refl `parallel` (refl `parallel` refl))
                ⨾ tripleRight combineUnits)
    ⨾ join {x = CUnit}



0 PointfulM : (a, b : Container) -> Type
PointfulM a b = (1 x : a.shp) -> Maybe (Value b (a.pos x))

0 PointfulL : (a, b : Container) -> Type
PointfulL a b =  (1 x : a.shp) -> Value b (a.pos x)

0 Pointful1 : (a, b : Container) -> Type
Pointful1 a b =  (1 x : a.shp) -> Value1 b (a.pos x)

val1ToSig1 : Value1 a b -@ Σ1 a.shp (\v => a.pos v -@ b)
val1ToSig1 (MkVal1 x y) = x @@ y

sig1ToVal1 : Σ1 a.shp (\v => a.pos v -@ b) -@ Value1 a b
sig1ToVal1 (x @@ y) = MkVal1 x y

PointfulF : PointfulL a (MaybeM b) -> PointfulM a b
PointfulF f x = case f x of
                     (MkVal (MkEx False b2) fn) => Nothing
                     (MkVal (MkEx True b2) fn) => Just (MkVal (b2 ()) (\y => fn (() ## y)))

fromLens : a =%> b -> PointfulL a b
-- fromLens y x = assert_linear $ MkVal (y.fwd x) (y.bwd x)

toPoint : a =%> b -> Pointful a b

fromLinear : Linear a b -@ Pointful1 a b
fromLinear (MkSLin f) x = sig1ToVal1 $ ?ret

toLinear : Pointful1 a b -> Linear a b
toLinear f = MkSLin $  ?toLinear_rhs

refl' : PointfulM EqProblem CUnit
refl' = PointfulF (fromLens refl) {a = EqProblem}

assoc' : PointfulM EqProblem (EqProblem ⊗ (EqProblem ⊗ EqProblem))
assoc'  = PointfulF (fromLens assoc) {a = EqProblem}

private autobind infixr 0 ?>>=
private autobind infixr 0 @>
private typebind infixr 0 =@

-- linear bind operator
(@>) : x -@ (x -@ y) -@ y
(@>) x f = f x


(?>>=) : (1 _ : Maybe x) -> (x -> Maybe y) -> Maybe y
(?>>=) Nothing f = Nothing
(?>>=) (Just z) f = f z

private infixl 5 $%
($%) : PointfulM a b -@ Value a r -@ Maybe (Value b r)
($%) f (MkVal x y) =
  (gn := f x) ?>>=
  Just (MkVal gn.arg (y . gn.fn))

splitupl : (v : Value (a ⊗ b) r)
       =@ Value a (b.pos v.arg.π2 -> r)
        * Value b (b.pos v.arg.π2)
splitupl (MkVal (x1 && x2) f) = MkVal x1 (\g, a => f (g && a)) && MkVal x2 id

pairupl : Value a (s -> r) -@ Value b s -@ Value (a ⊗ b) r
pairupl (MkVal v1 f1) (MkVal v2 f2) = MkVal (v1 && v2) (\(x1 && x2) => f1 x1 (f2 x2))


-- assocCompose : EqProblem =%> (MaybeCont ○ CUnit)
-- assocCompose
--     = sym
--    |> assoc
--    |> composeFunctor { a = MaybeCont }
--             ((refl `parallel` (refl `parallel` refl))
--                 |> tripleRight combineUnits)
--    |> join {x = CUnit}

assocPointful : PointfulM EqProblem CUnit
assocPointful x =
    (s := fromLens sym x) @>
    (ass := assoc' $% s) ?>>=
    (p1 && pn := splitupl ass {a = EqProblem, b = EqProblem ⊗ EqProblem}) @>
    (p2 && p3 := splitupl pn {a = EqProblem, b = EqProblem}) @>
    (ref1 := refl' $% p1) ?>>=
    (ref2 := refl' $% p2) ?>>=
    (ref3 := refl' $% p3) ?>>=
    (MkVal v mf := pairupl ref1 (pairupl ref2 ref3)) @>
      pure (unit (mf v))

symNonLinear : Pointful EqProblem EqProblem
symNonLinear x = let
     s = toPoint sym x
     s' = toPoint sym $- s
  in s

symLinear : PointfulL EqProblem EqProblem
symLinear x = let
     1 s = fromLens sym x
     -- 1 s' = fromLens sym x
  in s

{-

    LetL (s := fromLens sym x) |
    LetM (ass := assoc' $% s) |
    LetL (p1 && pn := splitupl ass {a = EqProblem, b = EqProblem ⊗ EqProblem}) |
    LetL (p2 && p3 := splitupl pn {a = EqProblem, b = EqProblem}) |
    LetM (ref1 := refl' $% p1) |
    LetM (ref2 := refl' $% p2) |
    LetM (ref3 := refl' $% p3) |
    LetL (ppp := pairupl ref2 ref3) |
    LetL (MkVal (() && (() && ()) mf := pairupl ref1 ppp) |
         pure ((arg := ()) `MkVal` mf (() && (() && ())))

%unbound_implicits off
assocZeroProof : Tactics.assocCompose.bwd
                   (((0 + 0) + 0) && (0 + (0 + 0))) (() ## ())
              === Sym (Assoc Refl Refl Refl)
assocZeroProof = Builtin.Refl


