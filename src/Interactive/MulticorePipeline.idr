module Interactive.MulticorePipeline

import System
import System.Concurrency
import Data.Vect
import Data.Vect.Quantifiers
import Data.IORef
import Data.DPair

import System.Clock

public export
Pipeline : Nat -> Type
Pipeline length = Vect length Type

public export
Fn : Type -> Type -> Type
Fn x y = x -> y

public export
FnM : (m : Type -> Type) -> Type -> Type -> Type
FnM m x y = x -> m y

pairUp : Vect (S n) a -> Vect n (a, a)
pairUp [a] = []
pairUp (x :: y :: xs) = (x, y) :: pairUp (y :: xs)

public export
Impl : Pipeline (2 + n) -> Type
Impl cs = All (uncurry Fn) (pairUp cs)

public export
ImplM : (m : Type -> Type) -> Pipeline (2 + n) -> Type
ImplM m cs = All (uncurry (FnM m)) (pairUp cs)

||| Wraps a pure function into an IO stage
export
record Stage (a : Type) (b : Type) where
  constructor MkStage
  name : String
  transform : a -> IO b

delay : Nat -> IO ()
delay n = sleep (cast n )

||| Run the given computation in an IO context and print the start and end of it
||| Also delay the computation by 1 second so that the multi-core effect can be seen
delayFn : Show x => Show y => (f : x -> y) -> x -> y
delayFn f val = believe_me $ do
  let result = f val
  delay 0
  pure result

||| Convert a pure pipeline implementation to IO stages
toStages :
    {p : Pipeline (2 + n)} -> (print : All Show p) =>
    ImplM IO p -> Vect (S n) String -> All (uncurry Stage) (pairUp p)
toStages {p = [a,b]} {print = [p1, p2]} [f] [name]
    = [MkStage name (f)]
toStages {p = (a :: b :: x :: xs)} {print = p1 :: p2 :: px} (f :: fs) (name :: names) =
  MkStage name (f) :: toStages {p = b :: x :: xs} fs names

covering
forever : Lazy (IO ()) -> IO ()
forever action = action >> forever action

record NamedChannel (a : Type) where
  constructor MkNamedChan
  name : String
  chan : Channel a

||| Creates and starts a pipeline stage
startStage : Show a => Show b => NamedChannel a -> NamedChannel b -> Stage a b -> IO ()
startStage inChan outChan stage = do
  forever $ do
    input <- channelGet inChan.chan
    putStrLn "recieved value \{show input} from \{inChan.name}"
    output <- stage.transform input
    putStrLn "produced value \{show output} sent to \{outChan.name}"
    channelPut outChan.chan output

||| Feeds initial inputs into the pipeline
feedInputs : Show a => NamedChannel a -> Vect jobs a -> IO ()
feedInputs chan [] = pure ()
feedInputs chan (x :: xs) = do
  putStrLn "feeding input \{show x} into the pipeline"
  channelPut chan.chan x
  feedInputs chan xs

||| Collects results from the pipeline
collectResults : Show a => NamedChannel a -> (jobs : Nat) -> IO (Vect jobs a)
collectResults chan 0 = pure []
collectResults chan (S k) = do
  x <- channelGet chan.chan
  putStrLn "channel \{chan.name} getting value \{ show x }"
  xs <- collectResults chan k
  pure (x :: xs)

startAll :
  (p : Pipeline (2 + n')) ->
  (stages : All (uncurry Stage) (pairUp p)) ->
  (channels : All NamedChannel p) ->
  (print : All Show p) =>
  IO ()
startAll [x, y] [stg] [inChan, outChan] {print = [p1, p2]}
    = putStrLn "starting stage \{stg.name} between layers \{inChan.name} and \{outChan.name}"
    >> ignore (fork (startStage inChan outChan stg))
startAll (x :: y :: z :: xs) (stg :: stages) (inChan :: outChan :: chans)
    {print = p1 :: p2 :: p3 :: p4}
    = putStrLn "starting stage \{stg.name} between layers \{inChan.name} and \{outChan.name}"
    >> (ignore (fork (startStage inChan outChan stg)))
    >> startAll (y :: z :: xs) stages (outChan :: chans) {print = p2 :: p3 :: p4}

startAllChannels : (p : Pipeline (S n)) -> All (const String) p -> IO (All NamedChannel p)
startAllChannels [x] [name] = do
  chan <- makeChannel {io = IO, a = x}
  putStrLn "created channel \{name}"
  pure ([ MkNamedChan name chan ])
startAllChannels (x :: y :: xs) (n1 :: n2 :: n3) = do
  chan <- makeChannel {io = IO, a = x}
  putStrLn "created channel \{n1}"
  rest <- startAllChannels (y :: xs) (n2 :: n3)
  pure (MkNamedChan n1 chan :: rest)

lastAll : {xs : Vect (S n) a} -> All p xs -> p (last xs)
lastAll [x] = x
lastAll (x :: y :: xs) = lastAll (y :: xs)

||| Runs a pipeline concurrently
export
runPipelineConc : (p : Pipeline (2 + n)) ->
                  (layerNames : All (const String) p) ->
                  (stagenames : Vect (S n) String) ->
                  (print : All Show p) => {jobs : Nat} ->
                  ImplM IO p -> Vect jobs (head p) -> IO (Vect jobs (last p))
runPipelineConc p@(p1 :: p2 :: ps) layerNames stageNames impl inputs {print = pr@(pr1 :: pr2 :: prx)} = do
  -- Create channels between stages
  channels <- startAllChannels p layerNames
  let stages = toStages @{pr} impl stageNames

  -- Start each stage in its own thread
  startAll (p1 :: p2 :: ps) stages channels @{pr}

  let inputNamedChannel : NamedChannel p1 = believe_me $ head channels
  let outputNamedChannel : NamedChannel (last p) = lastAll channels
  ignore $ fork $ feedInputs {jobs} @{pr1} inputNamedChannel inputs
  res <- collectResults @{lastAll pr} outputNamedChannel jobs
  pure res



















------------------------------------------------------------------------------------
-- Pipeline Example
------------------------------------------------------------------------------------

TestPipeline : Pipeline 4
TestPipeline = [String, Nat, Nat, String]

slowLength : String -> IO Nat
slowLength str =
  delay 5
  >> pure (length str)

slowDouble : Nat -> IO Nat
slowDouble n = delay 5 >> pure (n * n)

slowShow : Nat -> IO String
slowShow n = delay 5 >> pure (show n)

impl : ImplM IO TestPipeline
impl = [slowLength , slowDouble, slowShow]

RunM : Monad m => (p : Pipeline (2 + n)) -> ImplM m p -> head p -> m (last p)
RunM [x, y] [f] = f
RunM (s :: t :: x :: xs) (f :: cont) = RunM (t :: x :: xs) cont <=< f

-- Example usage:
export
example : IO ()
example = do

  let inputs = ["hello", "carabiner", "test"]
  start <- clockTime Monotonic
  results <- runPipelineConc TestPipeline ["input", "layer1", "layer2", "outout"] ["length", "twice", "printer"] impl inputs
  end <- clockTime Monotonic
  let diff_multicore = timeDifference end start
  start <- clockTime Monotonic
  computeAll <- traverse (RunM TestPipeline impl) inputs
  end <- clockTime Monotonic
  putStrLn "result slow \{show computeAll}"
  let diff_singlecore = timeDifference end start
  putStrLn "multicore time : \{show diff_multicore}"
  putStrLn "singlecore time : \{show diff_singlecore}"
