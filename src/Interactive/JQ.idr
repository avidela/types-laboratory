module Interactive.JQ

import Data.Container
import Data.Container.Category
import Data.Category.Functor
import Data.Container.Descriptions
import Data.Container.Morphism
import Data.Container.Monad
import Data.Container.Cartesian
import Data.Product
import Data.Sigma
import Data.List
import Data.List.Elem
import Data.Vect.Elem
import Proofs.Sigma
import Data.Vect

import Interactive.Server.Serialise

import Decidable.Equality

import Language.JSON

import System.File

%hide Prelude.(>=>)

%default total

Obj : Type
Obj = List (String, JSON)

CJSON : Container
CJSON = Const JSON

CBool : Container
CBool = Const Bool

CDouble : Container
CDouble = Const Bool

CString : Container
CString = Const Bool

CArray : Container
CArray = ListCont #> CJSON

private infixr 7 .|
-- composition of queries, implemented as kleisli composition of container morphisms
(.) : CJSON =%> MaybeM CJSON ->
      CJSON =%> MaybeM CJSON ->
      CJSON =%> MaybeM CJSON
(.) x y = (>=>) {m = MaybeM, c = CJSON} x y


namespace JSONString
  public export
  fromString : String -> JSON
  fromString = JString

-- match on bool
getBool : CJSON =%> MaybeM CBool
getBool =  findBool <! returnBool
  where
    findBool : JSON -> Ex MaybeCont Bool
    findBool (JBoolean x) = Just x
    findBool _ = Nothing

    returnBool : (x : JSON) ->
                 Σ (isTrue ((findBool x).ex1)) (\x => Bool) ->
                 JSON
    returnBool x y with (findBool x)
      returnBool x y | (MkEx True p2) = JBoolean y.π2
      returnBool x y | (MkEx False p2) = absurd y.π1

getList : CJSON =%> MaybeM (Const (List JSON))
getList =  isList <! returnList
  where
    isList : JSON -> Ex MaybeCont (List JSON)
    isList (JArray jsons) = Just jsons
    isList _ = Nothing

    returnList : (x : JSON) ->
                 pos (MaybeM (Const (List JSON))) (isList x)->
                 JSON
    returnList x y with (isList x)
      returnList x y | (MkEx False p2) = absurd y.π1
      returnList x y | (MkEx True p2) = JArray y.π2

getList' : CJSON =%> MaybeM (ListM CJSON)
getList' = getList ⨾ composeFunctor {a = MaybeCont} convertList

-- match on object
getObject : CJSON =%> MaybeM (Const Obj)
getObject =  matchObj <! updateObj
  where
    matchObj : JSON -> Ex MaybeCont Obj
    matchObj (JObject xs) = Just xs
    matchObj _ = Nothing

    updateObj : (val : JSON) ->
                pos (MaybeM (Const Obj)) (matchObj val) ->
                JSON
    updateObj val x with (matchObj val)
      updateObj val x | (MkEx False p2) = absurd x.π1
      updateObj val x | (MkEx True p2) = JObject x.π2


---------------------------------------------------------------------------------
-- Array lookup
---------------------------------------------------------------------------------

range : Nat -> Const (List a) =%> MaybeM (Const a)
range k =  inRange <! updateAt
    where
    inRange : List a -> Ex MaybeCont a
    inRange xs with (inBounds k xs)
      inRange xs | (Yes prf) = Just (index k xs)
      inRange xs | (No contra) = Nothing

    updateAt : (x : List a) -> Σ (isTrue ((inRange x).ex1)) (\x => a) -> List a
    updateAt x y with (inBounds k x)
      updateAt x y | (Yes prf) = replaceAt k y.π2 x
      updateAt x y | (No contra) = absurd y.π1

indexJSON : Nat -> CJSON =%> MaybeM CJSON
indexJSON k = (getList >=> range k) {m = (MaybeM), c = CJSON}

fromInteger : Integer -> CJSON =%> MaybeM CJSON
fromInteger = indexJSON . fromInteger {ty = Nat}

---------------------------------------------------------------------------------
-- Key lookup
---------------------------------------------------------------------------------
data HasKey : {0 k, v : Type} -> List (k, v) -> k -> Type where
  Here : HasKey ((k, v) :: xs) k
  There : HasKey xs k -> HasKey (x :: xs) k

hasKey : DecEq k => (x : k) -> (ls : List (k, v)) -> Maybe (HasKey ls x)
hasKey x [] = Nothing
hasKey x ((y, z) :: xs) with (decEq x y)
  hasKey x ((y, z) :: xs) | No _ = map There (hasKey x xs)
  hasKey x ((x, z) :: xs) | Yes Refl = Just Here

readVal : (xs : List (k, v)) -> HasKey xs x -> v
readVal ((x, v) :: xs) Here = v
readVal (x :: xs) (There y) = readVal xs y

writeAt : (xs : List (k, v)) -> HasKey xs x -> v -> List (k, v)
writeAt ((x, v) :: xs) Here z = (x, z) :: xs
writeAt (x :: xs) (There y) z = x :: writeAt xs y z

lookup : String -> Const (List (String, a)) =%> MaybeM (Const a)
lookup key = inList <! updateElem
    where
      inList : List (String, a) -> Ex MaybeCont a
      inList xs with (hasKey key xs)
        inList xs | Nothing = Nothing
        inList xs | (Just x) = Just (readVal xs x )

      updateElem : (xs : List (String, a)) -> pos (MaybeM (Const a)) (inList xs) -> List (String, a)
      updateElem xs x with (hasKey key xs)
        updateElem xs x | Nothing = absurd x.π1
        updateElem xs x | (Just y) = writeAt xs y x.π2

lookupJSON : String -> CJSON =%> MaybeM CJSON
lookupJSON key = (getObject >=> lookup key) {m = MaybeM, c = CJSON}

fromString : String -> CJSON =%> MaybeM CJSON
fromString = lookupJSON

---------------------------------------------------------------------------------
-- Lens operations
---------------------------------------------------------------------------------

carry : MaybeCont #> (Const a) =%> CUnit
carry =  (const ()) <!
  (\case (MkEx False p2) => \y => p2
         (MkEx True p2) => \_ => p2)

---------------------------------------------------------------------------------
-- JQ operations
---------------------------------------------------------------------------------

-- End the query by providing a couint
(.end) : CJSON =%> MaybeM CJSON -> (update : JSON -> JSON) ->  CJSON =%> MaybeM CUnit
(.end) lens update = (>=>) {a = CJSON, b = CJSON, m = MaybeM, c = CUnit} lens (
  (\_ => Just ()) <!
  (\x, _ => update x)
  )

covering
readJSONFile : (fileName : String) -> IO (Maybe JSON)
readJSONFile filename = do
  Right jsonFile <- readFile filename
    | Left _ => putStrLn "could not load JSON" *> pure Nothing
  let (Just jsonObj) = parse jsonFile
    | Nothing => putStrLn "could not parse JSON" *> pure Nothing
  pure (Just jsonObj)

covering
runQuery : (filename : String) -> (query : CJSON =%> MaybeM CUnit) -> IO ()
runQuery filename query = do
  Just jsonObj <- readJSONFile filename
  | Nothing => pure ()
  exec jsonObj query
  where
    exec : JSON -> CJSON =%> MaybeM CUnit -> IO ()
    exec json lens with (lens.fwd json) proof p
      exec json lens | (MkEx False p2) = putStrLn "failed query"
      exec json lens | (MkEx True p2) =
        let bval = lens.bwd json (rewrite p in () ## ())
        in putStrLn $ format 4 bval

covering
viewQuery : (filename : String) -> (query : CJSON =%> MaybeM CJSON) -> IO ()
viewQuery file query = do
  Just jsonObj <- readJSONFile file
  | Nothing => pure ()
  case (query.fwd jsonObj) of
       (MkEx True p) => putStrLn $ format 4 $ p ()
       (MkEx False p) => putStrLn "failed query"

combineJSON : Const a =%> MaybeM (Const b) ->
              Const a =%> MaybeM (Const b) ->
              Const a =%> MaybeM (Const b)
combineJSON x y =  fwdBoth <! bwdAnd
    where
      fwdBoth : a -> Ex MaybeCont b
      fwdBoth json with (x.fwd json) proof f1
        fwdBoth json | (MkEx True p) = y.fwd (x.bwd json (rewrite f1 in () ## p ()))
        fwdBoth json | (MkEx False p) = Nothing

      bwdAnd : (json : a) -> Σ (isTrue (fwdBoth json).ex1) (\x => b) -> a
      bwdAnd json arg with (x.fwd json) proof f1
        bwdAnd json arg | (MkEx True p) with (y.fwd (x.bwd json (rewrite f1 in () ## p ()))) proof f2
          bwdAnd json arg | (MkEx True p) | (MkEx True q) = y.bwd (x.bwd json (rewrite f1 in () ## p ())) (rewrite f2 in () ## q ())
          bwdAnd json arg | (MkEx True p) | (MkEx False q) = absurd arg.π1
        bwdAnd json arg | (MkEx False p) = absurd arg.π1

namespace ListDSL
  public export
  QueryBuilder : Nat -> Type
  QueryBuilder n = Vect n (CJSON =%> MaybeM CJSON)

  export
  fromQuery : QueryBuilder (S n) -> Vect.foldr1 (⊗) (Vect.replicate (S n) CJSON) =%> Vect.foldr1 (⊗) (Vect.replicate (S n) (MaybeM CJSON))
  fromQuery {n = Z} (x :: []) = x
  fromQuery {n = S n} (x :: (y :: z)) = let rec = fromQuery (y :: z) in x `parallel` rec

  export
  (.end) : QueryBuilder (S n) -> (update : Vect (S n) (JSON -> JSON)) ->  CJSON =%> MaybeM CUnit
  (.end) (x :: []) [fn] = x.end fn
  (.end) (x :: (y :: z)) (f :: fs) = let v1 = x.end f
                                         v2 = (y :: z).end fs
                                     in combineJSON v1 v2

  export
  (.view) : (query : QueryBuilder (S n)) -> CJSON =%> (MaybeM CJSON)
  (.view) (x :: []) = x
  (.view) (x :: (y :: z)) = let rec = (y :: z).view in combineJSON x rec


covering
main : IO ()
main = do
  runQuery "fruits.json" $ (1."color").end (const "blue")
  runQuery "fruits.json" $
      [ 1."color" , 2."color", 0."price"].end
      [ const "blue" , const "rose"
                     , const (JNumber 2.0)]

  viewQuery "fruits.json" $ [ 1."color"
                            , 2."color"
                            , 0."price"].view
















{-
covering
main : IO ()
main = do
  viewQuery "fruits.json" (indexJSON 1 .| lookupJSON "color")
  viewQuery "fruits.json" (indexJSON 1 )
  viewQuery "fruits.json" (identity ⨾ just)
  runQuery "fruits.json" $ (indexJSON 1 .| lookupJSON "color").end (\_ => JString "grün")
  pure ()

