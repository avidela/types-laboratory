module Interactive.IO

import Data.Container.Morphism
import Data.Container.Descriptions
import Data.String
import Interactive.Server.Serialise
import Data.Coproduct
import Data.Product
import Data.Sigma
import Proofs.Sigma
import Data.List1

import Debug.Trace

%hide Prelude.Interfaces.(>=>)

State : Container -> Container -> Container -> Type
State x y z = x ⊗ y =%> z

runState : State a b c -> a.shp -> b =%> c
runState op st =
  (\x => op.fwd (st && x)) <!
  (\x, y  => let gn = op.bwd (st && x) y in gn.π2)

map : a =%> b -> MaybeM a =%> MaybeM b
map = composeFunctor {a = MaybeCont}

kleisli : a =%> MaybeM b -> b =%> MaybeM c -> a =%> MaybeM c
kleisli x y = x ⨾ map {a=b} y ⨾ join

echo : Const String =%> CUnit
echo = embed id

parseNat : Const String =%> MaybeM (Const Nat)
parseNat = isNat <! printNat
  where
    isNat : String -> MaybeType Nat
    isNat x = let p = fromIdris $ parseInteger {a=Int} x
              in map cast p

    printNat : (x : String) -> Σ (isTrue (isNat x).ex1) (\_ => Nat) -> String
    printNat x y = show y.π2

double : Const Nat =%> Const Nat
double = (*2) <! (const id)

closeM : MaybeM (Const a) =%> MaybeM CUnit
closeM = fwd <! bwd
  where
    fwd : Ex MaybeCont a -> Ex MaybeCont ()
    fwd = map (const ())

    bwd : (x : Ex MaybeCont a) -> Σ (isTrue (fwd x).ex1) (\_ => ()) ->
          pos (MaybeM (Const a)) x
    bwd (MkEx False q) y = absurd y.π1
    bwd (MkEx True q) y = () ## q ()

export
runSequence : Encode b => Decode a => Const2 a b =%> CUnit -> IO ()
runSequence lens = do
  input <- getLine
  let Just decoded = decode {t = a} input
    | Nothing => putStrLn "could not decode '\{input}'"
  putStrLn (encode {t = b} (extract lens decoded))
  runSequence lens

export
runSequenceM : Encode b => Decode a => MkCont a (\_ => b) =%> MaybeM CUnit -> IO ()
runSequenceM lens = do
  input <- getLine
  let Just decoded = decode {t=a} input
    | Nothing => putStrLn "could not decode '\{input}'"
  rest decoded lens
  runSequenceM lens
  where
    rest : a -> Const2 a b =%> MaybeM CUnit -> IO ()
    rest x lens with (lens.fwd x) proof p
      rest x lens | (MkEx False p2) = putStrLn "error while running program"
      rest x lens | (MkEx True p2) = let bval = lens.bwd x (rewrite p in () ## ()) in
                                         putStrLn (encode bval)

Path : Type
Path = Prelude.List String

data Op
  = Plus Int Int
  | Mult Int Int

parseOperation : Const2 Path x =%> MaybeM (Const2 Op x)
parseOperation =  parseOp <! (\_ => π2)
  where
    parseOp : Path -> MaybeType Op
    parseOp ["mult", a, b] = fromIdris $ Mult <$> parseInteger a <*> parseInteger b
    parseOp ["plus", a, b] = fromIdris $ Plus <$> parseInteger a <*> parseInteger b
    parseOp _ = trace "could not parse op" Nothing

-- split line into path segments
parsePath : Const2 String x =%> (Const2 Path x)
parsePath =
    (toList . split (=='/') ) <!
    (\_ => id)

operate : Const2 Op Int =%> CUnit
operate = embed (\case (Plus a b) => a + b
                       (Mult a b) => a * b)

AuthLens : Container -> Container -> Type
AuthLens = State (Const $ Prelude.List String)

User : Type
User = String

authenticate : AuthLens (Const2 (User * Path) x) (MaybeM $ Const2 Path x)
authenticate =
  authenticate <!
  (\ls, x => ls.π1 && x.π2)
  where
    authenticate : List String * (User * Path) -> MaybeType Path
    authenticate (allowList && (user && arg)) =
      if user `elem` allowList
         then Just arg
         else trace "could not authenticate" Nothing

parseAuth : Const2 String x =%> MaybeM (Const2 User x * Const2 Path x)
parseAuth = parseSplit ⨾ composeFunctor {a=MaybeCont} carryBoth
  where
    parseSplit : Const2 String x =%> MaybeM (Const2 String x * Const2 String x)
    parseSplit =
      (\x => case words x of [a, b] => Just (a && b); _ => trace "could not split" Nothing)  <!
      (\_, x => dia x.π2)

    -- TODO: findout why we need c2=Const2 User x
    carryBoth : (Const2 String x * Const2 String x) =%> (Const2 User x * Const2 Path x)
    carryBoth = (identity (Const2 String x)) ~*~ parsePath

adjust : Const2 String Int * Const2 Path Int
     =%> MkCont ((String * Path)) (\x => Int)
adjust =  id <! (const (<+))

auth : List User -> Const2 String Int =%> MaybeM (Const2 Path Int)
auth st = kleisli { b = Const2 User Int * Const2 Path Int,
                    c = Const2 Path Int}
                  parseAuth
        $ (adjust ⨾ runState {a = Const (List String)} authenticate st)



runCalc : Const2 Path Int =%> MaybeM CUnit
runCalc = parseOperation ⨾ map operate











main : IO ()
-- main = runSequenceM $ parsePath ⨾ runCalc
-- main = runSequenceM $ kleisli {c = CUnit} (auth ["alice", "bob"]) runCalc






































