module Interactive.Parsing

import Data.Container.Morphism
import Data.Container
import Data.Container.Descriptions.List
import Data.Container.Descriptions.Maybe

import Data.List.Quantifiers
import Data.Fin
import Data.Product
import Data.Coproduct
import Data.Sigma
import Data.String
import Data.Maybe
import Data.Integral

import System.File

---------------------------------------------------------------------------------
-- Some data type
---------------------------------------------------------------------------------
data Some : (a -> Type) -> List a -> Type where
  Take : {0 p : a -> Type} -> p x -> Some p ls -> Some p (x :: ls)
  Drop : {0 p : a -> Type} ->        Some p ls -> Some p (x :: ls)
  End : Some p []

zipIndex : {0 a : Type} -> {0 xs : List a} -> {0 p : a -> Type} ->
           Some p xs -> List (Σ (Fin (length xs)) (\i => p (index' xs i)))
zipIndex (Take x y) = (FZ ## x) :: map (\(i ## z) => FS i ## z) (zipIndex y)
zipIndex (Drop x) = map (\(i ## x) => FS i ## x) (zipIndex x)
zipIndex End = []

getMissing : {xs : List a} -> Some p xs -> List (Fin (length xs))
getMissing {xs = (x :: ls)} (Take e y) = map FS (getMissing y)
getMissing {xs = (x :: ls)} (Drop xs) = FZ :: map FS (getMissing {xs = ls} xs)
getMissing {xs = []} End = []
  where
    rangeList : (xs : List a) -> List (Fin (length xs))
    rangeList [] = []
    rangeList (x :: xs) = FZ :: map FS (rangeList xs)

---------------------------------------------------------------------------------
-- Some container monad
---------------------------------------------------------------------------------

SomeC : Container -> Container
SomeC c2 =
    (xs : List c2.shp) !>
    (Some c2.pos xs)

map : a =%> b -> SomeC a =%> SomeC b
map x = MkMorphism (map x.fwd) backward
  where
    backward : (xs : List a.shp) ->
               Some b.pos (map x.fwd xs) -> Some a.pos xs
    backward [] End = End
    backward (y :: xs) (Take p ys) = Take (x.bwd y p) (backward xs ys)
    backward (y :: xs) (Drop ys) = Drop (backward xs ys)

splitSome : {xs : List a} -> Some p (xs ++ ys) -> Some p xs * Some p ys
splitSome x {xs = []}  = End && x
splitSome (Take x z) {xs = (y :: xs)}  = let r : ? ; r = splitSome z in Take x r.π1 && r.π2
splitSome (Drop x) {xs = (y :: xs)}  = let r : ? ; r = splitSome x in Drop r.π1 && r.π2

join : SomeC (SomeC a) =%> SomeC a
join = MkMorphism flatList joinSomeBwd
  where
    joinSomeBwd : (xs : List (List a.shp)) -> Some a.pos (flatList xs) -> Some (Some a.pos) xs
    joinSomeBwd [] x = End
    joinSomeBwd (y :: xs) x = uncurry Take (mapSnd (joinSomeBwd xs) (splitSome x))

allToSome : AllList ((x : a) !> Maybe (b x)) =%> SomeC (MkCont a b)
allToSome = MkMorphism id rebuild
  where
    rebuild : (xs : List a) -> Some b xs -> All (\x => Maybe (b x)) xs
    rebuild (x :: ls) (Take y z) = Just y :: rebuild ls z
    rebuild (x :: ls) (Drop y) = Nothing :: rebuild ls y
    rebuild [] End = []

someToAll : SomeC (MkCont a b) =%> AllList ((x : a) !> Maybe (b x))
someToAll = MkMorphism id rebuild
  where
    rebuild : (xs : List a) -> All (\x => Maybe (b x)) xs -> Some b xs
    rebuild [] [] = End
    rebuild (x :: xs) (Nothing :: z) = Drop (rebuild xs z)
    rebuild (x :: xs) ((Just y) :: z) = Take y (rebuild xs z)

---------------------------------------------------------------------------------
-- Line splitter
---------------------------------------------------------------------------------
splitFile : (String :- String) =%> SomeC (String :- output)
splitFile = MkMorphism lines printErrors
  where
    printError : (orig : List String) -> (i : Fin (length orig)) -> String
    printError orig i = "At line \{show (cast {to = Nat} i)}: Could not parse \"\{index' orig i}\""

    printErrors : (input : String) -> Some (const error) (lines input) -> String
    printErrors input x = unlines (map (printError (lines input)) (getMissing x))

splitFileMaybe : (String :- String) =%> AllList (CUnit + (String :- output))
splitFileMaybe = MkMorphism (map (+>) . Data.String.lines) printErrors
  where
    getIndices : (ls : List a) -> All (choice (const ()) (const output)) (map (+>) ls) -> List (Fin (length (ls)))
    getIndices [] x = []
    getIndices (x :: xs) (y :: ys) = FZ :: map FS (getIndices xs ys)

    printError : (orig : List String) -> (i : Fin (length orig)) -> String
    printError orig i = "At line \{show (cast {to = Nat} i)}: Could not parse \"\{index' orig i}\""

    printErrors : (input : String) ->
                  All (choice (const ()) (const output)) (map (+>) (lines input)) -> String
    printErrors input x = unlines (map (printError (lines input)) (getIndices (lines input) x))

---------------------------------------------------------------------------------
-- Example parser
---------------------------------------------------------------------------------

containsEven : String -> Maybe Int
containsEven str = parseInteger str >>= (\i : Int => toMaybe (even i) i)

parserLens : (String :- Maybe Int) =%> CUnit
parserLens = embed containsEven

lineParser : SomeC (String :- Int) =%> CUnit
lineParser = someToAll |> AllListMap parserLens |> close

composedParser : (String :- String) =%> CUnit
composedParser = splitFile |> lineParser

mainProgram : String -> String
mainProgram = extract composedParser

main : IO ()
main = do putStrLn "give me a file name"
          fn <- getLine
          Right fileContent <- readFile fn
          | Left err => printLn err
          let output = mainProgram fileContent
          putStrLn output
          main

---------------------------------------------------------------------------------
-- Example Filesystem dispatch
---------------------------------------------------------------------------------

Filename = String
Content = String
Filesystem = List (Filename * Content)

handleFiles : Interpolation error =>
              (Filesystem :- String) =%> SomeC (String :- error)
handleFiles = MkMorphism (map π2) matchErrors
  where
    zipWithPath : (files : List (String * String)) ->
                  Some (const error) (map π2 files) -> List (String * error)
    zipWithPath ((path && _) :: ls) (Take z y) = (path && z) :: zipWithPath ls y
    zipWithPath (x :: ls) (Drop y) = zipWithPath ls y
    zipWithPath [] End = []

    matchErrors : (files : List (String * String)) ->
                  Some (const error) (map π2 files) ->
                  String
    matchErrors files x = unlines (map (\(path && err) => "In file '\{path}':\n\{err}") (zipWithPath files x))

filesystemParser : (Filesystem :- String) =%> CUnit
filesystemParser = handleFiles |> map splitFile |> join {a = String :- Int} |> lineParser

fsProgram : Filesystem -> String
fsProgram = extract filesystemParser

yesno : IO Bool
yesno = do response <- getLine
           if response == "y" || response == "Y"
              then pure True
              else if response == "n" || response == "N"
              then pure False
              else putStrLn "Could not parse response" >> yesno

askList : List String -> IO (List String)
askList acc = do
  putStrLn "enter a file name"
  name <- getLine
  putStrLn "done? y/n"
  False <- yesno
  | True => pure (name :: acc)
  askList (name :: acc)

main2 : IO ()
main2 = do files <- askList []
           filesAndContent <- traverse (\fn => map (fn &&) <$> readFile fn) (reverse files)
           putStrLn "got content"
           let Right contents = sequence filesAndContent
               | Left err => printLn err
           putStrLn "no errors"
           let result = fsProgram contents
           putStrLn result
