module Interactive.Compiler

import Language.Lambda
import Language.Scoped
import Language.STLC
import Data.Container
import Data.Container.Descriptions.List
import Data.Container.Descriptions.Maybe
import Data.Container.Morphism
import Data.Fin
import Data.String
import Data.Product
import Data.Sigma
import Data.List.Quantifiers
import Data.List.Elem
import Data.Vect
import Decidable.Equality


export infix 7 ∈
(∈) : a -> Prelude.List a -> Type
(∈) x xs = Elem x xs

findElem : DecEq a => (x : a) -> (ls : List a) -> Maybe (x ∈ ls)
findElem x [] = Nothing
findElem x (y :: xs) with (decEq x y)
  findElem x (x :: xs) | Yes Refl = Just Here
  findElem x (y :: xs) | No _ = There <$> findElem x xs

IndexedError : Type -> List String -> Type
IndexedError error xs = Fin (length xs) * error

fromNat : ((ls : List a) !> Vect (length ls) a) =%> ((n : Nat) !> Vect n a)
fromNat = MkMorphism length (\_ => id)

reconstruct : (List a :- List a) =%> ((n : Nat) !> Vect n a)
reconstruct = MkMorphism length (\_ => toList)

-- between "All" and "Any", a thinning of some sort
data Some : (a -> Type) -> List a -> Type where
  Take : {0 p : a -> Type} -> p x -> Some p ls -> Some p (x :: ls)
  Drop : {0 x : a} -> {0 p : a -> Type} -> Some p ls -> Some p (x :: ls)
  Empty : Some p xs

data (<=) : List a -> List a -> Type where
  Take' : xs <= ys -> (x :: xs) <= (x :: ys)
  Drop' : xs <= ys -> xs <= (x :: ys)
  None' : [] <= xs

Uninhabited (s :: ss <= []) where
  uninhabited _ impossible

0 Subset : Container -> Container
Subset x = (xs : List x.shp) !> Σ (List x.shp) (<= xs)

SubsetJoin : Subset (Subset a) =%> Subset a
SubsetJoin = MkMorphism flatList bwdSubset
  where
    bwdSubset : (x : List (List a.shp)) -> Σ (List a.shp) (<= flatList x) -> Σ (List (List a.shp)) (<= x)
    bwdSubset x (p1 ## p2) with (flatList x)
      bwdSubset x ([] ## None') | [] = [] ## None'
      bwdSubset x ((y :: xs) ## p2) | [] = absurd p2
      bwdSubset x (p1 ## p2) | (y :: xs) = ?bwdsss_rhs_0_rhs0_1

0 Some' : (a -> Type) -> List a -> Type
Some' p xs = Σ (List a) (\sub => (sub <= xs) * All p sub)
-- Still wish I could write `Σ (sub : List a) | (sub <= xs) * All p sub`

SomeList : Container -> Container
SomeList c2 =
    (xs : List c2.shp) !>
    (Some c2.pos xs)

SomeCont : Container -> Container
SomeCont x = AllList (MaybeA x)

toCont : SomeList a =%> SomeCont a
toCont = MkMorphism (map Just) ( ?toCont_rhs_1)
  where
    back : (xs : List (a .shp)) -> All (Sometimes a.pos) (mapImpl Just xs) -> Some a.pos xs
    back [] y = Empty
    back (x :: xs) (Aye y :: ys) = Take y (back xs ys)

fromCont : SomeCont a =%> SomeList a
fromCont = MkMorphism getJust rebuild
  where
    getJust : {0 a : Type} -> List (Maybe a) -> List a
    getJust [] = []
    getJust (Just x :: xs) = x :: getJust xs
    getJust (Nothing :: xs) = getJust xs

    rebuild : (x : List (Maybe a.shp)) -> Some a.pos (getJust x) -> All (Sometimes a.pos) x
    rebuild [] y = []
    rebuild (Nothing :: xs) y = Nah :: rebuild xs y
    rebuild ((Just x) :: xs) (Take y z) = Aye y :: rebuild xs z
    rebuild ((Just x) :: xs) (Drop y) = ?aa :: ?hui
    rebuild ((Just x) :: xs) Empty = Aye ?ada :: ?rebuild_rhs_6

SomeListF : a =%> b -> SomeList a =%> SomeList b
SomeListF x = MkMorphism (map x.fwd) backward
  where
    backward : (xs : List a.shp) ->
               Some b.pos (map x.fwd xs) -> Some a.pos xs
    backward _ Empty = Empty
    backward (y :: xs) (Take p ys) = Take (x.bwd y p) (backward xs ys)
    backward (y :: xs) (Drop ys) = Drop (backward xs ys)

splitSome : {xs : List a} -> Some p (xs ++ ys) -> Some p xs * Some p ys
splitSome x {xs = []}  = Empty && x
splitSome (Take x z) {xs = (y :: xs)}  = let r : ? ; r = splitSome z in Take x r.π1 && r.π2
splitSome (Drop x) {xs = (y :: xs)}  = let r : ? ; r = splitSome x in Drop r.π1 && r.π2
splitSome Empty {xs = (y :: xs)}  = Empty && Empty

joinSome : SomeList (SomeList a) =%> SomeList a
joinSome = MkMorphism flatList joinSomeBwd
  where
    joinSomeBwd : (xs : List (List a.shp)) -> Some a.pos (flatList xs) -> Some (Some a.pos) xs
    joinSomeBwd [] x = Empty
    joinSomeBwd (y :: xs) x = uncurry Take (mapSnd (joinSomeBwd xs) (splitSome x))


fromList : (xs : List a) -> All (\_ => Maybe b) xs
fromList [] = []
fromList (x :: xs) = Nothing :: fromList xs

allToSome : AllList (a :- Maybe b) =%> SomeList (a :- b)
allToSome = MkMorphism id rebuild
  where
    rebuild : (x : List a) -> Some (const b) x -> All (\_ => Maybe b) x
    rebuild (x :: ls) (Take y z) = Just y :: rebuild ls z
    rebuild (x :: ls) (Drop y) = Nothing :: rebuild ls y
    rebuild xs Empty = fromList xs

zipIndex : {0 a : Type} -> {0 xs : List a} -> {0 p : a -> Type} ->
           Some p xs -> List (Σ (Fin (length xs)) (\i => p (index' xs i)))
zipIndex (Take x y) = (FZ ## x) :: map (\(i ## z) => FS i ## z) (zipIndex y)
zipIndex (Drop x) = map (\(i ## x) => FS i ## x) (zipIndex x)
zipIndex Empty = []

lengthMap : {f : a -> b} -> (xs : List a) -> List.length xs === List.length (map f xs)
lengthMap [] = Refl
lengthMap (x :: xs) = cong S (lengthMap {f} xs)

Filesystem : Type
Filesystem = List (String * String)

handleFiles : Interpolation error =>
              (Filesystem :- String) =%> SomeList (String :- error)
handleFiles = MkMorphism (map π2) matchErrors
  where
    zipWithPath : (files : List (String * String)) ->
                  Some (const error) (map π2 files) -> List (String * error)
    zipWithPath ((path && _) :: ls) (Take z y) = (path && z) :: zipWithPath ls y
    zipWithPath (x :: ls) (Drop y) = zipWithPath ls y
    zipWithPath files Empty = []

    matchErrors : (files : List (String * String)) ->
                  Some (const error) (map π2 files) ->
                  String
    matchErrors files x = unlines (map (\(path && err) => "In file \{path}:\n\{err}") (zipWithPath files x))

simpleParser2 : Interpolation error =>
               (String :- String) =%> SomeList (String :- error)
simpleParser2 = MkMorphism lines printErrors
  where
    printError : (Fin n * error) -> String
    printError (index && msg) = "At line \{show (cast {to = Nat} index)}:\n\{msg}"

    printErrors : (input : String) -> Some (const error) (lines input) -> String
    printErrors input x = unlines (map (printError . (\(i ## x) => i && x)) (zipIndex x))

composedParser :  (Filesystem :- String) =%> SomeList (String :- String)
composedParser = handleFiles {error = String} |> SomeListF (simpleParser2 {error = String}) |> joinSome {a = String :- String}


half : Bool -> (x : List String) ->  Some (\x => String) x
half _ [] = Empty
half False (x :: xs) = Drop (half True xs)
half True (x :: xs) = Take x (half False xs)

pickHalf : SomeList (String :- String) =%> CUnit
pickHalf = MkMorphism (const ()) (\x, _ => half True x)

testParser : Filesystem -> String
testParser xs = (composedParser |> pickHalf).bwd xs ()

testFilesystem : Filesystem
testFilesystem =
    [ "file1" && unlines (map ("content1-line" ++) (map show [0 .. 10]))
    , "file2" && unlines (map ("content2-line" ++) (map show [0 .. 10]))
    , "file3" && unlines (map ("content3-line" ++) (map show [0 .. 10]))
    , "file4" && unlines (map ("content4-line" ++) (map show [0 .. 10]))
    , "file5" && unlines (map ("content5-line" ++) (map show [0 .. 10]))
    , "file6" && unlines (map ("content6-line" ++) (map show [0 .. 10]))
    , "file7" && unlines (map ("content7-line" ++) (map show [0 .. 10]))
    ]

simpleParser : Show error =>
               (String :- String) =%> ((xs : List String) !> (List (Fin (length xs) * error)))
simpleParser = MkMorphism lines (\xs, ys => ?huw)
  where
    printError : (Fin n * error) -> String
    printError (index && msg) = "At line \{show (cast {to = Nat} index)}:\n\{show msg}"

    printErrors : List (Fin n * error) -> String
    printErrors ls = unlines (map printError ls)

{-
errorsParser : Monoid error =>
               (String :- error) =%> ((xs : List String) !> List (Fin (length xs) * error))
errorsParser = MkMorphism lines (\xs, ys => collectErrors ys)
  where
    collectErrors : List (Fin n * error) -> error
    collectErrors ls = foldl (<+>) neutral (map π2 ls)


-- findIndex : (a -> Bool) -> (xs : List a) -> Maybe (Fin (length xs))
-- check the scope of a lambda calculus given a context
checkScopeMaybe : LC -> (ctx : List String) -> Maybe (Scoped (length ctx))
checkScopeMaybe (Var str) ctx =
  case findIndex (== str) ctx of
       Nothing => Nothing
       Just idx => Just (Var idx)
checkScopeMaybe (Lam name x) ctx =
  Lam <$> checkScopeMaybe x (name :: ctx)
checkScopeMaybe (App x y) ctx = App <$> checkScopeMaybe x ctx <*> checkScopeMaybe y ctx

data ErrorList e a = Errors (List e) | Value a

Functor (ErrorList e) where
  map f (Errors ls) = Errors ls
  map f (Value x) = Value (f x)

Applicative (ErrorList a) where
  pure x = Value x
  (<*>) f arg = case (f, arg) of
                     (Errors ls, Errors xs) => Errors (ls ++ xs)
                     (Errors xs, _) => Errors xs
                     (_, Errors xs) => Errors xs
                     (Value f, Value x) => Value (f x)

Monad (ErrorList a) where
  (>>=) m f = case m of
                   (Value x) => f x
                   (Errors xs) => Errors xs


public export
data StrScoped : List a -> Type where
  Var : a ∈ names -> StrScoped names
  App : StrScoped ns -> StrScoped ns -> StrScoped ns
  Lam : StrScoped (n :: ns) -> StrScoped ns

checkScopeEither : LC -> (ctx : List String) -> ErrorList String (StrScoped ctx)
checkScopeEither (Var str) ctx =
  case findElem str ctx of
       Nothing => Errors [str]
       Just prf => Value (Var prf)
checkScopeEither (Lam name x) ctx =
  Lam <$> checkScopeEither x (name :: ctx)
checkScopeEither (App x y) ctx = App <$> checkScopeEither x ctx <*> checkScopeEither y ctx

namespace Lamdba
  public export
  data LCError : LC -> Type -> Type where
    VarLoc : err -> (name : String) ->  LCError (Var name) err
    LamLoc : err -> (name: String) -> (body : LCError expr err) -> LCError (Lam name expr) err
    AppLoc : err -> LCError fn err -> LCError arg err -> LCError (App fn arg) err

namespace Scope
  public export
  data ScopeError : Scoped n -> Type -> Type where
    VarLoc : err -> (idx : Fin n) -> ScopeError (Var idx) err
    LamLoc : err -> ScopeError body err -> ScopeError (Lam body) err
    AppLoc : err -> ScopeError fn err -> ScopeError arg err -> ScopeError (App fn arg) err


LCPhase : Type -> Container
LCPhase err = (lc : LC) !> List (LCError lc err)

ScopePhase : Type -> Container
ScopePhase err = (result : ErrorList err (List String)) !>
  case result of
       Errors errs => Void
       Value scope => StrScoped (scope)

recontextualize : (lc : LC) -> (case (checkScopeEither lc []) of
                                    Errors errs => Void
                                    Value scoped => ?hieo) -> List (LCError lc String)

checkScopeInit : LC -> Maybe (Scoped Z)
checkScopeInit x = checkScopeMaybe x []

TypedContext : Type
TypedContext = Ctxt ()

typecheck : (ctx : TypedContext) -> Scoped (length ctx) -> Maybe (Ty ())
-- typecheck [] (Var x) = absurd x
-- typecheck (y &. z) (Var x) = Just (STLC.lookup y _ )
-- typecheck ctx (Lam x) = ?typecheck_rhs2
-- typecheck ctx (App x y) = ?typecheck_rhs4

