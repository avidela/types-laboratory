module Interactive.ParsePrint

import Data.List
import Data.List1

import Text.Parser
import Text.Lexer

import Data.String

data AST = Number Int
         | Infix String AST AST

data Token = TNumber Int
           | LParen
           | RParen
           | TOperator String



data Error : Type where
  ParseError : String -> Error

readInteger : String -> Either Error Integer

readOperator : String -> Either Error (Integer -> Integer -> Integer)

eval : String -> Either Error Integer
eval s = do
    (let [x,y,z]  = forget $ split isSpace s | _ => Left (ParseError s)
         v1 := readInteger x
         op := readOperator y
         v2 := readInteger z
     in op <*> v1 <*> v2)
