module Category where
import Agda.Builtin.Equality
import Data.Product
open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open ≡-Reasoning

open Agda.Builtin.Equality
open Data.Product 

record Category {n m : Level} : Set (lsuc (n ⊔ m)) where
  constructor MkCat
  infixl 19 _;_
  infixr 20 _~>_
  field
    o : Set n
    _~>_ : o -> o -> Set m
    id : {x : o} -> x ~> x
    _;_ : {x y z : o} -> x ~> y -> y ~> z -> x ~> z
    idr : {x y : o} -> (f : x ~> y) -> f ; id ≡ f
    idl : {x y : o} -> (f : x ~> y) -> id ; f ≡ f
    assoc : {x y z w : o} -> (f : x ~> y) -> (g : y ~> z) -> (h : z ~> w) ->
            (f ; g) ; h ≡ f ; (g ; h)

record Functor {n : Level} (c1 c2 : Category {n} {n}) : Set n where
    open Category c1 using () renaming (_~>_ to _~1>_)
    open Category c2 using () renaming (_~>_ to _~2>_)
    open Category using (o ; id)
    field
      f : o c1 -> o c2
      mapMor : {a b : o c1} -> a ~1> b -> f a ~2> f b

_*_ : {l : Level} -> Category {l} {l} -> Category {l} {l} -> Category {l} {l}
MkCat o1 m1 id1 comp1 idr1 idl1 assoc1 * MkCat o2 m2 id2 comp2 idr2 idl2 assoc2 = MkCat
  (o1 × o2)
  (λ (x1 , x2) (y1 , y2) → m1 x1 y1 × m2 x2 y2)
  (id1 , id2)
  (λ (x1 , y1) (x2 , y2) → comp1 x1 x2 , comp2 y1 y2)
  (λ (f1 , f2) → cong₂ _,_ (idr1 f1) (idr2 f2))
  (λ where
     (f1 , f2) → cong₂ _,_ (idl1 f1) (idl2 f2))
  (λ where
    (f1 , f2) (g1 , g2) (h1 , h2) → cong₂ _,_ (assoc1 f1 g1 h1) (assoc2 f2 g2 h2))
  
Bifunctor : (a b c : Category) -> Set1
Bifunctor a b c = Functor (a * b) c