module Container.Morphism.Cartesian where

open import Function.Inverse
open import Relation.Binary
import Container

record CartMor (c1 c2 : Container.Container) : Set where
    constructor MkCartMor
    open Container.Container
    field
        fwd : shape c1 -> shape c2
        bwd : (x : shape c1) -> position c2 (fwd x) ↔ position c1 x

open CartMor

compose : {a b c : Container.Container} -> CartMor a b -> CartMor b c -> CartMor a c
compose (MkCartMor f1 b1) (MkCartMor f2 b2) = record { fwd =  λ x -> f2 (f1 x); bwd = λ x → b1 x ∘ b2 (f1 x) }