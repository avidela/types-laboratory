module Container.Morphism.Distrib where

open import Container
open import Container.Morphism
open import Container.List
open import Container.Maybe
open import Data.List renaming (map to listMap) hiding (concat)
open import Data.Maybe renaming (map to maybeMap)
open import Data.List.Relation.Unary.All
open import Relation.Binary.PropositionalEquality.Core using (_≡_ ; subst ; sym ; cong ; cong₂)
open import Data.Maybe.Relation.Unary.Any renaming (map to mapAny)
open import Relation.Binary.Indexed.Heterogeneous
open import Relation.Binary 
open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.HeterogeneousEquality using (_≅_)


open ≡-Reasoning

variable
  l l' : Level


appConcat : {a b c : Set} -> (f : a -> b -> c) -> Maybe a -> Maybe b -> Maybe c
appConcat f (just x) y = maybeMap (f x) y
appConcat f nothing y = nothing

distribListMaybe : {a : Set} -> List (Maybe a) -> Maybe (List a)
distribListMaybe [] = just []
distribListMaybe (x ∷ xs) = appConcat _∷_ x (distribListMaybe xs)



distrib<- : {x : Container} -> 
            (v : List (Maybe (Container.shape x))) -> 
            Any (All (Container.position x)) (distribListMaybe v) -> 
            All (Any (Container.position x)) v
    

distrib<-View : 
  {a : Set} -> {p : a -> Set} -> 
  (xs : Maybe (List a)) ->
  (z : a) ->
  (v : List (Maybe a)) ->
  (prf : xs ≡ distribListMaybe v) ->
  (_ : Any (All p) (maybeMap (z ∷_) xs)) ->
  All (Any p) (just z ∷ v)
distrib<-View (just x1) z v prf (just (px ∷ x)) = (just px) ∷ distrib<- v (subst (Any (All _)) prf (just x))

distrib<- [] a = []
distrib<- (just z ∷ xs) y = distrib<-View (distribListMaybe xs) z xs refl y 


distrib : (x : Container) -> ListM (MaybeM x) =%> MaybeM (ListM x)
distrib x = distribListMaybe % distrib<-

maybeDoesNothing : {a : Set} -> (m : Maybe a) -> maybe (λ x -> just x) nothing m ≡ m
maybeDoesNothing {a} (just x) = _≡_.refl
maybeDoesNothing {a} nothing = _≡_.refl

consAppendDistribute : {a : Set} -> (x : Maybe a) -> (xs ys : Maybe (List a)) -> appConcat _++_ (appConcat _∷_ x xs) ys ≡
    appConcat _∷_ x (appConcat _++_ xs ys)
consAppendDistribute (just x) (just x₁) (just x₂) = refl
consAppendDistribute (just x) (just x₁) nothing = refl
consAppendDistribute (just x) nothing ys = refl
consAppendDistribute nothing xs ys = refl


distribSplit : ∀ {a : Set} (xs ys : List (Maybe a)) -> 
    distribListMaybe {a} (xs ++ ys) ≡ appConcat _++_ (distribListMaybe xs) (distribListMaybe ys)
distribSplit {a} [] [] = _≡_.refl
distribSplit {a} [] (x ∷ ys) = sym (maybeDoesNothing _)
distribSplit {a} (x ∷ xs) ys = let qx = distribSplit xs ys in begin
    appConcat _∷_ x (distribListMaybe (xs ++ ys))
    ≡⟨ cong (appConcat _∷_ x) qx ⟩
    appConcat _∷_ x (appConcat _++_ (distribListMaybe xs) (distribListMaybe ys))
    ≡⟨ consAppendDistribute x (distribListMaybe xs) (distribListMaybe ys) ⟨ 
    appConcat _++_ (appConcat _∷_ x (distribListMaybe xs)) (distribListMaybe ys)
    ∎

checkApp : (x : Maybe (List A)) -> (y : Maybe (List (List A))) -> 
           appConcat _++_ x (maybeMap cat y) ≡ maybeMap cat (appConcat _∷_ x y)
checkApp (just x) (just x₁) = refl
checkApp (just x) nothing = refl
checkApp nothing y = refl

listDistribPrf : (xs : List (List (Maybe A))) -> 
  distribListMaybe (cat xs) ≡ maybeMap cat (distribListMaybe (listMap distribListMaybe xs))
listDistribPrf [] = _≡_.refl
listDistribPrf (x ∷ xs) = let rec = listDistribPrf xs in 
  begin 
    distribListMaybe (x ++ cat xs)
  ≡⟨ distribSplit x (cat xs) ⟩
    appConcat _++_ (distribListMaybe x) (distribListMaybe (cat xs))
  ≡⟨ cong (appConcat _++_  (distribListMaybe x)) rec ⟩
    appConcat _++_ (distribListMaybe x) (maybeMap cat (distribListMaybe (listMap distribListMaybe xs)))
  ≡⟨ checkApp (distribListMaybe x) (distribListMaybe (listMap distribListMaybe xs)) ⟩
    maybeMap cat (appConcat _∷_ (distribListMaybe x) (distribListMaybe (listMap distribListMaybe xs)))
  ≡⟨ refl ⟩
    maybeMap cat (distribListMaybe (distribListMaybe x ∷ listMap distribListMaybe xs))
  ≡⟨ refl ⟩ 
    maybeMap cat (distribListMaybe (listMap distribListMaybe (x ∷ xs)))
  ∎


byebye : {x : Container} -> 
         (v : List (List (Maybe (Container.shape x))))
         (z  : Any (All (Container.position x)) (distribListMaybe (cat v))) →
         (z2 : Any (All (Container.position x)) (maybeMap cat (distribListMaybe (listMap distribListMaybe v)))) ->
         join<- v (distrib<- (cat v) z) ≡
         mapL<- distribListMaybe distrib<- v
          (distrib<- (listMap distribListMaybe v)
          (maybeBackward cat join<-
            (distribListMaybe (listMap distribListMaybe v)) z2))
byebye = ?


--                List distrib_x
-- List (List (Maybe x)) ----> List (Maybe (List x))
--          |                       | distrib_(List x)
--          |                       V
--   join   |                 Maybe (List (List x))
--          |                       | Maybe_join
--          V                       V
--     List (Maybe x) ------> Maybe (List x)
--                   distrib_x
diagram1 : {x : Container} -> let
  left : ListM (ListM (MaybeM x)) =%> ListM (MaybeM x)
  left = listJoin {MaybeM x}
  bot : ListM (MaybeM x) =%> MaybeM (ListM x)
  bot = distrib x
  top : ListM (ListM (MaybeM x)) =%> ListM (MaybeM (ListM x))
  top = ListF (distrib x)
  topRight : ListM (MaybeM (ListM x)) =%> MaybeM (ListM (ListM x))
  topRight = distrib (ListM x)
  botRight : MaybeM (ListM (ListM x)) =%> MaybeM (ListM x)
  botRight = MaybeF listJoin
  in (left %> bot) MorEq (top %> topRight %> botRight)
diagram1 = MkMorEq listDistribPrf byebye

{-
-- diagram 2: commutes with maybe join
--
--                distrib_(Maybe x)
--   List (Maybe (Maybe x)) ----> Maybe (List (Maybe x))
--           |                       |
--           |                       | Maybe distrib
--           |                       V
-- List join |                 Maybe (Maybe (List x))
--           |                       |
--           |                       | join (List x)
--           V                       V
--      List (Maybe x) ------> Maybe (List x)
--                     distrib_x
diagram2 : {x : Container} -> let
  left : ListM (MaybeM (MaybeM x)) =%> ListM (MaybeM x)
  left = ListF maybeJoin
  bot : ListM (MaybeM x) =%> MaybeM (ListM x)
  bot = distrib x
  top : ListM (MaybeM (MaybeM x)) =%> MaybeM (ListM (MaybeM x))
  top = distrib (MaybeM x)
  topRight : MaybeM (ListM (MaybeM x)) =%> MaybeM (MaybeM (ListM x))
  topRight = MaybeF (distrib x)
  botRight : MaybeM (MaybeM (ListM x)) =%> MaybeM (ListM x)
  botRight = maybeJoin
  in left %> bot ≡ top %> topRight %> botRight
diagram2 = {!   !}

-- triangle 1: commutes with pure List
--
--                   Maybe pure_List
--        Maybe x -----------> Maybe (List x)
--            |                       |
--            |                       |
--            |                       |
--  list_pure |                       | id
--            |                       |
--            |                       |
--            V                       V
--      List (Maybe x) -----> Maybe (List x) 
--                    distrib_x
triangle1 : {x : Container} -> MaybeF listPure ≡ (listPure %> distrib x)
triangle1 = {!   !}

-- triangle 2: commutes with pure Maybe
--
--                   pure_Maybe
--        List x -----------> Maybe (List x)
--            |                     |
--            |                     |
--            |                     |
--  List pure |                     | id
--            |                     |
--            |                     |
--            V                     V
--      List (Maybe x) -----> Maybe (List x) 
--                    distrib_x
triangle2 : {x : Container} ->
    maybePure ≡ ListF maybePure %> distrib x
triangle2 = {!   !}   
   
-}    