{-# OPTIONS --allow-unsolved-metas #-}
module Container.Morphism where

open import Container 
open import Function.Base
open import Relation.Binary.PropositionalEquality.Core using (_≡_ ; subst ; sym ; cong₂ ; trans)

infix 40 _=%>_

variable
  A B C : Set
  A' : A -> Set
  B' : B -> Set
  C' : C -> Set
  c1 c2 dom cod : Container

postulate 
  funExt : {A : Set} {B : A → Set} {f g : (x : A) → B x} →
    (∀ x → f x ≡ g x) → f ≡ g


record _=%>_ (c1 c2 : Container) : Set where
  constructor _%_
  open Container.Container
  field
    fwd : shape c1 -> shape c2
    bwd : (x : shape c1) -> position c2 (fwd x) -> position c1 x

data [_•_]=_ : forall {a b c} -> (b -> c) -> (a -> b) -> (a -> c) -> Set1 where
  comp : forall {a b c} -> {f : b -> c} -> {g : a -> b} -> {h : a -> c} -> f • g ≡ h -> [ f • g ]= h

extract : forall {a b c} -> {f : b -> c} -> {g : a -> b} -> {h : a -> c} -> [ f • g ]= h -> f • g ≡ h
extract (comp x) = x

open _=%>_

record _MorEq_ {dom cod} (a b : dom =%> cod) : Set where
  constructor MkMorEq
  open Container.Container
  field
    fwdEq : (v : shape dom) -> fwd a v ≡ fwd b v
    bwdEq : (v : shape dom) -> (z : position cod (fwd a v)) ->
             bwd a v z ≡  bwd b v (subst (position cod) (fwdEq v) z)

congdep2 : ∀ {A C : Set} {B : A → Set} (f : (x : A) → B x → C) {x y : A} →
    {u : B x} → {v : B y} → 
    (p : x ≡ y) → u ≡ subst B (sym p) v  → f x u ≡ f y v
congdep2 f _≡_.refl _≡_.refl = _≡_.refl

infixl 19 _%>_
_%>_ : {a b c : Container} -> (a =%> b) -> (b =%> c) -> (a =%> c)
(f1 % b1) %> (f2 % b2) = (f2 • f1) % λ x -> b1 x •  b2 (f1 x)

shp : Container -> Set
shp (s , _) = s

pos : (x : Container) -> shp x -> Set
pos (_ , p) = p

data [_••_]=_ : {a b c : Container} -> 
               {f : shp a -> shp b} -> {g : shp b -> shp c} -> {h : shp a -> shp c} ->
               ((x : shp a) -> pos b (f x) -> pos a x) -> 
               ((x : shp b) -> pos c (g x) -> pos b x) -> 
               ((x : shp a) -> pos c (h x) -> pos a x) -> Set1 where
  compi : {a b c : Container} -> 
               {f : shp a -> shp b} -> {g : shp b -> shp c} -> {h : shp a -> shp c} ->
               (prf : [ g • f ]= h) ->
               (b1 : (x : shp a) -> pos b (f x) -> pos a x) -> 
               (b2 : (x : shp b) -> pos c (g x) -> pos b x) -> 
               [_••_]=_ {a} {b} {c} b1 b2 (λ x -> b1 x • b2 (f x))

               
idMorphism : {x : Container} -> x =%> x
idMorphism = id % λ x → id 

data [_%>_]=_ : forall {a b c} -> a =%> b -> b =%> c -> a =%> c -> Set1 where
  cc : forall {a b c} {c1 : a =%> b} {c2 : b =%> c} {c3 : a =%> c} 
    -> (p1 : [ fwd c2 • fwd c1 ]= fwd c3)
    -> (p2 : [_••_]=_ {a}{b}{c} (bwd c1) (bwd c2) (bwd c3))
    -> [ c1 %> c2 ]= c3

extractC : forall {a b c} -> {c1 : a =%> b} -> {c2 : b =%> c} -> {c3 : a =%> c} ->
           [ c1 %> c2 ]= c3 -> c1 %> c2 ≡ c3
extractC (cc (comp _) (compi prf _ _)) = _≡_.refl 