module Container.Maybe where

open import Container
open import Container.Morphism
open import Agda.Builtin.Bool
open import Data.Maybe
open import Data.Maybe.Relation.Unary.Any renaming (map to mapAny)

data IsTrue : Bool -> Set where
  TT : IsTrue true

MaybeCont : Container
MaybeCont = Bool , IsTrue

MaybeM : Container -> Container
MaybeM c = Maybe (Container.shape c) , Any (Container.position c)

maybeBackward : {a b : Container} ->
    (fwd : Container.shape a → Container.shape b) ->
    (bwd : (x : Container.shape a) → Container.position b (fwd x) → Container.position a x) ->
    (x : Maybe (Container.shape a)) →
    Any (Container.position b) (map fwd x) →
    Any (Container.position a) x
maybeBackward f b (just x) (just x₁) = just (b x x₁)

MaybeF : {a b : Container} -> a =%> b -> MaybeM a =%> MaybeM b
MaybeF (fwd % bwd) = map  fwd % maybeBackward fwd bwd

maybePure : {x : Container} -> x =%> MaybeM x
maybePure = just % λ x x₂ → drop-just x₂

join : {x : Set} -> Maybe (Maybe x) -> Maybe x
join (just x) = x
join _ = nothing

joinBackward : {a : Container} -> (x : Maybe (Maybe (Container.shape a))) →
      Any (Container.position a) (join x) →
      Any (Any (Container.position a)) x
joinBackward (just .(just _)) (just x) = just (just x)

maybeJoin : {x : Container} -> MaybeM (MaybeM x) =%> MaybeM x
maybeJoin = join % joinBackward