module Container.List where

open import Container
open import Container.Morphism
open import Data.Fin
open import Agda.Builtin.Nat
open import Agda.Builtin.Sigma using (_,_)

open import Data.List renaming (map to listMap) hiding (concat)
open import Data.List.Base
open import Data.List.Relation.Unary.All renaming (map to allMap ; head to allHead)
open import Data.List.Relation.Unary.All.Properties using (++⁻)
open import Data.Product.Base as Prod using (_×_)

ListCont : Container
ListCont = Nat , Fin


ListM : Container -> Container
ListM (s , p) = List s , All p


cat : {a : Set} -> List (List a) -> List a
cat [] = []
cat (c ∷ xs) = c ++ cat xs

mapL<- : {a b : Container} -> 
    (fwd : Container.shape a -> Container.shape b) -> 
    (bwd : (x : Container.shape a) -> Container.position b (fwd x) -> Container.position a x)
    (x : Container.shape (ListM a)) →
    Container.position (ListM b) (listMap fwd x) →
    Container.position (ListM a) x
mapL<- fwd bwd [] y = []
mapL<- fwd bwd (x ∷ xs) (px ∷ ys) = bwd x px ∷ mapL<- fwd bwd xs ys

ListF : {a b : Container} -> a =%> b -> ListM a =%> ListM b
ListF (fwd % bwd) = listMap fwd % mapL<- fwd bwd

listPure : {x : Container} -> x =%> ListM x
listPure = [_] % λ x x₂ → allHead x₂

join<- : {a : Set} -> {p : a -> Set} -> 
    (x : List (List a)) →
      All p (cat x) →
      All (All p) x

backwardView : {a : Set} -> {p : a -> Set} -> 
    (x1 : List (List a)) -> 
    (x : List a) -> (y : All p (cat (x ∷ x1))) -> 
    (All p x × All p (cat x1)) ->
    All (All p) (x ∷ x1)
backwardView x1 x y (fst , snd) = fst ∷ join<- x1 snd

join<- [] y = []
join<- (x ∷ xs) y = backwardView xs x y (++⁻ x y)

listJoin : {x : Container} -> ListM (ListM x) =%> ListM x
listJoin = cat % join<-

--                List pure
--      (List x) ──────────> List (List x)
--          │                    │
--          │                    │
--          │                    │ join
--          │                    │
--          │ id                 V
--          └────────────────> (List x)
--               
joinPure : {x : Container} -> [ listPure %> listJoin {x} ]= idMorphism
joinPure = cc fwdEq bwdEq
  where
    fwdEq : [ cat • (λ x₁ → x₁ ∷ []) ]= (λ x₁ → x₁)
    fwdEq = comp (funExt λ x → {!   !}) 

    bwdEq : {x : Container} -> [_••_]=_ {ListM x} {ListM (ListM x)} {ListM x} (_=%>_.bwd listPure)  (_=%>_.bwd listJoin)
      (_=%>_.bwd idMorphism)
    bwdEq {x} = {!   !} 