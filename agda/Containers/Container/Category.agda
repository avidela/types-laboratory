module Container.Category where

open import Category
open import Container
open import Container.Morphism
open import Agda.Primitive


open Category.Category


Cont : Category {lsuc lzero}
o Cont = Container
(_~>_) Cont a b = a =%> b
id Cont = idMorphism
(_;_) Cont f g = f %> g
idr Cont (fwd % bwd) = {!!}
idl Cont f = {!!}
assoc Cont f g h = {!!}