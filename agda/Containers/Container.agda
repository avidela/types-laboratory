module Container where

open import Agda.Builtin.Sigma


record Container : Set1 where
  constructor _,_
  field
    shape : Set
    position : shape -> Set

open Container

_∘_ : Container -> Container -> Container
(s1 , p1) ∘ (s2 , p2) = Σ s1 (λ x → p1 x -> s2) , λ (x , f) -> Σ (p1 x) (λ x' ->  p2 (f x'))

_▷_ : Container -> Container -> Container
(s1 , p1) ▷ (s2 , p2) = Σ s1 (λ x → p1 x -> s2) , λ (x , f) -> (x' : p1 x) -> (p2 (f x'))


I : Container -> Set -> Set
I (s , p) t = Σ s (λ y -> p y -> t) 

infixr 9 _•_
_•_ : ∀ {A B C : Set} → (B → C) → (A → B) → A → C
(f • g) x = f (g x)
