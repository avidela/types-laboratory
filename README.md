# Types laboratory

This is a testing ground for ideas using dependent types and cateogry theory.

## How to build and run

- Install [pack](https://github.com/stefan-hoeck/idris2-pack)
- Execute `pack install-deps types-lab.ipkg`
- Install lsp `pack install-app idris2-lsp`
- Run in repl with `pack --rlwrap repl` and then load the file with quotation marks around it, for example `:l "src/Interactive/SQL.idr"`


